import SchemaObject from 'schema-object'

export {ApiError} from '../schemas'

export const Category = new SchemaObject({
  ID: Number,
  Description: String,
  IsLeaf: Boolean,
  Rentable: Boolean,
  // Children: null,
  CategoryPhoto: {
    type: String,
    transform: value => {
      return value.Url
    },
  },
})

export const GetCategoryChildren = new SchemaObject({
  list: [Category],
})
