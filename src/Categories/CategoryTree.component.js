import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Text, View, FlatList} from 'react-native'
import {Colors} from '../Layout/Colors'
import {Avatar} from 'react-native-elements'
import ButtonNative from '../MyComponents'

const styles = {
  category: {
    button: {
      marginRight: 5,
      marginLeft: 5,
      borderRadius: 20,
      height: 40,
      minWidth: 120,
      flexDirection: 'row',
      alignItems: 'center',
    },
    avatar: {
      marginLeft: 5,
      marginRight: 5,
    },
    text: {
      color: 'white',
    },
  },
  selectCategory: {
    button: {
      backgroundColor: Colors.primaryColor,
      paddingRight: 10,
    },
  },
  selectedCategory: {
    button: {
      backgroundColor: Colors.secondary1Color,
    },
  },
}

class CategoryTree extends Component {

  renderItem = ({item}) => (
    <Category
      item={item}
      onPress={() => {
        this.props.setLoading(true)
        this.props.addCategory(item)
        this.props.getCategory(item.ID)
        this.props.setParams({...this.props.params, category: item.ID}, this.props.token, this.props.userID)
      }}
      buttonStyle={styles.selectCategory.button}
      disabled={this.props.loading}
      showClose={false}
    />
  )

  renderSelectedItem = ({item, index}) => (
    <View key={item.ID} style={{alignItems: 'center', flexDirection: 'row'}}>
      {index > 0 ? <Text> > </Text> : null}
      <Category
        item={item}
        onPress={() => {
          this.props.setLoading(true)
          this.props.removeCategories(index)

          const goToCategory = index > 0 ? this.props.selectedCategories[index - 1].ID : 1
          this.props.getCategory(goToCategory)
          this.props.setParams({...this.props.params, category: goToCategory}, this.props.token, this.props.userID)
        }}
        buttonStyle={styles.selectedCategory.button}
        disabled={this.props.loading}
        showClose={true}
      />
    </View>
  )

  renderList = (categories, renderItem) => {
    return (
      <FlatList
        horizontal={true}
        contentContainerStyle={{height: 60, alignItems: 'center'}}
        data={categories}
        keyExtractor={(item) => item.ID}
        renderItem={renderItem}
      />
    )
  }

  render() {
    const {selectedCategories, categories} = this.props
    return (
      <View style={{flex: 0}}>
        {selectedCategories.length > 0 ? this.renderList(selectedCategories, this.renderSelectedItem) : null}
        {categories.length > 0 ? this.renderList(categories, this.renderItem) : null}
      </View>
    )
  }

  static propTypes = {
    selectedCategories: PropTypes.array.isRequired,
    categories: PropTypes.array.isRequired,
    token: PropTypes.string.isRequired,
    userID: PropTypes.string,
    params: PropTypes.any,
    loading: PropTypes.bool,
    setParams: PropTypes.func.isRequired,
    getCategory: PropTypes.func.isRequired,
    addCategory: PropTypes.func.isRequired,
    removeCategories: PropTypes.func.isRequired,
    setLoading: PropTypes.func.isRequired,
  }
}

const Category = ({item, onPress, buttonStyle, showClose, disabled}) => (
  <ButtonNative
    key={item.ID}
    disabled={disabled}
    styles={{...styles.category.button, ...buttonStyle}}
    onPress={onPress}
  >
    <Avatar containerStyle={styles.category.avatar} rounded title={item.Description[0].toUpperCase()}/>
    <Text numberOfLines={1} ellipsizeMode='tail' style={styles.category.text}>
      {item.Description}
    </Text>
    {showClose === false ? null :
      <Avatar containerStyle={styles.category.avatar} height={25} width={25} rounded icon={{name: 'close'}}/>
    }
  </ButtonNative>
)

Category.propTypes = {
  item: PropTypes.object.isRequired,
  onPress: PropTypes.func.isRequired,
  buttonStyle: PropTypes.object,
  showClose: PropTypes.bool,
  disabled: PropTypes.bool,
}

Category.defaultProps = {
  buttonStyle: {},
  showClose: false,
  disabled: false,
}


export default CategoryTree
