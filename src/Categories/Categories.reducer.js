const initialState = {
  categoriesCommunity: [],
  selectedCategoriesCommunity: [],
  categoriesMyObjects: [],
  selectedCategoriesMyObjects: [],
  categoriesSharedCatalog: [],
  selectedCategoriesSharedCatalog: [],
  loadingCommunity: true,
  loadingMyObjects: false,
  loadingSharedCatalog: true,
}

const Category = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_CATEGORIES_COMMUNITY':
      return {...state, categoriesCommunity: action.categories}
    case 'ADD_CATEGORY_COMMUNITY':
      return {...state, selectedCategoriesCommunity: [...state.selectedCategoriesCommunity, action.category]}
    case 'REMOVE_CATEGORIES_COMMUNITY':
      return {
        ...state,
        selectedCategoriesCommunity: state.selectedCategoriesCommunity.filter((category, index) => index < action.index),
      }
    case 'SET_CATEGORY_LOADING_COMMUNITY':
      return {...state, loadingCommunity: action.boolean}
    case 'GET_CATEGORIES_MY_OBJECTS':
      return {...state, categoriesMyObjects: action.categories}
    case 'ADD_CATEGORY_MY_OBJECTS':
      return {...state, selectedCategoriesMyObjects: [...state.selectedCategoriesMyObjects, action.category]}
    case 'REMOVE_CATEGORIES_MY_OBJECTS':
      return {
        ...state,
        selectedCategoriesMyObjects: state.selectedCategoriesMyObjects.filter((category, index) => index < action.index),
      }
    case 'SET_CATEGORY_LOADING_MY_OBJECTS':
      return {...state, loadingMyObjects: action.boolean}
    case 'GET_CATEGORIES_SHARED_CATALOG':
      return {...state, categoriesSharedCatalog: action.categories}
    case 'ADD_CATEGORY_SHARED_CATALOG':
      return {...state, selectedCategoriesSharedCatalog: [...state.selectedCategoriesSharedCatalog, action.category]}
    case 'REMOVE_CATEGORIES_SHARED_CATALOG':
      return {
        ...state,
        selectedCategoriesSharedCatalog: state.selectedCategoriesSharedCatalog.filter((category, index) => index < action.index),
      }
    case 'SET_CATEGORY_LOADING_SHARED_CATALOG':
      return {...state, loadingSharedCatalog: action.boolean}
    case 'CLEAN_CATEGORIES':
      return {...initialState}
    default:
      return state
  }
}

export default Category
