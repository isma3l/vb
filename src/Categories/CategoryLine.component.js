import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Text, View, FlatList, Dimensions, ImageBackground, TouchableHighlight} from 'react-native'

const categoryWidth = Dimensions.get('window').width * .9
const categoryHeight = categoryWidth / 2

const styles = {
  container: {
    marginLeft: 10,
    marginRight: 10,
    width: categoryWidth,
    height: categoryHeight,
    overflow:'hidden',
    borderRadius: 3,
  },
  image: {
    flexGrow: 1,
    height: null,
    width: null,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
  text: {
    textAlign: 'center',
    color: '#FFF',
    backgroundColor: 'rgba(0,0,0,0)',
    fontSize: 20,
  },
}

class CategoryLine extends Component {

  renderItem = (item, selectedCategories) => {

    function isSelected(list, target) {
      for (let i = 0; i < list.length; i++) {
        if (target.ID === list[i].ID) return true
      }
    }

    return (
      <Category
        item={item}
        onPress={() => {
          if (selectedCategories.length > 0) {
            const previousCategory = selectedCategories[0].ID
            this.props.removeCategories(0)
            if (previousCategory === item.ID) {
              this.props.setParams({...this.props.params, category: 1}, this.props.token, this.props.userID)
            } else {
              this.props.addCategory(item)
              this.props.setParams({...this.props.params, category: item.ID}, this.props.token, this.props.userID)
            }
          } else {
            this.props.addCategory(item)
            this.props.setParams({...this.props.params, category: item.ID}, this.props.token, this.props.userID)
          }
        }}
        dimmed={selectedCategories.length === 0 ? false : !isSelected(selectedCategories, item)}
      />
    )
  }

  render() {
    const {selectedCategories, categories} = this.props
    return (
      <View style={{flex: 0}}>
        {categories.length <= 0 ? null :
          <FlatList
            horizontal={true}
            contentContainerStyle={{height: categoryHeight, alignItems: 'center'}}
            data={categories}
            keyExtractor={item => item.ID}
            renderItem={({item}) => this.renderItem(item, selectedCategories)}
            showsHorizontalScrollIndicator={false}
          />
        }
      </View>
    )
  }

  static propTypes = {
    selectedCategories: PropTypes.array.isRequired,
    categories: PropTypes.array.isRequired,
    token: PropTypes.string.isRequired,
    userID: PropTypes.string,
    params: PropTypes.any,
    loading: PropTypes.bool,
    setParams: PropTypes.func.isRequired,
    getCategory: PropTypes.func.isRequired,
    addCategory: PropTypes.func.isRequired,
    removeCategories: PropTypes.func.isRequired,
    setLoading: PropTypes.func.isRequired,
    cleanCategories: PropTypes.func.isRequired,
  }
}

const Category = ({item, onPress, dimmed}) => (
  <TouchableHighlight
    key={item.ID}
    onPress={onPress}
    style={{...styles.container, opacity: dimmed ? .5 : 1}}
  >
    <ImageBackground
      style={styles.image}
      resizeMode={'cover'}
      source={{uri: item.CategoryPhoto}}
    >
      <Text style={styles.text}>
        {item.Description}
      </Text>
    </ImageBackground>

  </TouchableHighlight>
)

Category.propTypes = {
  item: PropTypes.object.isRequired,
  onPress: PropTypes.func.isRequired,
  dimmed: PropTypes.bool,
}

Category.defaultProps = {
  dimmed: false,
}


export default CategoryLine
