import React from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import CategoryActions from './Categories.actions'
import Component from './CategoryLine.component'

class Container extends React.Component {

  componentDidMount() {
    this.props.cleanCategories()
    this.props.getCategory(1)
  }

  render() {
    return <Component {...this.props}/>
  }

  static propTypes = {
    selectedCategories: PropTypes.array,
    categories: PropTypes.array,
    loading: PropTypes.bool,
    token: PropTypes.string,
    userID: PropTypes.string,
    cleanCategories: PropTypes.func.isRequired,
    getCategory: PropTypes.func.isRequired,
    setLoading: PropTypes.func.isRequired,
    addCategory: PropTypes.func.isRequired,
    removeCategories: PropTypes.func.isRequired,
  }
}

const mapStateToProps = (state, ownProps) => {

  let key = ''
  if (ownProps.isCommunity) key = 'Community'
  else if (ownProps.isSharedCatalog) key = 'SharedCatalog'
  else key = 'MyObjects'

  return {
    selectedCategories: state.Category['selectedCategories' + key],
    categories: state.Category['categories' + key],
    loading: state.Category['loading' + key],
    token: state.Login.token,
    userID: ownProps.userID,
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    cleanCategories: () => dispatch(CategoryActions.cleanCategories(ownProps.isCommunity)),
    getCategory: id => dispatch(CategoryActions.getCategory(id, ownProps.isCommunity, ownProps.isSharedCatalog)),
    setLoading: boolean => dispatch(CategoryActions.setLoading(boolean, ownProps.isCommunity, ownProps.isSharedCatalog)),
    addCategory: category => dispatch(CategoryActions.addCategory(category, ownProps.isCommunity, ownProps.isSharedCatalog)),
    removeCategories: index => dispatch(CategoryActions.removeCategories(index, ownProps.isCommunity, ownProps.isSharedCatalog)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Container)
