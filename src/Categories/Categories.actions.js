import {Category} from '../Endpoint'
import {Alert} from 'react-native'
import {apiPublic} from '../api'
import {ApiError, GetCategoryChildren} from './schemas'

export default {

  getCategory(id, isCommunity = false, isSharedCatalog = false) {
    return dispatch => {
      dispatch(this.setLoading(true, isCommunity, isSharedCatalog))
      apiPublic.get(Category + '/category/' + id + '/children')
        .then(response => {
          dispatch(this.setLoading(false, isCommunity, isSharedCatalog))
          const categories = new GetCategoryChildren({list: response.data.description})
          if (isCommunity) {
            dispatch({type: 'GET_CATEGORIES_COMMUNITY', categories: categories.list})
          } else if (isSharedCatalog) {
            dispatch({type: 'GET_CATEGORIES_SHARED_CATALOG', categories: categories.list})
          } else {
            dispatch({type: 'GET_CATEGORIES_MY_OBJECTS', categories: categories.list})
          }
        })
        .catch(error => {
          dispatch(this.setLoading(false, isCommunity, isSharedCatalog))
          let message = ''
          if (error.response) {
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },

  addCategory(category, isCommunity, isSharedCatalog = false) {
    if (isCommunity)
      return {type: 'ADD_CATEGORY_COMMUNITY', category}
    if (isSharedCatalog)
      return {type: 'ADD_CATEGORY_SHARED_CATALOG', category}
    else
      return {type: 'ADD_CATEGORY_MY_OBJECTS', category}
  },

  setLoading(boolean, isCommunity, isSharedCatalog = false) {
    if (isCommunity)
      return {type: 'SET_CATEGORY_LOADING_COMMUNITY', boolean}
    if (isSharedCatalog)
      return {type: 'SET_CATEGORY_LOADING_SHARED_CATALOG', boolean}
    else
      return {type: 'SET_CATEGORY_LOADING_MY_OBJECTS', boolean}
  },

  removeCategories(index, isCommunity, isSharedCatalog = false) {
    if (isCommunity)
      return {type: 'REMOVE_CATEGORIES_COMMUNITY', index}
    if (isSharedCatalog)
      return {type: 'REMOVE_CATEGORIES_SHARED_CATALOG', index}
    else
      return {type: 'REMOVE_CATEGORIES_MY_OBJECTS', index}
  },

  cleanCategories() {
    return {type: 'CLEAN_CATEGORIES'}
  },
}
