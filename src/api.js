import axios from 'axios'

/**
 * Use it for external generic APIs
 * @type {AxiosInstance}
 */
export const apiExternal = axios.create({
  timeout: 5000,
})

/**
 * Use it for the public endpoints of our API
 * @type {AxiosInstance}
 */
export const apiPublic = axios.create({
  timeout: 5000,
})

/**
 * Use it for the private endpoints of our API
 * These require a token.
 * @type {AxiosInstance}
 */
export const apiPrivate = axios.create({
  timeout: 5000,
  headers: {'Authorization': ' '}
})
