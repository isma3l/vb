const initialState = {
    cards: [],
    loading: false
};

//AvatarAction 0: do nothing 1: save new avatar 2: delete avatar
//AddressAction 0: add 1: modify
const CreditCard = (state = initialState, action) => {
    switch (action.type) {
        case "GET_CARDS":
            return {...state, cards: action.cards, loading: action.loading}
        case "SET_CARDS_LOADING":
            return {...state, loading: action.loading}
        case "GET_PAYMENT_METHODS":
            return {...state, loading: false, methods: action.methods}
        default:
            return state;
    }
};

export default CreditCard;