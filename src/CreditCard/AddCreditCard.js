import React, { Component } from 'react';
import {Text, TouchableNativeFeedback, View, ActivityIndicator, TouchableOpacity, Alert, ScrollView} from 'react-native'
import {styles} from "../Styles";
import {Colors} from "../Layout/Colors";
import {connect} from "react-redux";
import CreditCardActions from "./CreditCard.actions";
import ButtonNative from "../MyComponents"
import DoneBar from "done-bar";
import {CreditCardInput} from "react-native-credit-card-input";
import {FadeInView} from "../AnimatedComponents";
import KeyboardView from "../KeyboardView";

class AddCreditCard extends Component {
    state = {
        form: {
            valid: false
        },
        keyboardType: "default"
    }

    componentDidMount() {
        this.props.getPaymentMethods();
    }

    renderSpinner = () => (
        <View style={styles.centerContainer}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    addCreditCard = () => {
        const values = this.state.form.values;
        const card_number = values.number.replace(/\s/g,'');

        const data = {
            "security_code": values.cvc,
            "expiration_month": parseInt(values.expiry.substring(0, 2), 10),
            "expiration_year": 2000 + parseInt(values.expiry.substring(3, 5), 10),
            "card_number": card_number,
            "cardholder": {
                "name": values.name
            }
        };
        if (this.props.navigation.state.params) {
            this.props.addCreditCard(this.props.token, data, this.props.methods, this.props.navigation.state.params.redirectRoute);
        }
        else {
            this.props.addCreditCard(this.props.token, data, this.props.methods);
        }
    }

    renderAddCreditCard = () => {
        return (
            <KeyboardView style={{flex: 1, justifyContent: "center"}} keyboardVerticalOffset={150}>
                <FadeInView style={{backgroundColor: "white", paddingTop: 20}}>
                    <CreditCardInput
                        labels={{number: "NUMERO DE TARJETA", expiry: "EXP.", cvc: "CVC/CCV", name: "NOMBRE COMPLETO"}}
                        placeholders={{ number: "1234 5678 1234 5678", expiry: "MM/YY", cvc: "CVC", name: "Nombre y Apellido" }}
                        onChange={form => this.setState({form})}
                        onFocus={name => {
                            if(name === "name"){
                                this.setState({keyboardType: "default"})
                            } else {
                                this.setState({keyboardType: "numeric"})
                            }
                        }}
                        inputContainerStyle={{borderBottomColor: Colors.primaryColor, borderBottomWidth: 1}}
                        requiresName={true}
                    />
                    <View style={{marginTop: 10}}>
                        <ButtonNative
                            styles={this.state.form.valid ? styles.buttonStyle : styles.disableButton}
                            onPress={() => this.addCreditCard()}
                            disabled={!this.state.form.valid}
                        >
                            <Text style={{color: "white"}}>Agregar Tarjeta</Text>
                        </ButtonNative>
                    </View>
                </FadeInView>
                <DoneBar
                    keyboardType={this.state.keyboardType}
                    onDone={() => console.log('done!')}
                />
            </KeyboardView>
        )
    }

    render() {
        return this.props.loading ? this.renderSpinner() : this.renderAddCreditCard()
    }
}

const mapStateToProps = state => {
    return {
        token: state.Login.token,
        loading: state.CreditCard.loading,
        methods: state.CreditCard.methods
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        addCreditCard(token, data, methods, redirectRoute) {
            dispatch({type: "SET_CARDS_LOADING", loading: true})
            dispatch(CreditCardActions.addCreditCard(token, data, methods, redirectRoute))
        },
        setLoading(boolean) {
            dispatch({type: "SET_LOADING", loading: boolean})
        },
        getPaymentMethods() {
            dispatch({type: "SET_CARDS_LOADING", loading: true});
            dispatch(CreditCardActions.getPaymentMethods());
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddCreditCard);