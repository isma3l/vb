import {MercadoPago, UserProfile} from '../Endpoint'
import {Alert} from 'react-native'
import {navigatorRef} from '../../App'
import {NavigationActions} from 'react-navigation'
import {MERCADOPAGOPUBLICKEY} from '../Keys'
import {apiExternal, apiPrivate} from '../api'
import {ApiError} from '../Categories/schemas'

export default {
  addCreditCard(token, data, methods, redirectRoute = 'Profile', markAsMain) {
    return dispatch => {
      apiExternal.post(MercadoPago + '/v1/card_tokens', data, {params: {public_key: MERCADOPAGOPUBLICKEY}})
        .then(response => {
          const formData = new FormData()
          const first_six_digits = response.data.first_six_digits

          let type = ''

          methods.forEach(method => {
            method.settings.forEach(setting => {
              const pattern = new RegExp(setting.bin.pattern)
              const exclusionPattern = new RegExp(setting.bin.exclusion_pattern)
              if (pattern.test(first_six_digits) && !exclusionPattern.test(first_six_digits))
                type = method.id
            })
          })

          //TODO schema
          formData.append('token', response.data.id)
          formData.append('payment_method', type)

          apiPrivate.post(UserProfile + '/user/cards', formData)
            .then((response) => {
              dispatch({type: 'SET_CARDS_LOADING', loading: false})
              if (redirectRoute === 'CreditCardList') {
                navigatorRef.dispatch(NavigationActions.back())
                dispatch(this.getCreditCards(token))
              } else if(redirectRoute === 'AccountCreditCardList') {
                  navigatorRef.dispatch(NavigationActions.back())
                  dispatch({type: 'SET_CARDS_LOADING', loading: true})

                  if(markAsMain) {
                    const cardId = response.data.description.ID
                    dispatch(this.markMainCard(token, cardId))
                  } else {
                    dispatch(this.getCreditCards(token))
                  }
              } else {
                navigatorRef.dispatch(NavigationActions.navigate({routeName: redirectRoute}))
              }
            })
            .catch(error => {
              dispatch({type: 'SET_CARDS_LOADING', loading: false})
              let message = ''
              if (error.response) {
                if (error.response.status === 401 || error.response.status === 403) {
                  console.warn('redirected due to 401 or 403')
                  navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
                  return
                }
                const response = new ApiError(error.response.data)
                message = response.description
              } else if (error.request) {
                console.warn(error.request)
                message = 'Algo salió mal, revisá tu conexión a internet'
              } else {
                console.warn('Error', error.message)
                message = 'Algo salió mal.'
              }
              Alert.alert('Error', message, [{text: 'OK'}])
            })
        })
        .catch(error => {
          dispatch({type: 'SET_LOADING', loading: false})
          console.log(error.response)
          //TODO schema
          if (error.response.data.cause.code = 306) {
            Alert.alert('Error', 'Se han ingresado carácteres inválidos en el nombre de la tarjeta')
          } else {
            Alert.alert('Error', 'Hubo un problema al comunicarse con Mercado Pago')
          }
        })
    }
  },

  getCreditCards() {
    return dispatch => {
      apiPrivate.get(UserProfile + '/user/cards')
        .then(response => {
          dispatch({type: 'GET_CARDS', cards: response.data.description, loading: false})
          console.log(response)
        })
        .catch(error => {
          dispatch({type: 'SET_CARDS_LOADING', loading: false})
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },


  deleteCard(token, id) {
    return dispatch => {
      apiPrivate.delete(UserProfile + '/user/cards/' + id)
        .then(() => {
          dispatch(this.getCreditCards())
        })
        .catch(error => {
          dispatch({type: 'SET_CARDS_LOADING', loading: false})
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },

  markMainCard(token, id) {
    return dispatch => {
      apiPrivate.put(UserProfile + '/user/cards/' + id + '/main')
        .then(() => {
          dispatch(this.getCreditCards())
        })
        .catch(error => {
          dispatch({type: 'SET_CARDS_LOADING', loading: false})
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },

  getPaymentMethods() {
    return dispatch => {
      apiExternal.get(MercadoPago + '/v1/payment_methods', {
        params: {
          public_key: MERCADOPAGOPUBLICKEY,
          attributes: 'id,settings',
        },
      })
        .then(response => {
          //TODO schema
          dispatch({type: 'GET_PAYMENT_METHODS', methods: response.data})
        })
        .catch(error => {
          console.log(error.response)
          //TODO schema
          Alert.alert('Error', error.response.data.description)
        })
    }
  },
  loading(boolean) {
    return {
      type: 'SET_CARDS_LOADING',
      loading: boolean,
    }
  }
}
