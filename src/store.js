import {createStore, compose}   from 'redux';
import { applyMiddleware } from 'redux'
import rootReducer from './reducers';

import thunk from 'redux-thunk';

const enhancer = compose(
    applyMiddleware(thunk),
    window.devToolsExtension ? window.devToolsExtension() : f => f
);

const store = createStore(
    rootReducer,
    enhancer
);

export default store;
