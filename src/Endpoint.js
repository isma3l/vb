export let Environment = "develop";

export let AccountManager = "https://vb-develop-accountmanager.herokuapp.com";
export let Community = "https://vb-develop-community.herokuapp.com";
export let Category = "https://vb-develop-categorycreation.herokuapp.com";
export let Timeslots = "https://vb-develop-timeslot.herokuapp.com";
export let UserProfile = "https://vb-develop-userprofile.herokuapp.com";
export let ObjectAdmin = "https://vb-develop-objectadministratio.herokuapp.com";
export let MercadoPago = "https://api.mercadopago.com";
export let Transactions = "https://vb-develop-transactionhandler.herokuapp.com";
export let RentManager = "https://vb-develop-rentmanager.herokuapp.com";
export let Notifications = "https://vb-develop-notifications.herokuapp.com";

export function changeEnvironment() {
    if (Environment === "develop") {
        Environment = "service"
    } else {
        Environment = "develop"
    }

    AccountManager = "https://vb-" + Environment + "-accountmanager.herokuapp.com";
    Community = "https://vb-" + Environment + "-community.herokuapp.com";
    Category = "https://vb-" + Environment + "-categorycreation.herokuapp.com";
    Timeslots = "https://vb-" + Environment + "-timeslot.herokuapp.com";
    UserProfile = "https://vb-" + Environment + "-userprofile.herokuapp.com";
    ObjectAdmin = "https://vb-" + Environment + "-objectadministratio.herokuapp.com";
    Transactions = "https://vb-" + Environment + "-transactionhandler.herokuapp.com";
    RentManager = "https://vb-" + Environment + "-rentmanager.herokuapp.com";
    Notifications = "https://vb-" + Environment + "-notifications.herokuapp.com";
}
