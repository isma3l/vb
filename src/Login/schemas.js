import SchemaObject from 'schema-object'

export {ApiError} from '../schemas'

export const Login = new SchemaObject({
  email: String,
  id: String,
  name: String,
  lastName: String,
  missingProfileInformation: [String],
  permissions: [String],
  profilePicture: String,
  token: String,
})
