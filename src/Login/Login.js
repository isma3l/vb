import React, { Component } from 'react';
import {ScrollView, Text, View, TouchableOpacity, ActivityIndicator, Platform, Keyboard} from 'react-native'
import {LogoWithDescription} from "../LogoScreen/Logo";
import Actions from "./Login.actions";
import {connect} from "react-redux";
import {Colors} from "../Layout/Colors";
import {styles} from "../Styles";
import {BackButton, FBButton, HRWithLabel, PrimaryButton} from "../MyComponents";
import KeyboardView from "../KeyboardView";
import {changeEnvironment, Environment} from "../Endpoint";
import {TextField} from "react-native-material-textfield";

class Login extends Component {

    state = {
        email: "",
        password: ""
    };

    componentDidMount() {
        this.props.setLoading(false);
        this._sub = this.props.navigation.addListener(
            'willBlur',
            () => Keyboard.dismiss()
        );
    }

    componentWillUnmount() {
        this._sub.remove();
    }

    login = () => {
        const state = this.state;
        this.props.setLoading(true);
        const formData = new FormData();
        formData.append("email", state.email);
        formData.append("password", state.password);
        this.props.login(formData, this.props.redirectRoute);
    };

    loginWithFacebook = () => {
        this.props.setLoading(true);
        this.props.loginWithFB(this.props.redirectRoute);
    };

    renderSpinner = () => (
        <View style={styles.centerContainer}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    render() {
        return (
            <KeyboardView style={{flex: 1}}>
                    {this.props.loading ? this.renderSpinner() : this.renderLogin()}
            </KeyboardView>
        )
    }

    renderLogin = () => {
        const titleLogo = ["¡Qué bueno", "volverte a ver!"];
        const subtitleLogo = ["¿Eres nuevo por aquí? "];
        const linkTextLogo = "Registrate ahora";

        return (
            <ScrollView contentContainerStyle={styles.loginContainer} keyboardShouldPersistTaps='always'>
                <BackButton navigation={this.props.navigation}/>
                <View style={{
                    minHeight: 240,
                    justifyContent: "center",
                    alignItems: Platform.OS === "ios" ? "flex-start" : "center",
                    width: "90%",
                    marginTop: Platform.OS === "ios" ? 20 : 0,
                }}>
                    <LogoWithDescription
                        title={titleLogo}
                        subtitle={subtitleLogo}
                        linkLabel={linkTextLogo}
                        onPress={() => this.props.navigation.navigate("SignUp")}
                    />
                </View>
                {Platform.OS === "android" &&
                    <View style={{width: "100%"}}>
                        <FBButton onPress={this.loginWithFacebook}/>
                        <HRWithLabel padding={20} label="O ingresa tus datos"/>
                    </View>
                }
                <View style={{flexDirection: "column", width: "90%", justifyContent: "center", marginBottom: 10}}>
                    <TextField label="Email"
                               inputContainerStyle={{width: "100%", height: 60}}
                               autoCapitalize="none"
                               tintColor={Colors.primaryColor}
                               keyboardType="email-address"
                               lineWidth={0.5}
                               placeholderTextColor="grey"
                               defaultValue={this.state.email}
                               onChangeText={text => this.setState({email: text})}
                               autoCorrect={false}
                    />
                    <TextField label="Contraseña"
                               inputContainerStyle={{width: "100%", height: 60}}
                               placeholderTextColor="grey"
                               tintColor={Colors.primaryColor}
                               lineWidth={0.5}
                               defaultValue={this.state.password}
                               secureTextEntry={true}
                               onChangeText={text => this.setState({password: text})}
                               autoCorrect={false}
                    />
                </View>
                <View style={{width: "100%"}}>
                    <PrimaryButton onPress={this.login} label="Entrar" />
                </View>
                <View style={{flexDirection: "row"}}>
                    <TouchableOpacity onPress={() => {
                        const navigation = this.props.navigation;
                        navigation.navigate('SendRecoveryCode');
                    }}>
                        <View>
                            <Text style={styles.forgetPasswordText}>Olvidé mi contraseña</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        changeEnvironment();
                        this.props.navigation.navigate('LayoutGuest');
                    }}>
                        <View>
                            <Text style={styles.forgetPasswordText}>Cambiar
                                a {Environment === "develop" ? "staging" : "develop"}</Text>
                        </View>
                    </TouchableOpacity>

                </View>
            </ScrollView>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.Login.loading,
        redirectRoute: state.Login.redirectRoute
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        login(formData, redirectRoute){
            dispatch(Actions.login(formData, redirectRoute))
        },
        loginWithFB(redirectRoute){
            dispatch(Actions.loginWithFB(redirectRoute));
        },
        setLoading(boolean){
            dispatch(Actions.loading(boolean))
        },
        loginGuest(){
            dispatch(Actions.loginGuest());
        },
        setName(name){
            dispatch(Actions.setName(name));
        },
        setGuest(boolean){
            dispatch(Actions.setGuest(boolean))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
