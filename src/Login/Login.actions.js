import {AccountManager, UserProfile} from '../Endpoint'
import store from 'react-native-simple-store'
import {NavigationActions} from 'react-navigation'
import {navigatorRef} from '../../App'
import {Alert} from 'react-native'
import {LoginManager as FbLoginManager, AccessToken as FbAccessToken} from 'react-native-fbsdk'
import {Login as LoginSchema, ApiError} from './schemas'
import {apiPublic, apiPrivate} from '../api'

export default {

  login(formData, redirectRoute = 'Layout') {
    return dispatch => {
      apiPublic.post(AccountManager + '/login', formData)
        .then(response => {
          dispatch(this.loading(false))

          const data = new LoginSchema(response.data.description)

          return Promise.all([
            store.save('user_id', data.id),
            store.save('token', data.token),
          ])
            .then(() => {
              dispatch(this.setToken(data.token, data.id))
              dispatch(this.setMissingInformation(data.missingProfileInformation))

              navigatorRef.dispatch(NavigationActions.navigate({routeName: redirectRoute}))
            })
            .catch(err => {
              console.error(err)
              throw Error('Error al guardar credenciales')
            })
        })
        .catch(error => {
          dispatch(this.loading(false))
          let message = ''
          if (error.response) {
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },

  signUp(formData, redirectRoute = 'Layout') {
    return dispatch => {
      apiPublic.post(AccountManager + '/signup', formData)
        .then(response => {
          dispatch(this.loading(false))

          const data = new LoginSchema(response.data.description)

          return Promise.all([
            store.save('user_id', data.id),
            store.save('token', data.token),
          ])
            .then(() => {
              dispatch(this.setToken(data.token, data.id))
              dispatch(this.setMissingInformation(data.missingProfileInformation))

              navigatorRef.dispatch(NavigationActions.navigate({routeName: redirectRoute}))
            })
            .catch(err => {
              console.error(err)
              throw Error('Error al guardar credenciales')
            })
        })
        .catch(error => {
          dispatch(this.loading(false))
          let message = ''
          if (error.response) {
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },

  loginWithFB(redirectRoute = 'Layout') {
    return dispatch => {
      FbLoginManager.logInWithReadPermissions(['public_profile', 'email'])
        .then(result => {

          if (result.isCancelled) return dispatch(this.loading(false))

          FbAccessToken.getCurrentAccessToken()
            .then(fbData => {

              const formData = new FormData()
              formData.append('access_token', fbData.accessToken)
              formData.append('facebook_id', fbData.userID)

              apiPublic.post(AccountManager + '/login/facebook', formData)
                .then(response => {
                  dispatch(this.loading(false))

                  const data = new LoginSchema(response.data.description)

                  return Promise.all([
                    store.save('user_id', data.id),
                    store.save('token', data.token),
                  ])
                    .then(() => {
                      dispatch(this.setToken(data.token, data.id))
                      dispatch(this.setMissingInformation(data.missingProfileInformation))

                      navigatorRef.dispatch(NavigationActions.navigate({routeName: redirectRoute}))
                    })
                    .catch(err => {
                      console.error(err)
                      throw Error('Error al guardar credenciales')
                    })
                })
                .catch(error => {
                  dispatch(this.loading(false))
                  let message = ''
                  if (error.response) {
                    const response = new ApiError(error.response.data)
                    message = response.description
                  } else if (error.request) {
                    console.warn(error.request)
                    message = 'Algo salió mal, revisá tu conexión a internet'
                  } else {
                    console.warn('Error', error.message)
                    message = 'Algo salió mal.'
                  }
                  Alert.alert('Error', message, [{text: 'OK'}])
                })
            })

        }, error => {
          dispatch(this.loading(false))
          console.warn(error)
          Alert.alert('Error', 'Hubo un error al ingresar con Facebook. Inténtelo nuevamente.')
          FbLoginManager.logOut()
        })
    }
  },

  changePassword(oldPassword, newPassword) {

    const formData = new FormData()

    formData.append('old_password', oldPassword)
    formData.append('new_password', newPassword)

    return dispatch => {
      apiPrivate.put(UserProfile + '/user/password', formData)
        .then(() => {
          dispatch(this.loading(false))
          Alert.alert('Éxito', 'Contraseña modificada.', [{text: 'OK'}])
          navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Profile'}))
        })
        .catch(error => {
          dispatch(this.loading(false))
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },


  sendRecoveryCode(formData) {
    return dispatch => {
      apiPublic.post(AccountManager + '/recoverPassword', formData)
        .then(() => {
          dispatch(this.loading(false))
          navigatorRef.dispatch(NavigationActions.navigate({
            routeName: 'RecoverNewPassword',
            params: {editEmail: false},
          }))
        })
        .catch(error => {
          dispatch(this.loading(false))
          let message = ''
          if (error.response) {
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },

  recoverNewPassword(email, code, password, backKey) {

    const formData = new FormData()

    formData.append('email', email)
    formData.append('recoveryCode', code)
    formData.append('password', password)

    return dispatch => {
      apiPublic.post(AccountManager + '/changePassword', formData)
        .then(() => {
          dispatch(this.loading(false))
          navigatorRef.dispatch(NavigationActions.back({key: backKey}))
          Alert.alert('Éxito', 'Ingrese a su cuenta con su nueva contraseña')
        })
        .catch(error => {
          dispatch(this.loading(false))
          let message = ''
          if (error.response) {
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },

  loading(boolean) {
    return {
      type: 'SET_LOADING',
      loading: boolean,
    }
  },

  setActualScreen(screen) {
    return {
      type: 'SET_ACTUAL_SCREEN',
      screen,
    }
  },

  backScreen() {
    return {
      type: 'BACK_SCREEN',
    }
  },

  setToken(token, userID) {
    return {
      type: 'SET_TOKEN',
      token,
      userID,
    }
  },

  setPrefix(prefix) {
    return {
      type: 'SET_PREFIX',
      prefix,
    }
  },

  setPhone(phone) {
    return {
      type: 'SET_PHONE',
      phone,
    }
  },

  setEmail(email) {
    return {
      type: 'SET_EMAIL',
      email,
    }
  },

  toggleDrawer(boolean) {
    return {
      type: 'TOGGLE_DRAWERIOS',
      boolean,
    }
  },

  setMissingInformation(array) {
    return {
      type: 'SET_MISSING_INFORMATION',
      array,
    }
  },

  deleteMissingInformation(string) {
    return {
      type: 'DELETE_MISSING_INFORMATION',
      string,
    }
  },

  setRedirectRoute(redirectRoute) {
    return {
      type: 'SET_REDIRECT_ROUTE',
      redirectRoute,
    }
  },

  storeBackKey(backKey) {
    return {
      type: 'STORE_BACK_KEY',
      backKey,
    }
  },
}
