import React, {Component} from 'react'
import {View, ActivityIndicator, Alert, Platform, ScrollView} from 'react-native'
import {LogoWithDescription} from '../LogoScreen/Logo'
import Actions from '../Login/Login.actions'
import {connect} from 'react-redux'
import {Colors} from '../Layout/Colors'
import {styles} from '../Styles'
import {BackButton, PrimaryButton} from '../MyComponents'
import KeyboardView from '../KeyboardView'
import {TextField} from 'react-native-material-textfield'

class RecoverNewPassword extends Component {

  state = {
    editEmail: this.props.navigation.state.params ? this.props.navigation.state.params.editEmail : true,
    password: '',
    confirmPassword: '',
    code: '',
  }

  componentDidMount() {
    this.props.setLoading(false)
  }

  recoverPassword = () => {
    const state = this.state

    //TODO move rule somewhere else
    if (state.password.length < 6) {
      Alert.alert('Error', 'La contraseña debe ser de 6 o más caracteres', [{text: 'OK'}])
      return
    }

    if (state.password !== state.confirmPassword) {
      Alert.alert('Error', 'Las contraseñas no son iguales', [{text: 'OK'}])
      return
    }

    this.props.setLoading(true)
    this.props.recoverNewPassword(this.props.email, state.code, state.password, this.props.backKey)

  }

  renderSpinner = () => (
    <View style={styles.centerContainer}>
      <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
    </View>
  )

  render() {
    return (
      <KeyboardView
        style={{height: '100%', width: '100%'}}
      >
        <View style={styles.container}>
          {this.props.loading ? this.renderSpinner() : this.renderRecoverNewPassword()}
        </View>
      </KeyboardView>
    )
  }

  renderRecoverNewPassword = () => {
    const titleLogo = ['¡Restablece tu contraseña!']
    const subtitleLogo = ['Crea una nueva contraseña para tu cuenta ']

    return (
      <ScrollView contentContainerStyle={styles.loginContainer} keyboardShouldPersistTaps='always'>
        <BackButton navigation={this.props.navigation}/>
        <View style={{
          minHeight: 240,
          justifyContent: 'center',
          alignItems: Platform.OS === 'ios' ? 'flex-start' : 'center',
          width: '90%',
          marginTop: Platform.OS === 'ios' ? 20 : 0,
        }}>
          <LogoWithDescription
            title={titleLogo}
            subtitle={subtitleLogo}
            onPress={() => this.props.navigation.navigate('SignUp')}
          />
        </View>
        <View style={{flexDirection: 'column', width: '90%', justifyContent: 'center', marginBottom: 20}}>
          <TextField
            label="Email"
            inputContainerStyle={{width: '100%', height: 60}}
            autoCapitalize="none"
            tintColor={Colors.primaryColor}
            keyboardType="email-address"
            lineWidth={0.5}
            placeholderTextColor="grey"
            defaultValue={this.props.email}
            onChangeText={text => this.props.setEmail(text)}
            disabled={!this.state.editEmail}
            autoCorrect={false}
          />
          <TextField
            label="Código"
            inputContainerStyle={{width: '100%', height: 60}}
            underlineColorAndroid={Colors.primaryColor}
            placeholderTextColor="grey"
            maxLength={5}
            autoCapitalize="none"
            tintColor={Colors.primaryColor}
            keyboardType="phone-pad"
            defaultValue={this.state.code}
            onChangeText={text => this.setState({code: text})}/>
          <TextField
            label="Nueva contraseña"
            inputContainerStyle={{width: '100%', height: 60}}
            placeholderTextColor="grey"
            tintColor={Colors.primaryColor}
            lineWidth={0.5}
            defaultValue={this.state.password}
            secureTextEntry={true}
            autoCorrect={false}
            onChangeText={text => this.setState({password: text})}/>
          <TextField
            label="Repetir contraseña"
            inputContainerStyle={{width: '100%', height: 60}}
            placeholderTextColor="grey"
            tintColor={Colors.primaryColor}
            lineWidth={0.5}
            defaultValue={this.state.confirmPassword}
            secureTextEntry={true}
            onChangeText={text => this.setState({confirmPassword: text})}
            autoCorrect={false}
          />
        </View>
        <PrimaryButton
          label="Restablecer contraseña"
          onPress={() => this.recoverPassword()}
        />
      </ScrollView>
    )
  }
}

const mapStateToProps = state => {
  return {
    loading: state.Login.loading,
    token: state.Login.token,
    email: state.Login.email,
    backKey: state.Login.backKey,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setLoading: boolean => dispatch(Actions.loading(boolean)),
    setEmail: email => dispatch(Actions.setEmail(email)),
    recoverNewPassword: (email, code, password, backKey) => dispatch(Actions.recoverNewPassword(email, code, password, backKey)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RecoverNewPassword)
