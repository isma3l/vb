import React, { Component } from 'react';
import {ScrollView, Text, View, ActivityIndicator, Alert, Platform, Keyboard} from 'react-native'
import {LogoWithDescription} from "../LogoScreen/Logo";
import Actions from "../Login/Login.actions";
import {connect} from "react-redux";
import {Colors} from "../Layout/Colors";
import {styles} from "../Styles";
import {BackButton, FBButton, HRWithLabel, PrimaryButton} from "../MyComponents";
import KeyboardView from "../KeyboardView";
import {TextField} from "react-native-material-textfield";

class SignUp extends Component {

    state = {
        email: "",
        password: "",
        confirmPassword: ""
    }

    componentDidMount() {
        this.props.setLoading(false);
        this._sub = this.props.navigation.addListener(
            'willBlur',
            () => Keyboard.dismiss()
        );
    }

    componentWillUnmount() {
        this._sub.remove();
    }

    signUp = () => {
        const state = this.state;
        if (state.password.length < 6) {
            Alert.alert("Error", "La contraseña debe ser de 6 o más caracteres", [{text: 'OK'}]);
            return
        }
        if (state.password !== state.confirmPassword) {
            Alert.alert("Error", "Las contraseñas no son iguales", [{text: 'OK'}])
        } else {
            const formData = new FormData();

            formData.append("email", state.email);
            formData.append("password", state.password);
            formData.append("name", state.name);
            formData.append("last_name", state.last_name);

            this.props.setLoading(true)
            this.props.signUp(formData, this.props.redirectRoute);
        }
    }


    renderSpinner = () => (
        <View style={styles.centerContainer}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    render() {
        return (
            <KeyboardView style={{flex: 1}}>
                {this.props.loading ? this.renderSpinner() : this.renderNewUser()}
            </KeyboardView>
        )
    }

    renderNewUser = () => {
        const titleLogo = ["¡Hey!", "Bienvenido"];
        const subtitleLogo = ["Registrarte no te tomará mas de 1 minuto.", "¿Ya tenes usuario? "];
        const linkTextLogo = "Ingresa aquí";

        return (
            <ScrollView contentContainerStyle={styles.loginContainer} keyboardShouldPersistTaps='always'>
                <BackButton navigation={this.props.navigation}/>
                <View style={{minHeight: 240, justifyContent: "center", alignItems: Platform.OS === "ios" ? "flex-start" : "center", width: "90%", marginBottom: Platform.OS === "ios" ? 40 : 0, marginTop: Platform.OS === "ios" ? 20 : 0}}>
                    <LogoWithDescription title={titleLogo} subtitle={subtitleLogo} linkLabel={linkTextLogo}
                                         onPress={() => this.props.navigation.navigate("Login")}/>
                </View>
                <View style={{width: "100%"}}>
                    <FBButton onPress={() => this.props.loginWithFB(this.props.redirectRoute)}/>
                    <HRWithLabel padding={20} label="O ingresa tus datos" />
                </View>
                <View style={{width: "90%", marginBottom: 20}}>
                    <TextField label="Nombre"
                               inputContainerStyle={{width: "100%", height: 60}}
                               tintColor={Colors.primaryColor}
                               lineWidth={0.5}
                               placeholderTextColor="grey"
                               defaultValue={this.state.name}
                               onChangeText={text => this.setState({name: text})}
                               autoCorrect={false}
                    />
                    <TextField label="Apellido"
                               inputContainerStyle={{width: "100%", height: 60}}
                               placeholderTextColor="grey"
                               tintColor={Colors.primaryColor}
                               lineWidth={0.5}
                               defaultValue={this.state.last_name}
                               onChangeText={text => this.setState({last_name: text})}
                               autoCorrect={false}
                    />
                    <TextField label="Email"
                               inputContainerStyle={{width: "100%", height: 60}}
                               autoCapitalize="none"
                               tintColor={Colors.primaryColor}
                               keyboardType="email-address"
                               lineWidth={0.5}
                               placeholderTextColor="grey"
                               defaultValue={this.state.email}
                               onChangeText={text => this.setState({email: text})}
                               autoCorrect={false}
                    />
                    <TextField label="Contraseña"
                               inputContainerStyle={{width: "100%", height: 60}}
                               placeholderTextColor="grey"
                               tintColor={Colors.primaryColor}
                               lineWidth={0.5}
                               defaultValue={this.state.password}
                               secureTextEntry={true}
                               onChangeText={text => this.setState({password: text})}
                               autoCorrect={false}
                    />
                    <TextField label="Confirmar contraseña"
                               inputContainerStyle={{width: "100%", height: 60}}
                               placeholderTextColor="grey"
                               tintColor={Colors.primaryColor}
                               lineWidth={0.5}
                               defaultValue={this.state.confirmPassword}
                               secureTextEntry={true}
                               onChangeText={text => this.setState({confirmPassword: text})}
                               autoCorrect={false}
                    />
                </View>
                <PrimaryButton onPress={this.signUp} label="Entrar" />
                <View style={{width: "90%", alignSelf: "center", marginVertical: 20}}>
                    <Text style={{textAlign: "center"}}>
                        Al entrar aceptas los términos y condiciones de uso de Space.Guru
                    </Text>
                </View>
            </ScrollView>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.Login.loading,
        redirectRoute: state.Login.redirectRoute
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        signUp(formData, redirectRoute){
            dispatch(Actions.signUp(formData, redirectRoute))
        },
        setLoading(boolean){
            dispatch(Actions.loading(boolean))
        },
        loginWithFB(redirectRoute){
            dispatch(Actions.loading(true));
            dispatch(Actions.loginWithFB(redirectRoute));
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
