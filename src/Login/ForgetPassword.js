import React, { Component } from 'react';
import {
    TouchableNativeFeedback, Text, View, TouchableOpacity, TextInput, ActivityIndicator,
    Platform, ScrollView
} from 'react-native'
import Actions from "../Login/Login.actions";
import {connect} from "react-redux";
import {Colors} from "../Layout/Colors";
import {styles, VerifyPhoneStyles} from "../Styles";
import {Logo, LogoWithDescription} from "../LogoScreen/Logo";
import {BackButton, PrimaryButton} from "../MyComponents";
import {TextField} from "react-native-material-textfield";
import KeyboardView from "../KeyboardView";


class ForgetPassword extends Component {
    componentDidMount() {
        this.props.setLoading(false);
    }

    renderSpinner = () => (
        <View style={styles.centerContainer}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    renderForgetPassword = () => {
        const titleLogo = ["Introduce tu email para restablecer tu contraseña"];
        const subtitleLogo = [];

        return (
            <ScrollView contentContainerStyle={styles.loginContainer} keyboardShouldPersistTaps='always'>
                <BackButton navigation={this.props.navigation}/>
                <View style={{
                    minHeight: 240,
                    justifyContent: "center",
                    alignItems: Platform.OS === "ios" ? "flex-start" : "center",
                    width: "90%",
                    marginTop: Platform.OS === "ios" ? 20 : 0,
                }}>
                    <LogoWithDescription
                        title={titleLogo}
                        subtitle={subtitleLogo}
                        onPress={() => this.props.navigation.navigate("SignUp")}
                    />
                </View>
                <View style={{flexDirection: "column", width: "90%", justifyContent: "center", marginBottom: 20}}>
                    <TextField label="Email"
                               inputContainerStyle={{width: "100%", height: 60}}
                               autoCapitalize="none"
                               tintColor={Colors.primaryColor}
                               keyboardType="email-address"
                               lineWidth={0.5}
                               placeholderTextColor="grey"
                               onChangeText={text => this.props.setEmail(text)}
                               autoCorrect={false}
                    />
                </View>
                <View style={{width: "100%"}}>
                    <PrimaryButton onPress={() => this.sendRecoveryCode()} label="Restablecer contraseña" />
                    <TouchableOpacity
                        onPress={() => {
                            this.props.storeBackKey(navigation.state.key)
                            this.props.navigation.navigate('RecoverNewPassword', {editEmail: true})
                        }}
                        style={{alignSelf: "center"}}
                    >
                        <View>
                            <Text style={styles.forgetPasswordText}>Ya tenes un código?</Text>
                        </View>
                    </TouchableOpacity>
                </View>

            </ScrollView>
        )
    }

    sendRecoveryCode = () => {
        const formData = new FormData();
        formData.append("email", this.props.email);
        this.props.setLoading(true);
        this.props.storeBackKey(this.props.navigation.state.key);
        this.props.sendRecoveryCode(formData);
    }

    render() {
        return (
            <KeyboardView
                style={{height: "100%", width: "100%"}}
            >
                {this.props.loading ? this.renderSpinner() : this.renderForgetPassword()}
            </KeyboardView>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.Login.loading,
        token: state.Login.token,
        email: state.Login.email
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        sendRecoveryCode(formData){
            dispatch(Actions.sendRecoveryCode(formData))
        },
        setLoading(boolean){
            dispatch(Actions.loading(boolean))
        },
        setEmail(email) {
            dispatch(Actions.setEmail(email))
        },
        storeBackKey(backKey){
            dispatch(Actions.storeBackKey(backKey));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ForgetPassword);
