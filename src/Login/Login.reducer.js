const initialState = {
    loading: false,
    screen: "Login",
    previousScreens: [],
    openios: false,
    name: "",
    guest: true,
    token: "",
    phone: {
        prefix: "+54",
        phone: ""
    },
    email: "",
    userID: "",
    redirectRoute: "Layout"
};

const Login = (state = initialState, action) => {
    switch (action.type) {
        case "SET_REDIRECT_ROUTE":
            return {...state, redirectRoute: action.redirectRoute}
        case "SET_LOADING":
            return {...state, loading: action.loading};
        case "SET_ACTUAL_SCREEN":
            return {...state, previousScreens: [...state.previousScreens, state.screen], screen: action.screen};
        case "BACK_SCREEN":
            return {...state, screen: state.previousScreens[state.previousScreens.length - 1], previousScreens: state.previousScreens.pop()}
        case "SET_TOKEN":
            return {...state, token: action.token, userID: action.userID}
        case "SET_PREFIX":
            return {...state, phone: {...state.phone, prefix: action.prefix}}
        case "SET_PHONE":
            return {...state, phone: {...state.phone, phone: action.phone}}
        case "SET_EMAIL":
            return {...state, email: action.email}
        case "REMOVE_TOKEN":
            return {...state, token: "", userID: ""}
        case "SET_MISSING_INFORMATION":
            return {...state, missingInformation: action.array}
        case "DELETE_MISSING_INFORMATION":
            return {...state, missingInformation: state.missingInformation.filter(missingInfo => missingInfo !== action.string)}
        case "STORE_BACK_KEY":
            return {...state, backKey: action.backKey}
        default:
            return state;
    }
};

export default Login;