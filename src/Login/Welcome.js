import React, { Component } from 'react';
import {ScrollView, Text, View, Image, ActivityIndicator} from 'react-native'
import Actions from "./Login.actions";
import {connect} from "react-redux";
import {Colors} from "../Layout/Colors";
import {styles, WelcomeStyle} from "../Styles";

import KeyboardView from "../KeyboardView";
import {WELCOMEIMAGE} from "../Images";
import {BackButton, FBButton, GradientButton, HRWithLabel} from "../MyComponents";

class Welcome extends Component {
    state = {
        email: "",
        password: ""
    };

    componentDidMount() {
        this.props.setLoading(false);
    }

    login() {
        const state = this.state;
        this.props.setLoading(true);
        const formData = new FormData();
        formData.append("email", state.email);
        formData.append("password", state.password);
        this.props.login(formData);
    };

    loginWithFacebook = () => {
        this.props.setLoading(true);
        this.props.loginWithFB(this.props.redirectRoute);
    };

    renderSpinner = () => (
        <View style={styles.centerContainer}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    render() {
        return (
            <KeyboardView style={styles.container}>
                    {this.props.loading ? this.renderSpinner() : this.renderWelcome()}
            </KeyboardView>
        )
    }

    renderPage = (item) => {
        return (
            <View key={item.key} style={{flex: 1, alignItems: "center", marginBottom: 40, marginTop: 20}}>
                <Image source={item.image}/>
                <View style={{marginHorizontal: "10%", marginTop: 16, marginBottom: 14}}>
                    <Text style={WelcomeStyle.title}>{item.title}</Text>
                </View>
                <View style={{marginHorizontal: "10%"}}>
                    <Text style={WelcomeStyle.subtitle}>{item.subtitle}</Text>
                </View>
            </View>
        )
    }

    renderWelcome = () => {
        //TODO replace app name with env var
        const Pages = [
            {image: WELCOMEIMAGE, title: "Bienvenido a Space.Guru", subtitle: "La forma simple de almacenar lo que ya no usás, y rentar lo que necesitás.", key: 0},
            {image: WELCOMEIMAGE, title: "Bienvenido a Space.Guru", subtitle: "La forma simple de almacenar lo que ya no usás, y rentar lo que necesitás.", key: 1},
            {image: WELCOMEIMAGE, title: "Bienvenido a Space.Guru", subtitle: "La forma simple de almacenar lo que ya no usás, y rentar lo que necesitás.", key: 2}
        ]

        const loginLabel = "Iniciar sesión";
        const signupLabel = "¡Crear una cuenta!";
        const hrLabel = "¿Eres nuevo?";

        return (
            <View style={{backgroundColor: Colors.backgroundColor2, flex: 1}}>
                <BackButton navigation={this.props.navigation}/>
                <ScrollView>
                    {this.renderPage(Pages[0])}
                    <FBButton onPress={this.loginWithFacebook}/>
                    <View style={{marginBottom: 20}}>
                        <GradientButton label={loginLabel} onPress={() => this.props.navigation.navigate("Login")}/>
                    </View>
                    <HRWithLabel padding={30} label={hrLabel}/>
                    <View style={{marginTop: 20, marginBottom: 20}}>
                        <GradientButton label={signupLabel} onPress={() => this.props.navigation.navigate("SignUp")}/>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.Login.loading,
        redirectRoute: state.Login.redirectRoute
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        login(formData){
            dispatch(Actions.login(formData))
        },
        loginWithFB(redirectRoute){
            dispatch(Actions.loginWithFB(redirectRoute));
        },
        setLoading(boolean){
            dispatch(Actions.loading(boolean))
        },
        loginGuest(){
            dispatch(Actions.loginGuest());
        },
        setName(name){
            dispatch(Actions.setName(name));
        },
        setGuest(boolean){
            dispatch(Actions.setGuest(boolean))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Welcome);
