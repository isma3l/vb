import {Platform, TouchableNativeFeedback, TouchableOpacity, View, Text, TouchableHighlight} from "react-native";
import React from "react";
import {WelcomeStyle} from "./Styles";
import LinearGradient from "react-native-linear-gradient";
import {Colors} from "./Layout/Colors";
import {Button, Icon} from "react-native-elements"
import Hr from "react-native-hr-plus";
import {NavigationActions} from "react-navigation";

export default class ButtonNative extends React.Component {
    render() {
        if (Platform.OS === "android") {
            return (
                <TouchableNativeFeedback
                    {...this.props}
                    disabled={this.props.disabled}
                >
                    <View style={this.props.styles}>
                        {this.props.children}
                    </View>
                </TouchableNativeFeedback>
            );
        } else {
            const props = {...this.props, styles: undefined};
            return (
                <TouchableOpacity {...props} style={this.props.styles} disabled={this.props.disabled}>
                    {this.props.children}
                </TouchableOpacity>
            )
        }

    }
}

export const GradientButton = (props) => (
    <TouchableOpacity activeOpacity={0.75} onPress={props.onPress}>
        <LinearGradient colors={[Colors.gradientColorA, Colors.gradientColorB]} start={{x: 0, y: 0}} end={{x: 1, y: 1}} style={WelcomeStyle.welcomeButtons}>
            <View style={{backgroundColor: "transparent"}}>
                <Text style={WelcomeStyle.buttonText}>
                    {Platform.OS === "ios" ? props.label : props.label.toUpperCase()}
                </Text>
            </View>
        </LinearGradient>
    </TouchableOpacity>
)

export const PrimaryButton = (props) => {
    return (
        <TouchableOpacity
            activeOpacity={0.75}
            disabled={props.disabled}
            onPress={props.onPress}
            style={[WelcomeStyle.welcomeButtons, {backgroundColor: Colors.primaryColor, opacity: 1}, props.style]}
        >
            <Text style={WelcomeStyle.buttonText}>
                {Platform.OS === "ios" ? props.label : props.label.toUpperCase()}
            </Text>
        </TouchableOpacity>
    )
}

export const SecondaryButton = (props) => {
    return (
        <TouchableOpacity
            activeOpacity={0.75}
            onPress={props.onPress}
            style={[WelcomeStyle.welcomeButtons, {borderColor: Colors.primaryColor, borderWidth: 2, borderRadius: 5}, props.style]}
        >
            <Text style={[WelcomeStyle.buttonText, {color: Colors.primaryColor}]}>
                {Platform.OS === "ios" ? props.label : props.label.toUpperCase()}
            </Text>
        </TouchableOpacity>
    )
}

export const PrimaryHighlightButton = (props) => {
    return (
        <TouchableHighlight
            activeOpacity={0.75}
            disabled={props.disabled}
            onPress={props.onPress}
            style={[WelcomeStyle.welcomeButtons, props.style, props.disabled ? {backgroundColor: Colors.primaryColor, opacity: 0.3} : {backgroundColor: Colors.primaryColor, opacity: 1}]}
        >
            <Text style={WelcomeStyle.buttonText}>
                {Platform.OS === "ios" ? props.label : props.label.toUpperCase()}
            </Text>
        </TouchableHighlight>
    )
}

const fbLabel = "Continúa con Facebook";

export const FBButton = (props) => (
    <Button
        containerViewStyle={WelcomeStyle.facebookButton}
        buttonStyle={{backgroundColor: Colors.fbButtonColor, borderRadius: 5, height: 50}}
        textStyle={{fontSize: Platform.OS === "ios" ? 17 : 14}}
        Component={TouchableOpacity}
        activeOpacity={0.75}
        onPress={props.onPress}
        title={Platform.OS === "ios" ? fbLabel : fbLabel.toUpperCase()}
        leftIcon={{name: "logo-facebook", type: "ionicon", size: 25, style: {position: "absolute", left: 15}}}
    />
)

export const HRWithLabel = (props) => (
    <View style={{width: "90%", justifyContent: "center", alignItems: "center", alignSelf: "center"}}>
        <Hr color={Colors.hrColor} width={1}>
            <Text style={{paddingHorizontal: props.padding ? props.padding : 20}}>{props.label}</Text>
        </Hr>
    </View>
)

export const BackButton = (props) => (
    <TouchableOpacity
        onPress={() => props.navigation.dispatch(NavigationActions.back())}
        style={{position: "absolute", top: 32, left: 8, zIndex: 10, padding: 10}}
    >
        <Icon
            name="ios-arrow-back"
            type="ionicon"
            color={Colors.primaryColor}
        />
    </TouchableOpacity>
)
