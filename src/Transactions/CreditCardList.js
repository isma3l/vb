import React, { Component } from 'react';
import {View, ActivityIndicator, ScrollView} from 'react-native'
import {NavigationActions} from "react-navigation";
import {ListItem} from "react-native-elements";
import {styles} from "../Styles";
import {Colors} from "../Layout/Colors";
import LoginActions from "../Login/Login.actions";
import {connect} from "react-redux";
import ButtonNative from "../MyComponents";
import {FadeInView} from "../AnimatedComponents";
import CardsActions from "../CreditCard/CreditCard.actions";

class CreditCardList extends Component {

    componentDidMount(){
        this.props.setLoading(true);
        this.props.getCreditCards(this.props.token);
    }

    selectCreditCard(ID) {
        const formData = new FormData();
        formData.append("card_id", ID);
        this.props.navigation.dispatch(NavigationActions.back());
        this.props.modifyTransaction(this.props.token, formData, this.props.transactionID);
    }

    renderCards(cards){
        const cardsView = cards.map((card, i) => {
            return (
                <View key={i} style={{borderBottomWidth: 1, borderBottomColor: Colors.borderColor}}>
                    <ListItem
                        title={"Tarjeta " + (i + 1)}
                        component={ButtonNative}
                        wrapperStyle={{paddingTop: 10, paddingBottom: 10, margin: 0}}
                        key={card.ID}
                        subtitle={"Tarjeta terminada en " + card.LastDigits + " (" + card.Type +")"}
                        leftIcon={{name: "credit-card", color: Colors.primaryColor}}
                        onPress={() => this.selectCreditCard(card.ID)}
                        hideChevron={true}
                    />
                </View>
            )
        })

        cardsView.push(
            <View key={cards.length} style={{borderBottomWidth: 1, borderBottomColor: Colors.borderColor}}>
                <ListItem
                    title={"Añadir nueva tarjeta"}
                    component={ButtonNative}
                    containerStyle={{width: "100%"}}
                    wrapperStyle={{paddingTop: 15, paddingBottom: 15, margin: 0}}
                    leftIcon={{name: "add", color: Colors.primaryColor, style: {marginLeft: 0}}}
                    onPress={() => {
                        this.props.navigation.navigate("AddCreditCard", {redirectRoute: "CreditCardList"});
                    }}
                    hideChevron={true}
                />
            </View>
        )

        return (
            <FadeInView style={{backgroundColor: "white"}}>
                <ScrollView>
                    {cardsView}
                </ScrollView>
            </FadeInView>
        )
    }

    renderSpinner = () => (
        <View style={styles.centerContainer}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    render() {
        return this.props.loading ? this.renderSpinner() : this.renderCards(this.props.cards)
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        loading: state.CreditCard.loading,
        token: state.Login.token,
        cards: state.CreditCard.cards,
        transactionID: ownProps.navigation.state.params.transactionID,
        modifyTransaction: ownProps.navigation.state.params.modifyTransaction,
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        setLoading(boolean){
            dispatch(LoginActions.loading(boolean))
        },
        getCreditCards(token) {
            dispatch({type: "SET_CARDS_LOADING", loading: true});
            dispatch(CardsActions.getCreditCards(token));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CreditCardList);
