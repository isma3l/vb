import React, { Component } from 'react'
import {View, Text, ActivityIndicator, ScrollView, FlatList, Image, TouchableHighlight, SafeAreaView} from 'react-native'
import {connect} from "react-redux"
import {styles} from "../Styles"
import {Colors} from "../Layout/Colors"
import DepositActions from '../NewDeposit/Deposit.actions'
import {CLOTHES, RETIRO_PROGRAMADO, BICI, BOX} from "../Images";
import Icon from "react-native-vector-icons/MaterialIcons";
import LinearGradient from "react-native-linear-gradient";
import ItemModalPrice from "./ItemModalPrice";
import {FadeInView} from "../AnimatedComponents";

class ModalPricesScreen extends Component {

    componentDidMount() {
        this.props.getPrices()
    }

    renderItem = ({item}) => {
        return <ItemModalPrice item={item}/>
    }

    renderHeader = () => {
        return (
            <View style={{paddingBottom:32, width:"100%", backgroundColor:"white", alignItems:"center",
                borderTopLeftRadius:4, borderTopRightRadius:4, borderBottomWidth:0.5, borderColor: Colors.borderColor}}>
                <TouchableHighlight style={{alignItems: "center", flexDirection:"row", width:"100%", justifyContent:"flex-end",
                marginTop:2, marginRight:2}} onPress={this.props.hideModal}>
                    <View style={{ height: 30, width:30, justifyContent:"center", alignItems:"center"}}>
                        <Icon
                            name="close"
                            size={12}
                            color={Colors.darkGray}
                        />
                    </View>
                </TouchableHighlight>
                <View style={{marginTop:6, width:"70%"}}>
                    <Text style={{textAlign:"center", fontSize:15}}>
                        Space Gurú te lo pone claro:
                        Pagás por lo que almacenás, una vez al mes
                    </Text>
                </View>

            </View>
        )
    }

    renderFooter = () => {
        return (
            <View style={{width:"100%", justifyContent:"center", alignItems:"center"}}>
                <View style={{width:"100%", backgroundColor:"#F7F8F8", paddingBottom:0, paddingTop:12}}>
                    <Image source={RETIRO_PROGRAMADO} resizeMode={'contain'}  />
                </View>

                <View style={{width:"100%", height: 48, padding:0, margin:0, alignItems:"center"}}>
                    <LinearGradient colors={['#FF4378', '#FFBA5F']} start={{x: 0, y: 0}} end={{x: 1, y: 1}}
                                    style={{flex: 1, width:"100%", justifyContent:"center",
                                    borderBottomEndRadius:4}}>
                        <Text style={{textAlign:"center", color:"white", fontSize:11}}>
                            Free pickups & deliveries always available
                        </Text>
                    </LinearGradient>
                </View>

            </View>
        )
    }

    renderSpinner = () => (
        <View style={{height: "100%", alignItems: "center", justifyContent: "center"}}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    renderContentModal = () => {
        return (

                <ScrollView style={{minHeight:"100%"}}>
                    <FlatList
                        data={this.props.prices}
                        renderItem={this.renderItem}
                        keyExtractor={(_, index) => index.toString()}
                        ListHeaderComponent ={this.renderHeader}
                        ListFooterComponent={this.renderFooter}
                    />
                </ScrollView>


        )
    }


    render() {
        return (
        <SafeAreaView>
            <FadeInView >
                { this.props.loadingPrices ? this.renderSpinner() : this.renderContentModal() }
            </FadeInView>
        </SafeAreaView>
        )
    }
}

const mapStateToProps = state => ({
    prices: state.Deposit.prices,
    loadingPrices: state.Deposit.loadingPrices
})

const mapDispatchToProps = dispatch => ({
    getPrices() {
        dispatch(DepositActions.getPrices())
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(ModalPricesScreen)
