import React, { Component } from 'react'
import {View, Text, ActivityIndicator, Image, Animated, TouchableHighlight} from 'react-native'
import {CLOTHES, BICI, BOX} from "../Images";
import {Colors} from "../Layout/Colors";


const expandedDuration = 400;
const padding = 16
const heightImage = 32

class ItemModalPrice extends Component {

    constructor(props) {
        super(props)

        this.state = {
            minHeight:0,
            expandable: false,
            exampleHeight: new Animated.Value(0),
            imageHeight: new Animated.Value(heightImage)
        }
    }

    expand = () => {
        const {expandable, minHeight, titleHeight} = this.state
        this.setState({expandable: !expandable})

        const height = minHeight + titleHeight + padding

        Animated.timing(this.state.exampleHeight, {
            toValue: expandable ? 0 : height,
            duration: expandedDuration
        }).start()

        Animated.timing(this.state.imageHeight, {
            toValue: !expandable ? 0 : heightImage,
            duration: expandedDuration
        }).start()

    }

    getImage = (item) => {
        switch (item.ID % 3) {
            case 1:
                return <Image source={CLOTHES} resizeMode={'contain'} style={{ height:32, maxWidth:"70%"}} />
            case 2:
                return <Image source={BICI}  resizeMode={'contain'} style={{ height:32, maxWidth:"70%"}} />
            case 0:
                return <Image source={BOX}  resizeMode={'contain'} style={{ height:32, maxWidth:"70%"}} />
            default:
                return <View style={{height:1}}/>
        }
    }

    setHeight = (event) => {
        this.setState({minHeight: event.nativeEvent.layout.height})
    }

    setTitleHeight = (event) => {
        this.setState({titleHeight: event.nativeEvent.layout.height})
    }

    render() {
        const item = this.props.item

        return (
            <View style={{
                paddingVertical: 12,
                backgroundColor: "#F7F8F8",
                justifyContent: "center",
                alignItems: "center",
                borderBottomWidth: 0.5,
                borderColor: Colors.borderColor,
            }}>
                <TouchableHighlight
                    style={{position: "absolute", right: 12, top: 4}}
                    onPress={this.expand}
                    underlayColor='transparent'
                >
                    <Text style={{fontSize:22, color: Colors.primaryColor}}>
                        {this.state.expandable ? '-' : '+'}
                    </Text>
                </TouchableHighlight>
                <Animated.View style={{height: this.state.imageHeight}} >
                    {this.getImage(item)}
                </Animated.View>

                <View style={{width:"75%"}}>
                    <View style={{height: 40, backgroundColor: "#F7F8F8", justifyContent: "center"}}>
                        <Text style={{fontSize: 22, textAlign: "center", color: Colors.primaryColor, fontWeight: 'bold'}}>
                            ${item.Price}/mes
                        </Text>
                    </View>
                    <Animated.View style={{height: this.state.exampleHeight, paddingBottom: 8}}>
                        <Text
                            style={{fontSize: 13, textAlign: 'center', color: Colors.secondaryTextColor, fontWeight: 'bold'}}
                            onLayout={this.setTitleHeight}
                        >
                            {typeof item.Description === 'string' ? item.Description.toUpperCase() : ''}
                        </Text>
                        <Text
                            style={{fontSize: 12, textAlign: 'center', color: 'gray', marginTop: 8}}
                            onLayout={this.setHeight}
                        >
                            {item.Examples !== "" ? item.Example : "No hay descripción disponible"}
                        </Text>
                    </Animated.View>
                </View >
            </View>
        )
    }

}

export default ItemModalPrice
