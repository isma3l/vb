import React, {Component} from 'react';
import {Text, View, ActivityIndicator, FlatList, ScrollView, TouchableOpacity, Keyboard} from 'react-native'
import {Colors} from "../Layout/Colors";
import {alertSelectAddressAlert, DepositStyles, styles} from "../Styles";
import moment from "moment";
import {PrimaryHighlightButton, SecondaryButton} from "../MyComponents";
import {Calendar} from "react-native-calendars";
import ModalSelector from 'react-native-modal-selector';
import {TextField} from "react-native-material-textfield";
import {Icon} from "react-native-elements"
import Modal from "react-native-modal";
import {AddressModal} from "./Modals";
import ModalPrices from "./ModalPrices";

class SelectTimeSlotNewDesign extends Component {

    state = {
        isVisibleAddress: false,
        isVisibleStorage: false,
    }

    componentDidMount() {
        this._sub = this.props.navigation.addListener(
          'willBlur',
          () => Keyboard.dismiss()
        );
    }

    componentWillUnmount() {
        this._sub.remove();
    }

    setTimeslot() {
        const formData = new FormData();

        if (this.props.isDeposit) {
            formData.append("date", Math.round(moment(this.props.selectedDay.dateString).valueOf() / 1000))
            formData.append("time_slot_id", this.props.selectedHour);
            this.props.modifyTransaction(this.props.token, formData, this.props.transactionID);
        }

        if (this.props.isDelivery) {
            formData.append("start_date", Math.round(this.props.selectedDay / 1000))
            formData.append("time_slot_start_date", this.props.selectedHour);
            this.props.modifyTransaction(this.props.token, formData, this.props.transactionID);
        }

        if (this.props.isPickup) {
            formData.append("end_date", Math.round(this.props.selectedDay / 1000))
            formData.append("time_slot_end_date", this.props.selectedHour);
            this.props.modifyTransaction(this.props.token, formData, this.props.transactionID);
        }

        this.props.navigation.navigate(this.props.redirectRoute);
    }

    selectAddress(ID) {
        const formData = new FormData();
        formData.append("address", ID);
        this.props.modifyTransaction(this.props.token, formData, this.props.transactionID);
    }

    filterTodayTimeslot = (selectedDay, timeslot) => {
        const itemMinutes = timeslot.Hour * 60 + timeslot.Minute;

        const today = moment();
        const getCurrentMinutes = moment().hours() * 60 + moment().minutes();

        if (moment(selectedDay.dateString).isSame(today, "day")){
            if (itemMinutes <= getCurrentMinutes) {
                return false
            }
        }

        return true
    }


    formatTimeslot = (item) => {
        const hours = item.Hour < 10 ? "0" + item.Hour.toString() : item.Hour.toString();
        const minutes = item.Minute < 10 ? "0" + item.Minute.toString() : item.Minute.toString();

        return {
            key: item.ID,
            label: moment(hours + minutes, "HHmm").format("hh:mm A"),
            id: item.ID
        }
    }

    renderItem = ({item}) => {
        return (
            <TouchableOpacity
                style={{
                    flexDirection: "row",
                    padding: 10,
                    borderColor: 'gray',
                    borderBottomWidth: 1,
                    alignItems: "center",
                    height: 65
                }}
                onPress={() => {
                    this.props.setCurrentAddress(item.ID);
                }}
            >
                <View style={{width: "90%"}}>
                    <Text style={{fontSize: 15, color: Colors.subtitleLogoColor}}>
                        {item.Address ? item.Address + " " + (item.Floor ? ("Piso " + item.Floor) : "") + (item.Apartment ? (" Dpto. " + item.Apartment) : "") : "No hay dirección seleccionada"}
                    </Text>
                </View>
                {item.ID === this.props.currentAddress && <View style={{flex: 1}}><Icon name='check' color={Colors.primaryColor} size={24}/></View>}
            </TouchableOpacity>
        )
    }

    renderAlertBody = (addressList) => {
        return (
            <FlatList
                contentContainerStyle={{height: 225}}
                data={addressList}
                keyExtractor={(item) => item.ID}
                renderItem={this.renderItem}
            />
        )
    }


    renderSpinner = (styles) => (
        <View style={styles}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    renderSelectTimeslot = () => {
        const timeSlots = this.props.timeSlots.filter(timeslot => this.filterTodayTimeslot(this.props.selectedDay, timeslot))
        const data = [
            { key: -1, section: true, label: 'Horario' },
            ...timeSlots.map(timeslot => this.formatTimeslot(timeslot))
        ];

        if (data.length === 1) data.push({key: 0, section: true, label: "No hay horarios disponibles para este día"})

        const minutesZone = moment().utcOffset();

        let occupiedDates = {};

        this.props.occupiedDays.forEach(occupiedDay => {
            const date = moment(occupiedDay).subtract(minutesZone, "minutes");
            occupiedDates = {...occupiedDates, [date.format("YYYY-MM-DD")]: {disabled: true, disableTouchEvent: true}}
        })

        const clientAddress = this.props.transaction.ClientAddress;

        return (
            <ScrollView style={{minHeight: "100%"}}>
                <View style={DepositStyles.depositDescription}>
                    {this.props.isDeposit ?
                        <View>
                            <Text style={styles.titleNewDesign}>¿Cuándo podemos pasar a retirar los ítems?</Text>
                        </View>
                        :
                        <View>
                            <Text style={styles.titleNewDesign}>¡Perfecto!</Text>
                            <Text style={styles.titleNewDesign}>Estamos en tu zona</Text>
                            <View style={{marginTop: 5}}>
                                <Text style={styles.productDescriptionLabel}>Indicanos cuándo querés recibir este
                                    artículo</Text>
                            </View>
                        </View>
                    }
                </View>
                <View style={DepositStyles.addressSelector}>
                    <Text style={{color: "black", fontWeight: "bold", fontSize: 13}}>Dirección de retiro</Text>
                    <Text style={{fontSize: 15, color: Colors.subtitleLogoColor}}>{clientAddress.Address ? clientAddress.Address + " " + (clientAddress.Floor ? ("Piso " + clientAddress.Floor) : "") + (clientAddress.Apartment ? (" Dpto. " + clientAddress.Apartment) : "") : "No hay dirección seleccionada"}</Text>
                    <Icon
                        size={17}
                        name='chevron-down'
                        type='entypo'
                        color={Colors.primaryColor}
                        containerStyle={{position: "absolute", right: 15, top: 10}}
                        onPress={() => this.setState({isVisibleAddress: true})}
                    />
                </View>
                <View style={{marginTop: 9, marginBottom: 30, alignSelf: "center", width: '100%', paddingHorizontal: 16}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("AddAddress")}>
                        <Text style={{fontWeight: "bold", color: Colors.primaryColor, alignSelf: "flex-end", textAlign: 'right', fontSize: 13}}>
                            Crear nueva dirección
                        </Text>
                    </TouchableOpacity>
                </View>
                <View style={{flexDirection: "column", width: "100%", justifyContent: "center", paddingHorizontal: 4}}>
                    <Calendar
                        markedDates={{
                            [moment(this.props.selectedDay.dateString).format("YYYY-MM-DD")]: {selected: true},
                            ...occupiedDates
                        }}
                        // Initially visible month. Default = Date()
                        selectedDate={this.props.selectedDay.dateString}
                        // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                        minDate={this.props.minDate.format("YYYY-MM-DD")}
                        maxDate={this.props.maxDate.format("YYYY-MM-DD")}
                        // Handler which gets executed on day press. Default = undefined
                        onDayPress={date => {
                            const selectedDay = date.valueOf();
                            this.props.setDate({selectedDay: selectedDay, selectedHour: undefined, disableContinue: true, hourString: ""});
                            this.props.getTimeslots({date: Math.round(selectedDay.timestamp / 1000)});
                        }}
                        firstDay={0}
                        hideExtraDays={true}
                        markingType="simple"
                        theme={{
                            calendarBackground: "transparent",
                            dayTextColor: "black",
                            textDisabledColor: Colors.disableDayColor,
                            arrowColor: Colors.subtitleLogoColor,
                            selectedDayBackgroundColor: Colors.primaryColor,
                            selectedDayTextColor: '#FFF',
                            todayTextColor: Colors.primaryColor,
                            textMonthFontSize: 13,
                        }}
                    />
                </View>
                <View style={{flexDirection: "column", width: "100%", justifyContent: "center", marginVertical: 40}}>
                    <ModalSelector
                        data={data}
                        supportedOrientations={['portrait']}
                        accessible={true}
                        onChange={(option)=> {
                            this.props.setDate({selectedHour: option.id, disableContinue: false, hourString: option.label});
                        }}
                        animationType={"fade"}
                        cancelText={"Cancelar"}
                    >
                        <TextField label="Horario de retiro"
                                   inputContainerStyle={{width: "90%", height: 60, marginHorizontal: "5%"}}
                                   autoCapitalize="none"
                                   tintColor={Colors.primaryColor}
                                   lineWidth={0.5}
                                   editable={false}
                                   placeholderTextColor="grey"
                                   value={this.props.hourString}
                        />

                    </ModalSelector>
                </View>
                <View style={{justifyContent: "center", marginBottom: 20}}>
                    <View style={{marginBottom: 10}}>
                        <SecondaryButton
                            label="Costos de almacenaje"
                            onPress={() => this.setState({isVisibleStorage: true})}
                        />
                    </View>
                    <PrimaryHighlightButton
                        onPress={() => this.setTimeslot()}
                        disabled={this.props.disableContinue}
                        label="Continuar"
                    />
                </View>
            </ScrollView>
        )
    }

    hideModal = () => {
        this.setState({ isVisibleStorage: false })
    }

    render() {
        if (this.props.loading) {
            return this.renderSpinner({height: "100%", alignItems: "center", justifyContent: "center"})
        }

        return (
            <View>
                <AddressModal
                    {...this.props}
                    exitModal={() => this.setState({ isVisibleAddress: false})}
                    selectAddress={(id) => this.selectAddress(id)}
                    isVisibleAddress={this.state.isVisibleAddress}
                />
                <Modal
                    style={alertSelectAddressAlert.modalContainer}
                    isVisible={this.state.isVisibleStorage}
                    onBackdropPress={() => this.setState({ isVisibleStorage: false })}
                >
                    <View>
                        <ModalPrices hideModal={this.hideModal}/>
                    </View>
                </Modal>
                {this.renderSelectTimeslot()}
            </View>
        )
    }
}

export default SelectTimeSlotNewDesign;
