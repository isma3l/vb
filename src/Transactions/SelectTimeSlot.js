import React, { Component } from 'react';
import {Text,  View,  TouchableOpacity, ActivityIndicator, FlatList} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';
import {connect} from "react-redux";
import VerifyInformation from "../VerifyInformation/VerifyInformation";
import {Colors} from "../Layout/Colors";
import {DepositStyles, styles} from "../Styles";
import CalendarStrip from 'react-native-calendar-strip';
import esLocale from "moment/locale/es";
import moment from "moment";
import ButtonNative from "../MyComponents";
import {FadeInView} from "../AnimatedComponents";

class SelectTimeslot extends Component {
    setTimeslot() {
        const formData = new FormData();

        if (this.props.isDeposit) {
            formData.append("date", Math.round(this.props.selectedDay / 1000))
            formData.append("time_slot_id", this.props.selectedHour);
            this.props.modifyTransaction(this.props.token, formData, this.props.transactionID);
        }

        if (this.props.isDelivery) {
            formData.append("start_date", Math.round(this.props.selectedDay / 1000))
            formData.append("time_slot_start_date", this.props.selectedHour);
            this.props.modifyTransaction(this.props.token, formData, this.props.transactionID);
        }

        if (this.props.isPickup) {
            formData.append("end_date", Math.round(this.props.selectedDay / 1000))
            formData.append("time_slot_end_date", this.props.selectedHour);
            this.props.modifyTransaction(this.props.token, formData, this.props.transactionID);
        }

        this.props.navigation.navigate(this.props.redirectRoute);
    }

    getAddressDescription = (address) => {
        if (this.props.isDelivery) {
            return "¿A que hora te encontramos en " + address + "?"
        }
        if (this.props.isPickup) {
            return "¿A que hora retiramos el objeto en " + address + "?"
        }
        return "¿Cuándo lo pasamos a buscar a " + address + "?";
    }

    renderItem = ({item}) => {
        const hours = item.Hour < 10 ? "0" + item.Hour.toString() : item.Hour.toString();
        const minutes = item.Minute < 10 ? "0" + item.Minute.toString() : item.Minute.toString();
        const itemMinutes = item.Hour * 60 + item.Minute;

        const today = moment();
        const getCurrentMinutes = moment().hours() * 60 + moment().minutes();

        if (moment(this.props.selectedDay).isSame(today, "day")){
            if (itemMinutes <= getCurrentMinutes) {
                return null
            }
        }

        return (
            <ButtonNative
                styles={this.props.selectedHour === item.ID ? DepositStyles.timeSlotSelectedContainer : DepositStyles.timeSlotContainer}
                onPress={() => this.props.setDate({selectedHour: item.ID, disableContinue: false})}
            >
                <Text style={[{fontSize: 16}, this.props.selectedHour === item.ID ? {color: "white"} : {}]}>{hours}:{minutes}</Text>
            </ButtonNative>
        )
    }

    renderTimeSlots = () => {
        return (
            <View style={{backgroundColor: "white"}}>
                <FadeInView style={{height: 170, paddingTop: 5, paddingBottom: 5}}>
                    <FlatList
                        data={this.props.timeSlots}
                        numColumns={3}
                        contentContainerStyle={{justifyContent: "center", alignItems: "center"}}
                        keyExtractor={(item) => item.ID}
                        renderItem={this.renderItem}
                    />
                </FadeInView>
            </View>
        )
    }

    renderSpinner = (styles) => (
        <View style={styles}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    renderSelectTimeslot = () => {
        return (
            <View style={{minHeight: "100%"}}>
                <View style={DepositStyles.depositDescription}>
                    <Text style={{textAlign: "center", fontSize: 24}}>{this.getAddressDescription(this.props.transaction.ClientAddress.Address)}</Text>
                    {!this.props.isPickup &&
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('AddressList', {
                        transactionID: this.props.transactionID,
                        modifyTransaction: this.props.modifyTransaction
                    })}>
                        <View style={{alignSelf: "center"}}>
                            <Text style={[styles.forgetPasswordText, {fontWeight: "bold", textDecorationLine: "none"}]}>Cambiar
                                Dirección</Text>
                        </View>
                    </TouchableOpacity>
                    }
                </View>
                <CalendarStrip
                    locale={{name: 'es', config: esLocale}}
                    daySelectionAnimation={{type: 'border', duration: 200, borderHighlightColor: Colors.primaryColor}}
                    style={{height: 100, padding: 0, justifyContent: "center"}}
                    calendarHeaderStyle={{color: 'black'}}
                    calendarColor={"white"}
                    weekendDateNumberStyle={{minWidth: 36, minHeight: 36, lineHeight: 28}}
                    dateNumberStyle={{color: 'black', minWidth: 36, minHeight: 36, lineHeight: 28}}
                    dateNameStyle={{color: 'black'}}
                    onDateSelected={date => {
                        const selectedDay = date.valueOf();
                        this.props.setDate({selectedDay: selectedDay, selectedHour: undefined, disableContinue: true});
                        this.props.getTimeslots(this.props.token, {date: Math.round(selectedDay / 1000)});
                    }}
                    highlightDateNumberStyle={{flex: 1, lineHeight: 28, color: "white", backgroundColor: Colors.primaryColor, borderRadius: 18, minWidth: 36, minHeight: 36}}
                    highlightDateNameStyle={{color: Colors.primaryColor}}
                    minDate={this.props.minDate}
                    maxDate={this.props.maxDate}
                    datesWhitelist={[{start: this.props.minDate, end: this.props.maxDate}]}
                    datesBlacklist={this.props.occupiedDays.map(day => moment(day.substring(0, 10)))}
                    disabledDateNameStyle={{color: Colors.disableColor}}
                    disabledDateNumberStyle={{color: Colors.disableColor, minWidth: 36, minHeight: 36, lineHeight: 28}}
                    iconContainer={{flex: 0.1}}
                    selectedDate={moment(this.props.selectedDay)}
                    updateWeek={false}
                    showMonth={false}
                />
                {this.props.loadingTimeSlots ? this.renderSpinner([styles.container, {height: 170, backgroundColor: "white", justifyContent: "center", alignItems: "center"}]) : this.renderTimeSlots()}
                <View style={{backgroundColor: "white", flex: 1, justifyContent: "center"}}>
                    <ButtonNative
                        onPress={() => this.setTimeslot()}
                        styles={this.props.disableContinue ? styles.continueDisabledButton : styles.continueButton}
                        disabled={this.props.disableContinue}
                    >
                        <Text style={{color: "white", fontWeight: "bold"}}>Continuar</Text>
                    </ButtonNative>
                </View>
            </View>
        )
    }

    render() {
        if (this.props.loading) {
            return this.renderSpinner({height: "100%", alignItems: "center", justifyContent: "center"})
        }
            return this.renderSelectTimeslot()
    }
}

export default SelectTimeslot;