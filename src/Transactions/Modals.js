import React, { Component } from 'react';
import {
    Text, View, Image, ActivityIndicator, FlatList, ScrollView, Picker, TouchableOpacity,
    StyleSheet
} from 'react-native'
import {Colors} from "../Layout/Colors";
import {alertSelectAddressAlert} from "../Styles";
import {Icon, ListItem} from "react-native-elements"
import Modal from "react-native-modal";

export class AddressModal extends Component {
    renderItem = ({item}) => {
        return (
            <TouchableOpacity
                style={{
                    flexDirection: "row",
                    padding: 10,
                    borderColor: '#D4D8E0',
                    borderBottomWidth: 1,
                    alignItems: "center",
                    height: 65
                }}
                onPress={() => {
                    this.props.setCurrentAddress(item.ID);
                }}
            >
                <View style={{width: "90%"}}>
                    <Text style={{fontSize: 15, color: Colors.subtitleLogoColor}}>
                        {item.Address ? item.Address + " " + (item.Floor ? ("Piso " + item.Floor) : "") + (item.Apartment ? (" Dpto. " + item.Apartment) : "") : "No hay dirección seleccionada"}
                    </Text>
                </View>
                {item.ID === this.props.currentAddress && <View style={{flex: 1}}><Icon name='check' color={Colors.primaryColor} size={24}/></View>}
            </TouchableOpacity>
        )
    }

    renderAlertBody = (addressList) => {
        return (
            <View style={{height: 225}}>
                <FlatList
                    data={addressList}
                    keyExtractor={(item) => item.ID}
                    renderItem={this.renderItem}
                />
            </View>
        )
    }

    render() {
        const addressList = this.props.addresses.filter(address => address.InDeliveryRange);

        return (
            <Modal
                style={alertSelectAddressAlert.modalContainer}
                isVisible={this.props.isVisibleAddress}
                onBackdropPress={() => this.props.exitModal()}
            >
                <View style={alertSelectAddressAlert.modalView}>
                    <View style={alertSelectAddressAlert.titleContainer}>
                        <Text style={alertSelectAddressAlert.titleText}>Selecciona una dirección</Text>
                    </View>
                    <View>
                        {this.renderAlertBody(addressList)}
                    </View>
                    <View style={alertSelectAddressAlert.buttonContainer}>
                        <TouchableOpacity
                            style={[alertSelectAddressAlert.button, {borderRightWidth: 1, borderColor: '#D4D8E0'}]}
                            onPress={() => {
                                this.props.exitModal();
                                this.props.setCurrentAddress(this.props.transaction.ClientAddress.ID);
                            }}
                        >
                            <Text style={[alertSelectAddressAlert.buttonText, {borderRightWidth: 1, borderColor: '#D4D8E0'}]}>Cancelar</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={alertSelectAddressAlert.button}
                            onPress={() => {
                                this.props.exitModal();
                                this.props.selectAddress(this.props.currentAddress);
                            }}
                        >
                            <Text style={alertSelectAddressAlert.buttonText}>Continuar</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        )
    }
}

export class CreditCardModal extends Component {
    renderItem = ({item}) => {
        return (
            <TouchableOpacity
                style={{
                    padding: 10,
                    borderColor: '#D4D8E0',
                    borderBottomWidth: 1,
                    height: 65
                }}
                onPress={() => {
                    this.props.setCurrentCard(item.ID);
                }}
            >
                <View style={{flexDirection: "row", alignItems: "center", flex: 1}}>
                    <View style={{width: 60, paddingLeft: 0, padding: 10}}>
                        <Image
                            source={{uri: item.TypePhoto.replace('http', 'https')}}
                            style={{width: 50, height: 45}}
                            resizeMode={"contain"}
                        />
                    </View>
                    <View>
                        <Text style={{fontSize: 15, color: Colors.secondaryTextColor}}>
                            •••• •••• •••• {item.LastDigits}
                        </Text>
                        <Text style={{fontSize: 15, color: Colors.secondaryTextColor}}>
                            Vto: {item.Expiration}
                        </Text>
                        {item.Main &&
                        <View>
                            <Text style={{fontWeight: "bold", marginTop: 10, fontSize: 13, marginBottom:4}}>Tarjeta Primaria</Text>
                        </View>
                        }
                    </View>
                    {item.ID === this.props.currentCard && <View style={{position: "absolute", right: 10, marginVertical: "auto"}}><Icon name='check' color={Colors.primaryColor} size={24}/></View>}
                </View>
            </TouchableOpacity>
        )
    }

    renderAlertBody = (cards) => {
        return (
            <FlatList
                contentContainerStyle={{paddingBottom: 40}}
                data={cards}
                keyExtractor={(item) => item.ID}
                renderItem={this.renderItem}
                ListFooterComponent={
                    <TouchableOpacity
                        style={{position: "absolute", right: 10, top: 10}}
                        onPress={() => {
                            this.props.exitModal();
                            this.props.navigation.navigate("AddCreditCard");
                        }}
                    >
                        <Text style={{color: Colors.primaryColor, fontWeight: "bold"}}>Agregar tarjeta</Text>
                    </TouchableOpacity>
                }
            />
        )
    }

    render() {
        return (
            <Modal
                style={alertSelectAddressAlert.modalContainer}
                isVisible={this.props.isVisibleCard}
                onBackdropPress={() => this.props.exitModal()}
            >
                <View style={alertSelectAddressAlert.modalView}>
                    <View style={alertSelectAddressAlert.titleContainer}>
                        <Text style={alertSelectAddressAlert.titleText}>Selecciona una tarjeta</Text>
                    </View>
                    <View style={{height: 225}}>
                        {this.renderAlertBody(this.props.cards)}
                    </View>
                    <View style={alertSelectAddressAlert.buttonContainer}>
                        <TouchableOpacity
                            style={[alertSelectAddressAlert.button, {borderRightWidth: 1, borderColor: Colors.borderColor}]}
                            onPress={() => {
                                this.props.exitModal();
                                this.props.setCurrentCard(this.props.transaction.Card.ID);
                            }}
                        >
                            <Text style={[alertSelectAddressAlert.buttonText, {borderRightWidth: 1, borderColor: Colors.borderColor}]}>Cancelar</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={alertSelectAddressAlert.button}
                            onPress={() => {
                                this.props.exitModal();
                                this.props.selectCard(this.props.currentCard);
                            }}
                        >
                            <Text style={alertSelectAddressAlert.buttonText}>Continuar</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        )
    }
}
