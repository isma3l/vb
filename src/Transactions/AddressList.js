import React, { Component } from 'react';
import {Text, View, ActivityIndicator, ScrollView} from 'react-native'
import {NavigationActions} from "react-navigation";
import {List, ListItem, Avatar} from "react-native-elements";
import {styles} from "../Styles";
import {Colors} from "../Layout/Colors";
import LoginActions from "../Login/Login.actions";
import {connect} from "react-redux";
import ProfileActions from "../Profile/Profile.actions";
import ButtonNative from "../MyComponents";
import {FadeInView} from "../AnimatedComponents";

class AddressList extends Component {
    componentDidMount(){
        this.props.setLoading(true);
        this.props.getProfile(this.props.token);
    }

    selectAddress(ID) {
        const formData = new FormData();
        formData.append("address", ID);
        this.props.navigation.dispatch(NavigationActions.back());
        this.props.modifyTransaction(this.props.token, formData, this.props.transactionID);
    }

    renderAddresses(addresses){
        const addressView =  addresses.map((address, i) => {
            return (
                <View key={address.ID} style={{borderBottomWidth: 1, borderBottomColor: Colors.borderColor}}>
                    <ListItem
                        title={"Domicilio " + (i + 1)}
                        component={ButtonNative}
                        wrapperStyle={{paddingTop: 10, paddingBottom: 10, margin: 0}}
                        onPress={() => this.selectAddress(address.ID)}
                        subtitle={address.Address + " " + (address.Floor ? ("Piso " + address.Floor) : "") + (address.Apartment ? (" Dpto. " + address.Apartment) : "")}
                        leftIcon={{name: "home", color: Colors.primaryColor}}
                        hideChevron={true}
                    />
                </View>

            )
        })
        addressView.push(
            <View key={addresses.length} style={{borderBottomWidth: 1, borderBottomColor: Colors.borderColor}}>
                <ListItem
                    title={"Añadir nueva dirección"}
                    component={ButtonNative}
                    containerStyle={{width: "100%"}}
                    wrapperStyle={{paddingTop: 15, paddingBottom: 15, margin: 0}}
                    leftIcon={{name: "add", color: Colors.primaryColor, style: {marginLeft: 0}}}
                    onPress={() => {
                        this.props.setModifyAddress(0, {PostalCode: {}});
                        this.props.navigation.navigate("Address", {redirectRoute: "AddressList"});
                    }}
                    hideChevron={true}
                />
            </View>
        )
        return (
            <FadeInView style={{backgroundColor: "white"}}>
                <ScrollView>
                    {addressView}
                </ScrollView>
            </FadeInView>
        )
    }

    renderSpinner = () => (
        <View style={styles.centerContainer}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    render() {
        return this.props.loading ? this.renderSpinner() : this.renderAddresses(this.props.profile.profile.Addresses)
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        loading: state.Login.loading,
        token: state.Login.token,
        profile: state.Profile,
        transactionID: ownProps.navigation.state.params.transactionID,
        modifyTransaction: ownProps.navigation.state.params.modifyTransaction,
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        setLoading(boolean){
            dispatch(LoginActions.loading(boolean))
        },
        getProfile(token) {
            dispatch(ProfileActions.getProfile(token))
        },
        setModifyAddress(addressAction, address) {
            dispatch(ProfileActions.setModifyAddress(addressAction, address))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddressList);