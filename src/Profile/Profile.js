import React, { Component } from 'react';
import {Text, View, ActivityIndicator, TouchableOpacity, Alert, ScrollView, Platform, FlatList} from 'react-native'
import IconOcticons from "react-native-vector-icons/Octicons";
import {Avatar} from "react-native-elements";
import {styles} from "../Styles";
import {Colors} from "../Layout/Colors";
import LoginActions from "../Login/Login.actions";
import {connect} from "react-redux";
import ProfileActions from "../Profile/Profile.actions";
import {FadeInView} from "../AnimatedComponents";
import CategoryActions from "../Categories/Categories.actions";
import KeyboardView from "../KeyboardView";
import {TextField} from "react-native-material-textfield";
import ImagePicker from "react-native-image-picker"
import {ProfileStyles} from '../Styles'
import {AccountOptionsModal} from './Modals'

// More info on all the options is below in the README...just some common use cases shown here
const options = {
  title: 'Elegir foto',
  maxWidth: 1000,
  maxHeight: 1000,
  cancelButtonTitle: "Cancelar",
  takePhotoButtonTitle: "Tomar foto",
  allowsEditing: true,
  chooseFromLibraryButtonTitle: "Escoger foto de la galería",
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
};

const labels = {
  SAVE: "Guardar",
  FLOOR: "Piso",
  APARTMENT: "Dpto",
  MAIN_ADDRESS: "Dirección primaria",
  NAME: "Nombre",
  LAST_NAME: "Apellido",
  EMAIL: "Email",
  MODIFIED_PASSWORD: "Modificar contraseña",
  MY_ADDRESS: "Mis direcciones",
  CREATE_ADDRESS: "Crear nueva dirección",
  PHONE_NUMBER: "Número de teléfono"
}

class Profile extends Component {

  state = {
    visibleAccountModal: false,
    selectedAddress: null
  }


  static navigationOptions = ({ navigation }) => {
    const params = navigation.state.params || {};

    return {
      headerRight:
        (<FadeInView>
            <TouchableOpacity style={{marginRight:16}} onPress={params.saveChanges}>
              <Text style={{color:Colors.fbButtonColor}}>{labels.SAVE}</Text>
            </TouchableOpacity>
          </FadeInView>
        )
    }
  }

  componentDidMount() {
    this.props.setLoading(true);
    this.props.getProfile();

    this.props.navigation.setParams({ saveChanges: this.saveChanges });
  }

  saveChanges = () => {
    const editedName = this.props.nameAvatar.Name
    const editedLastName = this.props.nameAvatar.LastName
    const {Name, LastName} = this.props.profile.profile

    let formData = null
    let formDataAvatar = null

    if(editedName !== Name || editedLastName !== LastName) {
      formData = new FormData();
      formData.append("first_name", editedName);
      formData.append("last_name", editedLastName);
    }

    const avatarAction = this.props.profile.avatarAction;
    if(avatarAction === 1) { // 1: modified avatar
      const {uri, type, fileName} = this.props.profile.newAvatar;
      let name = fileName ? fileName : "photo" + Math.floor(Math.random() * Math.floor(999999)) + ".jpg"

      formDataAvatar = new FormData();
      formDataAvatar.append("profile_picture", { uri, type, name });
    }

    if(formData !== null || formDataAvatar !== null) {
      this.props.setLoading(true);
      this.props.saveAvatarAndName(this.props.token, formData, formDataAvatar, avatarAction)
    }
  }

  editAvatar = () => {
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else {
        this.props.setAvatar(response);
      }
    });
  }

  renderAddress = ({item, index}) => {
    const lastItem = index + 1 === this.props.profile.profile.Addresses.length
    const borderBottomWidth = lastItem === false ? 0.5 : 0
    const floor = item.Floor ? `${labels.FLOOR} ${item.Floor}` : ""
    const apartment = item.Apartment ? `${labels.APARTMENT} ${item.Apartment}` : ""
    const address = `${item.Address} ${floor} ${apartment}`

    return (
      <View style={[ProfileStyles.containerItemAddress, {borderBottomWidth: borderBottomWidth}]}>
        <Text style={ProfileStyles.titleItemAddress}>{address}</Text>

        { item.MainAddress ? <Text style={ProfileStyles.mainAddress}>{labels.MAIN_ADDRESS}</Text> : null }

        <TouchableOpacity style={ProfileStyles.iconItemAddress}
                          onPress={() => { this.setState({visibleAccountModal: true, selectedAddress: item}) }}>
          <IconOcticons
            name="kebab-vertical"
            size={18}
            color={Colors.primaryColor}
          />
        </TouchableOpacity>

      </View>
    )
  }

  renderSpinner = () => (
    <View style={styles.centerContainer}>
      <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
    </View>
  )

  getTitle = () => {
    const profile = this.props.nameAvatar;
    let title = "";
    if (profile) {
      if (profile.Name !== "") {
        title = title + profile.Name[0].toUpperCase();
      }
      if (profile.LastName !== "") {
        title = title + profile.LastName[0].toUpperCase();
      }
    }
    if (title === "") title = "VB";
    return title
  }




  renderProfile = (profile) => {
    const profilePicture = this.props.nameAvatar.ProfilePicture

    return (
      <FadeInView style={{flex: 1}}>
        <KeyboardView style={{flex: 1, alignItems: "center"}}>
          <ScrollView style={{flex: 1, width:"100%"}}>
            <TouchableOpacity style={ProfileStyles.containerAvatar}
                              onPress={() => this.editAvatar()}>
              {profilePicture !== "" ?
                <Avatar width={120} height={120} source={{uri: profilePicture}} rounded /> :
                <Avatar width={120} height={120} title={this.getTitle()} rounded />
              }
            </TouchableOpacity>

            <AccountOptionsModal
              visibleAccountModal={this.state.visibleAccountModal}
              closeAccountModal={() => {this.setState({visibleAccountModal: false})}}
              markMainAddress={()=> {
                this.props.markMainAddress(this.props.token, this.state.selectedAddress.ID)
                this.setState({visibleAccountModal: false})
              }}
              modifyAddress={() => {
                this.props.setModifyAddress(1, this.state.selectedAddress);
                this.props.navigation.navigate('Address');
                this.setState({visibleAccountModal: false})
              }}
              deleteAddress={() => {
                this.props.setLoading(true);
                this.props.deleteAddress(this.props.token, this.state.selectedAddress.ID);
                this.setState({visibleAccountModal: false})
              }}
            />

            <View style={ProfileStyles.container}>
              <TextField label="Nombre"
                         inputContainerStyle={ProfileStyles.textField}
                         autoCapitalize="none"
                         tintColor={Colors.primaryColor}
                         keyboardType="default"
                         lineWidth={0.5}
                         placeholderTextColor="grey"
                         value={this.props.nameAvatar.Name}
                         onChangeText={text => this.props.setName({Name: text})}
                         autoCorrect={false}
              />
              <TextField label={labels.LAST_NAME}
                         inputContainerStyle={ProfileStyles.textField}
                         autoCapitalize="none"
                         tintColor={Colors.primaryColor}
                         keyboardType="default"
                         lineWidth={0.5}
                         placeholderTextColor="grey"
                         value={this.props.nameAvatar.LastName}
                         onChangeText={text => this.props.setName({LastName: text})}
                         autoCorrect={false}
              />

              <TouchableOpacity onPress={() => this.props.navigation.navigate("ModifyEmail")}>
                <TextField label={labels.EMAIL}
                           inputContainerStyle={ProfileStyles.textField}
                           autoCapitalize="none"
                           tintColor={Colors.primaryColor}
                           keyboardType="email-address"
                           lineWidth={0.5}
                           placeholderTextColor="grey"
                           defaultValue={profile.Email}
                           disabled={true}
                           autoCorrect={false}
                />
              </TouchableOpacity>

              <TouchableOpacity     onPress={() => { this.props.navigation.navigate("ChangePassword") }}
                                    style={{width:"50%"}}>
                <View style={ProfileStyles.modifiedPassword}>
                  <Text style={{color:Colors.fbButtonColor}}>{labels.MODIFIED_PASSWORD}</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => this.props.navigation.navigate("VerifyYourPhone")}>
                <TextField label={labels.PHONE_NUMBER}
                           inputContainerStyle={ProfileStyles.textField}
                           autoCapitalize="none"
                           tintColor={Colors.primaryColor}
                           keyboardType="numeric"
                           lineWidth={0.5}
                           placeholderTextColor="grey"
                           defaultValue={profile.Phone}
                           disabled={true}
                           autoCorrect={false}
                />
              </TouchableOpacity>

              <View style={ProfileStyles.containerAddress}>
                <Text style={ProfileStyles.titleAddress}>{labels.MY_ADDRESS}</Text>

                <FlatList
                  style={ProfileStyles.addresses}
                  data={profile.Addresses}
                  keyExtractor={ item => item.ID.toString() }
                  renderItem={this.renderAddress}
                />
              </View>

              <View style={ProfileStyles.linkNewAddress}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.setModifyAddress(0, {PostalCode: {}});
                    this.props.navigation.navigate("Address");
                  }}
                  style={ProfileStyles.containerLinkNewAddress}>
                  <View style={ProfileStyles.subContainerLinkNewAddress}>
                    <Text style={{color:Colors.fbButtonColor}}>Crear nueva dirección</Text>
                  </View>
                </TouchableOpacity>
              </View>

            </View>


          </ScrollView>
        </KeyboardView>
      </FadeInView>
    )
  }

  render() {
    return this.props.loading ? this.renderSpinner() : this.renderProfile(this.props.profile.profile)
  }
}

const mapStateToProps = state => {
  return {
    loading: state.Login.loading,
    token: state.Login.token,
    profile: state.Profile,
    nameAvatar: state.Profile.NameAvatar
  }
};

const mapDispatchToProps = dispatch =>{
  return{
    setLoading(boolean){
      dispatch(LoginActions.loading(boolean))
    },
    getProfile(token) {
      dispatch(ProfileActions.getProfile(token))
    },
    setName(name){
      dispatch(ProfileActions.setName(name))
    },
    setAvatar(avatar){
      dispatch(ProfileActions.setAvatar(avatar))
    },
    getNameAndAvatar(data) {
      dispatch(ProfileActions.getNameAndAvatar(data))
    },
    setModifyAddress(addressAction, address) {
      dispatch(ProfileActions.setModifyAddress(addressAction, address))
    },
    deleteAddress(token, id) {
      dispatch(ProfileActions.deleteAddress(token, id))
    },
    markMainAddress(token, id) {
      dispatch(LoginActions.loading(true))
      dispatch(ProfileActions.markMainAddress(token, id))
    },
    logOut() {
      dispatch({type: "REMOVE_TOKEN"})
      dispatch(CategoryActions.cleanCategories())
    },
    saveAvatarAndName(token, formData, formData2, actionAvatar){
      dispatch(ProfileActions.saveAvatarAndName(token, formData, formData2, actionAvatar))
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);

