import React, { Component } from 'react';
import {Text, TouchableNativeFeedback, View, ActivityIndicator} from 'react-native';
import {NavigationActions} from "react-navigation";
import Icon from 'react-native-vector-icons/MaterialIcons';
import {List, ListItem, FormLabel, FormInput, FormValidationMessage, Avatar} from "react-native-elements";
import {EditAvatar, styles} from "../Styles";
import {Colors} from "../Layout/Colors";
import LoginActions from "../Login/Login.actions";
import ProfileActions from "./Profile.actions";
import {connect} from "react-redux";
import ImagePicker from 'react-native-image-picker';
import ButtonNative from "../MyComponents";
import KeyboardView from "../KeyboardView";

// More info on all the options is below in the README...just some common use cases shown here
const options = {
    title: 'Elegir foto',
    maxWidth: 1000,
    maxHeight: 1000,
    cancelButtonTitle: "Cancelar",
    takePhotoButtonTitle: "Tomar foto",
    allowsEditing: true,
    chooseFromLibraryButtonTitle: "Escoger foto de la galería",
    storageOptions: {
        skipBackup: true,
        path: 'images'
    }
};

class ModifyAvatar extends Component {
    static navigationOptions = {
        headerTitle: 'Editar Perfil',
        drawerIcon: ({ tintColor }) => (
            <Icon name="person" size={24}/>
        )
    };

    changePhoto = () => {
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else {
                let source = { uri: response.uri };
                this.props.setAvatar(response);
                this.setState({
                    avatarSource: source
                });
            }
        });
    }

    getTitle = () => {
        const profile = this.props.nameAvatar;
        let title = "";
        if (profile) {
            if (profile.name !== "") {
                title = title + profile.name[0].toUpperCase();
            }
            if (profile.lastName !== "") {
                title = title + profile.lastName[0].toUpperCase();
            }
        }
        if (title === "") title = "VB";
        return title
    }

    saveChanges = () => {
        const newAvatar = this.props.profile.newAvatar;
        const nameAvatar = this.props.nameAvatar;
        const avatarAction = this.props.profile.avatarAction;
        const formData = new FormData();
        let formData2;
        formData.append("first_name", nameAvatar.name);
        formData.append("last_name", nameAvatar.lastName);
        formData.append("email", this.props.profile.profile.Email.Email);

        switch (avatarAction) {
            case 1:
                formData2 = new FormData();
                formData2.append("profile_picture", {
                    uri: newAvatar.uri,
                    type: newAvatar.type,
                    name: newAvatar.fileName ? newAvatar.fileName : "photo" + Math.floor(Math.random() * Math.floor(999999)) + ".jpg"
                });
                break;
            default:
                formData2 = null;
                break;
        }
        this.props.setLoading(true);
        this.props.saveAvatarAndName(this.props.token, formData, formData2, avatarAction)
    }

    renderSpinner = () => (
        <View style={styles.centerContainer}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    renderProfile = () => (
        <KeyboardView style={{flex: 1, alignItems: "center"}}>
            <View style={styles.defaultContainer}>
                <FormLabel>Avatar</FormLabel>
                <View style={{flexDirection: "row", marginTop: 10}}>
                    <View style={{flex: 1, alignItems: "center", justifyContent: "center"}}>
                        {this.props.nameAvatar.profilePicture !== "" ?
                            <Avatar rounded width={100} height={100} source={{uri: this.props.nameAvatar.profilePicture}}/> :
                            <Avatar rounded width={100} height={100} title={this.getTitle()} />
                        }

                    </View>
                    <View style={{flex: 1, alignItems: "center", justifyContent: "center"}}>
                        <ButtonNative
                            onPress={() => this.changePhoto()}
                            styles={EditAvatar.changePhoto}
                        >
                            <Text style={{color: Colors.loginColor}}>Elegir Foto</Text>
                        </ButtonNative>
                        <ButtonNative
                            onPress={() => this.props.deleteAvatar()}
                            styles={EditAvatar.deletePhoto}
                        >
                            <Text style={{color: Colors.primaryColor}}>Eliminar Foto</Text>
                        </ButtonNative>
                    </View>
                </View>
                <FormLabel>Nombre</FormLabel>
                <FormInput
                    placeholder={"Ingrese su nombre"}
                    inputStyle={styles.colorInputTextField}
                    underlineColorAndroid={Colors.primaryColor}
                    onChangeText={text => this.props.setName({name: text})}
                    defaultValue={this.props.nameAvatar.name}
                />
                <FormLabel>Apellido</FormLabel>
                <FormInput
                    placeholder={"Ingrese su apellido"}
                    inputStyle={styles.colorInputTextField}
                    underlineColorAndroid={Colors.primaryColor}
                    onChangeText={text => this.props.setName({lastName: text})}
                    defaultValue={this.props.nameAvatar.lastName}
                />
                <ButtonNative
                    onPress={() => this.saveChanges()}
                    styles={EditAvatar.confirmChanges}
                >
                    <Text style={{color: "white"}}>Guardar cambios</Text>
                </ButtonNative>
                <ButtonNative
                    onPress={() => this.props.navigation.dispatch(NavigationActions.back())}
                    styles={EditAvatar.discardChanges}
                >
                    <Text style={{color: Colors.primaryColor}}>Descartar cambios</Text>
                </ButtonNative>
            </View>
        </KeyboardView>
    )

    render() {
        return this.props.loading ? this.renderSpinner() : this.renderProfile()
    }
}

const mapStateToProps = state => {
    return {
        loading: state.Login.loading,
        nameAvatar: state.Profile.NameAvatar,
        token: state.Login.token,
        profile: state.Profile
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        setLoading(boolean){
            dispatch(LoginActions.loading(boolean))
        },
        setAvatar(avatar){
            dispatch(ProfileActions.setAvatar(avatar))
        },
        deleteAvatar(){
            dispatch(ProfileActions.deleteAvatar())
        },
        setName(name){
            dispatch(ProfileActions.setName(name))
        },
            saveAvatarAndName(token, formData, formData2, actionAvatar){
            dispatch(ProfileActions.saveAvatarAndName(token, formData, formData2, actionAvatar))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ModifyAvatar);
