import React, { Component } from 'react';
import {Platform, Text, View, ActivityIndicator} from 'react-native'
import Actions from "../Login/Login.actions";
import {connect} from "react-redux";
import {Colors} from "../Layout/Colors";
import {styles, VerifyPhoneStyles} from "../Styles";
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import ProfileActions from "../Profile/Profile.actions";
import ButtonNative from "../MyComponents";
import {FormInput} from "react-native-elements";
import KeyboardView from "../KeyboardView";
import {GOOGLEAPIKEY} from "../Keys";

class Address extends Component {

    componentDidMount() {
        this.props.setLoading(false);
    }

    addAddress = () => {
        const address = this.props.address;
        const formData = new FormData();
        formData.append("address", address.address);
        if (address.floor) formData.append("floor", address.floor);
        if (address.apartment) formData.append("apartment", address.apartment);
        formData.append("postal_code", address.postalCode);
        this.props.setLoading(true);
        if (this.props.addressAction === 1) {
            this.props.modifyAddress(this.props.token, formData, address.id);
        } else {
            if (this.props.navigation.state.params) {
                this.props.addAddress(this.props.token, formData, this.props.navigation.state.params.redirectRoute);
            }
            else {
                this.props.addAddress(this.props.token, formData);
            }
        }
    }

    renderSpinner = () => (
        <View style={styles.centerContainer}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    GooglePlacesInput = () => {
        return (
            <GooglePlacesAutocomplete
                placeholder='Domicilio'
                minLength={2} // minimum length of text to search
                autoFocus={false}
                returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                listViewDisplayed='false'    // true/false/undefined
                fetchDetails={true}
                renderDescription={(row) => row.description || row.vicinity} // custom description render
                onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                    let address = details.formatted_address;
                    let postalCode = "";
                    details.address_components.forEach(comp => {
                        comp.types.forEach(type => {
                            if (type === "postal_code") postalCode = comp.long_name.substring(0, 5);
                        })
                    })
                    this.props.setAddress({address, postalCode})
                }}

                getDefaultValue={() => {
                    if (this.props.address.address)
                        return this.props.address.address;
                    else
                        return ""
                }}

                query={{
                    // available options: https://developers.google.com/places/web-service/autocomplete
                    key: GOOGLEAPIKEY,
                    language: 'es', // language of the results
                    types: 'address', // default: 'geocode'
                    components: "country:ar"
                }}

                placeholderTextColor="grey"
                underlineColorAndroid={Colors.primaryColor}
                autoCapitalize="none"

                styles={{
                    container: {
                        height: 50
                    },
                    textInputContainer: {
                        backgroundColor: 'rgba(0,0,0,0)',
                        borderTopWidth: 0,
                        borderBottomWidth: 0,
                        padding: 0,
                        marginLeft: 0,
                        flex: 1,
                    },
                    textInput: {
                        marginLeft: 0,
                        marginRight: 0,
                        marginBottom: 0,
                        height: Platform.OS === "ios" ? 40 : 50,
                        color: 'black',
                        borderBottomWidth: Platform.OS === "ios" ? 1 : 0,
                        fontSize: 14,
                        padding: 0,
                    },
                    description: {
                        fontWeight: 'bold',
                    },
                    predefinedPlacesDescription: {
                        color: '#1faadb'
                    },
                    listView: {
                        zIndex: 999,
                        position: "absolute",
                        top: 50,
                        backgroundColor: "white",
                        elevation: 3,
                        height: 120
                    }
                }}

                nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch

//                filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities

                debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
                //renderLeftButton={()  => <Image source={require('path/custom/left-icon')} />}
            />
        );
    }

    render() {
        return (
            <View
                style={{height: "100%", width: "100%"}}
            >
                <View style={styles.container}>
                    {this.props.loading ? this.renderSpinner() : this.renderAddress()}
                </View>
            </View>
        )
    }

    renderAddress = () => (
        <KeyboardView style={styles.centerContainer}>
            <View style={VerifyPhoneStyles.descriptionContainer}>
                <Text style={styles.subtitle}>Completa tu domicilio</Text>
                <View style={{height: 50, zIndex: Platform.OS === "ios"  ? 1 : undefined}}>
                    {this.GooglePlacesInput()}
                </View>
                <View style={styles.floorContainer}>
                    <FormInput
                        placeholder="Piso"
                        underlineColorAndroid={Colors.primaryColor}
                        inputStyle={styles.colorInputTextField}
                        containerStyle={{flexGrow: 1, flexBasis: 0, alignSelf: "flex-start"}}
                        placeholderTextColor="grey"
                        autoCapitalize="none"
                        defaultValue={this.props.address.floor}
                        onChangeText={text => this.props.setAddress({floor: text})}
                    />
                    <FormInput
                        placeholder="Apartamento"
                        underlineColorAndroid={Colors.primaryColor}
                        containerStyle={{flexGrow: 1, flexBasis: 0, alignSelf: "flex-start"}}
                        inputStyle={styles.colorInputTextField}
                        placeholderTextColor="grey"
                        autoCapitalize="none"
                        defaultValue={this.props.address.apartment}
                        onChangeText={text => this.props.setAddress({apartment: text})}
                    />
                </View>
                <ButtonNative
                    onPress={() => this.addAddress()}
                    styles={styles.buttonStyle}
                >
                    <Text style={{color: "white", fontWeight: "bold"}}>Finalizar</Text>
                </ButtonNative>
            </View>
        </KeyboardView>
    )
}

const mapStateToProps = state => {
    return {
        loading: state.Login.loading,
        missingInfo: state.Login.missingInformation,
        token: state.Login.token,
        address: state.Profile.address,
        addressAction: state.Profile.addressAction,
        redirectRoute: state.VerifyInformation.route
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        setLoading(boolean){
            dispatch(Actions.loading(boolean))
        },
        setAddress(address) {
            dispatch(ProfileActions.setAddress(address))
        },
        addAddress(token, formData, redirectRoute) {
            dispatch(ProfileActions.addAddress(token, formData, redirectRoute))
        },
        modifyAddress(token, formData, id) {
            dispatch(ProfileActions.modifyAddress(token, formData, id))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Address)

