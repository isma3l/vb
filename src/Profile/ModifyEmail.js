import React, { Component } from 'react';
import {TouchableNativeFeedback, Text, View, TouchableOpacity, TextInput, ActivityIndicator} from 'react-native'
import Actions from "../Login/Login.actions";
import ProfileActions from "../Profile/Profile.actions";
import {connect} from "react-redux";
import {Colors} from "../Layout/Colors";
import {styles, VerifyPhoneStyles} from "../Styles";
import {Logo} from "../LogoScreen/Logo";
import ButtonNative from "../MyComponents";
import {FormInput} from "react-native-elements";
import KeyboardView from "../KeyboardView";

class ModifyEmail extends Component {
    componentDidMount() {
        this.props.setLoading(false);
    }

    renderSpinner = () => (
        <View style={styles.centerContainer}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    renderModifyEmail = () => (
        <View style={styles.centerContainer}>
            <Logo/>
            <View style={VerifyPhoneStyles.descriptionContainer}>
                <Text style={styles.subtitle}>Modificar Email</Text>
                <Text>Ingrese aqui el nuevo email al que quiere asociar su cuenta</Text>
            </View>
            <View style={VerifyPhoneStyles.verifyContainer}>
                <FormInput placeholder="Email"
                           containerStyle={VerifyPhoneStyles.code}
                           inputStyle={styles.colorInputTextField}
                           keyboardType="email-address"
                           underlineColorAndroid={Colors.primaryColor}
                           placeholderTextColor="grey"
                           autoCapitalize="none"
                           onChangeText={text => this.props.setEmail(text)}/>
                <ButtonNative
                    onPress={() => this.modifyEmail()}
                    styles={styles.buttonStyle}
                >
                    <Text style={{color: "white", fontWeight: "bold"}}>Siguiente</Text>
                </ButtonNative>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('VerifyEmail')}>
                    <View>
                        <Text style={styles.forgetPasswordText}>Ya tenes un código?</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <TouchableOpacity onPress={() => {
                this.props.navigation.navigate('Home');
            }}>
                <View>
                    <Text style={styles.forgetPasswordText}>Volver al Home</Text>
                </View>
            </TouchableOpacity>
        </View>
    )

    modifyEmail = () => {
        const formData = new FormData();
        formData.append("email", this.props.email);
        this.props.setLoading(true);
        this.props.modifyEmail(this.props.token, formData);
    }

    render() {
        return (
            <KeyboardView
                style={{height: "100%", width: "100%"}}
            >
                {this.props.loading ? this.renderSpinner() : this.renderModifyEmail()}
            </KeyboardView>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.Login.loading,
        token: state.Login.token,
        email: state.Profile.email
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        modifyEmail(token, formData){
            dispatch(ProfileActions.modifyEmail(token, formData))
        },
        setLoading(boolean){
            dispatch(Actions.loading(boolean))
        },
        setEmail(email) {
            dispatch(ProfileActions.setEmail(email))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ModifyEmail);
