import {UserProfile} from "../Endpoint";
import axios from "axios";
import {Alert, DeviceEventEmitter} from "react-native";
import {navigatorRef} from "../../App";
import moment from "moment";
import {NavigationActions} from "react-navigation";
import {apiPrivate} from "../api";
import {ApiError} from "../NewDeposit/schemas";

export default {
    getProfile() {
        return dispatch => {
          apiPrivate.get(`${UserProfile}/user/profile`)
            .then(response => {
                dispatch({type: "SET_LOADING", loading: false})
                dispatch({type: "GET_PROFILE", profile: response.data.description});

                // used to edit user data
                let {Name, LastName, ProfilePicture } = response.data.description
                ProfilePicture = ProfilePicture && ProfilePicture.Url !== "" ? ProfilePicture.Url : ""
                dispatch(this.getNameAndAvatar({Name, LastName, ProfilePicture}))
            })
            .catch(error => {
                dispatch({type: "SET_LOADING", loading: false});
                let message = ''
                if (error.response) {
                    if (error.response.status === 401 || error.response.status === 403) {
                      console.warn('redirected due to 401 or 403')
                      navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
                      return
                    }
                    const response = new ApiError(error.response.data)
                    message = response.description
                } else if (error.request) {
                    console.warn(error.request)
                    message = 'Algo salió mal, revisá tu conexión a internet'
                } else {
                    console.warn('Error', error.message)
                    message = 'Algo salió mal.'
                }
                Alert.alert('Error', message, [{text: 'OK'}])
            })
        }
    },

    getUserAddress(token) {
        return dispatch => {
            axios.get(UserProfile + "/user/address", {headers: {Authorization: token}})
                .then(response => {
                    dispatch({type: "SET_LOADING", loading: false})
                    dispatch({type: "GET_ADDRESSES", addresses: response.data.description});
                })
                .catch(response => {
                    dispatch({type: "SET_LOADING", loading: false});
                    console.log(response.response);
                    Alert.alert("Error", "Hubo un problema al cargar la información");
                })
        }
    },

    modifyEmail(token, formData) {
        return dispatch => {
            axios.put(UserProfile + "/user/profile/email", formData, {headers: {Authorization: token}})
                .then(response => {
                    dispatch({type: "SET_LOADING", loading: false})
                    Alert.alert("Éxito", response.data.description)
                    navigatorRef.dispatch(NavigationActions.navigate({routeName: "VerifyEmail"}));
                })
                .catch(response => {
                    dispatch({type: "SET_LOADING", loading: false});
                    console.log(response.response);
                    Alert.alert(response.response.data.description);
                })
        }
    },

    verifyEmail(token, formData, redirectRoute = 'Profile'){
        return dispatch => {
            if (redirectRoute !== "Profile") dispatch({type: "SET_REDIRECT_ROUTE", route: undefined})
            axios.put(UserProfile + "/user/verifyEmail", formData, {headers: {Authorization: token}})
                .then(response => {
                    dispatch({type: "SET_LOADING", loading: false});
                    Alert.alert("Éxito", response.data.description)
                    navigatorRef.dispatch(NavigationActions.navigate({routeName: redirectRoute}));
                })
                .catch(error => {
                    dispatch({type: "SET_LOADING", loading: false})
                    console.log(error.response);
                    Alert.alert("Error", error.response.data.description);
                })
        }
    },

    verifyEmailWithoutLog(formData){
        return dispatch => {
            axios.put(UserProfile + "/verifyEmail", formData)
                .then(response => {
                    dispatch({type: "SET_LOADING", loading: false})
                    navigatorRef.dispatch(NavigationActions.navigate({routeName: "Login"}));
                })
                .catch(error => {
                    dispatch({type: "SET_LOADING", loading: false})
                    console.log(error.response);
                    Alert.alert("Error", error.response.data.description);
                })
        }
    },

    getNameAndAvatar(data) {
        return {type: "GET_NAME_AVATAR", data}
    },
    setAvatar(avatar) {
        return {type: "SET_AVATAR", avatar}
    },
    deleteAvatar() {
        return {type: "DELETE_AVATAR"}
    },
    setName(name) {
        return {type: "SET_NAME", name}
    },
    setEmail(email) {
       return {type: "SET_PROFILE_EMAIL", email}
    },

    setAddress(address) {
        return {type: "SET_ADDRESS", address}
    },

    addAddress(token, formData, redirectRoute = 'Profile') {
        return dispatch => {
            if (redirectRoute !== "Profile") dispatch({type: "SET_REDIRECT_ROUTE", route: undefined})
            axios.post(UserProfile + "/user/address", formData, {headers: {Authorization: token}})
                .then(response => {
                    dispatch({type: "SET_LOADING", loading: false});
                    dispatch({type: "CLEAN_ADDRESS"});
                    if (redirectRoute === "AddressList"){
                        navigatorRef.dispatch(NavigationActions.back());
                        dispatch(this.getProfile());
                    } else if (redirectRoute === "Profile"){
                      navigatorRef.dispatch(NavigationActions.back());
                      dispatch({type: "SET_LOADING", loading: true});
                      dispatch(this.getProfile());
                    }
                    else {
                        navigatorRef.dispatch(NavigationActions.navigate({routeName: redirectRoute}));
                    }
                })
                .catch(error => {
                    dispatch({type: "SET_LOADING", loading: false});
                    let message = ''
                    if (error.response) {
                        if (error.response.status === 401 || error.response.status === 403) {
                          console.warn('redirected due to 401 or 403')
                          navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
                          return
                        }
                        const response = new ApiError(error.response.data)
                        message = response.description
                    } else if (error.request) {
                        console.warn(error.request)
                        message = 'Algo salió mal, revisá tu conexión a internet'
                    } else {
                        console.warn('Error', error.message)
                        message = 'Algo salió mal.'
                    }
                    Alert.alert('Error', message, [{text: 'OK'}])
                })
        }
    },

    modifyAddress(token, formData, id) {
        return dispatch => {
            apiPrivate.put(`${UserProfile}/user/address/${id}`, formData)
                .then(response => {
                    dispatch({type: "SET_LOADING", loading: false});
                    navigatorRef.dispatch(NavigationActions.back());
                    dispatch({type: "SET_LOADING", loading: true});
                    dispatch(this.getProfile());
                })
                .catch(error => {
                    dispatch({type: "SET_LOADING", loading: false});
                    let message = ''
                    if (error.response) {
                        if (error.response.status === 401 || error.response.status === 403) {
                          console.warn('redirected due to 401 or 403')
                          navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
                          return
                        }
                        const response = new ApiError(error.response.data)
                        message = response.description
                    } else if (error.request) {
                        console.warn(error.request)
                        message = 'Algo salió mal, revisá tu conexión a internet'
                    } else {
                        console.warn('Error', error.message)
                        message = 'Algo salió mal.'
                    }
                    Alert.alert('Error', message, [{text: 'OK'}])
                })
        }
    },

    deleteAddress(token, id) {
        return dispatch => {
          apiPrivate.delete(`${UserProfile}/user/address/${id}`)
            .then(response => {
                dispatch(this.getProfile(token))
            })
            .catch(error => {
              dispatch({type: "SET_LOADING", loading: false});
              let message = ''
              if (error.response) {
                if (error.response.status === 401 || error.response.status === 403) {
                  console.warn('redirected due to 401 or 403')
                  navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
                  return
                }
                const response = new ApiError(error.response.data)
                message = response.description
              } else if (error.request) {
                console.warn(error.request)
                message = 'Algo salió mal, revisá tu conexión a internet'
              } else {
                console.warn('Error', error.message)
                message = 'Algo salió mal.'
              }
              Alert.alert('Error', message, [{text: 'OK'}])
            })
        }
    },

    markMainAddress(token, id) {
        return dispatch => {
          apiPrivate.put(`${UserProfile}/user/address/${id}/main`, null)
            .then(response => {
                dispatch(this.getProfile(token))
            })
            .catch(error => {
              dispatch({type: "SET_LOADING", loading: false});
              let message = ''
              if (error.response) {
                if (error.response.status === 401 || error.response.status === 403) {
                  console.warn('redirected due to 401 or 403')
                  navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
                  return
                }
                const response = new ApiError(error.response.data)
                message = response.description
              } else if (error.request) {
                console.warn(error.request)
                message = 'Algo salió mal, revisá tu conexión a internet'
              } else {
                console.warn('Error', error.message)
                message = 'Algo salió mal.'
              }
              Alert.alert('Error', message, [{text: 'OK'}])
            })
        }
    },

    setModifyAddress(addressAction, address) {
        return {type: "SET_MODIFY_ADDRESS", addressAction, address}
    },

    showSmsError(value) {
        return {type: "SHOW_SMS_ERROR", value}
    },

    SMSVerification(token, formData, redirectRoute = 'Profile'){
        return dispatch => {
            if (redirectRoute !== "Profile") dispatch({type: "SET_REDIRECT_ROUTE", route: undefined})
            axios.post(UserProfile + "/user/phone", formData, {headers: {Authorization: token}})
                .then(response => {
                    dispatch({type: "SET_LOADING", loading: false});
                    navigatorRef.dispatch(NavigationActions.navigate({routeName: redirectRoute}));
                    if (redirectRoute !== "Profile") {
                        dispatch({type: "SET_REDIRECT_ROUTE", route: undefined})
                    }
                })
                .catch(error => {
                    dispatch(this.showSmsError(true))
                    dispatch({type: "SET_LOADING", loading: false})
                })
        }
    },

    verifyYourPhone(token, formData) {
        return dispatch => {
            axios.post(UserProfile + "/user/phone/verificationSMS", formData, {headers: {Authorization: token}})
                .then(response => {
                    dispatch({type: "SET_LOADING", loading: false})
                    navigatorRef.dispatch(NavigationActions.navigate({routeName: 'SMSVerification'}));
                })
                .catch(error => {
                    console.log(error.response)
                    dispatch({type: "SET_LOADING", loading: false})
                    Alert.alert("Error", error.response.data.description);
                })
        }
    },
    saveAvatarAndName(token, formData, formDataAvatar, actionAvatar) {
        return dispatch => {
            const arrayActions = [];

            if(formData !== null) {
              arrayActions.push(axios.put(UserProfile + "/user/profile", formData, {headers: {Authorization: token}}))
            }

            // Se esta manteniendo las acciones que se usaban antes para eliminar y editar datos
            // por si se necesitan en el futuro
            switch(actionAvatar) {
                case 1:
                    arrayActions.push(axios.post(UserProfile + "/user/profile/picture", formDataAvatar, {headers: {Authorization: token}}))
                    break;
                case 2:
                    arrayActions.push(axios.delete(UserProfile + "/user/profile/picture", {headers: {Authorization: token}}))
                    break;
            }

            axios.all(arrayActions)
                .then(axios.spread((name, avatar) => {
                    dispatch(this.getProfile());
                    navigatorRef.dispatch(NavigationActions.back())
                }))
                .catch(error => {
                    dispatch({type: "SET_LOADING", loading: false})
                    dispatch(this.getProfile());

                    let message = ''
                    if (error.response) {
                        if (error.response.status === 401 || error.response.status === 403) {
                        console.warn('redirected due to 401 or 403')
                        navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
                        return
                        }
                        const response = new ApiError(error.response.data)
                        message = response.description
                    } else if (error.request) {
                        console.warn(error.request)
                        message = 'Algo salió mal, revisá tu conexión a internet'
                    } else {
                        console.warn('Error', error.message)
                        message = 'Algo salió mal.'
                      }
                    Alert.alert('Error', message, [{text: 'OK'}])
            })
        }
    }
}
