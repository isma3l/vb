import React, { Component } from 'react';
import {TouchableNativeFeedback, Text, View, TouchableOpacity, TextInput, ActivityIndicator, Alert} from 'react-native'
import {Logo} from "../LogoScreen/Logo";
import ProfileActions from "./Profile.actions";
import Actions from "../Login/Login.actions";
import {connect} from "react-redux";
import {Colors} from "../Layout/Colors";
import {styles, VerifyPhoneStyles} from "../Styles";
import ButtonNative from "../MyComponents";
import {FormInput} from "react-native-elements";
import KeyboardView from "../KeyboardView";
import DoneBar from "done-bar";

class VerifyEmail extends Component {
    state = {
        email: "",
        code: "",
        showEmail: !!this.props.navigation.state.params,
        keyboardType: 'default'
    }

    componentDidMount() {
        this.props.setLoading(false);
    }

    verifyEmail = (routeName) => {
        const state = this.state;

        const formData = new FormData();
        formData.append("code", state.code);

        this.props.setLoading(true);

        if (state.showEmail) {
            formData.append("email", state.email);
            this.props.verifyEmailWithoutLog(formData)
        } else {
            this.props.verifyEmail(this.props.token, formData, routeName);
        }
    }

    renderSpinner = () => (
        <View style={styles.centerContainer}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    render() {
        return (
            <KeyboardView
                style={{height: "100%", width: "100%"}}
                keyboardType={this.state.keyboardType}
            >
                <View style={styles.container}>
                    {this.props.loading ? this.renderSpinner() : this.renderVerifyEmail()}
                </View>
                <DoneBar
                    keyboardType={this.state.keyboardType}
                    onDone={() => console.log('done!')}
                />
            </KeyboardView>
        )
    }

    renderVerifyEmail = () => (
        <View style={styles.centerContainer}>
            <Logo/>
            <View style={VerifyPhoneStyles.descriptionContainer}>
                <Text style={styles.subtitle}>Verificar Email</Text>
                <Text style={VerifyPhoneStyles.description}>Ingrese aquí el código recibido. El nuevo mail confirmado será usado para iniciar sesión desde ahora.</Text>
            </View>
            <View style={VerifyPhoneStyles.verifyContainer}>
                <FormInput placeholder="Email"
                           containerStyle={VerifyPhoneStyles.code}
                           inputStyle={styles.colorInputTextField}
                           keyboardType="email-address"
                           underlineColorAndroid={Colors.primaryColor}
                           placeholderTextColor="grey"
                           onFocus={() => this.setState({ keyboardType: 'email-address' })}
                           autoCapitalize="none"
                           onChangeText={text => this.setState({email: text})}/>
                <FormInput placeholder="Código"
                           containerStyle={VerifyPhoneStyles.code}
                           inputStyle={styles.colorInputTextField}
                           underlineColorAndroid={Colors.primaryColor}
                           placeholderTextColor="grey"
                           onFocus={() => this.setState({ keyboardType: 'numeric' })}
                           keyboardType="numeric"
                           autoCapitalize="none"
                           defaultValue={this.state.code}
                           onChangeText={text => this.setState({code: text})}/>
                <ButtonNative
                    onPress={() => this.verifyEmail(this.props.redirectRoute)}
                    styles={styles.buttonStyle}
                >
                    <Text style={{color: "white", fontWeight: "bold"}}>Continuar</Text>
                </ButtonNative>
            </View>
            <TouchableOpacity onPress={() => {
                this.props.navigation.navigate('Home');
            }}>
                <View>
                    <Text style={styles.forgetPasswordText}>Volver al Home</Text>
                </View>
            </TouchableOpacity>
            {this.state.showEmail && <TouchableOpacity onPress={() => {
                this.props.navigation.navigate('Login');
            }}>
                <View>
                    <Text style={styles.forgetPasswordText}>Volver al Login</Text>
                </View>
            </TouchableOpacity>
            }
        </View>
    )
}

const mapStateToProps = state => {
    return {
        loading: state.Login.loading,
        token: state.Login.token,
        email: state.Login.email,
        redirectRoute: state.VerifyInformation.route
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        setLoading(boolean){
            dispatch(Actions.loading(boolean))
        },
        setEmail(email){
            dispatch(Actions.setEmail(email))
        },
        verifyEmail(token, formData, redirectRoute){
            dispatch(ProfileActions.verifyEmail(token, formData, redirectRoute))
        },
        verifyEmailWithoutLog(formData){
            dispatch(ProfileActions.verifyEmailWithoutLog(formData))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(VerifyEmail);
