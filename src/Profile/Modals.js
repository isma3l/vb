import React, { Component } from 'react';
import {Text, View, Image, ActivityIndicator, FlatList, ScrollView, Picker, TouchableOpacity,StyleSheet, TouchableHighlight} from 'react-native'
import {Colors} from "../Layout/Colors";
import Modal from "react-native-modal";

const labels = {
  MARK_MAIN_ADDRESS: "Usar como dirección principal",
  MODIFY_ADDRESS: "Modificar dirección",
  DELETE_ADDRESS: "Eliminar dirección",
  CANCEL: "Cancelar",
  USE_MAIN_CARD: "Usar como tarjeta principal",
  DELETE_CARD: "Eliminar tarjeta"
}

export class AccountOptionsModal extends Component {
  render() {
    return (
      <Modal
        style={AccountStyle.containerAccount}
        isVisible={this.props.visibleAccountModal}
        onBackdropPress={() => this.props.closeAccountModal()}
        onBackButtonPress={() => this.props.closeAccountModal()}
      >
        <View style={{width:"100%"}}>
          <TouchableOpacity style={[AccountStyle.optionMarkMainAddress, AccountStyle.options]}
            onPress={() => { this.props.markMainAddress() }}>
            <Text style={AccountStyle.text}>{labels.MARK_MAIN_ADDRESS}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[AccountStyle.optionModifyAddress, AccountStyle.options]}
            onPress={() => {this.props.modifyAddress()}}>
            <Text style={AccountStyle.text}>{labels.MODIFY_ADDRESS}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[AccountStyle.optionDeleteAddress, AccountStyle.options]}
            onPress={() => {this.props.deleteAddress()}}>
            <Text style={{textAlign:"center", color: "red"}}>{labels.DELETE_ADDRESS}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[AccountStyle.options, AccountStyle.cancel]} onPress={() => this.props.closeAccountModal()}>
            <Text style={AccountStyle.textCancel}>{labels.CANCEL}</Text>
          </TouchableOpacity>
        </View>

      </Modal>
    )
  }
}

export class CardModalOptions extends Component {
  render() {
    return (
      <Modal
        style={AccountStyle.containerAccount}
        isVisible={this.props.visibleCardModal}
        onBackdropPress={() => this.props.closeCardModal()}
        onBackButtonPress={() => this.props.closeCardModal()}
        >
        <View style={{width:"100%"}}>
          <TouchableOpacity style={[AccountStyle.optionMarkMainAddress, AccountStyle.options]}
                            onPress={() => { this.props.markCardAsMain() }}>
            <Text style={AccountStyle.text}>{labels.USE_MAIN_CARD}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[AccountStyle.optionDeleteAddress, AccountStyle.options]}
                            onPress={ () => {this.props.deleteCard()} }>
            <Text style={{textAlign:"center", color: "red"}}>{labels.DELETE_CARD}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[AccountStyle.options, AccountStyle.cancel]}
                            onPress={ () => {this.props.closeCardModal()} }>
            <Text style={AccountStyle.textCancel}>{labels.CANCEL}</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    )
  }
}


const radiusAccountModal = 12

const AccountStyle = StyleSheet.create({
  containerAccount: {
    flex:1,
    justifyContent:"flex-end",
    alignItems:"center",
    marginBottom:8
  },
  options: {
    backgroundColor: Colors.backgroundColor,
    height:50,
    borderColor: Colors.borderColor,
    alignItems:"center",
    justifyContent:"center"
  },
  cancel: {
    backgroundColor:"white",
    borderRadius:12,
  },
  optionMarkMainAddress: {
    borderTopRightRadius: radiusAccountModal,
    borderTopLeftRadius: radiusAccountModal,
    borderBottomWidth: 0.7,
  },
  optionModifyAddress: {
    borderBottomWidth: 0.7
  },
  optionDeleteAddress: {
    marginBottom:4,
    borderBottomRightRadius: radiusAccountModal,
    borderBottomLeftRadius: radiusAccountModal
  },
  text: {
    textAlign:"center",
    color: Colors.fbButtonColor
  },
  textCancel: {
    textAlign:"center", color: "red"
  }
})
