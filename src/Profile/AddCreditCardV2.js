import React, { Component } from 'react';
import {Text, TouchableNativeFeedback, View, ActivityIndicator, TouchableOpacity, Alert, ScrollView} from 'react-native'
import {styles} from "../Styles";
import {Colors} from "../Layout/Colors";
import {connect} from "react-redux";
import CreditCardActions from "../CreditCard/CreditCard.actions";
import {PrimaryButton} from "../MyComponents"
import DoneBar from "done-bar";
import {CreditCardInput} from "react-native-credit-card-input";
import {FadeInView} from "../AnimatedComponents";
import KeyboardView from "../KeyboardView";
import RoundCheckbox from 'rn-round-checkbox';


class AddCreditCardV2 extends Component {
  state = {
    form: {
      valid: false
    },
    keyboardType: "default",
    markAsMain: false
  }

  componentDidMount() {
    this.props.getPaymentMethods();
  }

  renderSpinner = () => (
    <View style={styles.centerContainer}>
      <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
    </View>
  )

  addCreditCard = () => {
    const values = this.state.form.values;
    const card_number = values.number.replace(/\s/g,'');

    const data = {
      "security_code": values.cvc,
      "expiration_month": parseInt(values.expiry.substring(0, 2), 10),
      "expiration_year": 2000 + parseInt(values.expiry.substring(3, 5), 10),
      "card_number": card_number,
      "cardholder": {
        "name": values.name
      }
    };
    if (this.props.navigation.state.params) {
      this.props.addCreditCard(this.props.token, data, this.props.methods, this.props.navigation.state.params.redirectRoute, this.state.markAsMain);
    }
    else {
      this.props.addCreditCard(this.props.token, data, this.props.methods);
    }
  }

  renderAddCreditCard = () => {
    return (
      <FadeInView style={{flex: 1}}>
        <KeyboardView style={{flex: 1, justifyContent: "center", height: '100%'}} keyboardVerticalOffset={150}>
          <ScrollView contentContainerStyle={{minHeight: '100%'}}>

            <View style={{marginTop:20, marginBottom:16, width:"100%", paddingHorizontal:17}}>
              <Text style={{fontSize:26, color: "black", fontWeight:"bold"}}>Agrega una</Text>
              <Text style={{fontSize:26, color: "black", fontWeight:"bold"}}>tarjeta de crédito</Text>
              <Text style={{fontSize:15, color: Colors.subtitleLogoColor, marginTop:4}}>Descuida, tus datos están seguros</Text>
            </View>

            <CreditCardInput
              labels={{number: "Titular de la tarjeta", expiry: "Exp.", cvc: "CVC/CCV", name: "Nombre completo"}}
              placeholders={{ number: "1234 5678 1234 5678", expiry: "MM/YY", cvc: "CVC", name: "Nombre y Apellido" }}
              onChange={form => this.setState({form})}
              onFocus={name => {
                if(name === "name"){
                  this.setState({keyboardType: "default"})
                } else {
                  this.setState({keyboardType: "numeric"})
                }
              }}
              inputContainerStyle={{borderBottomColor: Colors.primaryColor, borderBottomWidth: 1}}
              requiresName={true}
            />

            <View style={{flexDirection:"row", width:"100%", marginVertical:16, paddingHorizontal:18, alignItems:"center"}}>

              <RoundCheckbox
                size={24}
                checked={this.state.markAsMain}
                backgroundColor="white"
                iconColor="#4434A5"
                onValueChange={()=>{this.setState({markAsMain: !this.state.markAsMain})}}
              />

              <Text style={{marginLeft:8}}>Usar tarjeta como principal</Text>
            </View>

            <View style={{width: "100%", marginTop:10}}>
              <PrimaryButton onPress={() => this.addCreditCard()} disabled={!this.state.form.valid} label="Guardar"
              />
            </View>
          </ScrollView>
          <DoneBar
            keyboardType={this.state.keyboardType}
            onDone={() => console.log('done!')}
          />
        </KeyboardView>
      </FadeInView>
    )
  }

  render() {
    return this.props.loading ? this.renderSpinner() : this.renderAddCreditCard()
  }
}

const mapStateToProps = state => {
  return {
    token: state.Login.token,
    loading: state.CreditCard.loading,
    methods: state.CreditCard.methods
  }
};

const mapDispatchToProps = dispatch =>{
  return{
    addCreditCard(token, data, methods, redirectRoute, markAsMain) {
      dispatch({type: "SET_CARDS_LOADING", loading: true})
      dispatch(CreditCardActions.addCreditCard(token, data, methods, redirectRoute, markAsMain))
    },
    setLoading(boolean) {
      dispatch({type: "SET_LOADING", loading: boolean})
    },
    getPaymentMethods() {
      dispatch({type: "SET_CARDS_LOADING", loading: true});
      dispatch(CreditCardActions.getPaymentMethods());
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddCreditCardV2);
