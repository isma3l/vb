import React, { Component } from 'react';
import {
    Text, View, ActivityIndicator, TouchableOpacity, Alert, ScrollView, Platform,
    StyleSheet, Image
} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';
import {List, ListItem, Avatar} from "react-native-elements";
import {styles, WelcomeStyle} from "../Styles";
import {Colors} from "../Layout/Colors";
import LoginActions from "../Login/Login.actions";
import {connect} from "react-redux";
import ProfileActions from "../Profile/Profile.actions";
import {FadeInView} from "../AnimatedComponents";
import ButtonNative, {PrimaryButton} from "../MyComponents";
import CardsActions from "../CreditCard/CreditCard.actions";
import MissingInfoAction from "../VerifyInformation/VerifyInformation.actions";
import storeStorage from "react-native-simple-store";
import store from "../store";
import CategoryActions from "../Categories/Categories.actions";
import LinearGradient from "react-native-linear-gradient";
import {ITEMS} from "../Images";
import Hr from "react-native-hr-plus";

class MyAccount extends Component {
    static navigationOptions = {
        headerTitle: 'Perfil',
        drawerLabel: 'Perfil',
        drawerIcon: ({ tintColor }) => (
            <Icon name="person" size={24}/>
        ),
    };

    componentDidMount(){
        this.props.setLoading(true);
        this.props.getProfile(this.props.token);
    }

    renderSpinner = () => (
        <View style={styles.centerContainer}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    renderProfile = (profile) => {
        let title = "";
        if (profile) {
            if (profile.Name !== "") {
                title = title + profile.Name[0].toUpperCase();
            }
            if (profile.LastName !== "") {
                title = title + profile.LastName[0].toUpperCase();
            }
        }
        if (title === "") title = "VB";


        return (
            <FadeInView style={{flex: 1}}>
                <ScrollView style={{flex: 1}}>
                    <View style={MyAccountStyles.gradientContainer}>
                        <LinearGradient colors={[Colors.gradientColorA, Colors.gradientColorB]} start={{x: 0, y: 0}} end={{x: 1, y: 1}} style={{flex: 1}}>
                            <View style={{backgroundColor: "transparent", flex: 1, alignItems: "center", marginTop: 68}}>
                                {(profile.ProfilePicture && profile.ProfilePicture.Url !== "") ?
                                    <Avatar containerStyle={MyAccountStyles.avatarStyle} height={82} width={82} source={{uri: profile.ProfilePicture.Url}} rounded style/>
                                    :
                                    <Avatar containerStyle={MyAccountStyles.avatarStyle} height={82} width={82} large title={title} rounded/>
                                }
                                <Text style={MyAccountStyles.userNameStyle}>¡Hola {profile.Name}!</Text>
                                <View style={{flexDirection: "row", justifyContent: "center", alignItems: "center", marginVertical: 15}}>
                                    <Image source={ITEMS} resizeMode={"contain"} style={{height: 24, width: 24}}/>
                                    <Text style={MyAccountStyles.quantityItems}> {profile.ItemAmount} </Text>
                                    <Text style={MyAccountStyles.itemsLabel}>ítems</Text>
                                </View>
                                <PrimaryButton label="Programar Retiro" onPress={() => this.props.navigation.navigate("NewDeposit")}/>
                            </View>
                        </LinearGradient>
                    </View>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate("ProfileTabs")}>
                        <Text style={MyAccountStyles.menuItem}>Mi Cuenta</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {}}>
                        <Text style={MyAccountStyles.menuItem}>Historial</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {}}>
                        <Text style={MyAccountStyles.menuItem}>Balance</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {}}>
                        <Text style={MyAccountStyles.menuItem}>Facturación</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {}}>
                        <Text style={[MyAccountStyles.menuItem, {color: Colors.gradientColorB}]}>¡Invita amigos y ganá créditos!</Text>
                    </TouchableOpacity>
                    <View style={{marginBottom: 20, width: "90%", alignSelf: "center"}}>
                      <Hr color={Colors.hrColor} width={1}><Text/></Hr>
                    </View>
                    <TouchableOpacity onPress={() => {}}>
                        <Text style={MyAccountStyles.menuItem}>Ayuda</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {}}>
                        <Text style={MyAccountStyles.menuItem}>Sobre Space.Gurú</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        storeStorage.delete("user_id")
                            .then(rs => {
                                storeStorage.delete("token")
                                    .then(response => {
                                        this.props.logOut();
                                        this.props.navigation.navigate("LayoutGuest");
                                    })
                            })
                    }}>
                        <Text style={MyAccountStyles.menuItem}>Cerrar sesión</Text>
                    </TouchableOpacity>
                </ScrollView>
            </FadeInView>
        )
    }

    render() {
        return this.props.loading ? this.renderSpinner() : this.renderProfile(this.props.profile.profile)
    }
}

const mapStateToProps = state => {
    return {
        loading: state.Login.loading,
        token: state.Login.token,
        profile: state.Profile,
        cards: state.CreditCard.cards,
        missingInfo: state.VerifyInformation.missingInformation
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        setLoading(boolean){
            dispatch(LoginActions.loading(boolean))
        },
        getProfile(token) {
            dispatch(ProfileActions.getProfile(token))
        },
        getNameAndAvatar(data) {
            dispatch(ProfileActions.getNameAndAvatar(data))
        },
        setModifyAddress(addressAction, address) {
            dispatch(ProfileActions.setModifyAddress(addressAction, address))
        },
        deleteAddress(token, id) {
            dispatch(ProfileActions.deleteAddress(token, id))
        },
        getCreditCards(token) {
            dispatch({type: "SET_CARDS_LOADING", loading: true});
            dispatch(CardsActions.getCreditCards(token));
        },
        deleteCard(token, id) {
            dispatch({type: "SET_CARDS_LOADING", loading: true});
            dispatch(CardsActions.deleteCard(token, id));
        },
        getMissingInfo() {
            dispatch({type: "SET_LOADING_VERIFY_INFORMATION", loading: true});
            dispatch(MissingInfoAction.getMissingInfo());
        },
        markMainCard(token, cardID) {
            dispatch({type: "SET_CARDS_LOADING", loading: true})
            dispatch(CardsActions.markMainCard(token, cardID))
        },
        markMainAddress(token, id) {
            dispatch(LoginActions.loading(true))
            dispatch(ProfileActions.markMainAddress(token, id))
        },
        logOut() {
            dispatch({type: "REMOVE_TOKEN"})
            dispatch(CategoryActions.cleanCategories())
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MyAccount);


const MyAccountStyles = StyleSheet.create({
    gradientContainer: {
        height: 340,
        justifyContent: "center",
        marginBottom: 20
    },
    userNameStyle: {
        color: "white",
        fontSize: 22,
        fontWeight: "bold",
        textAlign: "center",
        marginTop: 30
    },
    quantityItems: {
        color: "black",
        fontSize: 12
    },
    itemsLabel: {
        color: "white",
        fontSize: 12
    },
    avatarStyle: {
        shadowRadius: 2,
        shadowOffset: {width: 0, height: 6},
        shadowOpacity: 0.2
    },
    menuItem: {
        fontSize: 22,
        marginBottom: 20,
        fontWeight: "bold",
        marginLeft: 20
    }
})
