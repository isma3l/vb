const initialState = {
    profile: {
        Name: "",
        LastName: "",
        ProfilePicture: null,
        Email: "",
        Addresses: []
    },
    NameAvatar: {},
    newAvatar: {},
    avatarAction: 0,
    email: "",
    address: {},
    addresses: [],
    addressAction: 0,
    smsError: ""
};

//AvatarAction 0: do nothing 1: save new avatar 2: delete avatar
//AddressAction 0: add 1: modify
const Profile = (state = initialState, action) => {
    switch (action.type) {
        case "GET_PROFILE":
            return {...state, profile: action.profile};
        case "GET_ADDRESSES":
            return {...state, addresses: action.addresses}
        case "GET_NAME_AVATAR":
            return {...state, NameAvatar: action.data, avatarAction: 0}
        case "SET_AVATAR":
            return {...state, NameAvatar: {...state.NameAvatar, ProfilePicture: action.avatar.uri}, avatarAction: 1, newAvatar: action.avatar}
        case "DELETE_AVATAR":
            return {...state, NameAvatar: {...state.NameAvatar, ProfilePicture: ""}, avatarAction: state.profile.ProfilePicture.Url === "" ? 0 : 2}
        case "SET_NAME":
            return {...state, NameAvatar: {...state.NameAvatar, ...action.name}}
        case "SET_PROFILE_EMAIL":
            return {...state, email: action.email}
        case "SET_MODIFY_ADDRESS":
            return {...state, addressAction: action.addressAction, address: {
                apartment: action.address.Apartment ? action.address.Apartment : "",
                floor: action.address.Floor ? action.address.Floor : "",
                address: action.address.Address,
                postalCode: action.address.PostalCode.PostalCode,
                id: action.address.ID
            }}
        case "SET_ADDRESS":
            return {...state, address: {...state.address, ...action.address}};
        case "CLEAN_ADDRESS":
            return {...state, address: {}}
        case "SHOW_SMS_ERROR":
            return { ...state, smsError: action.value }
        default:
            return state;
    }
};

export default Profile;
