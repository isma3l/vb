import React, {Component} from 'react'
import {Text, View, TouchableOpacity, TextInput, ActivityIndicator, ScrollView} from 'react-native'
import Actions from '../Login/Login.actions'
import {connect} from 'react-redux'
import {Colors} from '../Layout/Colors'
import {styles, VerifyPhoneStyles} from '../Styles'
import ProfileActions from './Profile.actions'
import {PrimaryButton} from '../MyComponents'
import {FormInput} from 'react-native-elements'
import KeyboardView from '../KeyboardView'


class VerifyYourPhone extends Component {
  state = {
    keyboardType: 'default',
  }

  componentDidMount() {
    this.props.setLoading(false)
  }

  verifyYourPhone = () => {
    const formData = new FormData()
    formData.append('phone_number', this.props.Phone.prefix + this.props.Phone.phone)
    this.props.setLoading(true)
    this.props.verifyYourPhone(this.props.token, formData)
  }

  renderSpinner = () => (
    <View style={styles.centerContainer}>
      <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
    </View>
  )

  renderVerifyYourPhone = () => {
    return (
      <View style={{minHeight: '100%'}}>
        <KeyboardView style={{flex: 1}} behavior="padding">
          <View style={styles.container}>
            <ScrollView style={{minHeight: '100%'}} keyboardShouldPersistTaps="handled">
              <View style={VerifyPhoneStyles.centerContainer}>
                <Text style={VerifyPhoneStyles.title}>
                  Para comenzar,
                </Text>
                <Text style={VerifyPhoneStyles.title}>
                  debemos verificar tu número de teléfono
                </Text>
                <Text style={VerifyPhoneStyles.subtitle}>
                  Recibirás un SMS. Tu compañia teléfonica puede aplicar algunos cargos.
                </Text>

                <View style={VerifyPhoneStyles.phoneNumberContainer}>

                  <View style={[VerifyPhoneStyles.containerFormInput, {width: '20%'}]}>
                    <FormInput
                      placeholder="+54"
                      containerStyle={VerifyPhoneStyles.contentFormInput}
                      underlineColorAndroid={Colors.primaryColor}
                      inputStyle={[styles.colorInputTextField, {textAlign: 'center'}]}
                      placeholderTextColor="grey"
                      maxLength={3}
                      onFocus={() => this.setState({keyboardType: 'numeric'})}
                      autoCapitalize="none"
                      keyboardType="phone-pad"
                      defaultValue={this.props.Phone.prefix}
                      onChangeText={text => this.props.setPrefix(text)}
                    />
                    <View style={VerifyPhoneStyles.containerDescriptionForm}>
                      <Text style={VerifyPhoneStyles.descriptionForm}>Código país</Text>
                    </View>

                  </View>

                  <View style={[VerifyPhoneStyles.containerFormInput, {width: '70%'}]}>
                    <FormInput
                      containerStyle={VerifyPhoneStyles.contentFormInput}
                      placeholder="Número de Teléfono"
                      inputStyle={[styles.colorInputTextField, {textAlign: 'center'}]}
                      underlineColorAndroid={Colors.primaryColor}
                      placeholderTextColor="grey"
                      keyboardType="phone-pad"
                      onFocus={() => this.setState({keyboardType: 'numeric'})}
                      autoCapitalize="none"
                      defaultValue={this.props.Phone.phone}
                      onChangeText={text => this.props.setPhone(text)}
                    />
                    <View style={VerifyPhoneStyles.containerDescriptionForm}>
                      <Text style={VerifyPhoneStyles.descriptionForm}>
                        Incluye el código de área.
                      </Text>
                      <Text style={VerifyPhoneStyles.descriptionForm}>
                        Ej: 11 para Bs As.
                      </Text>
                    </View>
                  </View>

                </View>

                <PrimaryButton
                  onPress={() => this.verifyYourPhone()}
                  label={'Verificar'}
                />
                <TouchableOpacity
                  style={{alignSelf: 'center'}}
                  onPress={() => this.props.navigation.navigate('SMSVerification', {showPhone: true})}
                >
                  <View>
                    <Text style={styles.forgetPasswordText}>Ya tenes un código?</Text>
                  </View>
                </TouchableOpacity>

              </View>
            </ScrollView>
          </View>
        </KeyboardView>
      </View>
    )
  }

  render() {
    return this.props.loading ? this.renderSpinner() : this.renderVerifyYourPhone()
  }
}

const mapStateToProps = state => {
  return {
    loading: state.Login.loading,
    token: state.Login.token,
    Phone: state.Login.phone,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setLoading(boolean) {
      dispatch(Actions.loading(boolean))
    },
    setPrefix(prefix) {
      dispatch(Actions.setPrefix(prefix))
    },
    setPhone(phone) {
      dispatch(Actions.setPhone(phone))
    },
    verifyYourPhone(token, formData) {
      dispatch(ProfileActions.verifyYourPhone(token, formData))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(VerifyYourPhone)


/*
import React, { Component } from 'react';
import {Text, View, TouchableOpacity, TextInput, ActivityIndicator} from 'react-native'
import {Logo} from "../LogoScreen/Logo";
import Actions from "../Login/Login.actions";
import {connect} from "react-redux";
import {Colors} from "../Layout/Colors";
import {styles, VerifyPhoneStyles} from "../Styles";
import {Card} from "react-native-elements"
import ProfileActions from "./Profile.actions";
import ButtonNative from "../MyComponents";
import {FormInput} from "react-native-elements";
import KeyboardView from "../KeyboardView";
import DoneBar from "done-bar";

class VerifyYourPhone extends Component {
    state = {
        keyboardType: 'default'
    }
    componentDidMount() {
        this.props.setLoading(false);
    }

    verifyYourPhone = () => {
        const formData = new FormData();
        formData.append("phone_number", "+" + this.props.Phone.prefix + this.props.Phone.phone);
        this.props.setLoading(true);
        this.props.verifyYourPhone(this.props.token, formData)
    }

    renderSpinner = () => (
        <View style={styles.centerContainer}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    render() {
        return (
            <KeyboardView
                style={{height: "100%", width: "100%"}}
                keyboardType={this.state.keyboardType}
            >
                <View style={styles.container}>
                    {this.props.loading ? this.renderSpinner() : this.renderVerifyYourPhone()}
                </View>
                <DoneBar
                    keyboardType={this.state.keyboardType}
                    onDone={() => console.log('done!')}
                />
            </KeyboardView>
        )
    }

    renderVerifyYourPhone = () => (
        <View style={styles.centerContainer}>
            <Logo/>
            <View style={VerifyPhoneStyles.descriptionContainer}>
                <Text style={styles.subtitle}>Verificar tu teléfono</Text>
                <Text style={VerifyPhoneStyles.description}>Space.Guru Box enviará un SMS para verificar su número de teléfono. Cargos pueden ser aplicados por su compañia teléfonica</Text>
            </View>
            <View style={VerifyPhoneStyles.verifyContainer}>
                <View style={VerifyPhoneStyles.phoneNumberContainer}>
                    <Text style={{flexGrow: 0}}>+ </Text>
                    <FormInput placeholder="Prefijo"
                               containerStyle={VerifyPhoneStyles.prefix}
                               underlineColorAndroid={Colors.primaryColor}
                               inputStyle={styles.colorInputTextField}
                               placeholderTextColor="grey"
                               maxLength={2}
                               onFocus={() => this.setState({ keyboardType: 'numeric' })}
                               autoCapitalize="none"
                               keyboardType="numeric"
                               defaultValue={this.props.Phone.prefix}
                               onChangeText={text => this.props.setPrefix(text)}
                    />
                    <FormInput containerStyle={VerifyPhoneStyles.phoneNumber}
                               placeholder="Número de Teléfono"
                               inputStyle={styles.colorInputTextField}
                               underlineColorAndroid={Colors.primaryColor}
                               placeholderTextColor="grey"
                               keyboardType="numeric"
                               onFocus={() => this.setState({ keyboardType: 'numeric' })}
                               autoCapitalize="none"
                               defaultValue={this.props.Phone.phone}
                               onChangeText={text => this.props.setPhone(text)}
                    />
                </View>
                <ButtonNative
                    onPress={() => this.verifyYourPhone()}
                    styles={styles.buttonStyle}
                >
                    <Text style={{color: "white", fontWeight: "bold"}}>Verificar</Text>
                </ButtonNative>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('SMSVerification', {showPhone: true})}>
                    <View>
                        <Text style={styles.forgetPasswordText}>Ya tenes un código?</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <TouchableOpacity onPress={() => {
                this.props.navigation.navigate('Profile');
                this.props.navigation.navigate('Home');
            }}>
                <View>
                    <Text style={styles.forgetPasswordText}>Volver al Home</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}

const mapStateToProps = state => {
    return {
        loading: state.Login.loading,
        token: state.Login.token,
        Phone: state.Login.phone
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        setLoading(boolean){
            dispatch(Actions.loading(boolean))
        },
        setPrefix(prefix){
            dispatch(Actions.setPrefix(prefix))
        },
        setPhone(phone){
            dispatch(Actions.setPhone(phone))
        },
        verifyYourPhone(token, formData){
            dispatch(ProfileActions.verifyYourPhone(token, formData))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(VerifyYourPhone);

*/
