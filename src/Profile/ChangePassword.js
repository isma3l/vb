import React, {Component} from 'react'
import {Text, View, ActivityIndicator, Alert} from 'react-native'
import {Logo} from '../LogoScreen/Logo'
import Actions from '../Login/Login.actions'
import {connect} from 'react-redux'
import {Colors} from '../Layout/Colors'
import {styles, VerifyPhoneStyles} from '../Styles'
import {FormInput} from 'react-native-elements'
import ButtonNative from '../MyComponents'
import KeyboardView from '../KeyboardView'

class ChangePassword extends Component {

  state = {
    oldPassword: '',
    newPassword: '',
    confirmPassword: '',
  }

  componentDidMount() {
    this.props.setLoading(false)
  }

  changePassword = () => {
    const state = this.state

    //TODO rule should be somewhere else
    if (state.newPassword.length < 6) {
      Alert.alert('Error', 'La contraseña debe ser de 6 o más carácteres', [{text: 'OK'}])
      return
    }

    if (state.newPassword !== state.confirmPassword) {
      Alert.alert('Error', 'Las contraseñas no son iguales', [{text: 'OK'}])
      return
    }

    this.props.setLoading(true)
    this.props.changePassword(state.oldPassword, state.newPassword)
  }

  renderSpinner = () => (
    <View style={styles.centerContainer}>
      <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
    </View>
  )

  render() {
    return (
      <View style={{height: '100%', width: '100%'}}>
        <View style={styles.container}>
          {this.props.loading ? this.renderSpinner() : this.renderNewUser()}
        </View>
      </View>
    )
  }

  renderNewUser = () => (
    <KeyboardView style={styles.centerContainer}>
      <Logo/>
      <View style={VerifyPhoneStyles.descriptionContainer}>
        <Text style={styles.subtitle}>Cambiar Contraseña</Text>
        <Text>Por tu seguridad, ingresá primero tu contraseña actual, y luego la nueva contraseña.</Text>
        <View>
          <FormInput
            containerStyle={styles.code}
            inputStyle={styles.colorInputTextField}
            placeholder="Contraseña actual"
            autoCapitalize="none"
            secureTextEntry={true}
            underlineColorAndroid={Colors.primaryColor}
            placeholderTextColor="grey"
            onChangeText={text => this.setState({oldPassword: text})}
          />
          <FormInput
            containerStyle={styles.code}
            inputStyle={styles.colorInputTextField}
            placeholder="Nueva Contraseña"
            underlineColorAndroid={Colors.primaryColor}
            placeholderTextColor="grey"
            autoCapitalize="none"
            secureTextEntry={true}
            onChangeText={text => this.setState({newPassword: text})}
          />
          <FormInput
            containerStyle={styles.code}
            inputStyle={styles.colorInputTextField}
            placeholder="Confirmar Nueva Contraseña"
            underlineColorAndroid={Colors.primaryColor}
            placeholderTextColor="grey"
            autoCapitalize="none"
            secureTextEntry={true}
            onChangeText={text => this.setState({confirmPassword: text})}
          />
        </View>
        <ButtonNative
          onPress={() => this.changePassword()}
          styles={styles.buttonStyle}
        >
          <Text style={{color: 'white', fontWeight: 'bold'}}>Cambiar Contraseña</Text>
        </ButtonNative>
      </View>
    </KeyboardView>
  )
}

const mapStateToProps = state => {
  return {
    loading: state.Login.loading,
    token: state.Login.token,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    changePassword: (oldPassword, newPassword) => dispatch(Actions.changePassword(oldPassword, newPassword)),
    setLoading: boolean => dispatch(Actions.loading(boolean)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword)
