import React, { Component } from 'react';
import {Text, View, TouchableOpacity, TextInput, ActivityIndicator, ScrollView} from 'react-native'
import {Logo} from "../LogoScreen/Logo";
import Actions from "../Login/Login.actions";
import ProfileActions from "./Profile.actions";
import {connect} from "react-redux";
import {Colors} from "../Layout/Colors";
import {SmsVerification, styles, VerifyPhoneStyles} from "../Styles";
import ButtonNative from "../MyComponents";
import {FormInput} from "react-native-elements";
import KeyboardView from "../KeyboardView";
import DoneBar from "done-bar";


class SMSVerification extends Component {
    form = {}

    componentDidMount() {
        this.props.setLoading(false);
    }

    state = {
        code: "",
        firstDigit: "",
        secondDigit: "",
        thirdDigit: "",
        fourthDigit: "",
        showPhone: !!this.props.navigation.state.params,
        keyboardType: "default"
    }

    renderSpinner = () => (
        <View style={styles.centerContainer}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    checkFullInputs = () => {
        const {firstDigit, secondDigit, thirdDigit, fourthDigit} = this.state
        return firstDigit !== "" && secondDigit !== "" && thirdDigit !== "" && fourthDigit !== ""
    }


    SMSVerification = () => {
        const formData = new FormData();
        formData.append("phone_number", "+" + this.props.Phone.prefix + this.props.Phone.phone);
        formData.append("code", this.state.firstDigit + this.state.secondDigit + this.state.thirdDigit + this.state.fourthDigit);
        this.props.setLoading(true);
        this.props.SMSVerification(this.props.token, formData, this.props.redirectRoute)
    }

    renderSMSVerification = () => (
        <ScrollView contentContainerStyle={{minHeight:"100%"}}>
            <View style={[VerifyPhoneStyles.centerContainer, {paddingHorizontal:0}]}>
                <View style={{paddingHorizontal:16}}>
                    <Text style={VerifyPhoneStyles.title}>
                        Ingresa el código
                    </Text>
                    <Text style={VerifyPhoneStyles.title}>
                        enviado por SMS
                    </Text>
                    <Text style={VerifyPhoneStyles.subtitle}>
                        El código enviado tiene una validez de 2 horas desde generado.
                    </Text>
                </View>

                <View style={[VerifyPhoneStyles.phoneNumberContainer, {paddingHorizontal:24}]}>
                    <FormInput
                        containerStyle={SmsVerification.containerFormInput}
                        inputStyle={[SmsVerification.input,
                            {borderBottomColor: this.props.smsError ? Colors.pink: Colors.underline}]}
                        keyboardType={"numeric"}
                        underlineColorAndroid={Colors.primaryColor}
                        placeholderTextColor="grey"
                        autoCapitalize="none"
                        maxLength={1}
                        onFocus={() => this.setState({ keyboardType: 'numeric' })}
                        inputAccessoryView={true}
                        defaultValue={this.state.firstDigit}
                        onChangeText={
                            text => {
                                if(text.length > 0) {
                                    this.setState({firstDigit: text}, () => {
                                        if (this.checkFullInputs()) {
                                            this.SMSVerification()
                                        } else {
                                            this.form.secondDigit.focus()
                                        }
                                    })
                                } else {
                                    this.setState({firstDigit: ""})
                                    this.props.showSmsError(false)
                                }
                            }
                        }
                        autoFocus = {true}
                        returnKeyType = {"next"}
                        textInputRef={(c) => { this.form.firstDigit = c; }}
                    />


                    <FormInput
                        containerStyle={SmsVerification.containerFormInput}
                        inputStyle={[SmsVerification.input,
                            {borderBottomColor: this.props.smsError ? Colors.pink: Colors.underline}]}
                        keyboardType={"numeric"}
                        underlineColorAndroid={Colors.primaryColor}
                        placeholderTextColor="grey"
                        autoCapitalize="none"
                        maxLength={1}
                        onFocus={() => this.setState({ keyboardType: 'numeric' })}
                        inputAccessoryView={true}
                        defaultValue={this.state.secondDigit}
                        onChangeText={
                            text => {
                                if(text.length > 0) {
                                    this.setState({secondDigit: text}, () => {
                                        if (this.checkFullInputs()) {
                                            this.SMSVerification()
                                        } else {
                                            this.form.thirdDigit.focus();
                                        }
                                    })
                                } else {
                                    this.setState({secondDigit: ""})
                                    this.props.showSmsError(false)
                                    this.form.firstDigit.focus()
                                }
                            }
                        }
                        returnKeyType = {"next"}
                        textInputRef={(c) => { this.form.secondDigit = c; }}
                    />

                    <FormInput
                        containerStyle={SmsVerification.containerFormInput}
                        inputStyle={[SmsVerification.input,
                            {borderBottomColor: this.props.smsError ? Colors.pink: Colors.underline}]}
                        keyboardType={"numeric"}
                        underlineColorAndroid={Colors.primaryColor}
                        placeholderTextColor="grey"
                        autoCapitalize="none"
                        maxLength={1}
                        onFocus={() => this.setState({ keyboardType: 'numeric' })}
                        inputAccessoryView={true}
                        defaultValue={this.state.thirdDigit}
                        onChangeText={
                            text => {
                                if(text.length > 0) {
                                    this.setState({thirdDigit: text}, () => {
                                        if (this.checkFullInputs()) {
                                            this.SMSVerification()
                                        } else {
                                            this.form.fourthDigit.focus()
                                        }
                                    })
                                } else {
                                    this.setState({thirdDigit: ""})
                                    this.props.showSmsError(false)
                                    this.form.secondDigit.focus()
                                }
                            }
                        }
                        returnKeyType = {"next"}
                        textInputRef={(c) => { this.form.thirdDigit = c; }}

                    />

                    <FormInput
                        containerStyle={SmsVerification.containerFormInput}
                        inputStyle={[SmsVerification.input,
                            {borderBottomColor: this.props.smsError ? Colors.pink: Colors.underline }]}
                        keyboardType={"numeric"}
                        placeholderTextColor="grey"
                        autoCapitalize="none"
                        maxLength={1}
                        onFocus={() => this.setState({ keyboardType: 'numeric' })}
                        inputAccessoryView={true}
                        defaultValue={this.state.fourthDigit}
                        onChangeText={
                            text => {
                                if(text.length > 0) {
                                    this.setState({fourthDigit: text}, () => {
                                        if (this.checkFullInputs()) {
                                            this.SMSVerification()
                                        }
                                    })
                                } else {
                                    this.setState({fourthDigit: ""})
                                    this.props.showSmsError(false)
                                    this.form.thirdDigit.focus();
                                }
                            }
                        }
                        returnKeyType = {"next"}
                        textInputRef={(c) => { this.form.fourthDigit = c; }}

                    />

                </View>

                { this.props.smsError ?
                    <View style={{width:"100%", paddingVertical:16, justifyContent:"center",
                        alignItems:"center", backgroundColor: Colors.pink, marginTop: "auto"}}>
                        <Text style={{color: "white", textAlign:"center"}}>
                            Código inválido. Vuelve a intentarlo.
                        </Text>
                    </View>
                    : null
                }

            </View>
        </ScrollView>
    )

    render() {
        return (
            <KeyboardView
                style={{height: "100%", width: "100%"}}
                keyboardType={this.state.keyboardType}
            >
                {this.props.loading ? this.renderSpinner() : this.renderSMSVerification()}
                <DoneBar
                    keyboardType={this.state.keyboardType}
                    onDone={() => console.log('done!')}
                />
            </KeyboardView>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.Login.loading,
        token: state.Login.token,
        Phone: state.Login.phone,
        redirectRoute: state.VerifyInformation.redirectRoute,
        smsError: state.Profile.smsError
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        setLoading(boolean){
            dispatch(Actions.loading(boolean))
        },
        SMSVerification(token, formData, redirectRoute){
            dispatch(ProfileActions.SMSVerification(token, formData, redirectRoute))
        },
        setPrefix(prefix){
            dispatch(Actions.setPrefix(prefix))
        },
        setPhone(phone){
            dispatch(Actions.setPhone(phone))
        },
        showSmsError(value){
            dispatch(ProfileActions.showSmsError(value))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SMSVerification);









/*

import React, { Component } from 'react';
import {Text, View, TouchableOpacity, TextInput, ActivityIndicator} from 'react-native'
import {Logo} from "../LogoScreen/Logo";
import Actions from "../Login/Login.actions";
import ProfileActions from "./Profile.actions";
import {connect} from "react-redux";
import {Colors} from "../Layout/Colors";
import {styles, VerifyPhoneStyles} from "../Styles";
import ButtonNative from "../MyComponents";
import {FormInput} from "react-native-elements";
import KeyboardView from "../KeyboardView";
import DoneBar from "done-bar";
class SMSVerification extends Component {
    componentDidMount() {
        this.props.setLoading(false);
    }

    state = {
        code: "",
        showPhone: !!this.props.navigation.state.params,
        keyboardType: "default"
    }

    renderSpinner = () => (
        <View style={styles.centerContainer}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    renderSMSVerification = () => (
        <View style={styles.centerContainer}>
            <Logo/>
            <View style={VerifyPhoneStyles.descriptionContainer}>
                <Text style={styles.subtitle}>Verificar SMS</Text>
                <Text>Ingrese aquí el código que recibió en su teléfono</Text>
            </View>
            <View style={VerifyPhoneStyles.verifyContainer}>
                <FormInput placeholder="Código"
                           containerStyle={VerifyPhoneStyles.code}
                           inputStyle={styles.colorInputTextField}
                           keyboardType={"numeric"}
                           underlineColorAndroid={Colors.primaryColor}
                           placeholderTextColor="grey"
                           autoCapitalize="none"
                           onFocus={() => this.setState({ keyboardType: 'numeric' })}
                           inputAccessoryView={true}
                           defaultValue={this.state.code}
                           onChangeText={text => this.setState({code: text})}/>
                <ButtonNative
                    onPress={() => this.SMSVerification()}
                    styles={styles.buttonStyle}
                >
                    <Text style={{color: "white", fontWeight: "bold"}}>Finalizar</Text>
                </ButtonNative>
            </View>
            <TouchableOpacity onPress={() => {
                this.props.navigation.navigate('Profile');
                this.props.navigation.navigate('Home');
            }}>
                <View>
                    <Text style={styles.forgetPasswordText}>Volver al Home</Text>
                </View>
            </TouchableOpacity>
        </View>
    )

    SMSVerification = () => {
        const formData = new FormData();
        formData.append("phone_number", "+" + this.props.Phone.prefix + this.props.Phone.phone);
        formData.append("code", this.state.code);
        this.props.setLoading(true);
        this.props.SMSVerification(this.props.token, formData, this.props.redirectRoute)
    }

    render() {
        return (
            <KeyboardView
                style={{height: "100%", width: "100%"}}
                keyboardType={this.state.keyboardType}
            >
                {this.props.loading ? this.renderSpinner() : this.renderSMSVerification()}
                <DoneBar
                    keyboardType={this.state.keyboardType}
                    onDone={() => console.log('done!')}
                />
            </KeyboardView>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.Login.loading,
        token: state.Login.token,
        Phone: state.Login.phone,
        redirectRoute: state.VerifyInformation.route
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        setLoading(boolean){
            dispatch(Actions.loading(boolean))
        },
        SMSVerification(token, formData, redirectRoute){
            dispatch(ProfileActions.SMSVerification(token, formData, redirectRoute))
        },
        setPrefix(prefix){
            dispatch(Actions.setPrefix(prefix))
        },
        setPhone(phone){
            dispatch(Actions.setPhone(phone))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SMSVerification);

*/
