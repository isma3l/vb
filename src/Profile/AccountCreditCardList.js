import React, { Component } from 'react';
import {connect} from "react-redux";
import {Text,  View,  TouchableOpacity, ActivityIndicator, ScrollView, Platform, FlatList, Image} from 'react-native';
import {Colors} from "../Layout/Colors";
import {FadeInView} from "../AnimatedComponents";
import CardsActions from '../CreditCard/CreditCard.actions'
import {ProfileStyles, styles} from '../Styles'
import IconOcticons from "react-native-vector-icons/Octicons";
import {CardModalOptions} from './Modals'


class AccountCreditCardList extends Component {

  state = {
    visibleCardModal: false,
    selectedCardID: ""
  }

  componentDidMount() {
    this.props.getCreditCards(this.props.token);
  }

  renderSpinner = () => (
    <View style={styles.centerContainer}>
      <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
    </View>
  )

  renderItem = ({item, index}) => {
    const lastItem = index + 1 === this.props.cards.length
    const borderBottomWidth = lastItem === false ? 0.5 : 0

    return (
      <View style={[ProfileStyles.containerItemAddress,
        {borderBottomWidth: borderBottomWidth, flexDirection: "row", alignItems: "center", paddingTop:8}]}>
        <View style={{width: 60, paddingLeft: 0, alignSelf:"flex-start"}}>
          <Image
            source={{uri: item.TypePhoto.replace('http', 'https')}}
            style={{width: 50, height: 45}}
            resizeMode={"contain"}
          />
        </View>
        <View>
          <Text style={{fontSize: 15, color: Colors.secondaryTextColor}}>
            •••• •••• •••• {item.LastDigits}
          </Text>
          <Text style={{fontSize: 15, color: Colors.secondaryTextColor}}>
            Vto: {item.Expiration}
          </Text>
          {item.Main &&
          <View>
            <Text style={{fontWeight: "bold", marginTop: 10, fontSize: 13, marginBottom:4}}>Tarjeta Primaria</Text>
          </View>
          }
        </View>

        <TouchableOpacity style={ProfileStyles.iconItemAddress}
                          onPress={ () => {this.setState({selectedCardID: item.ID, visibleCardModal: true})}}>
          <IconOcticons
            name="kebab-vertical"
            size={18}
            color={Colors.primaryColor}
          />
        </TouchableOpacity>


      </View>
    )
  }


  render() {
    return this.props.loading ? this.renderSpinner() :
     (
      <FadeInView style={{flex: 1}}>
        <ScrollView style={{flex: 1, width:"100%"}}>
          <View style={[ProfileStyles.container, {marginTop:20}]}>

            <CardModalOptions
              visibleCardModal = {this.state.visibleCardModal}
              closeCardModal = { () => {this.setState({visibleCardModal:false})} }
              markCardAsMain = {
                () => {
                  this.props.markMainCard(this.props.token, this.state.selectedCardID)
                  this.setState({visibleCardModal: false})
                }
              }
              deleteCard = {
                () => {
                  this.props.deleteCard(this.props.token, this.state.selectedCardID)
                  this.setState({visibleCardModal: false})
                }
              }
            />


            <FlatList
              style={ProfileStyles.addresses}
              data={this.props.cards}
              keyExtractor={ item => item.ID.toString() }
              renderItem={this.renderItem}
            />

            <View style={ProfileStyles.linkNewAddress}>
              <TouchableOpacity
                onPress={() => { this.props.navigation.navigate("AddCreditCard", {redirectRoute: "AccountCreditCardList"}); }}
                style={ProfileStyles.containerLinkNewAddress}>
                <View style={[ProfileStyles.subContainerLinkNewAddress, {paddingTop:8}]}>
                  <Text style={{color:Colors.fbButtonColor}}>Agregar tarjeta</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>

        </ScrollView>
      </FadeInView>
    )
  }
}

const mapStateToProps = state => ({
  token: state.Login.token,
  loading: state.CreditCard.loading,
  cards: state.CreditCard.cards,
  currentCard: state.Deposit.currentCard,
})

const mapDispatchToProps = dispatch => {
  return {
    getCreditCards(token) {
      dispatch(CardsActions.loading(true))
      dispatch(CardsActions.getCreditCards(token));
    },
    deleteCard(token, cardID) {
      dispatch({type: "SET_CARDS_LOADING", loading: true});
      dispatch(CardsActions.deleteCard(token, cardID));
    },
    markMainCard(token, cardID) {
      dispatch({type: "SET_CARDS_LOADING", loading: true})
      dispatch(CardsActions.markMainCard(token, cardID))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AccountCreditCardList)
