import SchemaObject from 'schema-object'

export {ApiError} from '../schemas'

const Photo = new SchemaObject({
  ID: Number,
  Url: String,
  MainPhoto: Boolean,
})

const User = new SchemaObject({
  ID: String,
  Name: String,
  LastName: String,
  ProfilePicture: String,
})

const Attribute = new SchemaObject({
  Description: String,
})

const Category = new SchemaObject({
  ID: Number,
  Description: String,
  IsLeaf: Boolean,
  Rentable: Boolean,
})

const Status = new SchemaObject({
  ID: Number,
  Description: String,
})

const Item = new SchemaObject({
  ID: String,
  Name: String,
  Description: String,
  RentPrice: Number,
  OwnerData: User,
  Photos: [Photo],
  Attributes: [Attribute],
  Categories: [Category],
  Product: String,
  ObjectStatus: Status,
  RentEnabled: Boolean,
  Rentable: Boolean,
  NeedsAuthorization: Boolean,
})

export const GetCommunityObjectID = new SchemaObject({
  item: Item,
})

const ItemReduced = new SchemaObject({
  ID: String,
  Name: String,
  RentPrice: Number,
  OwnerData: User,
  Photo: [Photo],
})

export const GetCommunityObject = new SchemaObject({
  communityObjects: [ItemReduced],
  quantity: Number,
})
