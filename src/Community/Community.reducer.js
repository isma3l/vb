const initialState = {
    items: {},
    loading: true,
    loadingItem: true,
    products: [],
    params: {
        Limit: 10,
        Offset: 0
    },
    offsetMultiplier: 0,
    productDetails: {
        ID: "",
        OwnerData: {
            Name: "",
            LastName: "",
            Photos: []
        }
    },
    searchBar: false,
    blockTouch: false
};

const Community = (state = initialState, action) => {
    switch (action.type) {
        case "SET_LOADING_COMMUNITY":
            return {...state, loading: action.loading}
        case "SET_LOADING_COMMUNITY_ITEM":
            return {...state, loadingItem: action.loading}
        case "GET_COMMUNITY_ITEM":
            return {...state, productDetails: action.item};
        case "GET_COMMUNITY_ITEMS":
            return {...state, products: action.items, offsetMultiplier: 1 };
        case "GET_MORE_COMMUNITY_ITEMS":
            return {...state, products: [...state.products, ...action.items], offsetMultiplier: state.offsetMultiplier + 1}
        case "SET_PARAMS":
            return {...state, params: {...state.params, ...action.params}};
        case "SET_SEARCH_BAR":
            return {...state, searchBar: action.boolean};
        case "CLEAN_CATEGORIES":
            return {...state, params: {...initialState.params}}
        case "CLEAR_SEARCH":
            return {...state, params: {...state.params, name: undefined}}
        default:
            return state;
    }
};

export default Community;