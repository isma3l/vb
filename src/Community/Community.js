import React, { Component } from 'react';
import {connect} from 'react-redux';
import {ActivityIndicator, View, Platform, DeviceEventEmitter} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';
import Items from "../Items/Items";
import Actions from "./Community.actions";
import {Colors} from "../Layout/Colors";

import {styles} from "../Styles";
import {FadeInView} from "../AnimatedComponents";


class Community extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            tabBarLabel: 'Comunidad',
            headerTintColor: Colors.primaryColor
        }
    };

    componentDidMount() {
        this._sub = this.props.navigation.addListener(
            'didFocus',
            () => {
                this.props.cleanSearch(true);
                this.props.setSearchBar(false);
                this.props.getCommunityItems({...this.props.Community.params, Offset: 0});
            }
        );
    }

    componentWillUnmount() {
        this._sub.remove();
    }

    renderSpinner = () => (
        <View style={styles.centerContainer}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    renderItems = (products) => (
        <Items {...this.props}
            getInfo={this.props.getCommunityItems}
            loadMoreItems={this.props.loadMoreCommunityItems}
            multiplier={this.props.Community.offsetMultiplier}
            items={products}
            paramsSearch={this.props.Community.params}
            communityObjects={true}
        />
    )

    render() {
        const community = this.props.Community;
        const products = community.products;
        return (
            <FadeInView style={{minHeight: "100%"}}>
                {this.renderItems(products)}
            </FadeInView>
        )
    }
}

const mapStateToProps = state => {
    return {
        Community: state.Community,
        loading: state.Community.loading || state.Category.loadingCommunity,
        isCommunity: true
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        cleanSearch(){
            dispatch({type: "CLEAR_SEARCH"})
        },
        getItem(id){
            dispatch(Actions.setLoadingItem(true));
            dispatch(Actions.getItem(id))
        },
        getCommunityItems(params){
            dispatch(Actions.setLoading(true));
            dispatch(Actions.getCommunityItems(params))
        },
        loadMoreCommunityItems(params){
            dispatch(Actions.setLoading(true));
            dispatch(Actions.loadMoreCommunityItems(params))
        },
        setLoading(boolean){
            dispatch(Actions.setLoading(boolean))
        },
        setSearchBar(boolean){
            dispatch(Actions.setSearchBar(boolean))
        },
        setParams(params){
            dispatch(Actions.setParams(params))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Community);
