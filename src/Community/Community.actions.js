import {Alert} from 'react-native'
import {Community} from '../Endpoint'
import store from '../store'
import {apiPublic} from '../api'
import {ApiError, GetCommunityObjectID, GetCommunityObject} from './schemas'

export default {
  clearSearch() {
    return dispatch => {
      dispatch({type: 'CLEAR_SEARCH'})
      dispatch(this.setLoading(true))
      dispatch(this.getCommunityItems({...store.getState().Community.params, name: undefined}))
    }
  },
  getItem(id) {
    return dispatch => {
      apiPublic.get(Community + '/community/object/' + id)
        .then(response => {
          const parsed = new GetCommunityObjectID({item: response.data.description})
          dispatch({type: 'GET_COMMUNITY_ITEM', item: parsed.item})
          dispatch(this.setLoadingItem(false))
        })
        .catch(error => {
          dispatch(this.setLoadingItem(false))
          let message = ''
          if (error.response) {
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },

  getCommunityItems(params) {
    return dispatch => {
      apiPublic.get(Community + '/community/object', {params})
        .then(response => {
          dispatch(this.setLoading(false))
          const parsed = new GetCommunityObject(response.data.description)
          dispatch({type: 'GET_COMMUNITY_ITEMS', items: parsed.communityObjects})
        })
        .catch(error => {
          dispatch(this.setLoading(false))
          let message = ''
          if (error.response) {
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },

  loadMoreCommunityItems(params) {
    return dispatch => {
      apiPublic.get(Community + '/community/object', {params})
        .then(response => {
          dispatch(this.setLoading(false))
          const parsed = new GetCommunityObject(response.data.description)
          dispatch({type: 'GET_MORE_COMMUNITY_ITEMS', items: parsed.communityObjects})
        })
        .catch(error => {
          dispatch(this.setLoading(false))
          let message = ''
          if (error.response) {
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },

  setLoading(boolean) {
    return {type: 'SET_LOADING_COMMUNITY', loading: boolean}
  },


  setLoadingItem(boolean) {
    return {type: 'SET_LOADING_COMMUNITY_ITEM', loading: boolean}
  },

  setSearchBar(boolean) {
    return {type: 'SET_SEARCH_BAR', value: boolean}
  },

  setParams(params) {
    return dispatch => {
      dispatch({type: 'SET_PARAMS', params})
      dispatch(this.setLoading(true))
      dispatch(this.getCommunityItems(params))
    }
  },
}
