import TopTabs from './TopTabs'
import {notificationTabs as nav} from './routes'
import Activity from '../Notifications/Activity'
import Appointments from '../Notifications/Appointments'

const routes = {
  [nav.ACTIVITY]: {
    screen: Activity,
    title: 'Actividad',
  },
  [nav.APPOINTMENTS]: {
    screen: Appointments,
    title: 'Retiros',
  }
}

export default TopTabs(routes, nav.APPOINTMENTS)
