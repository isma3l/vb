import TopTabs from './TopTabs'
import {profileTabs as nav} from './routes'
import AccountCreditCardList from '../Profile/AccountCreditCardList'
import Profile from '../Profile/Profile'
import {View, Text} from 'react-native'
import React from "react";



const routes = {
  [nav.PROFILE]: {
    screen: Profile,
    title: 'Mi cuenta'
  },
  [nav.CARDS]: {
    screen: AccountCreditCardList,
    title: 'Tarjetas'
  }
}

export default TopTabs(routes, nav.PROFILE)
