import React from 'react'
import TopTabs from './TopTabs'
import {homeGuestTabs as nav} from './routes'
import Community from '../Community/Community'
import MyObjectsNotLoggedIn from '../MyObjects/MyObjectsNotLogged'
import Icon from "react-native-vector-icons/MaterialIcons";

const routes = {
  [nav.COMMUNITY]: {
    screen: Community,
    title: 'Comunidad',
  },
  [nav.MY_OBJECTS_NOT_LOGGED_IN]: {
    screen: MyObjectsNotLoggedIn,
    title: 'Mis ítems',
  }
}

const Component = TopTabs(routes, nav.MY_OBJECTS_NOT_LOGGED_IN)

Component.navigationOptions = ({navigation}) => {
  return {
    drawerIcon: ({tintColor}) => (
      <Icon name="home" size={24}/>
    ),
    drawerLabel: 'Home',
    headerTitle: 'Home',
  }
}

export default Component
