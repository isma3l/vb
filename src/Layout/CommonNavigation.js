import React from "react";
import {FloatingAction} from "react-native-floating-action";
import {SearchBar} from "react-native-elements";
import {NavigationActions, StackNavigator, TabBarTop, TabBarBottom} from "react-navigation";
import Icon from "react-native-vector-icons/MaterialIcons";
import {Platform, TouchableNativeFeedback, View} from "react-native";
import Icon2 from "react-native-vector-icons/Ionicons";

import Address from "../Profile/Address";
import ProductDetails from "../Items/ProductDetails";
import VerifyYourPhone from "../Profile/VerifyYourPhone";
import TransactionDetails from "../Notifications/TransactionDetails";
import ModifyEmail from "../Profile/ModifyEmail";
import NewDeposit from "../NewDeposit/NewDeposit";
import ModifyAvatar from "../Profile/ModifyAvatar";
import VerifyEmail from "../Profile/VerifyEmail";
import SMSVerification from "../Profile/SMSVerification";
import HomeGuest from './HomeGuestTabs'
import AddCreditCard from "../CreditCard/AddCreditCard";
import NewDepositDone from "../NewDeposit/Done";
import AddressList from "../Transactions/AddressList";
import Summary from "../NewDeposit/Summary";
import CreditCardList from "../Transactions/CreditCardList";
import ShareCatalog from "../ShareCatalog/ShareCatalog";
import ShareCatalogTabs from './ShareCatalogTabs'

import ButtonNative from "../MyComponents";
import {FadeInView} from "../AnimatedComponents";
import {Colors} from "./Colors";

import store from "../store";
import CommunityActions from "../Community/Community.actions";
import MyObjectActions from "../MyObjects/MyObjects.actions";
import SharedCatalogActions from "../ShareCatalog/ShareCatalog.actions";
import ChangePassword from "../Profile/ChangePassword"
import SelectRange from "../NewRent/SelectRange";
import DeliveryRent from "../NewRent/DeliveryRent";
import PickupRent from "../NewRent/PickupRent";
import RentSummary from "../NewRent/RentSummary";
import NotificationTabs from './NotificationTabs'
import AddAddress from "../NewDeposit/AddAddress";
import AddCreditCardTransaction from "../NewDeposit/AddCreditCardTransaction";
import Home from './HomeTabs'
import MyAccount from "../Profile/MyAccount";
import AccountTabs from './AccountTabs'
import AddCreditCardV2 from '../Profile/AddCreditCardV2'

const backgroundAnimation = Platform.Version >= 21 ? TouchableNativeFeedback.Ripple("grey", true) : null;

const getDrawerItem = navigation => {
    return (
        <ButtonNative
            onPress={() => {
                navigation.navigate('DrawerOpen');
            }}
            background={backgroundAnimation}
        >
            <View style={{alignItems: "center", justifyContent: "center", height: 50, width: 50}}>
                <Icon
                    name="menu"
                    size={26}
                    color={Colors.primaryColor}
                />
            </View>
        </ButtonNative>
    );
}

const goBackIconClose = navigation => {
    return (
        <ButtonNative
            onPress={() => {
                navigation.dispatch(NavigationActions.back());
            }}
            background={backgroundAnimation}
        >
            <View style={{alignItems: "center", justifyContent: "center", height: 50, width: 50}}>
                <Icon
                    name="close"
                    size={24}
                    color={Colors.primaryColor}
                />
            </View>
        </ButtonNative>
    );
}

const goBack = (navigation, backRoute = null) => {
    return (
        <ButtonNative
            onPress={() => {
                navigation.dispatch(NavigationActions.back({key: backRoute}));
            }}
            background={backgroundAnimation}
        >
            <View style={{alignItems: "center", justifyContent: "center", height: 50, width: 50}}>
                <Icon2
                    name="ios-arrow-back"
                    size={24}
                    color={Colors.primaryColor}
                />
            </View>
        </ButtonNative>
    );
}

const goToHomeCross = navigation => {
    return (
        <ButtonNative
            onPress={() => {
                navigation.navigate("Home");
            }}
            background={backgroundAnimation}
        >
            <View style={{alignItems: "center", justifyContent: "center", height: 50, width: 50}}>
                <Icon
                    name="close"
                    size={24}
                    color={Colors.primaryColor}
                />
            </View>
        </ButtonNative>
    );
}

const searchButton = (navigation) => (
    <ButtonNative
        onPress={() => {
            navigation.setParams({searchBar: true})
            store.dispatch({type: "SET_SEARCH_BAR", boolean: true})
        }}
        background={backgroundAnimation}
    >
        <View style={{alignItems: "center", justifyContent: "center", height: "100%", width: 50}}>
            <Icon
                name="search"
                size={26}
                color={Colors.primaryColor}
            />
        </View>
    </ButtonNative>
)

const searchBar = (searchParams, searchParamsMyObjects, token, userID) => (
    <FadeInView style={{width: "100%"}}>
        <SearchBar
            placeholder="Buscar..."
            onChangeText={text => {
                store.dispatch(CommunityActions.setParams({...searchParams, name: text}))
                store.dispatch(MyObjectActions.setParams({...searchParamsMyObjects, name: text}, token, userID))
            }}
            lightTheme
            loadingIcon={{color: Colors.primaryColor}}
            underlineColorAndroid={Colors.primaryColor}
            containerStyle={{width: "100%", backgroundColor: "transparent"}}
            inputStyle={{backgroundColor: "transparent"}}
            icon={{color: Colors.primaryColor}}
        />
    </FadeInView>
)

const cancelSearchButton = (navigation, token, userID) => (
    <FadeInView>
        <ButtonNative
            onPress={() => {
                navigation.setParams({searchBar: false});
                store.dispatch(CommunityActions.setSearchBar(false));
                store.dispatch(CommunityActions.clearSearch());
                store.dispatch(MyObjectActions.clearSearch(token, userID))
            }}
            background={backgroundAnimation}
        >
            <View style={{alignItems: "center", justifyContent: "center", height: 50, width: 50}}>
                <Icon
                    name="clear"
                    size={26}
                    color={Colors.primaryColor}
                />
            </View>
        </ButtonNative>
    </FadeInView>
)


const sharedCatalogSearchBar = (searchParams, token, userID) => (
    <FadeInView style={{width: "100%"}}>
        <SearchBar
            placeholder="Buscar..."
            onChangeText={text => {
                store.dispatch(SharedCatalogActions.setParams({...searchParams, name: text}, userID))
            }}
            lightTheme
            loadingIcon={{color: Colors.primaryColor}}
            underlineColorAndroid={Colors.primaryColor}
            containerStyle={{width: "100%", backgroundColor: "transparent"}}
            inputStyle={{backgroundColor: "transparent"}}
            icon={{color: Colors.primaryColor}}
        />
    </FadeInView>
)

const cancelSharedCatalogSearchButton = (navigation, token, userID) => (
    <FadeInView>
        <ButtonNative
            onPress={() => {
                navigation.setParams({searchBar: false});
                store.dispatch(SharedCatalogActions.clearSearch(userID))
            }}
            background={backgroundAnimation}
        >
            <View style={{alignItems: "center", justifyContent: "center", height: 50, width: 50}}>
                <Icon
                    name="clear"
                    size={26}
                    color={Colors.primaryColor}
                />
            </View>
        </ButtonNative>
    </FadeInView>
)

export const navigateOnce = (getStateForAction) => (action, state) => {
    const {type, routeName} = action;
    return (
        state &&
        type === NavigationActions.NAVIGATE &&
        routeName === state.routes[state.routes.length - 1].routeName
    ) ? null : getStateForAction(action, state);
    // you might want to replace 'null' with 'state' if you're using redux (see comments below)
};

const TabComponent = (props) => {
    const { navigationState } = props;

    let newProps = props;
    if (navigationState.index === 0) {
        newProps = Object.assign(
            {},
            props,
            {
                style: {
                    borderTopLeftRadius: 5,
                    borderBottomLeftRadius: 5
                }
            },
        );
    } else {
        newProps = Object.assign(
            {},
            props,
            {
                style: {
                    borderTopRightRadius: 5,
                    borderBottomRightRadius: 5
                }
            },
        );
    }
    if (Platform.OS === "android") {
        return <TabBarTop {...props} />
    } else {
        return <TabBarBottom {...newProps} />;
    }
};

const CatalogNavigation = (props) => {
    const navigation = props.navigation;
    const { params = {} } = navigation.state;
    const searchParams = store.getState().ShareCatalog.params;
    const token = store.getState().Login.token;
    const userID = store.getState().ShareCatalog.userID;

    let headerTitle = "Catálogo de " + navigation.state.params.name;
    let headerRight = searchButton(navigation);

    if (params.searchBar) {
        headerTitle = sharedCatalogSearchBar(searchParams, token, userID);
        headerRight = cancelSharedCatalogSearchButton(navigation, token, userID)
    }

    return {
        drawerLockMode: "locked-closed",
        headerTitle,
        headerRight,
        headerTintColor: Colors.primaryColor,
        gesturesEnabled: false
    }
}

export const ShareCatalogStack = StackNavigator({
    ShareCatalog: {
        screen: ShareCatalogTabs,
        navigationOptions: ({navigation}) => ({
            title: "Catalogos Compartidos",
            drawerIcon: ({ tintColor }) => (
                <Icon name="share" size={24}/>
            ),
            headerTintColor: Colors.primaryColor,
            headerLeft: Platform.OS !== "ios" ? getDrawerItem(navigation) : null,
        })
    },
    Catalog: {
        screen: ShareCatalog,
        navigationOptions: CatalogNavigation
    },
    ProductDetails: {
        screen: ProductDetails,
        navigationOptions: ({navigation}) => ({
            title: "Detalles",
            headerTintColor: Colors.primaryColor,
            drawerLockMode: "locked-closed"
        })
    }
}, {
    initialRouteName: "ShareCatalog"
});

const HomeWithButton = (props) => (
    <View style={{flex: 1}}>
        <Home navigation={props.navigation}/>
        <FloatingAction
            actions={[{
                icon: <Icon name="add" color='white' size={24}/>,
                name: "Nuevo depósito",
                position: 1,
                color: Colors.primaryColor
            }]}
            buttonColor={Colors.primaryColor}
            onPressItem={() => {
                props.navigation.navigate('NewDeposit')
            }}
            overrideWithAction={true}
        />
    </View>
)

HomeWithButton.router = Home.router;

HomeWithButton.navigationOptions = ({navigation}) => {
    return {
        drawerIcon: ({tintColor}) => (
            <Icon name="home" size={24}/>
        ),
        drawerLabel: 'Home',
        headerTitle: 'Home',
    }
}

const HomeNavigation = (props) => {
    const navigation = props.navigation;
    const { params = {} } = navigation.state;
    const searchParams = store.getState().Community.params;
    const searchParamsMyObjects = store.getState().MyObjects.params;
    const token = store.getState().Login.token;
    const userID = store.getState().Login.userID;

    if (Platform.OS === "android") {

        let headerLeft = getDrawerItem(navigation);
        let headerTitle = 'Home';
        let headerRight = searchButton(navigation);

        if (params.searchBar) {
            headerTitle = searchBar(searchParams, searchParamsMyObjects, token, userID);
            headerRight = cancelSearchButton(navigation, token, userID)
        }

        return {
            headerTitle,
            headerRight,
            headerLeft,
            headerTintColor: Colors.primaryColor,
            gesturesEnabled: false,
        }
    } else {
        return {
            headerTitle:
                <View style={{width: "100%", backgroundColor: Colors.backgroundColor}}>
                    <SearchBar
                      lightTheme round
                      placeholder="Buscar algo para rentar..."
                      inputStyle={{backgroundColor: "white", width: "90%", alignSelf: "center", textAlign: "center", borderRadius: 10}}
                      containerStyle={{marginTop: 20, backgroundColor: Colors.backgroundColor, borderTopWidth: 0, borderBottomWidth: 0}}
                      icon={{type: 'material', color: '#86939e', name: 'search', style: {fontSize: 24, left: 30, top: 12}}}
                      onChangeText={text => {
                          store.dispatch(CommunityActions.setParams({...searchParams, name: text}))
                          store.dispatch(MyObjectActions.setParams({...searchParamsMyObjects, name: text}, token, userID))
                      }}
                      loadingIcon={{color: Colors.primaryColor}}
                    />
                </View>,
            headerStyle: {
                paddingBottom: 10,
                borderBottomWidth: 0
            }
        }
    }
}


export const HomeStack = StackNavigator({
    Home: {
        screen: Platform.OS === "ios" ? Home : HomeWithButton,
        navigationOptions: HomeNavigation
    },
    ProductDetails: {
        screen: ProductDetails,
        navigationOptions: ({navigation}) => ({
            title: "Detalles",
            drawerLockMode: "locked-closed",
            headerTintColor: Colors.primaryColor
        })
    },
    SelectRange: {
        screen: SelectRange,
        navigationOptions: ({navigation}) => ({
            title: "Seleccionar días",
            drawerLockMode: "locked-closed",
            headerTintColor: Colors.primaryColor
        })
    },
    DeliveryRent: {
        screen: DeliveryRent,
        navigationOptions: ({navigation}) => ({
            title: "Delivery",
            drawerLockMode: "locked-closed",
            headerTintColor: Colors.primaryColor
        })
    },
    AddressList: {
        screen: AddressList,
        navigationOptions: ({navigation}) => ({
            title: "Seleccionar Dirección",
            drawerLockMode: "locked-closed",
            headerTintColor: Colors.primaryColor
        })
    },
    Address: {
        screen: Address,
        navigationOptions: ({navigation}) => ({
            title: "Nueva Dirección",
            drawerLockMode: "locked-closed",
            headerTintColor: Colors.primaryColor
        })
    },
    PickupRent: {
        screen: PickupRent,
        navigationOptions: ({navigation}) => ({
            title: "Pickup",
            drawerLockMode: "locked-closed",
            headerTintColor: Colors.primaryColor
        })
    },
    RentSummary: {
        screen: RentSummary,
        navigationOptions: ({navigation}) => ({
            title: "Resumen",
            drawerLockMode: "locked-closed",
            headerTintColor: Colors.primaryColor
        })
    },
    CreditCardList: {
        screen: CreditCardList,
        navigationOptions: ({navigation}) => ({
            title: "Tarjetas de crédito",
            drawerLockMode: "locked-closed",
            headerTintColor: Colors.primaryColor
        })
    },
    AddCreditCard: {
        screen: AddCreditCard,
        navigationOptions: ({navigation}) => ({
            title: "Añadir Tarjeta de crédito",
            drawerLockMode: "locked-closed",
            headerTintColor: Colors.primaryColor
        })
    }
}, {
    initialRouteName: "Home",
    disabledBackGesture: true
});

HomeStack.router.getStateForAction = navigateOnce(HomeStack.router.getStateForAction);

export const HomeGuestStack = StackNavigator({
    Home: {
        screen: HomeGuest,
        navigationOptions: HomeNavigation
    },
    ProductDetails: {
        screen: ProductDetails,
        navigationOptions: ({navigation}) => ({
            title: "Detalles",
            headerTintColor: Colors.primaryColor
        })
    }
}, {
    initialRouteName: "Home"
});

HomeGuestStack.router.getStateForAction = navigateOnce(HomeGuestStack.router.getStateForAction);


export const NotificationStack = StackNavigator({
    Notifications: {
        screen: NotificationTabs,
        navigationOptions: ({navigation}) => ({
            title: "Historial",
            headerLeft: Platform.OS !== "ios" ? getDrawerItem(navigation) : null,
            headerTintColor: Colors.primaryColor,
            headerStyle: {
                borderBottomWidth: 0,
                backgroundColor: Colors.backgroundColor,
            },
            drawerIcon: ({ tintColor }) => (
                <Icon name="notifications" size={24}/>
            ),
        })
    },
    TransactionDetails: {
        screen: TransactionDetails,
        navigationOptions: ({navigation}) => ({
            title: 'Detalles del ' + navigation.state.params.title,
            drawerLockMode: "locked-closed",
            headerTintColor: Colors.primaryColor
        })
    }
});

export const ProfileStack = StackNavigator({
    MyAccount: {
        screen: MyAccount,
        navigationOptions: ({navigation}) => ({
          headerLeft: Platform.OS !== "ios" ? getDrawerItem(navigation) : null,
          header: Platform.OS === "ios" ? null : <View/>,
          headerTintColor: Colors.primaryColor
        })
    },
    ProfileTabs: {
        screen: AccountTabs,
        navigationOptions: ({navigation}) => ({
          headerTitle: "Mi Cuenta",
          headerTintColor: Colors.primaryColor,
          headerStyle: Platform.OS === "ios" ? {borderBottomWidth: 0, backgroundColor: "transparent"} : {}
        })
    },
    ModifyAvatar: {
        screen: ModifyAvatar,
        navigationOptions: ({navigation}) => ({
            title: "Editar Perfil",
            headerTintColor: Colors.primaryColor
        })
    },
    SMSVerification: {
        screen: SMSVerification,
        navigationOptions: ({navigation}) => ({
            title: "Ingresar código",
            headerStyle: Platform.OS === "ios" ?
                {borderBottomWidth: 0, backgroundColor: Colors.backgroundColor} : {},
            drawerLockMode: "locked-closed",
            headerTintColor: Colors.primaryColor,
            headerLeft: null,
            headerRight: Platform.OS === "ios" && goBackIconClose(navigation)
        })
    },
    VerifyYourPhone: {
        screen: VerifyYourPhone,
        navigationOptions: ({navigation}) => ({
            title: "Verificar Teléfono",
            headerStyle: Platform.OS === "ios" ?
                {borderBottomWidth: 0, backgroundColor: Colors.backgroundColor} : {},
            drawerLockMode: "locked-closed",
            headerTintColor: Colors.primaryColor,
            headerRight: Platform.OS === "ios" && goBackIconClose(navigation),
            headerLeft: null
        })
    },
    ModifyEmail: {
        screen: ModifyEmail,
        navigationOptions: ({navigation}) => ({
            title: "Modificar Email",
            drawerLockMode: "locked-closed",
            headerTintColor: Colors.primaryColor
        })
    },
    VerifyEmail: {
        screen: VerifyEmail,
        navigationOptions: ({navigation}) => ({
            title: "Verificar Email",
            drawerLockMode: "locked-closed",
            headerTintColor: Colors.primaryColor
        })
    },
    Address: {
        screen: Address,
        navigationOptions: ({navigation}) => ({
            title: "Domicilio",
            drawerLockMode: "locked-closed",
            headerTintColor: Colors.primaryColor
        })
    },
    AddCreditCard: {
        screen: AddCreditCardV2,
        navigationOptions: ({navigation}) => {
            let options = {
              title: "Agregar Tarjeta de Pago",
              drawerLockMode: "locked-closed",
              headerTintColor: Colors.primaryColor,
              headerRight: Platform.OS === "ios" && goBackIconClose(navigation),
            }

            if(Platform.OS === "ios") {
              options.headerLeft = <View></View>
            }
            return options
        }
    },
    ChangePassword: {
        screen: ChangePassword,
        navigationOptions: ({navigation}) => ({
            title: "Cambiar Contraseña",
            drawerLockMode: "locked-closed",
            headerTintColor: Colors.primaryColor
        })
    }
}, {
    initialRouteName: "MyAccount",
    disabledBackGesture: true,
});

ProfileStack.router.getStateForAction = navigateOnce(ProfileStack.router.getStateForAction);

export const NewStack = StackNavigator({
    NewDeposit: {
        screen: NewDeposit,
        navigationOptions: ({navigation}) => ({
            title: "Nuevo Depósito",
            headerStyle: Platform.OS === "ios" ? {borderBottomWidth: 0, backgroundColor: "transparent"} : {},
            headerRight: Platform.OS === "ios" && goToHomeCross(navigation),
            headerTintColor: Colors.primaryColor
        })
    },
    AddAddress: {
        screen: AddAddress,
        navigationOptions: ({navigation}) => ({
            title: "Dirección de retiro",
            headerStyle: Platform.OS === "ios" ? {borderBottomWidth: 0, backgroundColor: Colors.backgroundColor} : {},
            headerRight: Platform.OS === "ios" && goToHomeCross(navigation),
            headerLeft: Platform.OS === "ios" && goBack(navigation),
            headerTintColor: Colors.primaryColor
        })
    },
    AddressList: {
        screen: AddressList,
        navigationOptions: ({navigation}) => ({
            title: "Seleccionar Dirección",
            drawerLockMode: "locked-closed",
            headerTintColor: Colors.primaryColor
        })
    },
    Address: {
        screen: Address,
        navigationOptions: ({navigation}) => ({
            title: "Nueva Dirección",
            drawerLockMode: "locked-closed",
            headerTintColor: Colors.primaryColor
        })
    },
    Summary: {
        screen: Summary,
        navigationOptions: ({navigation}) => ({
            title: "Resumen",
            drawerLockMode: "locked-closed",
            headerTintColor: Colors.primaryColor,
            headerStyle: Platform.OS === "ios" ? {borderBottomWidth: 0, backgroundColor: "transparent"} : {},
            headerRight: Platform.OS === "ios" && goToHomeCross(navigation),
            headerLeft: Platform.OS === "ios" && goBack(navigation),
        })
    },
    CreditCardList: {
        screen: CreditCardList,
        navigationOptions: ({navigation}) => ({
            title: "Tarjetas de crédito",
            drawerLockMode: "locked-closed",
            headerTintColor: Colors.primaryColor
        })
    },
    AddCreditCard: {
        screen: AddCreditCardTransaction,
        navigationOptions: ({navigation}) => ({
            title: "Añadir Tarjeta de crédito",
            drawerLockMode: "locked-closed",
            headerTintColor: Colors.primaryColor,
            headerStyle: Platform.OS === "ios" ? {borderBottomWidth: 0, backgroundColor: "transparent"} : {},
            headerRight: Platform.OS === "ios" && goToHomeCross(navigation),
            headerLeft: Platform.OS === "ios" && goBack(navigation),
        })
    },
    SMSVerification: {
        screen: SMSVerification,
        navigationOptions: ({navigation}) => ({
            title: "Ingresar código",
            headerStyle: Platform.OS === "ios" ?
                {borderBottomWidth: 0, backgroundColor: Colors.backgroundColor} : {},
            drawerLockMode: "locked-closed",
            headerTintColor: Colors.primaryColor,
            headerLeft: Platform.OS === "ios" && goBack(navigation),
            headerRight: Platform.OS === "ios" && goToHomeCross(navigation)
        })
    },
    VerifyYourPhone: {
        screen: VerifyYourPhone,
        navigationOptions: ({navigation}) => ({
            title: "Verificar Teléfono",
            headerStyle: Platform.OS === "ios" ?
                {borderBottomWidth: 0, backgroundColor: Colors.backgroundColor} : {},
            drawerLockMode: "locked-closed",
            headerTintColor: Colors.primaryColor,
            headerRight: Platform.OS === "ios" && goToHomeCross(navigation),
            headerLeft: null
        })
    },
    Done: {
        screen: NewDepositDone,
        navigationOptions: ({navigation}) => ({
          title: "Retiro Programado",
          drawerLockMode: "locked-closed",
          headerTintColor: Colors.primaryColor,
          headerStyle: Platform.OS === "ios" ? {borderBottomWidth: 0, backgroundColor: "transparent"} : {},
          headerRight: Platform.OS === "ios" && goToHomeCross(navigation),
          headerLeft: Platform.OS === "ios" && goBack(navigation),
        })
    }
}, {
    headerMode: "screen",
    initialRouteName: "NewDeposit"
});

NewStack.router.getStateForAction = navigateOnce(NewStack.router.getStateForAction);


export const LoginDrawer = ({navigation}) => navigation.navigate("Login", null);

LoginDrawer.navigationOptions =  {
    drawerLabel: 'Login',
    drawerIcon: ({ tintColor }) => (
        <Icon name="exit-to-app" size={24} />
    )
};

export const LogOut = ({navigation}) => navigation.navigate("LayoutGuest", null, "LOG_OUT");

LogOut.navigationOptions =  {
    drawerLabel: 'Salir',
    drawerIcon: ({ tintColor }) => (
        <Icon name="exit-to-app" size={24} />
    )
};


export const getCurrentRouteName = state => {
    const {index, routes, routeName} = state

    if(index !== undefined) {
        return getCurrentRouteName(routes[index])
    } else {
        return routeName
    }
}
