import React from 'react';
import {NavigationActions, StackNavigator, TabNavigator} from 'react-navigation';
import {Text, Image, View, Platform, Dimensions} from 'react-native'
import CategoryActions from "../Categories/Categories.actions";
import Login from '../Login/Login';
import Actions from "../Login/Login.actions";
import storeStorage from "react-native-simple-store";
import store from "../store"
import {Colors} from "./Colors";
import LogoScreen from "../LogoScreen/LogoScreen";
import SignUp from "../Login/SignUp"
import SMSVerification from "../Profile/SMSVerification";
import VerifyYourPhone from "../Profile/VerifyYourPhone";
import ProductDetails from "../Items/ProductDetails";
import ModifyAvatar from "../Profile/ModifyAvatar";
import ForgetPassword from "../Login/ForgetPassword";
import RecoverNewPassword from "../Login/RecoverNewPassword";
import ModifyEmail from "../Profile/ModifyEmail";
import VerifyEmail from "../Profile/VerifyEmail";
import Address from "../Profile/Address";
import LinearGradient from "react-native-linear-gradient";
import {
    HomeGuestStack, HomeStack, NewStack, ProfileStack, NotificationStack, ShareCatalogStack,
    navigateOnce
} from "./CommonNavigation";
import Welcome from "../Login/Welcome";
import { Icon as IconElements } from 'react-native-elements'
import {ICON_ACCOUNT, ICON_CHAT, ICON_HOME, ICON_NOTIFICATIONS} from '../Images'
import {getCurrentRouteName} from './CommonNavigation'


const tabBarLabel = (focused, text) => (
    Dimensions.get('window').width > 320
        ? <Text style={{fontSize: 11, color: focused ? '#5B687C' : '#5B687C'}}>{text}</Text>
        : null
)

const tabBarIcon = (focused, icon) => (
    <Image source={icon[focused ? 'ACTIVE' : 'INACTIVE']} style={{height: 24, width: 24}}/>
)

const Layout = TabNavigator({
    Home: {
        screen: HomeStack,
        navigationOptions: {
            tabBarIcon: ({focused}) => tabBarIcon(focused, ICON_HOME),
            tabBarLabel: ({focused}) => tabBarLabel(focused, "Inicio"),
        }
    },
    ShareCatalog: {
        screen: ShareCatalogStack,
        navigationOptions: {
            tabBarIcon: ({focused}) => tabBarIcon(focused, ICON_CHAT),
            tabBarLabel: ({focused}) => tabBarLabel(focused, "Compartidos"),
        }
    },
    NewItem: {
        screen: NewStack,
        navigationOptions: ({navigation}) => ({
            tabBarIcon: ({ tintColor }) => (
                <View style={{marginTop: 15}}>
                    <View style={{height: 36, width: 36, justifyContent: "center", borderRadius: 36, overflow: "hidden"}}>
                        <LinearGradient colors={['#FF4378', '#FFBA5F']} start={{x: 0, y: 0}} end={{x: 1, y: 1}} style={{flex: 1, padding: 6}}>
                            <IconElements name="add" size={24} color="white"/>
                        </LinearGradient>
                    </View>
                </View>
            ),
            tabBarLabel: " ",
            showLabel: false,
            tabBarOnPress: ({scene, jumpToIndex}) => {
                if (!scene.focused) {
                    jumpToIndex(scene.index)
                    navigation.dispatch(NavigationActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({routeName: 'NewDeposit'})]
                    }))
                }
            },
        })
    },
    Notifications: {
        screen: NotificationStack,
        navigationOptions: {
            tabBarIcon: ({focused}) => tabBarIcon(focused, ICON_NOTIFICATIONS),
            tabBarLabel: ({focused}) => tabBarLabel(focused, "Avisos"),
        }
    },
    Profile: {
        screen: ProfileStack,
        navigationOptions: {
            tabBarIcon: ({focused}) => tabBarIcon(focused, ICON_ACCOUNT),
            tabBarLabel: ({focused}) => tabBarLabel(focused, "Mi cuenta"),
        }
    },
}, {
    initialRouteName: "Home",
    disabledBackGesture: true,
    tabBarOptions: {
        activeTintColor: Colors.primaryColor,
        activeBackgroundColor: "white",
        inactiveTintColor: Colors.primaryColor,
        inactiveBackgroundColor: "white",
        style: {
            justifyContent: "center",
            height: 64,
            alignItems: "center",
            alignContent: "center",
            borderTopWidth: 0,
            shadowColor: '#000',
            shadowOffset: { width: 0, height: -1 },
            shadowOpacity: 0.2,
            shadowRadius: 2,
        },
        tabStyle: {
            justifyContent: "center",
            position: "relative",
            paddingVertical: 10,
            alignItems: "center",
            alignContent: "center"
        },
    }
})

const prevGetStateForActionLayout = Layout.router.getStateForAction;

Layout.router.getStateForAction = (action, state) => {
    if (action.type === 'Navigation/BACK' && state) {

      const currentRouteName = getCurrentRouteName(state)

      if (state.routes[0].routes[state.routes[0].index].routeName === "ProductDetails")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[state.routes[0].index].routeName === "SelectRange")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[state.routes[0].index].routeName === "DeliveryRent")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[state.routes[0].index].routeName === "AddressList")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[state.routes[0].index].routeName === "Address")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[state.routes[0].index].routeName === "PickupRent")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[state.routes[0].index].routeName === "RentSummary")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[state.routes[0].index].routeName === "CreditCardList")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[state.routes[0].index].routeName === "AddCreditCard")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[3].routes[state.routes[3].index].routeName === "TransactionDetails")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[1].routes[state.routes[1].index].routeName === "ProductDetails")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[1].routes[state.routes[1].index].routeName === "Catalog")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[4].routes[state.routes[4].index].routeName === "ModifyAvatar")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[4].routes[state.routes[4].index].routeName === "VerifyYourPhone")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[4].routes[state.routes[4].index].routeName === "SMSVerification")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[4].routes[state.routes[4].index].routeName === "ModifyEmail")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[4].routes[state.routes[4].index].routeName === "VerifyEmail")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[4].routes[state.routes[4].index].routeName === "Address")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[4].routes[state.routes[4].index].routeName === "ChangePassword")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[4].routes[state.routes[4].index].routeName === "AddCreditCard")
            return prevGetStateForActionLayout(action, state)
        if (state.routes[2].routes[state.routes[2].index].routeName === "AddAddress")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[2].routes[state.routes[2].index].routeName === "NewDeposit")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[2].routes[state.routes[2].index].routeName === "AddressList")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[2].routes[state.routes[2].index].routeName === "Address")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[2].routes[state.routes[2].index].routeName === "Summary")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[2].routes[state.routes[2].index].routeName === "CreditCardList")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[2].routes[state.routes[2].index].routeName === "AddCreditCard")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[2].routes[state.routes[2].index].routeName === "SMSVerification")
            return prevGetStateForActionLayout(action, state);
        if (currentRouteName === "PROFILE" || currentRouteName === "CARDS")
            return prevGetStateForActionLayout(action, state);
        return null
    }

    return prevGetStateForActionLayout(action, state);
}

const RootNavigator = StackNavigator({
    LogoScreen: {
        screen: LogoScreen,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
    LayoutGuest: {
        screen: HomeGuestStack,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
    Login: {
        screen: Login,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
    Welcome: {
        screen: Welcome,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
    Layout: {
        screen: Layout,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
    SignUp: {
        screen: SignUp,
        navigationOptions: ({navigation}) => ({
            headerTitle: "Sign Up",
            header: null
        })
    },
    SendRecoveryCode: {
        screen: ForgetPassword,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
    RecoverNewPassword: {
        screen: RecoverNewPassword,
        navigationOptions: ({navigation}) => ({
            header: null
        }),
    },
    VerifyEmail: {
        screen: VerifyEmail,
        navigationOptions: ({navigation}) => ({
            headerTitle: "Verificar Email"
        })
    }
}, {
    initialRouteName: "LogoScreen",
    navigationOptions: {
        backButtonTitle: '',
        headerTintColor: Colors.primaryColor,
        headerStyle: {
            backgroundColor: "white"
        }
    }
});

const prevGetStateForAction = RootNavigator.router.getStateForAction;

RootNavigator.router.getStateForAction = (action, state) => {
    // Do not allow to go back from Home
    if (action.routeName) {
        store.dispatch(Actions.setActualScreen(action.routeName));
    }

    if (action.type === 'Navigation/BACK'){
        try {
            store.dispatch(Actions.backScreen())
        }
        catch(err) {
            console.log(err);
        }
    }

    if (Platform.OS === "android" && action.type === 'Navigation/BACK' && state && state.routes[state.index].routeName === "Login") {
        return prevGetStateForAction({type: "Navigation/NAVIGATE", routeName: "LayoutGuest"}, state);
    }

    if (action.action === "LOG_OUT"){
        store.dispatch(CategoryActions.cleanCategories())
        storeStorage.delete("user_id")
            .then(rs => {
                storeStorage.delete("token")
                    .then(response => {
                        store.dispatch({type: "REMOVE_TOKEN"})
                        return prevGetStateForAction(action, state);
                    })
            })
    }
    return prevGetStateForAction(action, state);
};

RootNavigator.router.getStateForAction = navigateOnce(RootNavigator.router.getStateForAction);

export default RootNavigator;
