import TopTabs from './TopTabs'
import {homeTabs as nav} from './routes'
import Community from '../Community/Community'
import MyObjects from '../MyObjects/MyObjects'

const routes = {
  [nav.COMMUNITY]: {
    screen: Community,
    title: 'Comunidad',
  },
  [nav.MY_OBJECTS]: {
    screen: MyObjects,
    title: 'Mis ítems',
  }
}

export default TopTabs(routes, nav.MY_OBJECTS)
