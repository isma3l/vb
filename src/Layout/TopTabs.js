import React from 'react'
import {Platform, Text, View, StyleSheet} from 'react-native'
import {TabNavigator, TabBarTop} from 'react-navigation'
import {Colors} from './Colors'

const TabsOptions = {
  ios: {
    activeTintColor: '#FFF',
    activeBackgroundColor: Colors.primaryColor,
    inactiveTintColor: Colors.primaryColor,
    inactiveBackgroundColor: 'transparent',
    showIcon: false,
    showLabel: true,
    tabStyle: {
      justifyContent: 'center',
      padding: 0,
      margin: 0,
    },
    indicatorStyle: {
      backgroundColor: 'transparent',
    },
    labelStyle: {
      justifyContent: 'center',
      fontSize: 13,
      margin: 0,
      marginTop: 0,
      fontWeight: 'bold',
      alignSelf: 'center',
    },
    style: {
      backgroundColor: Colors.backgroundColor,
      width: '90%',
      alignSelf: 'center',
      height: 30,
      borderColor: Colors.primaryColor,
      borderWidth: 1,
      borderTopColor: Colors.primaryColor,
      borderBottomColor: Colors.primaryColor,
      borderRadius: 4,
      margin: 10,
    },
  },
  android: {
    activeTintColor: 'black',
    inactiveTintColor: 'black',
    showIcon: false,
    showLabel: true,
    upperCaseLabel: true,
    tabStyle: {
      flexDirection: 'row',
    },
    indicatorStyle: {
      backgroundColor: Colors.secondaryColor,
    },
    labelStyle: {
      fontWeight: '500',
    },
    style: {
      backgroundColor: 'white',
      justifyContent: 'center',
    },
    activeBackgroundColor: Colors.primaryColor,
    inactiveBackgroundColor: 'transparent',
  },
}

const initialConfig = {
  tabBarComponent: TabBarTop,
  tabBarPosition: 'top',
  tabBarOptions: Platform.select(TabsOptions)
}

const tblStyle = StyleSheet.create({
  container: {
    width: '100%',
    height: 30,
    justifyContent: 'center',
  },
  text: {
    fontSize: 13,
    textAlign: 'center',
  },
})

const containerFocusedStyle = focused => ({
  backgroundColor: focused ? Colors.primaryColor : '#FFF',
})

const textFocusedStyle = focused => ({
  color: focused ? '#FFF' : Colors.primaryColor,
})

/**
 * Creates top tabs that follow the given design.
 *
 * @param routes
 * @param initialRouteName
 * @param config Make sure you have a good reason to change this, otherwise leave it be
 */
export default (routes, initialRouteName, config = initialConfig) => {
  if (typeof routes[initialRouteName] === 'undefined') throw Error(`Initial route name '${initialRouteName}' is invalid`)
  const keys = Object.keys(routes)
  for (let i = 0; i < keys.length; i++) {
    if (routes[keys[i]].hasOwnProperty('navigationOptions') === false) {
      routes[keys[i]].navigationOptions = ({navigation}) => ({
        tabBarLabel: ({focused}) => {
          const {routeName} = navigation.state
          const title = routes[keys[i]].hasOwnProperty('title') ? routes[keys[i]].title : routeName
          return (
            <View style={[tblStyle.container, containerFocusedStyle(focused)]}>
              <Text style={[tblStyle.text, textFocusedStyle(focused)]}>
                {title}
              </Text>
            </View>
          )
        },
      })
    }
  }
  return TabNavigator(routes, {initialRouteName, ...config})
}
