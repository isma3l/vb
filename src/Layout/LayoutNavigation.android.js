import React from 'react';
import { StackNavigator, DrawerNavigator} from 'react-navigation';
import CategoryActions from "../Categories/Categories.actions";
import Login from '../Login/Login';
import Actions from "../Login/Login.actions";
import storeStorage from "react-native-simple-store";
import store from "../store";
import {Colors} from "./Colors";
import LogoScreen from "../LogoScreen/LogoScreen";
import SignUp from "../Login/SignUp"
import SMSVerification from "../Profile/SMSVerification";
import VerifyYourPhone from "../Profile/VerifyYourPhone";
import ProductDetails from "../Items/ProductDetails";
import ModifyAvatar from "../Profile/ModifyAvatar";
import ForgetPassword from "../Login/ForgetPassword";
import RecoverNewPassword from "../Login/RecoverNewPassword";
import ModifyEmail from "../Profile/ModifyEmail";
import VerifyEmail from "../Profile/VerifyEmail";
import Address from "../Profile/Address";
import {
    HomeGuestStack, HomeStack, NewStack, ProfileStack, NotificationStack, LoginDrawer, LogOut,
    ShareCatalogStack
} from "./CommonNavigation"
import AddCreditCard from "../CreditCard/AddCreditCard";
import Welcome from "../Login/Welcome";
import {getCurrentRouteName} from './CommonNavigation'


const Layout = DrawerNavigator({
    Home: {
        screen: HomeStack
    },
    Notifications: {
        screen: NotificationStack
    },
    Profile: {
        screen: ProfileStack
    },
    ShareCatalog: {
        screen: ShareCatalogStack
    },
    NewDeposit: {
        screen: NewStack
    },
    Logout: {
        screen: LogOut
    }
}, {
    initialRouteName: "Home",
    contentOptions: {
        activeTintColor: Colors.primaryColor
    },
})

const LayoutGuest= DrawerNavigator({
    Home: {
        screen: HomeGuestStack
    },
    LoginDrawer: {
        screen: LoginDrawer
    }
}, {
    initialRouteName: "Home",
    contentOptions: {
        activeTintColor: Colors.primaryColor
    },
})

const prevGetStateForActionLayoutGuest = LayoutGuest.router.getStateForAction;

LayoutGuest.router.getStateForAction = (action, state) => {
    if (action.type === 'Navigation/BACK' && state) {
        if (state.routes[0].routes[0].routes[state.routes[0].routes[0].index].routeName === "ProductDetails")
            return prevGetStateForActionLayout(action, state);
        return null
    }

    return prevGetStateForActionLayoutGuest(action, state);
}

const prevGetStateForActionLayout = Layout.router.getStateForAction;

Layout.router.getStateForAction = (action, state) => {
    if (action.type === 'Navigation/BACK' && state) {

        const currentRouteName = getCurrentRouteName(state)

        if (state.routes[0].routes[0].routes[state.routes[0].routes[0].index].routeName === "ProductDetails")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[0].routes[state.routes[0].routes[0].index].routeName === "SelectRange")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[0].routes[state.routes[0].routes[0].index].routeName === "DeliveryRent")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[0].routes[state.routes[0].routes[0].index].routeName === "AddressList")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[0].routes[state.routes[0].routes[0].index].routeName === "Address")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[0].routes[state.routes[0].routes[0].index].routeName === "PickupRent")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[0].routes[state.routes[0].routes[0].index].routeName === "RentSummary")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[0].routes[state.routes[0].routes[0].index].routeName === "CreditCardList")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[0].routes[state.routes[0].routes[0].index].routeName === "AddCreditCard")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[1].routes[state.routes[0].routes[1].index].routeName === "TransactionDetails")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[2].routes[state.routes[0].routes[2].index].routeName === "ModifyAvatar")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[2].routes[state.routes[0].routes[2].index].routeName === "VerifyYourPhone")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[2].routes[state.routes[0].routes[2].index].routeName === "SMSVerification")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[2].routes[state.routes[0].routes[2].index].routeName === "ModifyEmail")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[2].routes[state.routes[0].routes[2].index].routeName === "VerifyEmail")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[2].routes[state.routes[0].routes[2].index].routeName === "ChangePassword")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[2].routes[state.routes[0].routes[2].index].routeName === "Address")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[2].routes[state.routes[0].routes[2].index].routeName === "AddCreditCard")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[3].routes[state.routes[0].routes[3].index].routeName === "ProductDetails")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[3].routes[state.routes[0].routes[3].index].routeName === "Catalog")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[4].routes[state.routes[0].routes[4].index].routeName === "AddressList")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[4].routes[state.routes[0].routes[4].index].routeName === "Address")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[4].routes[state.routes[0].routes[4].index].routeName === "Summary")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[4].routes[state.routes[0].routes[4].index].routeName === "CreditCardList")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[4].routes[state.routes[0].routes[4].index].routeName === "AddCreditCard")
            return prevGetStateForActionLayout(action, state);
        if (state.routes[0].routes[4].routes[state.routes[0].routes[4].index].routeName === "AddAddress")
            return prevGetStateForActionLayout(action, state);
        if (currentRouteName === "PROFILE" || currentRouteName === "CARDS")
            return prevGetStateForActionLayout(action, state);

        return null
    }

    return prevGetStateForActionLayout(action, state);
}

const RootNavigator = StackNavigator({
    LogoScreen: {
        screen: LogoScreen,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
    LayoutGuest: {
        screen: LayoutGuest,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
    Login: {
        screen: Login,
        navigationOptions: ({navigation}) => ({
            headerTitle: "Login",
            header: null
        })
        },
        Welcome: {
            screen: Welcome,
            navigationOptions: ({navigation}) => ({
                header: null
            })
        },
    Layout: {
        screen: Layout,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
    SignUp: {
        screen: SignUp,
        navigationOptions: ({navigation}) => ({
            headerTitle: "Sign Up",
            header: null
        })
    },
    SendRecoveryCode: {
        screen: ForgetPassword,
        navigationOptions: ({navigation}) => ({
            headerTitle: "Enviar Código de recuperación"
        })
    },
    RecoverNewPassword: {
        screen: RecoverNewPassword,
        navigationOptions: ({navigation}) => ({
            headerTitle: "Rsestablecer contraseña"
        })
    },
    VerifyEmail: {
        screen: VerifyEmail,
        navigationOptions: ({navigation}) => ({
            headerTitle: "Verificar Email"
        })
    }
}, {
    initialRouteName: "LogoScreen",
    navigationOptions: {
        backButtonTitle: '',
        headerTintColor: Colors.primaryColor,
        headerStyle: {
            backgroundColor: "white"
        }
    }
});

const prevGetStateForAction = RootNavigator.router.getStateForAction;

RootNavigator.router.getStateForAction = (action, state) => {
    // Do not allow to go back from Home
    if (action.routeName) {
        store.dispatch(Actions.setActualScreen(action.routeName));
    }

    if (action.type === 'Navigation/BACK'){
        try {
            store.dispatch(Actions.backScreen())
        }
        catch(err) {
            console.log(err);
        }
    }

    if (action.type === 'Navigation/BACK' && state && state.routes[state.index].routeName === "Login") {
        return prevGetStateForAction({type: "Navigation/NAVIGATE", routeName: "LayoutGuest"}, state);
    }

    if (action.action === "LOG_OUT"){
        store.dispatch(CategoryActions.cleanCategories())
        storeStorage.delete("user_id")
            .then(rs => {
                storeStorage.delete("token")
                    .then(response => {
                        store.dispatch({type: "REMOVE_TOKEN"})
                        return prevGetStateForAction(action, state);
                    })
            })
    }
    return prevGetStateForAction(action, state);
};

export default RootNavigator;
