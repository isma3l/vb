import TopTabs from './TopTabs'
import {shareCatalogTabs as nav} from './routes'
import SharedWithMe from '../ShareCatalog/SharedWithMe'
import SharedWithOthers from '../ShareCatalog/SharedWithOthers'

const routes = {
  [nav.SHARED_WITH_ME]: {
    screen: SharedWithMe,
    title: 'Compartidos conmigo',
  },
  [nav.SHARED_WITH_OTHERS]: {
    screen: SharedWithOthers,
    title: 'Compartidos con otros',
  }
}

export default TopTabs(routes, nav.SHARED_WITH_ME)
