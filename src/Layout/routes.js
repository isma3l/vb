function routes(keys) {
  const route = {}
  for (let i = 0; i < keys.length; i++) route[keys[i]] = keys[i]
  return route
}

export const shareCatalogTabs = routes(['SHARED_WITH_ME', 'SHARED_WITH_OTHERS'])
export const notificationTabs = routes(['ACTIVITY', 'APPOINTMENTS'])
export const homeTabs = routes(['COMMUNITY', 'MY_OBJECTS'])
export const homeGuestTabs = routes(['COMMUNITY', 'MY_OBJECTS_NOT_LOGGED_IN'])
export const profileTabs = routes(['PROFILE', 'CARDS'])
