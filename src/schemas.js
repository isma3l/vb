import SchemaObject from 'schema-object'

export const Address = new SchemaObject({
  ID: Number,
  Address: String,
  Floor: String,
  Apartment: String,
  MainAddress: Boolean,
  Latitude: Number,
  Longitude: Number,
  PostalCode: {
    type: String,
    transform: value => {
      return value.PostalCode
    },
  },
  InDeliveryRange: Boolean,
})

export const User = new SchemaObject({
  ID: String,
  Name: String,
  LastName: String,
  Email: {
    type: String,
    transform: value => {
      return value.Email
    },
  },
  Phone: String,
  ProfilePicture: {
    type: String,
    transform: value => {
      return value.Url
    },
  },
})

export const TimeSlot = new SchemaObject({
  ID: Number,
  Hour: Number,
  Minute: Number,
})

export const TransactionReserve = new SchemaObject({
  ID: Number,
  Date: Date,
  TimeSlotID: Number,
  TimeSlot: TimeSlot,
})

export const CreditCard = new SchemaObject({
  ID: String,
  Main: Boolean,
  LastDigits: String,
  Type: String,
  TypePhoto: String,
})

export const ApiError = new SchemaObject({
  description: String,
  detail: {
    type: String,
    invisible: true,
  },
})
