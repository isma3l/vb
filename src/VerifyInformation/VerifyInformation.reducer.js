const initialState = {
    loading: true,
    missingInformation: [],
    redirectRoute: undefined
};

const VerifyInformation = (state = initialState, action) => {
    switch (action.type) {
        case "SET_LOADING_VERIFY_INFORMATION":
            return {...state, loading: action.loading}
        case "SET_MISSING_INFORMATION":
            return {...state, missingInformation: action.array}
        case "SET_REDIRECT_ROUTE":
            return {...state, redirectRoute: action.route}
        default:
            return state;
    }
};

export default VerifyInformation;