import {UserProfile} from '../Endpoint'
import {NavigationActions} from 'react-navigation'
import {Alert} from 'react-native'
import {navigatorRef} from '../../App'
import {apiPrivate} from '../api'
import {ApiError} from './schemas'

export default {
  setLoading(boolean) {
    return {type: 'SET_LOADING_VERIFY_INFORMATION', loading: boolean}
  },
  verifyInformation(route = 'NewDeposit') {
    return dispatch => {
      apiPrivate.get(UserProfile + '/user/missingInformation')
        .then(response => {
          dispatch(this.setLoading(false))

          const missingInfo = response.data.description.missingInformation.filter(info => info !== 'email')
          dispatch({type: 'SET_MISSING_INFORMATION', array: missingInfo})

          if (missingInfo.length > 0) {
            dispatch({type: 'SET_REDIRECT_ROUTE', route})

            function redirectTo(routeName) {
              navigatorRef.dispatch(NavigationActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({routeName})],
              }))
            }

            switch (missingInfo[0]) {
              case 'phone':
                redirectTo('VerifyYourPhone')
                break
              case 'address':
                redirectTo('AddAddress')
                break
              case 'card':
                redirectTo('AddCreditCard')
                break
              default:
            }
          } else {
            dispatch({type: 'SET_REDIRECT_ROUTE', route: undefined})
          }
        })
        .catch(error => {
          dispatch(this.setLoading(false))
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
          navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Home'}))
        })
    }
  },
  getMissingInfo() {
    return dispatch => {
      apiPrivate.get(UserProfile + '/user/missingInformation')
        .then(response => {
          dispatch(this.setLoading(false))
          const missingInfo = response.data.description.missingInformation
          dispatch({type: 'SET_MISSING_INFORMATION', array: missingInfo})
        })
        .catch(error => {
          dispatch(this.setLoading(false))
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },
}
