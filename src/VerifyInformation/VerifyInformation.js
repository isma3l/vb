import React, { Component } from 'react';
import {View, ActivityIndicator} from 'react-native'
import Actions from './VerifyInformation.actions';
import {connect} from "react-redux";
import {FadeInView} from "../AnimatedComponents";
import {Colors} from "../Layout/Colors";
import {styles} from "../Styles";

class VerifyInformation extends Component {
    componentDidMount() {
        this.props.verifyInformation(this.props.redirectRoute)
    }


    renderSpinner = () => (
        <View style={styles.centerContainer}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    render() {
        if (this.props.loading) {
            return this.renderSpinner()
        } else {
            return (
                <FadeInView>
                    {this.props.children}
                </FadeInView>
            )
        }
    }
}

const mapStateToProps = state => {
    return {
        loading: state.VerifyInformation.loading,
        token: state.Login.token
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        verifyInformation(redirectRoute){
            dispatch(Actions.setLoading(true));
            dispatch(Actions.verifyInformation(redirectRoute))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(VerifyInformation);
