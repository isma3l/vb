import React, {Component} from 'react'
import {connect} from 'react-redux'
import {ActivityIndicator, View, FlatList, Text, ScrollView, Platform, DeviceEventEmitter} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Actions from './Notifications.actions'
import {Colors} from '../Layout/Colors'
import {FadeInView} from '../AnimatedComponents'
import LinearGradient from 'react-native-linear-gradient'

const styles = {
  list: {
    paddingTop: 20, paddingRight: 10, paddingBottom: 10, paddingLeft: 10,
  },
  noEvents: {
    container: {
      backgroundColor: 'transparent', padding: 10,
    },
    text: {
      fontSize: 25, textAlign: 'center',
    },
  },
  item: {
    wrapper: {
      paddingTop: 5, paddingRight: 2, paddingBottom: 5, paddingLeft: 2
    },
    background: {
      borderRadius: 5,
      backgroundColor: 'white',
      shadowColor: 'black',
      shadowRadius: 2,
      shadowOffset: {width: 0, height: 1},
      shadowOpacity: 0.2,
    },
    container: {
      paddingTop: 12, paddingRight: 8, paddingBottom: 10, paddingLeft: 16,
    },
    paddedContainer: {
      paddingRight: 12,
    },
    title: {
      fontSize: 13, fontWeight: 'bold', marginBottom: 5, backgroundColor: 'transparent',
    },
    description: {
      fontSize: 15, marginBottom: 15, backgroundColor: 'transparent',
    },
    date: {
      fontSize: 12, opacity: .65, backgroundColor: 'transparent',
    },
    icon: {
      backgroundColor: 'transparent',
    },
  },
}

class Appointments extends Component {

  titlesByType = {
    1: 'Retiro programado',
    2: 'Envío programado',
    3: 'Retiro programado',
    4: 'Envío programado',
    5: 'Retiro programado',
  }

  descriptionsByType = {
    1: 'Solicitaste un retiro',
    2: 'Solicitaste un envío',
    3: 'Solicitaste un retiro',
    4: 'Solicitaste un envío',
    5: 'Solicitaste un retiro',
  }

  static navigationOptions = ({navigation}) => {
    return {
      tabBarLabel: 'Retiros',
      headerTintColor: Colors.primaryColor,
      tabBarIcon: ({tintColor}) => (
        <Icon name="calendar-blank" size={24} color={tintColor}/>
      ),
    }
  }

  componentDidMount() {
    this._sub = this.props.navigation.addListener(
      'didFocus',
      () => this.props.getAppointments()
    );
  }

  componentWillUnmount() {
    this._sub.remove();
  }

  renderSpinner = () => (
    <View style={styles.centerContainer}>
      <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
    </View>
  )

  renderAppointmentView = (appointment) => {

    const itemMoment = appointment.getMoment()
    const isUpcoming = itemMoment.isAfter()
    const date = itemMoment.format('D MMMM YYYY')

    let title = ''
    let description = ''

    let gradient = []
    let titleColor = ''
    let textColor = ''

    //TODO check if cancelled or not
    if (isUpcoming) {

      title = this.titlesByType[appointment.TransactionType.ID]
      description = this.descriptionsByType[appointment.TransactionType.ID]
        + ` para el ${itemMoment.format('D')} de ${itemMoment.format('MMMM')}`
        + (itemMoment.format('h') === '1' ? ' a la ' : ' a las ')
        + itemMoment.format('h:mm A')

      gradient = [Colors.gradientColorA, Colors.gradientColorB]
      titleColor = '#FFF'
      textColor = '#FFF'

    } else {

      title = 'Concretado'
      description = this.descriptionsByType[appointment.TransactionType.ID]

      gradient = ['#FFF', '#FFF']
      titleColor = Colors.primaryColor
      textColor = Colors.secondaryTextColor

    }

    return (
      <FadeInView key={appointment.ID} style={styles.item.wrapper}>
        <LinearGradient
          colors={gradient} start={{x: 0, y: 0}} end={{x: 1, y: 0}}
          style={styles.item.background}
        >
          <View style={{...styles.item.container, flexDirection: 'row'}}>
            <View style={{...styles.item.paddedContainer, flex: 1}}>
              <Text style={{...styles.item.title, color: titleColor}}>{title.toUpperCase()}</Text>
              <Text style={{...styles.item.description, color: textColor}}>{description}</Text>
              <Text style={{...styles.item.date, color: textColor}}>{date}</Text>
            </View>
            <View style={{width: 24}}>
              <MaterialIcons name="more-vert" size={24} color={textColor} style={styles.item.icon}/>
            </View>
          </View>
        </LinearGradient>
      </FadeInView>
    )
  }

  renderAppointments(appointments) {
    return (
      <ScrollView contentContainerStyle={styles.list}>
        <FlatList
          data={appointments}
          renderItem={({item}) => this.renderAppointmentView(item)}
          keyExtractor={item => item.ID}
        />
      </ScrollView>
    )
  }

  renderNoEventsMessage = () => (
    <View style={{...styles.noEvents.container, justifyContent: 'center', alignItems: 'center'}}>
      <Text style={styles.noEvents.text}>
        No hay eventos para mostrar
      </Text>
    </View>
  )

  transactionDetails = (transactionID, title) => {
    this.props.getTransaction(transactionID)
    this.props.navigation.navigate('TransactionDetails', {title})
  }

  render() {
    const {appointments} = this.props
    return (
      <View style={{flex: 1}}>
        {this.props.loading
          ? this.renderSpinner()
          : appointments.length === 0
            ? this.renderNoEventsMessage()
            : this.renderAppointments(appointments)
        }
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    token: state.Login.token,
    appointments: state.Notifications.appointments,
    loading: state.Notifications.loadingAppointments,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getAppointments() {
      dispatch(Actions.setLoadingAppointments(true))
      dispatch(Actions.getAppointments())
    },
    getTransaction(transactionID) {
      dispatch(Actions.setLoadingAppointments(true))
      dispatch(Actions.getTransaction(transactionID))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Appointments)
