import React, { Component } from 'react';
import {Text, View, ActivityIndicator, Alert, StyleSheet} from 'react-native'
import {NavigationActions} from "react-navigation";
import Icon from 'react-native-vector-icons/MaterialIcons';
import {ListItem} from "react-native-elements";
import {styles} from "../Styles";
import ButtonNative from "../MyComponents";
import {Colors} from "../Layout/Colors";
import moment from "moment/moment";
import Actions from "./Notifications.actions";
import {connect} from "react-redux";
import MapView, { Marker } from 'react-native-maps';
moment.locale('es')

class TransactionDetails extends Component {
    getTransactionDate = reserve => {
        const minutesZone = moment().utcOffset();
        const date = moment(reserve).subtract(minutesZone, 'minutes').format("LLLL");

        return date;
    }

    renderSpinner = () => (
        <View style={styles.centerContainer}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    renderTransaction = transaction =>  {
        const address = transaction.ClientAddress;
        const minutesZone = moment().utcOffset();

        const isFutureTransaction = moment(transaction.ReserveDate)
            .subtract(minutesZone, 'minutes')
            .isSameOrAfter();

        const isCancelled = this.props.transaction.TransactionStatus.ID === 6;

        return (
            <View style={{flex: 1}}>
                <View style={{height: "40%"}}>
                    <MapView
                        style={{flex: 1}}
                        initialRegion={{
                            latitude: this.props.location.lat,
                            longitude: this.props.location.lng,
                            latitudeDelta: 0.00922,
                            longitudeDelta: 0.00421,
                        }}
                    >
                        <Marker
                            coordinate={{
                                latitude: this.props.location.lat,
                                longitude: this.props.location.lng
                            }}
                        />
                    </MapView>
                </View>
                <View style={{backgroundColor: "white"}}>
                    <ListItem
                        title={"Donde"}
                        subtitle={address.Address + " " + (address.Floor ? ("Piso " + address.Floor) : "") + (address.Apartment ? (" Dpto. " + address.Apartment) : "")}
                        leftIcon={{name: "location-on", color: Colors.primaryColor}}
                        hideChevron={true}
                    />
                    <ListItem
                        title={"Cuando"}
                        subtitle={this.getTransactionDate(transaction.ReserveDate)}
                        leftIcon={{name: "watch-later", color: Colors.primaryColor}}
                        hideChevron={true}
                    />
                </View>
                {isFutureTransaction && !isCancelled &&
                <View style={{position: "absolute", bottom: 0, width: "100%", justifyContent: "center"}}>
                    <ButtonNative
                        onPress={() => Alert.alert("Cancelar Evento?", "¿Estás seguro que desea cancelar este evento?",
                            [{
                                text: "Sí", onPress: () => {
                                    this.props.cancelTransaction(transaction.ID);
                                    this.props.navigation.dispatch(NavigationActions.back())
                                }
                            }, {text: "No"}])}
                        styles={[styles.cancelAppointmentButton]}
                    >
                        <Text style={{color: "white", fontWeight: "bold"}}>Cancelar</Text>
                    </ButtonNative>
                </View>
                }
                {isCancelled &&
                <View style={{position: "absolute", bottom: 0, width: "100%", justifyContent: "center", padding: 10}}>
                    <Text style={{color: "red", textAlign: "center"}}>Esta transacción ha sido cancelada</Text>
                </View>
                }
            </View>
        )
    }

    render() {
        return (
            <View style={{flex: 1}}>
                {this.props.loading ? this.renderSpinner() : this.renderTransaction(this.props.transaction)}
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        token: state.Login.token,
        transaction: state.Notifications.transaction,
        loading: state.Notifications.loadingAppointments,
        location: state.Notifications.location,
        street: state.Notifications.street
    }
};

const mapDispatchToProps = dispatch =>{
    return {
        cancelTransaction(transactionID) {
            dispatch(Actions.setLoadingAppointments(true));
            dispatch(Actions.cancelTransaction(transactionID))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(TransactionDetails);

const styleMap = StyleSheet.create({
    map: {
        ...StyleSheet.absoluteFillObject,
    },
});
