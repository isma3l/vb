const initialState = {
    loadingActivity: false,
    loadingAppointments: false,
    loadingNotifications: true,
    activities: [],
    appointments: [],
    notifications: [],
    previousAppointments: [],
    futureAppointments: [],
    transaction: {},
    location: {
        lat: 34.6037,
        lng: 58.3816
    },
    street: {

    }
};

const Profile = (state = initialState, action) => {
    switch (action.type) {
        case "SET_LOADING_ACTIVITY":
            return {...state, loadingActivity: action.loading}
        case "SET_LOADING_APPOINTMENTS":
            return {...state, loadingAppointments: action.loading}
        case "SET_LOADING_NOTIFICATIONS":
            return {...state, loadingNotifications: action.loading}
        case "GET_ACTIVITIES":
            return {...state, activities: action.activities}
        case "GET_NOTIFICATIONS":
            return {...state, notifications: action.notifications, loadingNotifications: false}
        case "GET_APPOINTMENTS":
            return {...state, appointments: action.appointments, previousAppointments: action.previousAppointments, futureAppointments: action.futureAppointments}
        case "GET_TRANSACTIONS":
            return {...state, transaction: action.transaction}
        case "GET_LOCATION":
            return {...state, location: action.location, street: action.street}
        default:
            return state;
    }
};

export default Profile;