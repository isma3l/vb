import {Alert} from 'react-native'
import moment from 'moment'
import Geocoder from 'react-native-geocoder'
import {Community, UserProfile, Transactions, Notifications} from '../Endpoint'
import {apiPrivate} from '../api'
import {ApiError, GetUserTransactionsID} from './schemas'
import {navigatorRef} from '../../App'
import {NavigationActions} from 'react-navigation'

export default {
  setLoadingActivity(boolean) {
    return {type: 'SET_LOADING_ACTIVITY', loading: boolean}
  },
  setLoadingAppointments(boolean) {
    return {type: 'SET_LOADING_APPOINTMENTS', loading: boolean}
  },
  setLoadingNotifications(boolean) {
    return {type: 'SET_LOADING_NOTIFICATIONS', loading: boolean}
  },
  getAppointments() {
    return dispatch => {
      apiPrivate.get(UserProfile + '/user/transactions') //TODO limit, offset
        .then(response => {

          dispatch(this.setLoadingAppointments(false))

          //TODO arrays in schema-object seem to be bugged...
          // const parsed = new GetUserTransactions(response.data.description)
          const parsed = response.data.description
          if (parsed.transactions === null) parsed.transactions = []

          // Set useful function to each transaction
          let appointments = parsed.transactions.map(t => {
            t.getMoment = function() {
              return moment.utc(this.Reserve.Date).utcOffset(moment().utcOffset(), true)
                .hour(this.Reserve.TimeSlot.Hour)
                .minute(this.Reserve.TimeSlot.Minute)
            }
            return t
          })

          function sortAppointments(dir, a, b) {
            const DayA = a.getMoment()
            const DayB = b.getMoment()
            if (DayA.isBefore(DayB))
              return -1 * dir
            if (DayA.isAfter(DayB))
              return 1 * dir
            return 0
          }

          const futureAppointments = appointments
            .filter(a => a.Reserve.ID !== 0 && a.getMoment().isSameOrAfter())
            .sort((a, b) => sortAppointments(1, a, b))
          const previousAppointments = appointments
            .filter(a => a.Reserve.ID === 0 || a.getMoment().isBefore())
            .sort((a, b) => sortAppointments(-1, a, b))

          const sorted = [...futureAppointments, ...previousAppointments]

          dispatch({type: 'GET_APPOINTMENTS', appointments: sorted})
        })
        .catch(error => {
          dispatch(this.setLoadingAppointments(false))
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },
  getActivity() {
    //TODO review
    return dispatch => {
      apiPrivate.get(Community + '/catalogPermission', {params: {type: 'granted'}})
        .then(response => {
          dispatch(this.setLoadingActivity(false))
          dispatch({type: 'GET_ACTIVITY', activities: response.data.description.activities})
        })
        .catch(response => {
          dispatch(this.setLoadingActivity(false))
          console.log(response.response)
          Alert.alert('Error', response.response.data)
        })
    }
  },
  cancelTransaction(transactionID) {
    return dispatch => {
      apiPrivate.delete(Transactions + '/transactions/' + transactionID)
        .then(response => {
          dispatch(this.getAppointments())
          Alert.alert('Transacción cancelada', response.data.description)
        })
        .catch(error => {
          dispatch(this.setLoadingAppointments(false))
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },
  getTransaction(transactionID) {
    return dispatch => {
      apiPrivate.get(UserProfile + '/user/transactions/' + transactionID)
        .then(response => {

          const parsed = new GetUserTransactionsID({transaction: response.data.description})

          dispatch({type: 'GET_TRANSACTIONS', transaction: parsed.transaction})

          return Geocoder.geocodeAddress(parsed.transaction.ClientAddress.Address)
            .then(response => {
              const streetName = response[0].streetName
              const streetNumber = response[0].streetNumber
              dispatch({type: 'GET_LOCATION', location: response[0].position, street: {streetName, streetNumber}})
              dispatch(this.setLoadingAppointments(false))
            })
            .catch(err => {throw err})

        })
        .catch(error => {
          dispatch(this.setLoadingAppointments(false))
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },
  getNotifications() {
    return dispatch => {
      apiPrivate.get(Notifications + '/notifications/all')
        .then(response => {
          dispatch(this.setLoadingNotifications(false))

          //TODO arrays in schema-object seem to be bugged...
          // const parsed = new GetNotificationsAll(response.data.description)
          const parsed = response.data.description
          if (parsed.transactions === null) parsed.transactions = []

          dispatch({type: 'GET_NOTIFICATIONS', notifications: parsed.notifications})
        })
        .catch(error => {
          dispatch(this.setLoadingNotifications(false))
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },
}
