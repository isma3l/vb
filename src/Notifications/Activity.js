import React, { Component } from 'react';
import {connect} from 'react-redux';
import {ActivityIndicator, View, ScrollView, Text, FlatList, DeviceEventEmitter, Platform} from 'react-native'
import NotificationActions from "./Notifications.actions";
import {ListItem, List} from "react-native-elements";
import Icon from "react-native-vector-icons/MaterialIcons"
import {Colors} from "../Layout/Colors";
import {styles, NotificationStyles} from "../Styles";
import 'moment/locale/es'
import moment from "moment"

class Activity extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            tabBarLabel: 'Actividad',
            headerTintColor: Colors.primaryColor,
            tabBarIcon: ({ tintColor }) => (
                <Icon name="info-outline" size={24} color={tintColor}/>
            ),
        }
    };

    componentDidMount () {
        this.props.getNotifications();
    }
  
    renderNotification = (notification) => {
        let date = moment(notification.CreationDate).format("DD MMMM YYYY")

        return (
          <View style={{width:"100%", flexDirection:"row"}}>
              <View style={NotificationStyles.containerItem}>
                <View style={NotificationStyles.contentItem}>
                  <Text style={NotificationStyles.message}>{notification.Message}</Text>
                  <Text style={NotificationStyles.date}>{date}</Text>
                </View>
              </View>
          </View>
        )
    }

    renderSpinner = () => (
        <View style={styles.centerContainer}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    renderNotifications(notifications) {
        if (notifications.length > 0)
            return (
                <ScrollView
                    contentContainerStyle={{paddingBottom: 10, flex:1, marginTop:8}}
                >
                  <FlatList
                    data={this.props.notifications}
                    renderItem={({item}) => this.renderNotification(item)}
                    keyExtractor={item => item.ID}
                  />
                </ScrollView>
            )
        else
            return (
                <View style={{backgroundColor: "transparent", justifyContent: "center", alignItems: "center", padding: 10}}>
                    <Text style={{fontSize: 25, textAlign: "center"}}>
                        No hay notificaciones para mostrar
                    </Text>
                </View>
            )
    }

    render() {
        return (
            <View style={{flex: 1}}>
                {this.props.loading ? this.renderSpinner() : this.renderNotifications(this.props.notifications)}
            </View>
        )
    }

}

const mapStateToProps = state => {
    return {
        token: state.Login.token,
        notifications: state.Notifications.notifications,
        loading: state.Notifications.loadingNotifications
    }
};

const mapDispatchToProps = dispatch =>{
    return {
        getNotifications() {
            dispatch(NotificationActions.setLoadingNotifications(true));
            dispatch(NotificationActions.getNotifications())
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Activity);
