import SchemaObject from 'schema-object'
import {Address, TransactionReserve} from '../schemas'

export {ApiError} from '../schemas'

const TransactionType = new SchemaObject({
  ID: Number,
  Description: String,
})

const TransactionStatus = new SchemaObject({
  ID: Number,
  Description: String,
})

const Transaction = new SchemaObject({
  ID: String,
  TransactionType: TransactionType,
  Origin: String,
  Destination: String,
  ClientAddress: Address,
  Reserve: TransactionReserve,
  ReserveDate: Date,
  TransactionStatus: TransactionStatus,
})

export const GetUserTransactions = new SchemaObject({
  transactions: [Transaction],
  quantity: Number,
})

const CompleteTransaction = new SchemaObject({
  ID: String,
  CreationDate: Date,
  TransactionType: TransactionType,
  Problems: String,
  Realized: Boolean,
  Comments: String,
  Origin: String,
  Destination: String,
  ClientAddress: Address,
  TransactionStatus: [],
  Reserve: TransactionReserve,
  ConfirmationDate: Date,
  ReserveDate: Date,
})

export const GetUserTransactionsID = new SchemaObject({
  transaction: CompleteTransaction,
})

const NotificationType = new SchemaObject({
  ID: Number,
  Description: String,
})

const Notification = new SchemaObject({
  ID: Number,
  Message: String,
  NotificationType: NotificationType,
  NotificationAuthorizationID: Number,
  NotificationAuthorization: Object,
  CreationDate: Date,
})

export const GetNotificationsAll = new SchemaObject({
  notifications: [Notification],
  quantity: Number,
})
