import React from 'react';
import {Image, View, Text, TouchableOpacity, Platform, Dimensions} from 'react-native'
import {LOGO, LOGOWITHNAME} from "../Images";
import {LoginDrawer} from "../Layout/CommonNavigation";
import {LogoStyle} from "../Styles";
import {Colors} from "../Layout/Colors";

export const Logo = () => (
    <Image source={LOGOWITHNAME} resizeMode={"contain"} style={{height: 100, maxWidth: "80%"}}/>
)

const logoSize = (width, height) => {
    const maxWidth = 110;
    const maxHeight = 50;

    let ret;

    if (width >= height) {
        let ratio = maxWidth / width;
        const h = Math.ceil(ratio * height);

        if (h > maxHeight) {
            // Too tall, resize
            let ratio = maxHeight / height;
            const w = Math.ceil(ratio * width);
            ret = {
                'width': w,
                'height': maxHeight
            };
        } else {
            ret = {
                'width': maxWidth,
                'height': h
            };
        }

    } else {
        let ratio = maxHeight / height;
        const w = Math.ceil(ratio * width);

        if (w > maxWidth) {
            let ratio = maxWidth / width;
            const h = Math.ceil(ratio * height);
            ret = {
                'width': maxWidth,
                'height': h
            };
        } else {
            ret = {
                'width': w,
                'height': maxHeight
            };
        }
    }

    return ret;
}

export const ReducedLogo = () => (
    <Image source={LOGO} resizeMode={"contain"} style={logoSize(122, 238)}/>
)


export const LogoWithDescription = (props) => {
    const {height, width} = Dimensions.get('window');
    return (
        <View style={LogoStyle.container}>
            <View style={{marginVertical: Platform.OS === "ios" ? 70 : 20}}>
                <ReducedLogo/>
            </View>
            {Platform.OS === "ios"
                ? props.title.map((titleText, index) => <Text key={index} style={LogoStyle.title}>{titleText}</Text>)
                : <Text style={LogoStyle.title}>{props.title.join(' ')}</Text>
            }
            <View>
                {props.subtitle.map((titleText, index) => (
                    <View key={index} style={Platform.OS === "ios" ? {flexDirection: (width - (width * 5 / 100)) < 305 ? "column" : "row"} :{flexDirection: "row", alignItems: "center", justifyContent: "center"}}>
                        <Text style={LogoStyle.subtitle}>{titleText}</Text>
                        {props.linkLabel && index === (props.subtitle.length - 1) &&
                        <TouchableOpacity onPress={props.onPress}>
                            <Text style={[LogoStyle.subtitle, {fontWeight: "bold", color: Colors.primaryColor}]}>{props.linkLabel}</Text>
                        </TouchableOpacity>
                        }
                    </View>))}
            </View>
        </View>
    )
}
