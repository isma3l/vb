import React, {Component} from 'react'
import {View, ActivityIndicator} from 'react-native'
import Actions from '../Login/Login.actions'
import {connect} from 'react-redux'
import {Colors} from '../Layout/Colors'
import {styles} from '../Styles'
import {Logo} from './Logo'
import SimpleStore from 'react-native-simple-store'
import {NavigationActions} from 'react-navigation'
import {apiPrivate} from '../api'

class LogoScreen extends Component {

  componentDidMount() {

    //TODO should move this
    Promise.all([
      SimpleStore.get('token'),
      SimpleStore.get('user_id'),
    ])
      .then(results => {
        const token = results[0]
        const userId = results[1]

        if (!token || !userId) {
          this.props.navigation.dispatch(NavigationActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({routeName: 'LayoutGuest'})],
          }))
          return
        }

        apiPrivate.defaults.headers.Authorization = token

        this.props.setToken(token, userId)
        this.props.navigation.dispatch(NavigationActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({routeName: 'Layout'})],
        }))
      })
      .catch(error => {
        console.error(error)
        this.props.navigation.navigate('LayoutGuest')
      })
  }

  renderSpinner = () => (
    <View style={{
      alignItems: 'center',
      justifyContent: 'center',
    }}>
      <View style={{width: '80%', flex: 0}}>
        <Logo/>
        <ActivityIndicator animating={true} style={{flex: 0}} color={Colors.primaryColor} size={'large'}/>
      </View>
    </View>
  )

  render() {
    return (
      <View style={styles.centerContainer}>
        {this.renderSpinner()}
      </View>

    )
  }

}

const mapStateToProps = state => {
  return {
    loading: state.Login.loading,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setToken: (token, userID) => dispatch(Actions.setToken(token, userID)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LogoScreen)
