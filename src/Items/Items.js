import React, { Component } from 'react';
import {Text, View, FlatList, Image, RefreshControl, TouchableOpacity, Dimensions, Platform} from 'react-native'
import {Colors} from "../Layout/Colors";
import {styles} from "../Styles";
import {Avatar} from "react-native-elements";
import Categories from "../Categories/Categories";
import {BIKE, NOIMAGE} from "../Images";
import {FadeInView} from "../AnimatedComponents";
import {PrimaryButton} from "../MyComponents";

const DimensionWidth50 = Dimensions.get('window').width * 50 / 100;

class Item extends Component {

    renderNoProducts = () => (
        <View style={{
          alignItems: "center",
          justifyContent: "center",
          alignSelf: "center",
          width:"100%",
          paddingTop:24
        }}>

          <Image source={BIKE} resizeMode={"contain"} style={{height: 100, maxWidth: "70%"}}/>

          <Text style={{ textAlign: "center", color:"black", marginBottom:30, marginTop:16, fontSize:18 }}>
            No hay ítems en tu catálogo
          </Text>

          <View  style={{width:"64%"}}>
            <Text style={{textAlign:"center", color: Colors.subtitleLogoColor, fontSize:15}}>
              ¡Programá un retiro en tu domicilio de aquellos objetos que ya no usás y rentalos en nuestra comunidad!
            </Text>
          </View>

          <TouchableOpacity onPress={()=>{}}>
            <View>
              <Text style={{color:Colors.primaryColor, fontSize:15}}>¿Cómo funciona?</Text>
            </View>
          </TouchableOpacity>

          <View style={{width:"100%", height:44, marginTop:32}}>
            <PrimaryButton onPress={()=>{this.props.navigation.navigate('NewDeposit')}} label="Programar retiro" />
          </View>

        </View>

    );

    renderAvatar = (item) => {
        let title = "";
        if (item) {
            if (item.OwnerData.Name !== "") {
                title = title + item.OwnerData.Name[0].toUpperCase();
            }
            if (item.OwnerData.Name !== "") {
                title = title + item.OwnerData.LastName[0].toUpperCase();
            }
        }
        if (title === "") title = "VB";

        return (
            <View style={styles.userIcon}>
                {(item && item.OwnerData.ProfilePicture !== "") ?
                    <Avatar width={24} height={24} source={{uri: item.OwnerData.ProfilePicture + "?fit=crop&w=100&max-h=100"}} rounded/> :
                    <Avatar width={24} height={24} small title={title} rounded/>
                }
            </View>
        )
    }

    renderCategories = () => (
        <View style={{flex: 1}}>
            {this.props.isCommunity &&
              <View style={{marginTop: 30, marginBottom: 20, marginLeft: 16}}>
                <Text style={Platform.OS === "ios" ? {fontSize: 34, fontWeight: "bold"} : {fontSize: 14}}>
                  Explora
                </Text>
              </View>
            }
            {this.props.isMyObjects && this.props.items.length !== 0 &&
            <View style={{marginTop: 30, marginBottom: 60, marginLeft: 16}}>
              <Text style={Platform.OS === "ios" ? {fontSize: 34, fontWeight: "bold"} : {fontSize: 14}}>
                Mis ítems
              </Text>
            </View>
            }

            {this.props.isCommunity &&
              <Categories
                userID={this.props.userID}
                isCommunity={this.props.isCommunity}
                isSharedCatalog={this.props.isSharedCatalog}
                params={this.props.paramsSearch}
                setParams={this.props.setParams}
              />
            }

            {this.props.isCommunity && this.props.items.length !== 0 &&
              <View style={{marginTop: 30, marginBottom: 20, marginLeft: 16}}>
                <Text style={Platform.OS === "ios" ? {fontSize: 22, fontWeight: "bold"} : {fontSize: 14}}>
                  Items populares
                </Text>
              </View>
            }

        </View>
    )

    renderItem = ({item}) => {
        return (
            <FadeInView>
                <TouchableOpacity
                    style={styles.containerItem}
                    onPress={() => {
                        this.props.getItem(item.ID, this.props.token, this.props.userID);
                        this.props.navigation.navigate("ProductDetails", {isCommunity: this.props.isCommunity, isSharedCatalog: this.props.isSharedCatalog});
                    }}
                >
                    <View style={styles.labelItem}>
                        <Text style={styles.labelColor}>${item.RentPrice}/día</Text>
                    </View>
                    {(this.props.isCommunity || this.props.isSharedCatalog) && this.renderAvatar(item)}
                    <Image
                        style={{height: DimensionWidth50, width: DimensionWidth50, backgroundColor: "white"}}
                        resizeMode={'cover'}
                        source={item.Photo[0] ? {uri: item.Photo[0].Url + "?fit=crop&w=300&max-h=300"} : NOIMAGE}
                    />
                </TouchableOpacity>
            </FadeInView>
        )
    };

    render() {
      return (
            <FlatList
                refreshControl={
                    <RefreshControl
                        refreshing={!!this.props.loading}
                        onRefresh={() => this.props.getInfo(this.props.paramsSearch, this.props.userID)}
                        colors={[Colors.primaryColor]}
                        tintColor={Colors.primaryColor}
                    />
                }
                refreshing={!!this.props.loading}
                data={this.props.items}
                numColumns={2}
                keyExtractor={(item) => item.ID}
                renderItem={this.renderItem}
                ListHeaderComponent={this.renderCategories}
                ListEmptyComponent={this.renderNoProducts}
                onEndReached={() => {
                    if (this.props.multiplier !== 0) this.props.loadMoreItems({...this.props.paramsSearch, Offset: this.props.paramsSearch.Limit * this.props.multiplier}, this.props.userID)
                }}
                onEndReachedThreshold={0.5}
            />
        )
    }
}

export default Item;
