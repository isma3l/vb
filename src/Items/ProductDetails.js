import React, { Component } from 'react';
import {connect} from 'react-redux';
import {Text, ActivityIndicator, View, Image, Dimensions, Platform, Alert, TouchableOpacity, ScrollView} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';
import {Icon as IconElements, FormLabel, FormInput, ListItem, Avatar} from "react-native-elements";
import Items from "./Items";
import {NavigationActions} from "react-navigation";
import Actions from "../Community/Community.actions";
import MyObjectsActions from "../MyObjects/MyObjects.actions";
import {Colors} from "../Layout/Colors";
import {ProductDetailsStyle, styles} from "../Styles";
import Carousel from 'react-native-snap-carousel';
import {NOIMAGE} from "../Images/index";
import {FadeInView} from "../AnimatedComponents";
import ButtonNative, {BackButton, PrimaryButton} from "../MyComponents";
import KeyboardView from "../KeyboardView";
import LoginActions from "../Login/Login.actions";

const sliderWidth = Dimensions.get('window').width;
const itemWidth = sliderWidth * 80 / 100

const navigationOptions = {
    headerTitle: 'Detalles',
    tabBarLabel: 'Comunidad',
    drawerLockMode: "locked-closed"
}

class ProductDetails extends Component {
    static navigationOptions = Platform.OS === "android" ? navigationOptions : {
        ...navigationOptions,
        header: null
    };

    state = {
        editProduct: false
    }

    editButtonPress = (product) => {
        this.setState({editProduct: true, product: product.Name, description: product.Description});
    }

    modifyObject = (productDetails) => {
        this.setState({editProduct: false})
        const {product, description} = this.state;
        const {token, userID} = this.props;

        const formData = new FormData();
        formData.append("name", product);
        formData.append("description", description);

        this.props.editObject(token, productDetails.ID, formData, userID);
    }

    renderSpinner = () => (
        <View style={styles.centerContainer}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    renderProductImage = ({item, index}) => (
        <View style={{justifyContent: "center"}}>
            <Image source={{uri: item.Url}} style={{width: itemWidth, height: itemWidth, maxHeight: 300}}/>
        </View>
    )

    renderCarousel = (product) => {
        if (product.Photos[0]) {
            return (
                <Carousel
                    data={product.Photos}
                    containerCustomStyle={{backgroundColor: "white", flexGrow: 0}}
                    renderItem={this.renderProductImage}
                    itemWidth={itemWidth}
                    sliderWidth={sliderWidth}
                />
            )
        } else {
            return (
                <Image source={NOIMAGE} style={{height: itemWidth, width: itemWidth, alignSelf: "center"}}/>
            )
        }
    }

    renderRent = () => (
        <PrimaryButton
            onPress={() => {
                if (!this.props.token) {
                    this.props.setRedirectRoute("SelectRange");
                    this.props.navigation.navigate("Welcome");
                }
                else {
                    this.props.navigation.navigate("SelectRange")
                }
            }}
            label="Chequear disponibilidad"
        />
    )

    renderToRent = () => {
        return (
            <PrimaryButton
                onPress={() => {
                    if (!this.props.token) {
                        this.props.navigation.navigate("SignUp")
                    }
                    else {
                        Alert.alert("Objeto puesto para alquilar")
                    }
                }}
                label="Colocar objeto para alquilar"
            />
        )
    }

    renderEditProduct = (product) => {
        return (
            <ScrollView>
                {this.renderCarousel(product)}
                <FadeInView>
                    <View style={{flexGrow: 1, backgroundColor: "white", width: "100%", flexDirection: "column", alignContent: "space-between", marginBottom: 10}}>
                        <FormLabel>Nombre del Objeto</FormLabel>
                        <FormInput
                            placeholder={"Ingrese un nombre"}
                            underlineColorAndroid={Colors.primaryColor}
                            onChangeText={text => this.setState({product: text})}
                            defaultValue={this.state.product}
                        />
                        <FormLabel>Descripción</FormLabel>
                        <FormInput
                            placeholder={"Ingrese una descripción"}
                            underlineColorAndroid={Colors.primaryColor}
                            onChangeText={text => this.setState({description: text})}
                            defaultValue={this.state.description}
                        />
                    </View>
                    <View style={{flexDirection: "row", justifyContent: "space-around"}}>
                        <ButtonNative
                            styles={ProductDetailsStyle.sendProductEditButton}
                            onPress={() => this.modifyObject(product)}
                        >
                            <Text style={{color: "white", fontWeight: "bold"}}>Actualizar objeto</Text>
                        </ButtonNative>
                        <ButtonNative
                            onPress={() => this.setState({editProduct: false})}
                            styles={ProductDetailsStyle.cancelEditButton}
                        >
                            <Text style={{fontWeight: "bold"}}>Cancelar edición</Text>
                        </ButtonNative>
                    </View>
                </FadeInView>
            </ScrollView>
        )
    }

    renderProduct = (product) => {
        let title = "";
        if (product) {
            if (product.OwnerData.Name !== "") {
                title = title + product.OwnerData.Name[0].toUpperCase();
            }
            if (product.OwnerData.Name !== "") {
                title = title + product.OwnerData.LastName[0].toUpperCase();
            }
        }
        if (title === "") title = "VB";

        return (
            <ScrollView keyboardShouldPersistTaps='always'>
                {this.renderCarousel(product)}
                <View style={ProductDetailsStyle.productContainer}>
                    <FadeInView>
                        <View style={{paddingLeft: 20}}>
                            <View style={{position: "relative"}}>
                                <View style={{paddingRight: 50}}>
                                    <Text style={{fontSize: 22, fontWeight: "bold"}}>{product.Name !== "" ? product.Name : " "}</Text>
                                    <Text style={styles.categoryLabel}>{product.Categories[0].Description}</Text>
                                    <View style={{marginTop: 16, marginBottom: 16}}>
                                        <Text style={styles.priceDetailLabel}>${product.RentPrice}/día</Text>
                                    </View>
                                </View>
                                {(!this.props.isCommunity && !this.props.isSharedCatalog) &&
                                <View style={ProductDetailsStyle.editButton}>
                                    <IconElements
                                        component={TouchableOpacity}
                                        name="edit" color={Colors.primaryColor} size={28}
                                        iconStyle={{padding: 5, margin: "auto", marginRight: 15}}
                                        onPress={() => this.editButtonPress(product)}
                                    />
                                </View>
                                }
                            </View>
                            <View style={{marginTop: 5, marginBottom: 20}}>
                                <Text style={styles.productDescriptionLabel}>{product.Description}</Text>
                            </View>
                        </View>
                        {(this.props.isCommunity || this.props.isSharedCatalog) ? this.renderRent() : this.renderToRent()}
                        <View style={{width: "100%", justifyContent: "center", alignItems: "center", flexDirection: "row", marginTop: 20}}>
                            {(product && product.OwnerData.ProfilePicture !== "") ?
                                <Avatar small source={{uri: product.OwnerData.ProfilePicture}} rounded/> :
                                <Avatar small title={title} rounded/>
                            }
                            <Text style={{marginLeft: 10}}>Publicado por {product.OwnerData.Name} {product.OwnerData.LastName} </Text>
                        </View>
                    </FadeInView>
                </View>
            </ScrollView>
        )
    }

    render() {
        return (
            <KeyboardView style={{minHeight: "100%", backgroundColor: "white"}} keyboardVerticalOffset={80}>
                {Platform.OS === "ios" &&
                    <BackButton navigation={this.props.navigation}/>
                }
                {this.props.loading ? this.renderSpinner() :
                    this.state.editProduct ? this.renderEditProduct(this.props.product) :
                        this.renderProduct(this.props.product)}
            </KeyboardView>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        product: ownProps.navigation.state.params.isCommunity ?  state.Community.productDetails :
                    ownProps.navigation.state.params.isSharedCatalog ? state.ShareCatalog.productDetails :
                        state.MyObjects.productDetails,
        loading: ownProps.navigation.state.params.isCommunity ?  state.Community.loadingItem :
                    ownProps.navigation.state.params.isSharedCatalog ? state.ShareCatalog.loadingSharedWithMe :
                        state.MyObjects.loading,
        isCommunity: ownProps.navigation.state.params.isCommunity,
        isSharedCatalog: ownProps.navigation.state.params.isSharedCatalog,
        token: state.Login.token,
        userID: state.Login.userID,
    }
};

const mapDispatchToProps = dispatch =>{
    return {
        setLoading(boolean){
            dispatch(Actions.setLoading(boolean))
        },
        setMyObjectsLoading(boolean){
            dispatch(MyObjectsActions.setLoading(boolean))
        },
        editObject(token, objectID, formData, userID) {
            dispatch(MyObjectsActions.setLoading(true));
            dispatch(MyObjectsActions.editObject(token, objectID, formData, userID))
        },
        setRedirectRoute(route){
            dispatch(LoginActions.setRedirectRoute(route))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetails);
