import React, { Component } from 'react';
import {Text, View, TextInput, ActivityIndicator, Image, TouchableOpacity} from 'react-native'
import {LOGO} from "../Images/index";
import {Colors} from "../Layout/Colors";
import {SocialIcon} from "react-native-elements";
import {NotObjectStyles} from "../Styles";
import Icon from 'react-native-vector-icons/Entypo';
import Icon2 from 'react-native-vector-icons/EvilIcons';
import {PrimaryButton} from "../MyComponents";
import {BIKE} from "../Images";
import {connect} from "react-redux";
import Actions from "../Login/Login.actions";

class MyObjectsNotLogged extends Component {
    static navigationOptions = {
        headerTitle: 'Home',
        tabBarLabel: 'Mis ítems',
    };

    render() {
        return (
            <View style={{
                alignItems: "center",
                justifyContent: "center",
                alignSelf: "center",
                width:"100%",
                paddingTop:24
            }}>

                <Image source={BIKE} resizeMode={"contain"} style={{height: 100, maxWidth: "70%"}}/>

                <Text style={{ textAlign: "center", color:"black", marginBottom:30, marginTop:16, fontSize:18 }}>
                    No hay ítems en tu catálogo
                </Text>

                <View  style={{width:"64%"}}>
                    <Text style={{textAlign:"center", color: Colors.subtitleLogoColor, fontSize:15}}>
                        ¡Programá un retiro en tu domicilio de aquellos objetos que ya no usás y rentalos en nuestra comunidad!
                    </Text>
                </View>

                <TouchableOpacity onPress={()=>{}}>
                    <View>
                        <Text style={{color:Colors.primaryColor, fontSize:15}}>¿Cómo funciona?</Text>
                    </View>
                </TouchableOpacity>

                <View style={{width:"100%", height: 44, marginTop: 32}}>
                    <PrimaryButton
                        onPress={()=>{
                            this.props.setRedirectRoute('NewDeposit');
                            this.props.navigation.navigate('Welcome');
                        }}
                        label="Programar retiro"
                    />
                </View>
            </View>

        )
    }
}

const mapDispatchToProps = dispatch =>{
    return {
        setRedirectRoute(redirectRoute){
            dispatch(Actions.setRedirectRoute(redirectRoute));
        }
    }
};

export default connect(null, mapDispatchToProps)(MyObjectsNotLogged);