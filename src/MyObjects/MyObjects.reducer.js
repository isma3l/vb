const initialState = {
    items: {},
    loading: true,
    products: [],
    params: {
        Limit: 10,
        Offset: 0
    },
    offsetMultiplier: 0,
    productDetails: {}
};

const MyObjects = (state = initialState, action) => {
    switch (action.type) {
        case "GET_MY_OBJECT_ITEM":
            return {...state, productDetails: action.item}
        case "SET_LOADING_MYOBJECTS":
            return {...state, loading: action.loading}
        case "GET_MY_OBJECTS":
            return {...state, products: action.items, offsetMultiplier: 1 };
        case "LOAD_MORE_MY_OBJECTS":
            return {...state, products: [...state.products, ...action.items], offsetMultiplier: state.offsetMultiplier + 1}
        case "SET_PARAMS_MYOBJECTS":
            return {...state, params: {...state.params, ...action.params}};
        case "SET_SEARCH_BAR":
            return {...state, searchBar: action.boolean};
        case "CLEAR_SEARCH":
            return {...state, params: {...state.params, name: undefined}}
        default:
            return state;
    }
};

export default MyObjects;