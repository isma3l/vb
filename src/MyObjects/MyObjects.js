import React, { Component } from 'react';
import {connect} from 'react-redux';
import {Text, ActivityIndicator, View, Platform, DeviceEventEmitter} from 'react-native'
import Icon from 'react-native-vector-icons/Entypo';
import Items from "../Items/Items";
import Actions from "./MyObjects.actions";
import {Colors} from "../Layout/Colors";
import {NotObjectStyles, styles} from "../Styles";
import {FadeInView} from "../AnimatedComponents";

class MyObjects extends Component {
    static navigationOptions = {
        headerTitle: 'Home',
        tabBarLabel: 'Mis Ítems',
    };

    componentDidMount() {
        this._sub = this.props.navigation.addListener(
            'didFocus',
            () => {
                this.props.cleanSearch();
                this.props.setSearchBar(false);
                this.props.getMyObjects(this.props.MyObjects.params, this.props.token, this.props.userID)
            }
        );
    }

    componentWillUnmount() {
        this._sub.remove();
    }

    noObjects = () => (
        <View
            style={{height: "100%", width: "100%"}}
        >
            <View style={NotObjectStyles.container}>
                <View style={{
                    width: "80%",
                    flexDirection: "column",
                    alignItems: "center",
                    justifyContent: "center",
                    flexGrow: 1
                }}>
                    <Icon name="emoji-sad" size={80}/>

                    <Text style={{
                        textAlign: "center"
                    }}>Los objetos que tengas depositados aparecerán acá. No tiene objetos depositados actualmente</Text>
                </View>
            </View>
        </View>
    )

    renderItems = (products) => (
        <Items {...this.props}
               getInfo={this.props.getMyObjects}
               loadMoreItems={this.props.loadMoreMyObjects}
               multiplier={this.props.MyObjects.offsetMultiplier}
               items={products}
               paramsSearch={this.props.MyObjects.params}
               isCommunity={false}
               token={this.props.token}
               userID={this.props.userID}
        />
    )


    renderSpinner = () => (
        <View style={{height: "100%", justifyContent: "center", backgroundColor: Colors.backgroundColor}}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    render() {
        const myObjects = this.props.MyObjects.products;

        return (
            <FadeInView style={{minHeight: "100%"}}>
                {this.renderItems(myObjects)}
            </FadeInView>
        )
    }
}

const mapStateToProps = state => {
    return {
        MyObjects: state.MyObjects,
        token: state.Login.token,
        userID: state.Login.userID,
        loading: state.MyObjects.loading,
        isMyObjects: true
    }
};

const mapDispatchToProps = dispatch =>{
    return {
        getItem(id, token, userID){
            dispatch(Actions.setLoading(true));
            dispatch(Actions.getItem(token, userID, id))
        },
        setLoading(boolean){
            dispatch(Actions.setLoading(boolean));
        },
        cleanSearch() {
            dispatch({type: "CLEAN_SEARCH"});
        },
        getMyObjects(params, token, userID) {
            dispatch(Actions.setLoading(true));
            dispatch(Actions.getMyObjects(token, userID, params))
        },
        setSearchBar(boolean) {
            dispatch(Actions.setSearchBar(boolean))
        },
        setParams(params, token, userID) {
            dispatch(Actions.setParams(params, token, userID))
        },
        loadMoreMyObjects(params, token, userID){
            dispatch(Actions.setLoading(true));
            dispatch(Actions.loadMoreMyObjects(token, userID, params))
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MyObjects);
