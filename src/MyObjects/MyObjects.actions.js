import {Community, ObjectAdmin} from '../Endpoint'
import {Alert} from 'react-native'
import store from '../store'
import {apiPrivate} from '../api'
import {ApiError, GetCatalogUserObjects, GetCatalogUserObjectID} from './schemas'
import {navigatorRef} from '../../App'
import {NavigationActions} from 'react-navigation'

export default {
  getMyObjects(token, userID, params = {}) {
    return dispatch => {
      apiPrivate.get(Community + '/catalog/' + userID + '/object', {params})
        .then(response => {
          dispatch(this.setLoading(false))
          const parsed = new GetCatalogUserObjects(response.data.description)
          dispatch({type: 'GET_MY_OBJECTS', items: parsed.myObjects})
        })
        .catch(error => {
          dispatch(this.setLoading(false))
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },

  loadMoreMyObjects(token, userID, params = {}) {
    return dispatch => {
      apiPrivate.get(Community + '/catalog/' + userID + '/object', {params})
        .then(response => {
          dispatch(this.setLoading(false))
          const parsed = new GetCatalogUserObjects(response.data.description)
          dispatch({type: 'LOAD_MORE_MY_OBJECTS', items: parsed.myObjects})
        })
        .catch(error => {
          dispatch(this.setLoading(false))
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },

  getItem(token, userID, id) {
    return dispatch => {
      apiPrivate.get(Community + '/catalog/' + userID + '/object/' + id)
        .then(response => {
          const parsed = new GetCatalogUserObjectID({item: response.data.description})
          dispatch({type: 'GET_MY_OBJECT_ITEM', item: parsed.item})
          dispatch(this.setLoading(false))
        })
        .catch(error => {
          dispatch(this.setLoading(false))
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },

  setLoading(boolean) {
    return {type: 'SET_LOADING_MYOBJECTS', loading: boolean}
  },

  setSearchBar(boolean) {
    return {type: 'SET_SEARCH_BAR', value: boolean}
  },

  setParams(params, token, userID) {
    return dispatch => {
      dispatch({type: 'SET_PARAMS_MYOBJECTS', params})
      dispatch(this.setLoading(true))
      dispatch(this.getMyObjects(token, userID, params))
    }
  },

  clearSearch(token, userID) {
    return dispatch => {
      dispatch({type: 'CLEAR_SEARCH'})
      dispatch(this.setLoading(true))
      dispatch(this.getMyObjects(token, userID, {...store.getState().MyObjects.params, name: undefined}))
    }
  },

  editObject(token, objectID, formData, userID) {
    return dispatch => {
      apiPrivate.put(ObjectAdmin + '/object/' + objectID + '/description', formData)
        .then(() => {
          dispatch(this.getItem(token, userID, objectID))
        })
        .catch(error => {
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },
}
