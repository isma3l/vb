import {combineReducers} from 'redux';

import Community from './Community/Community.reducer';
import Login from "./Login/Login.reducer";
import Profile from "./Profile/Profile.reducer";
import Category from "./Categories/Categories.reducer";
import MyObjects from "./MyObjects/MyObjects.reducer";
import Deposit from "./NewDeposit/Deposit.reducer";
import VerifyInformation from "./VerifyInformation/VerifyInformation.reducer";
import CreditCard from "./CreditCard/CreditCard.reducer";
import ShareCatalog from "./ShareCatalog/ShareCatalog.reducer";
import Rent from "./NewRent/Rent.reducer";
import Notifications from "./Notifications/Notifications.reducer";

const rootReducer = combineReducers({
    Login,
    Community,
    Profile,
    Category,
    MyObjects,
    Deposit,
    Rent,
    VerifyInformation,
    CreditCard,
    ShareCatalog,
    Notifications
});

export default rootReducer;