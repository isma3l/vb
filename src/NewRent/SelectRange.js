import React, { Component } from 'react';
import {Text,  View,  TouchableOpacity, ActivityIndicator, FlatList} from 'react-native'
import {CalendarList, LocaleConfig} from "react-native-calendars";
import {connect} from "react-redux";
import VerifyInformation from "../VerifyInformation/VerifyInformation";
import {Colors} from "../Layout/Colors";
import {DepositStyles, styles} from "../Styles";
import moment from "moment";
import ButtonNative from "../MyComponents";
import RentActions from "./Rent.actions";

LocaleConfig.locales['es'] = {
    monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
    monthNamesShort: ['Ene.','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
    dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
    dayNamesShort: ['Dom','Lun','Mar','Mie','Jue','Vie','Sab']
};

LocaleConfig.defaultLocale = 'es';

class SelectRange extends Component {
    componentDidMount() {
        this.props.newRent(this.props.token);
        this.props.getAvailability(this.props.token, this.props.objectID, {startDate: Math.round(moment().valueOf() / 1000)});
    }

    addObjectToRent = () => {
        const formData = new FormData();
        const period = this.props.period;

        formData.append("object_id", this.props.objectID);
        formData.append("start_date", moment(period.startDate).valueOf() / 1000);
        formData.append("end_date", moment(period.endDate).valueOf() / 1000);

        this.props.addObjectToRent(this.props.token, formData, this.props.transaction.ID);

        this.props.setDeliveryDate({selectedDay: moment(period.startDate), disableContinue: true});
        this.props.setPickupDate({selectedDay: moment(period.endDate), disableContinue: true});

        this.props.navigation.navigate('DeliveryRent');
    }

    renderSpinner = (styles) => (
        <View style={styles}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    renderCalendar = () => {
        return (
            <View style={{minHeight: "100%"}}>
                <View style={DepositStyles.depositDescription}>
                    <Text style={{textAlign: "center", fontSize: 24}}>¿Cuándo va a estar usando este objeto?</Text>
                </View>
                <View style={{flex: 4}}>
                    <CalendarList
                        markedDates={{...this.props.disableDates, ...this.props.selectedDates}}
                        markingType={'period'}
                        pastScrollRange={0}
                        futureScrollRange={12}
                        minDate={moment().format("YYYY-MM-DD")}
                        onDayPress={day => {
                            const keys = Object.keys(this.props.disableDates);
                            const period = this.props.period;

                            const isDisabled = keys.includes(day.dateString);

                            const isDisableBetween = moment(day.dateString).isAfter(period.startDate) &&
                                moment(day.dateString).isAfter(period.endDate) &&
                                keys.some(key => moment(key).isAfter(period.startDate) && moment(key).isBefore(day.dateString));

                            if (!isDisabled && !isDisableBetween)
                                this.props.setDatePeriod(day, period);
                            if (!isDisabled && isDisableBetween) this.props.setDatePeriod(day, period, true);
                        }}
                    />
                </View>
                <View style={{backgroundColor: "white", flex: 1, justifyContent: "center"}}>
                    <ButtonNative
                        styles={!this.props.period.selectedStart ? styles.continueDisabledButton : styles.continueButton}
                        disabled={!this.props.period.selectedStart}
                        onPress={() => this.addObjectToRent()}
                    >
                        <Text style={{color: "white", fontWeight: "bold"}}>Continuar</Text>
                    </ButtonNative>
                </View>
            </View>
        )
    }

    render() {
        return (
            <VerifyInformation
                {...this.props}
                redirectRoute="SelectRange"
            >
                {this.props.loading ? this.renderSpinner({height: "100%", alignItems: "center", justifyContent: "center"}) : this.renderCalendar()}
            </VerifyInformation>
        )
    }
}


const mapStateToProps = state => {
    return {
        transaction: state.Rent.rent,
        disableDates: state.Rent.disableDates,
        selectedDates: state.Rent.selectedDates,
        period: state.Rent.period,
        loading: state.Rent.loadingAvailability || state.Rent.loading,
        objectID: state.Community.productDetails.ID,
        token: state.Login.token
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        newRent(token) {
            dispatch(RentActions.setLoading(true))
            dispatch(RentActions.newRent(token))
        },
        getAvailability(token, objectID, params) {
            dispatch(RentActions.setLoadingAvailability(true));
            dispatch(RentActions.getAvailability(token, objectID, params))
        },
        setDatePeriod(date, actualPeriod, selectOne) {
            dispatch(RentActions.setDatePeriod(date, actualPeriod, selectOne));
        },
        addObjectToRent(token, formData, transactionID) {
            dispatch(RentActions.setLoading(true));
            dispatch(RentActions.addObjectToRent(token, formData, transactionID))
        },

        setDeliveryDate(date) {
            dispatch(RentActions.setDeliveryDate(date));
        },

        setPickupDate(date) {
            dispatch(RentActions.setPickupDate(date));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SelectRange);

