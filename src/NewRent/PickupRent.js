import React, { Component } from 'react';
import RentActions from './Rent.actions';
import {connect} from "react-redux";
import moment from "moment";
import SelectTimeslot from "../Transactions/SelectTimeSlot";

class PickupRent extends Component {
    componentDidMount() {
        this.props.getOccupiedDates(this.props.token);
        this.props.getTimeslots(this.props.token, {date: Math.round(moment(this.props.minDate).valueOf() / 1000)})
    }

    render() {
        return (
            <SelectTimeslot {...this.props} isPickup={true} />
        )
    }
}


const mapStateToProps = state => {
    return {
        transaction: state.Rent.rent,
        loading: state.Rent.loading || state.Rent.loadingPickUpDays,
        selectedDay: state.Rent.selectedPickupDay,
        selectedHour: state.Rent.selectedPickupHour,
        token: state.Login.token,
        loadingTimeSlots: state.Rent.loadingTimeSlots,
        timeSlots: state.Rent.timeSlots,
        occupiedDays: state.Rent.occupiedDays,
        transactionID: state.Rent.rent.ID,
        disableContinue: state.Rent.disableContinue,
        minDate: moment(state.Rent.selectedDeliveryDay),
        maxDate: moment(state.Rent.period.endDate),
        redirectRoute: "RentSummary",
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        getOccupiedDates(token) {
            dispatch(RentActions.setLoadingPickupDates(true));
            dispatch(RentActions.getOccupiedDates(token))
        },
        getTimeslots(token, params) {
            dispatch(RentActions.setLoadingTimeslots(true));
            dispatch(RentActions.getTimeslots(token, params))
        },
        setDate(date) {
            dispatch(RentActions.setPickupDate(date))
        },
        modifyTransaction(token, formData, transactionID) {
            dispatch(RentActions.setLoading(true));
            dispatch(RentActions.modifyRent(token, formData, transactionID))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(PickupRent);
