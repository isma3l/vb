import moment from "moment";

const initialState = {
    items: {},
    loading: true,
    loadingPickUpDays: true,
    loadingTimeSlots: true,
    loadingAvailability: true,
    loadingPrices: true,
    dataPerDay: {},
    products: [],
    params: {},
    selectedDeliveryDay: moment().valueOf(),
    selectedDeliveryHour: undefined,
    selectedPickupDay: moment().valueOf(),
    selectedPickupHour: undefined,
    timeSlots: [],
    occupiedDays: [],
    rent: {
        Address: {
            Address: ""
        }
    },
    disableContinue: true,
    disableDates: {},
    selectedDates: {},
    period: {
        startDate: "",
        endDate: "",
        selectedStart: false,
        selectedEnd: false
    }
};

const Deposit = (state = initialState, action) => {
    switch (action.type) {
        case "SET_LOADING_RENT":
            return {...state, loading: action.loading}
        case "SET_LOADING_PICKUP_DAYS_RENT":
            return {...state, loadingPickUpDays: action.loading}
        case "SET_LOADING_TIMESLOTS_RENT":
            return {...state, loadingTimeSlots: action.loading}
        case "SET_LOADING_AVAILABILITY":
            return {...state, loadingAvailability: action.loading}
        case "SET_LOADING_RENT_PRICES":
            return {...state, loadingPrices: action.loading}
        case "SET_AVAILABILITY":
            return {...state, disableDates: action.disableDays, selectedDates: {}}
        case "SET_PERIOD":
            return {...state, period: {...state.period, ...action.period}, selectedDates: action.selectedDates}
        case "SET_CURRENT_RENT":
            return {...state, rent: {...state.rent, ...action.rent}, loading: false, disableContinue: true}
        case "SET_OCCUPIED_DAYS_RENT":
            return {...state, occupiedDays: action.occupiedDays, loadingPickUpDays: false}
        case "SET_TIMESLOTS_RENT":
            return {...state, timeSlots: action.timeSlots, loadingTimeSlots: false}
        case "SET_PICKUP_RENT_DATE":
        case "SET_DELIVERY_RENT_DATE":
            return {...state, ...action.date}
        case "SET_RENT_PRICES":
            return {...state, prices: action.prices, loadingPrices: false}
        case "BACK_TO_INITIAL_STATE":
            return {...initialState};
        default:
            return state;
    }
};

export default Deposit;