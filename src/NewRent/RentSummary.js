import React, { Component } from 'react';
import {Text,  View,  TouchableOpacity, ActivityIndicator, FlatList, ScrollView} from 'react-native';
import {List, ListItem, FormLabel, FormInput, FormValidationMessage, Avatar} from "react-native-elements";
import Icon from 'react-native-vector-icons/MaterialIcons';
import RentActions from './Rent.actions';
import {connect} from "react-redux";
import {Colors} from "../Layout/Colors";
import {DepositStyles, styles, VerifyPhoneStyles} from "../Styles";
import ButtonNative from "../MyComponents";
import {FadeInView} from "../AnimatedComponents";
import {NavigationActions} from "react-navigation";
import moment from "moment/moment";
import KeyboardView from "../KeyboardView";

moment.locale('es');

class RentSummary extends Component {
    componentDidMount() {
        const props = this.props;
        this.props.getPrices(props.token, props.rent.Delivery.ID, props.rent.Pickup.ID);
    }

    state = {
        comments: ""
    }

    finishRent = (token, transactionID) => {
        const comments = this.state.comments;
        if (comments === "") {
            this.props.finishTransaction(token, transactionID);
        } else {
            const formData = new FormData();
            formData.append("comments", comments);

            this.props.modifyTransaction(token, formData, transactionID, true);
        }
    }

    renderSpinner = (styles) => (
        <View style={styles}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    renderRentSummary = () => {
        const rent = this.props.rent;
        const card = rent.Card;
        const minutesZone = moment().utcOffset();

        const delivery = rent.Delivery;
        let deliveryDate = delivery.Reserve.Date;
        deliveryDate = moment(deliveryDate).subtract(minutesZone, 'minutes')
            .add(delivery.Reserve.TimeSlot.Hour, "hours")
            .add(delivery.Reserve.TimeSlot.Minute, "minutes")
            .format('LLLL');

        const pickup = rent.Pickup;
        let pickupDate = pickup.Reserve.Date;
        pickupDate = moment(pickupDate).subtract(minutesZone, 'minutes')
            .add(pickup.Reserve.TimeSlot.Hour, "hours")
            .add(pickup.Reserve.TimeSlot.Minute, "minutes")
            .format('LLLL');

        const subtitleCard = card.ID === 0 ? "No seleccionada" : "Tarjeta terminada en " + card.LastDigits + " (" + card.Type +")";

        const prices = this.props.prices;
        const totalPrice = prices.delivery + prices.pickup + rent.Price;

        return (
            <KeyboardView style={{height: "100%"}}>
                <ScrollView contentContainerStyle={{minHeight: "100%"}}>
                    <View>
                        <List containerStyle={styles.profileList}>
                            <View style={{borderBottomWidth: 1, borderBottomColor: Colors.borderColor}}>
                                <ListItem
                                    title={"Domicilio"}
                                    component={ButtonNative}
                                    wrapperStyle={{paddingTop: 10, paddingBottom: 10, margin: 0}}
                                    subtitleStyle={{width: "100%", flexWrap: "wrap", paddingRight: 10}}
                                    subtitle={rent.ClientAddress.Address}
                                    leftIcon={{name: "location-on", color: Colors.primaryColor}}
                                    onPress={() => this.props.navigation.navigate("AddressList", {transactionID: this.props.transactionID, modifyTransaction: this.props.modifyTransaction})}
                                    hideChevron={true}
                                />
                            </View>
                            <ListItem
                                title={"Fecha y hora Delivery"}
                                subtitle={deliveryDate}
                                leftIcon={{name: "calendar-clock", type: "material-community", color: Colors.primaryColor}}
                                hideChevron={true}
                            />
                            <ListItem
                                title={"Fecha y hora Pickup"}
                                subtitle={pickupDate}
                                leftIcon={{name: "calendar-clock", type: "material-community", color: Colors.primaryColor}}
                                hideChevron={true}
                            />
                        </List>
                    </View>
                    <View>
                        <View style={{backgroundColor: "white", borderBottomColor: Colors.borderColor, borderBottomWidth: 1, paddingBottom: 10}}>
                            <FormLabel>OBJETO</FormLabel>
                        </View>
                        <List containerStyle={styles.profileList}>
                            <ListItem
                                title={this.props.object.Name}
                                subtitle={this.props.object.Description}
                                leftIcon={{name: "box", type: "entypo", color: Colors.primaryColor}}
                                hideChevron={true}
                            />
                            <ListItem
                                title={"Alquilar desde:"}
                                subtitle={moment(rent.StartDate).subtract(minutesZone, "minutes").format("LL")}
                                leftIcon={{name: "calendar", type: "material-community", color: Colors.primaryColor}}
                                hideChevron={true}
                            />
                            <ListItem
                                title={"Alquilar hasta:"}
                                subtitle={moment(rent.EndDate).subtract(minutesZone, "minutes").format("LL")}
                                leftIcon={{name: "calendar", type: "material-community", color: Colors.primaryColor}}
                                hideChevron={true}
                            />
                        </List>
                    </View>
                    <View>
                        <View style={{backgroundColor: "white", borderBottomColor: Colors.borderColor, borderBottomWidth: 1, paddingBottom: 10}}>
                            <FormLabel>MÉTODO DE PAGO</FormLabel>
                        </View>
                        <List containerStyle={styles.profileList}>
                            <View style={{borderBottomWidth: 1, borderBottomColor: Colors.borderColor}}>
                                <ListItem
                                    title={"Tarjeta de Crédito"}
                                    component={ButtonNative}
                                    subtitle={subtitleCard}
                                    onPress={() => this.props.navigation.navigate("CreditCardList", {transactionID: this.props.transactionID, modifyTransaction: this.props.modifyTransaction})}
                                    wrapperStyle={{paddingTop: 10, paddingBottom: 10, margin: 0}}
                                    leftIcon={{name: "credit-card", color: Colors.primaryColor}}
                                    hideChevron={true}
                                />
                            </View>
                        </List>
                    </View>
                    <View>
                        <View style={{backgroundColor: "white", borderBottomColor: Colors.borderColor, borderBottomWidth: 1, paddingBottom: 10}}>
                            <FormLabel>FACTURACIÓN</FormLabel>
                        </View>
                        <List containerStyle={styles.profileList}>
                            <ListItem
                                title={"Delivery"}
                                rightIcon={<View style={{flexDirection: "row", alignItems: "center"}}>
                                    <Text>${prices.delivery.toFixed(2)}</Text>
                                </View>}
                            />
                            <ListItem
                                title={"Pickup"}
                                rightIcon={<View style={{flexDirection: "row", alignItems: "center"}}>
                                    <Text>${prices.pickup.toFixed(2)}</Text>
                                </View>}
                            />
                            <ListItem
                                title={"Precio del alquiler"}
                                rightIcon={<View style={{flexDirection: "row", alignItems: "center"}}>
                                    <Text>${rent.Price.toFixed(2)}</Text>
                                </View>}
                            />
                            <ListItem
                                title={"Total"}
                                rightIcon={<View style={{flexDirection: "row", alignItems: "center"}}>
                                    <Text style={{color: "black"}}>${totalPrice.toFixed(2)}</Text>
                                </View>}
                            />
                        </List>
                    </View>
                    <View style={{backgroundColor: "white"}}>
                        <FormLabel>COMENTARIO</FormLabel>
                        <FormInput
                            placeholder={"Ingrese un comentario"}
                            multiline={true}
                            inputStyle={styles.colorInputTextField}
                            underlineColorAndroid={Colors.primaryColor}
                            onChangeText={text => this.setState({comments: text})}
                        />
                    </View>
                    <View style={{backgroundColor: "white", justifyContent: "center", marginTop: "auto", paddingTop: 10}}>
                        <ButtonNative
                            onPress={() => this.finishRent(this.props.token, this.props.rent.ID)}
                            styles={card.ID === 0 ? styles.continueDisabledButton : styles.continueButton}
                            disabled={card.ID === 0}
                        >
                            <Text style={{color: "white", fontWeight: "bold"}}>Confirmar alquiler</Text>
                        </ButtonNative>
                    </View>
                </ScrollView>
            </KeyboardView>
        )
    }

    render() {
        return (
            <FadeInView >
                {this.props.loading ? this.renderSpinner({height: "100%", alignItems: "center", justifyContent: "center"}) : this.renderRentSummary()}
            </FadeInView>
        )
    }
}


const mapStateToProps = state => {
    return {
        rent: state.Rent.rent,
        loading: state.Rent.loading || state.Rent.loadingPrices,
        token: state.Login.token,
        object: state.Community.productDetails,
        transactionID: state.Rent.rent.ID,
        prices: state.Rent.prices
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        getPrices(token, deliveryID, pickupID) {
            dispatch(RentActions.setLoadingPrices(true))
            dispatch(RentActions.getPrices(token, deliveryID, pickupID))
        },
        modifyTransaction(token, formData, transactionID, finishTransaction) {
            dispatch(RentActions.setLoading(true));
            dispatch(RentActions.modifyRent(token, formData, transactionID, finishTransaction))
        },
        finishTransaction(token, transactionID) {
            dispatch(RentActions.setLoading(true));
            dispatch(RentActions.finishRent(token, transactionID))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(RentSummary);
