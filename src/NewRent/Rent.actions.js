import {Endpoint, Community, ObjectAdmin, Transactions, Timeslots, RentManager} from "../Endpoint";
import axios from "axios";
import {Alert} from "react-native";
import moment from "moment";
import {navigatorRef} from "../../App";
import {NavigationActions} from "react-navigation";
import {Colors} from "../Layout/Colors";

export default {
    setLoading(boolean) {
        return{type: "SET_LOADING_RENT", loading: boolean}
    },
    setLoadingAvailability(boolean) {
        return{type: "SET_LOADING_AVAILABILITY", loading: boolean}
    },
    setLoadingPickupDates(boolean) {
        return{type: "SET_LOADING_PICKUP_DAYS_RENT", loading: boolean}
    },
    setLoadingTimeslots(boolean) {
        return{type: "SET_LOADING_TIMESLOTS_RENT", loading: boolean}
    },
    setLoadingPrices(boolean) {
        return{type: "SET_LOADING_RENT_PRICES", loading: boolean}
    },
    setDatePeriod(day, actualPeriod, selectOne = false) {
        return dispatch => {
            const selectedDay = moment(day.dateString);
            const currentStartDate = moment(actualPeriod.startDate);
            const currentEndDate = moment(actualPeriod.endDate);

            if ((actualPeriod.startDate === "" && actualPeriod.endDate === "") || selectOne || selectedDay.isSameOrBefore(currentEndDate)) {
                dispatch({
                    type: "SET_PERIOD",
                    period: {
                        startDate: day.dateString,
                        endDate: day.dateString,
                        selectedStart: true,
                        selectedEnd: false
                    },
                    selectedDates: {
                        [day.dateString]: {
                            startingDay: true,
                            endingDay: true,
                            color: Colors.primaryColor,
                            textColor: "white"
                        }
                    }
                });
                return;
            }

            let selectedDates = {};

            const endDate = selectedDay;

            let currentDate = moment(day.dateString);

            while (currentDate.isSameOrAfter(currentStartDate)) {
                let date = {color: Colors.primaryColor, textColor: "white"};

                if (currentDate.isSame(endDate)) date = {...date, endingDay: true}
                if (currentDate.isSame(currentStartDate)) date = {...date, startingDay: true}

                selectedDates = {...selectedDates, [currentDate.format("YYYY-MM-DD")]: date}
                currentDate = currentDate.subtract(1, "day");
            }

            dispatch({
                type: "SET_PERIOD",
                period: {
                    endDate: moment(endDate).format("YYYY-MM-DD")
                },
                selectedDates
            })
        }
    },

    fix_key(key) {
        return key.replace('Address', 'ClientAddress');
    },

    newRent(token) {
        return dispatch => {
            axios.get(RentManager + "/rent/created", {headers: {Authorization: token}, params: {new: "new", type: "1"}})
                .then(response => {
                    const json = response.data.description;
                    const rent = Object.assign({}, ...Object.keys(json).map(key => ({[this.fix_key(key)]: json[key]})));
                    dispatch({type: "SET_CURRENT_RENT", rent: rent});
                })
                .catch(error => {
                    console.log(error.response);
                    dispatch(this.setLoading(false));
                    Alert.alert("Error", error.response.data.description);
                })
        }
    },
    getAvailability(token, objectID, params) {
        return dispatch => {
            axios.get(RentManager + "/object/"+ objectID + "/availability", {headers: {Authorization: token}, params: {...params, daysAmount: 365}})
                .then(response => {
                    dispatch(this.setLoadingAvailability(false));
                    const periodDisable = response.data.description;
                    let daysDisable = {};
                    if (periodDisable) {
                        periodDisable.forEach(period => {
                            const minutesZone = moment().utcOffset();
                            const startDate = moment(period.StartDate).subtract(minutesZone, "minutes");
                            let currentDate = moment(period.EndDate).subtract(minutesZone, "minutes");
                            while (currentDate.isSameOrAfter(startDate)) {
                                let date = {disabled: true, disableTouchEvent: true};
                                daysDisable = {...daysDisable, [currentDate.format("YYYY-MM-DD")]: date}
                                currentDate = currentDate.subtract(1, "day");
                            }
                        })
                    }
                    dispatch({type: "SET_AVAILABILITY", disableDays: daysDisable});
                })
                .catch(error => {
                    console.log(error.response);
                    Alert.alert("Error", error.response.data.description);
                    dispatch(this.setLoadingAvailability(false));
                })
        }
    },
    getRent(token) {
        return dispatch => {
            axios.get(RentManager + "/rent/created", {headers: {Authorization: token}, params: {type: 3}})
                .then(response => {
                    dispatch(this.setLoading(false));
                    dispatch({type: "SET_CURRENT_RENT", rent: response.data.description});
                })
                .catch(error => {
                    console.log(error.response);
                    dispatch(this.setLoading(false));
                    Alert.alert("Error", error.response.data.description);
                })
        }
    },
    getOccupiedDates(token) {
        return dispatch => {
            axios.get(Timeslots + "/timeSlot/occupiedDates", {headers: {Authorization: token}, params: {daysOnwards: 60}})
                .then(response => {
                    dispatch({type: "SET_OCCUPIED_DAYS_RENT", occupiedDays: response.data.description});
                })
                .catch(error => {
                    console.log(error.response);
                    Alert.alert("Error", error.response.data.description);
                    dispatch(this.setLoadingPickupDates(false));
                })
        }
    },

    getTimeslots(token, params) {
        return dispatch => {
            axios.get(Timeslots + "/timeSlot", {headers: {Authorization: token}, params})
                .then(response => {
                    dispatch({type: "SET_TIMESLOTS_RENT", timeSlots: response.data.description});
                })
                .catch(error => {
                    console.log(error.response);
                    dispatch(this.setLoadingTimeslots(false));
                    Alert.alert("Error", error.response.data.description);
                })
        }
    },

    setDeliveryDate(date) {
        if (date.selectedDay) {
            return {type: "SET_DELIVERY_RENT_DATE", date: {selectedDeliveryDay: date.selectedDay, disableContinue: date.disableContinue, selectedDeliveryHour: undefined}}
        }
        return {type: "SET_DELIVERY_RENT_DATE", date: {selectedDeliveryHour: date.selectedHour, disableContinue: date.disableContinue}}
    },

    setPickupDate(date) {
        if (date.selectedDay) {
            return {type: "SET_PICKUP_RENT_DATE",  date: {selectedPickupDay: date.selectedDay, disableContinue: date.disableContinue, selectedPickupHour: undefined}}
        }
        return {type: "SET_PICKUP_RENT_DATE",  date: {selectedPickupHour: date.selectedHour, disableContinue: date.disableContinue}}
    },

    modifyRent(token, formData, rentID, finishTransaction = false) {
        return dispatch => {
            axios.put(RentManager + "/rent/" + rentID, formData, {headers: {Authorization: token}})
                .then(response => {

                    const json = response.data.description;
                    const rent = Object.assign({}, ...Object.keys(json).map(key => ({[this.fix_key(key)]: json[key]})));
                    dispatch({type: "SET_CURRENT_RENT", rent});

                    if (finishTransaction) {
                        dispatch(this.finishRent(token, rentID))
                    } else {
                        dispatch(this.setLoading(false));
                    }
                })
                .catch(error => {
                    console.log(error.response);
                    navigatorRef.dispatch(NavigationActions.back());
                    dispatch(this.setLoading(false));
                    Alert.alert("Error", error.response.data.description);
                })
        }
    },

    addObjectToRent(token, formData, transactionID) {
        return dispatch => {
            axios.put(RentManager + "/rent/" + transactionID + "/object", formData, {headers: {Authorization: token}})
                .then(response => {
                    dispatch(this.setLoading(false));
                })
                .catch(error => {
                    console.log(error.response);
                    dispatch(this.setLoading(false));
                    navigatorRef.dispatch(NavigationActions.back());
                    Alert.alert("Error", error.response.data.description);
                })
        }
    },

    finishRent(token, rentID) {
        return dispatch => {
            axios.put(RentManager + "/rent/" + rentID + "/confirm", null, {headers: {Authorization: token}})
                .then(response => {
                    dispatch(this.setLoading(false));
                    Alert.alert("Éxito", response.data.description);
                    navigatorRef.dispatch(NavigationActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({routeName: "Layout"})]
                    }));
                    dispatch({type: "BACK_TO_INITIAL_STATE"})
                })
                .catch(error => {
                    console.log(error.response);
                    dispatch(this.setLoading(false));
                    Alert.alert("Error", error.response.data.description);
                })
        }
    },

    getPrices(token, deliveryID, pickupID) {
        return dispatch => {
            const getDeliveryPrice = () => axios.get(Transactions + "/transactions/price/" + deliveryID, {headers: {Authorization: token}});
            const getPickupPrice = () => axios.get(Transactions + "/transactions/price/" + pickupID, {headers: {Authorization: token}});

            axios.all([getDeliveryPrice(), getPickupPrice()])
                .then(axios.spread((deliveryResponse, pickupResponse) => {
                    dispatch({type: "SET_RENT_PRICES", prices: {delivery: deliveryResponse.data.description, pickup: pickupResponse.data.description}});
                }))
                .catch(response => {
                    this.setLoadingPrices(false);
                    Alert.alert("Error", response.response.data.description);
                    console.log(response.response);
                })
        }
    }
}
