import React, { Component } from 'react';
import {connect} from 'react-redux';
import {ActivityIndicator, View, Text, Platform, StatusBar, AlertIOS} from 'react-native';
import {FormInput} from "react-native-elements";
import {FloatingAction} from "react-native-floating-action";
import Icon from "react-native-vector-icons/MaterialIcons";
import Actions from "./ShareCatalog.actions";
import {Colors} from "../Layout/Colors";
import {alertShareStyles, styles} from "../Styles";
import {Catalogs} from "./CatalogList";
import ButtonNative from "../MyComponents";
import AwesomeAlert from "react-native-awesome-alert";

class SharedWithOthers extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            tabBarLabel: 'Compartidos con otros',
            headerTintColor: Colors.primaryColor,
            tabBarIcon: ({ tintColor }) => (
                <Icon name="share" size={24} color={tintColor}/>
            ),
        }
    };

    state = {
        modalVisible: false,
        text: ""
    }

    componentDidMount () {
        this.props.sharedCatalogWithOthers();
    }


    renderSpinner = () => (
        <View style={styles.centerContainer}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    renderAlertBody = (message) => {
        return (
            <View>
                <StatusBar backgroundColor={"rgba(49,49,49, 0.7)"}/>
                <View style={Platform.OS === "ios" ? {} :{paddingLeft: 15}}>
                    <Text style={Platform.OS === "ios" ? {} : {fontSize: 16}}>{message}</Text>
                </View>
                <FormInput
                    placeholder="Email"
                    inputStyle={styles.colorInputTextField}
                    keyboardType="email-address"
                    autoCapitalize="none"
                    onChangeText={text => this.setState({email: text})}
                />
            </View>

        )
    }

    shareCatalog = (email) => {
        const formData = new FormData();
        formData.append("requester", email);
        this.props.shareCatalog(formData)
    }

    openAlert = () => {
        const title = "Compartir Catálogo"
        const message = "Ingresá el e-mail de la persona que quieras que vea tu catálogo.";
        if (Platform.OS === "android")
            this.awesomeAlert.alert(
                title,
                this.renderAlertBody(message),
                [
                    {text: "CANCELAR", onPress: () => console.log("Cancel touch")},
                    {text: "ACEPTAR", onPress: () => this.shareCatalog(this.state.email)}
                ]
            )
        else
            AlertIOS.prompt(
                title,
                message,
                [
                    {text: "Cancelar", onPress: () => console.log("Cancel touch")},
                    {text: "Aceptar", onPress: mail => this.shareCatalog(mail)}
                ],
                'plain-text',
                "",
                'email-address'
            )
    }

    renderSharedWithOthers = () => {
        return (
            <View>
                {Platform.OS === "ios" &&
                <View style={{justifyContent: "center", alignItems: "center", marginBottom: 20, marginTop: 20}}>
                    <ButtonNative
                        onPress={() => this.openAlert()}
                        styles={{
                            flexDirection: "row",
                            backgroundColor: Colors.primaryColor,
                            justifyContent: "center",
                            padding: 5,
                            borderRadius: 5
                        }}
                    >
                        <Icon
                            name="share"
                            size={26}
                            color={"white"}
                        />
                        <View style={{marginLeft: 10, justifyContent: "center"}}>
                            <Text style={{color: "white"}}>Compartir Catálogo</Text>
                        </View>
                    </ButtonNative>
                </View>
                }
                <View>
                    <Catalogs
                        {...this.props}
                        revokeCatalog={true}
                    />
                </View>
            </View>
        )
    }

    render() {
        return (
            <View style={{flex: 1}}>
                {this.props.loading ? this.renderSpinner() : this.renderSharedWithOthers()}
                <AwesomeAlert
                    styles={alertShareStyles}
                    ref={ref => (this.awesomeAlert = ref)}
                    transparent={true}
                    animationType={"fade"}
                />
                {Platform.OS === "android" &&
                <FloatingAction
                    actions={[{
                        icon: <Icon name="share" color='white' size={24}/>,
                        name: "Compartir Catálogo",
                        position: 1,
                        color: Colors.primaryColor
                    }]}
                    buttonColor={Colors.primaryColor}
                    onPressItem={() => {
                        this.openAlert();
                    }}
                    overrideWithAction={true}
                />
                }
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        token: state.Login.token,
        catalogs: state.ShareCatalog.sharedWithOthers,
        loading: state.ShareCatalog.loadingSharedWithOthers
    }
};

const mapDispatchToProps = dispatch =>{
    return {
        sharedCatalogWithOthers() {
            dispatch(Actions.setLoadingSharedWithOthers(true));
            dispatch(Actions.getCatalogsSharedWithOthers())
        },
        revokePermissionToMyCatalog(permissionID) {
            dispatch(Actions.setLoadingSharedWithOthers(true));
            dispatch(Actions.revokePermissionToMyCatalog(permissionID))
        },
        shareCatalog(formData){
            dispatch(Actions.setLoadingSharedWithOthers(true));
            dispatch(Actions.shareCatalog(formData))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SharedWithOthers);
