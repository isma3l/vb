import React from 'react';
import {Colors} from "../Layout/Colors";
import ButtonNative from "../MyComponents";
import {View, ScrollView, Alert, Text} from 'react-native'
import {ListItem, Avatar} from "react-native-elements";
import {FadeInView} from "../AnimatedComponents";

export const Catalogs = (props) => {
    const catalogs = props.catalogs;

    const getTitle = (catalog) => {
        let title = "";
        if (catalog) {
            if (catalog.Name !== "") {
                 title = title + catalog.Name;
            }
            if (catalog.Name !== "" && catalog.LastName !== "") {
               title = title + " "
            }
            if (catalog.LastName !== "") {
                title = title + catalog.LastName;
            }
        }
        if (title === "") title = catalog.Email;
        return title
    }

    const getSubTitle = (catalog) => {
        let title = "";
        if (catalog) {
            if (catalog.Name !== "") {
                title = title + catalog.Name[0].toUpperCase();
            }
            if (catalog.LastName !== "") {
                title = title + catalog.LastName[0].toUpperCase();
            }
        }
        if (title === "") title = "VB";
        return title
    }

    const navigateCatalog = (catalog) => {
        props.cleanSearch();
        props.setSharedUserID(catalog.UserID);
        props.getCatalogObjects(props.params, catalog.UserID);
        props.navigation.navigate("Catalog", {name: getTitle(catalog)});
    }

    const catalogView = catalogs.map((catalog, i) => {
        return (
            <View key={catalog.ID} style={props.revokeCatalog ? null : {borderBottomWidth: 1, borderBottomColor: Colors.borderColor}}>
                <ListItem
                    title={getTitle(catalog)}
                    component={props.revokeCatalog ? null : ButtonNative}
                    onPress={() => props.revokeCatalog ? null : navigateCatalog(catalog)}
                    wrapperStyle={props.revokeCatalog ? null : {paddingTop: 10, paddingBottom: 10, margin: 0}}

                    subtitle={catalog.Email}
                    titleStyle={{marginLeft: 10}}
                    subtitleStyle={{marginLeft: 10}}
                    leftIcon={catalog.ProfilePicture !== "" ?
                        <Avatar rounded source={{uri: catalog.ProfilePicture}}/> :
                        <Avatar rounded title={getSubTitle(catalog)} />
                    }
                    rightIcon={props.revokeCatalog ? {name: "close", color: "black"} : {}}
                    onPressRightIcon={() => Alert.alert(
                        "Quitar acceso a catálogo",
                        "¿Estás seguro de que querés quitarle el acceso a " + catalog.Email,
                        [{text: "Aceptar", onPress: () => {
                                props.revokePermissionToMyCatalog(catalog.ID)
                            }}, {text: "Cancelar"}]
                    )}
                    hideChevron={!props.revokeCatalog}
                />
            </View>
        )
    })

    const renderNotCatalogs = (props) => (
        <View style={{backgroundColor: "transparent", justifyContent: "center", alignItems: "center", padding: 10}}>
            <Text style={{fontSize: 25, textAlign: "center"}}>
                {props.revokeCatalog ? "No compartiste tu catálogo con nadie" : "Nadie te compartió su catálogo"}
            </Text>
        </View>
    )

    return (
        <FadeInView style={catalogs.length > 0 ? {backgroundColor: "white"} : {}}>
            <ScrollView>
                {catalogs.length > 0 ? catalogView : renderNotCatalogs(props)}
            </ScrollView>
        </FadeInView>
    )
}
