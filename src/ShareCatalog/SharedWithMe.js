import React, { Component } from 'react';
import {connect} from 'react-redux';
import {ActivityIndicator, View} from 'react-native'
import Icon from "react-native-vector-icons/MaterialCommunityIcons"
import Actions from "./ShareCatalog.actions";
import {Colors} from "../Layout/Colors";
import {styles} from "../Styles";
import {Catalogs} from "./CatalogList";

class SharedWithMe extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            tabBarLabel: 'Compartidos conmigo',
            headerTintColor: Colors.primaryColor,
            tabBarIcon: ({ tintColor }) => (
                <Icon name="share" size={24} color={tintColor}/>
            ),
        }
    };

    componentDidMount () {
        this.props.sharedCatalogWithMe();
    }

    renderSpinner = () => (
        <View style={styles.centerContainer}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    render() {
        return (
            <View style={{flex: 1}}>
                {this.props.loading ? this.renderSpinner() : <Catalogs {...this.props}/>}
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        token: state.Login.token,
        catalogs: state.ShareCatalog.sharedWithMe,
        loading: state.ShareCatalog.loadingSharedWithMe,
        params: state.ShareCatalog.params
    }
};

const mapDispatchToProps = dispatch =>{
    return {
        sharedCatalogWithMe() {
            dispatch(Actions.setLoadingSharedWithMe(true));
            dispatch(Actions.getCatalogsSharedWithMe())
        },
        getCatalogObjects(params, userID) {
            dispatch(Actions.setLoadingSharedWithMe(true));
            dispatch(Actions.getCatalogObjects(userID, params))
        },
        setSharedUserID(id) {
            dispatch(Actions.setSharedUserID(id))
        },
        cleanSearch() {
            dispatch({type: "CLEAN_SEARCH"});
        }

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SharedWithMe);
