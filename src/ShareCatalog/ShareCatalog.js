import React, { Component } from 'react';
import {connect} from 'react-redux';
import {ActivityIndicator, View} from 'react-native'
import Icon from 'react-native-vector-icons/Entypo';
import Items from "../Items/Items";
import Actions from "./ShareCatalog.actions";
import {Colors} from "../Layout/Colors";
import {FadeInView} from "../AnimatedComponents";

class ShareCatalog extends Component {
    static navigationOptions = {
        tabBarIcon: ({ tintColor }) => (
            <Icon name="box" size={24} color={tintColor}/>
        ),
    };

    componentDidMount() {
        this.props.setSearchBar(false);
    }

    renderItems = (products) => (
        <Items
            {...this.props}
            getInfo={this.props.getCatalogObjects}
            loadMoreItems={this.props.loadMoreCatalogObjects}
            multiplier={this.props.Catalog.offsetMultiplier}
            items={products}
            paramsSearch={this.props.Catalog.params}
            isCommunity={false}
            isSharedCatalog={true}
            token={this.props.token}
            userID={this.props.userID}
        />
    )


    renderSpinner = () => (
        <View style={{height: "100%", justifyContent: "center"}}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    render() {
        const catalogObjects = this.props.Catalog.products;
        return (
            <FadeInView>
                {this.renderItems(catalogObjects)}
            </FadeInView>
        )
    }
}

const mapStateToProps = state => {
    return {
        Catalog: state.ShareCatalog,
        token: state.Login.token,
        userID: state.ShareCatalog.userID,
        loading: state.ShareCatalog.loadingSharedWithMe || state.Category.loadingSharedCatalog
    }
};

const mapDispatchToProps = dispatch =>{
    return {
        getItem(id, userID){
            dispatch(Actions.setLoadingSharedWithMe(true));
            dispatch(Actions.getItem(userID, id));
        },
        setLoading(boolean){
            dispatch(Actions.setLoadingSharedWithMe(boolean));
        },
        cleanSearch() {
            dispatch({type: "CLEAN_SEARCH"});
        },
        getCatalogObjects(params, userID) {
            dispatch(Actions.setLoadingSharedWithMe(true));
            dispatch(Actions.getCatalogObjects(userID, params))
        },
        setSearchBar(boolean) {
            dispatch(Actions.setSearchBar(boolean))
        },
        setParams(params, token, userID) {
            dispatch(Actions.setParams(params, token, userID))
        },
        loadMoreCatalogObjects(params, userID){
            dispatch(Actions.setLoadingSharedWithMe(true));
            dispatch(Actions.loadMoreCatalogObjects(userID, params))
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ShareCatalog);
