const initialState = {
    loadingSharedWithMe: false,
    loadingSharedWithOthers: false,
    sharedWithMe: [],
    sharedWithOthers: [],
    items: {},
    products: [],
    params: {
        Limit: 10,
        Offset: 0
    },
    offsetMultiplier: 0,
    productDetails: {},
    userID: ""
};

const Profile = (state = initialState, action) => {
    switch (action.type) {
        case "SET_LOADING_SHAREDWITHME":
            return {...state, loadingSharedWithMe: action.loading}
        case "SET_LOADING_SHAREDWITHOTHERS":
            return {...state, loadingSharedWithOthers: action.loading}
        case "GET_CATALOGS_SHAREDWITHME":
            return {...state, sharedWithMe: action.sharedWithMe}
        case "GET_CATALOGS_SHAREDWITHOTHERS":
            return {...state, sharedWithOthers: action.sharedWithOthers}
        case "GET_CATALOG_ITEM":
            return {...state, productDetails: action.item}
        case "GET_CATALOG_OBJECTS":
            return {...state, products: action.items, offsetMultiplier: 1 };
        case "LOAD_MORE_CATALOG_OBJECTS":
            return {...state, products: [...state.products, ...action.items], offsetMultiplier: state.offsetMultiplier + 1}
        case "SET_PARAMS_CATALOG":
            return {...state, params: {...state.params, ...action.params}};
        case "SET_SEARCH_BAR_CATALOG":
            return {...state, searchBar: action.boolean};
        case "CLEAR_SEARCH":
            return {...state, params: {...state.params, name: undefined}}
        case "SET_SHARED_USER_ID":
            return {...state, userID: action.id}
        default:
            return state;
    }
};

export default Profile;