import {Alert} from 'react-native'
import {Community} from '../Endpoint'
import store from '../store'
import {apiPrivate} from '../api'
import {NavigationActions} from 'react-navigation'
import {ApiError} from './schemas'
import {navigatorRef} from '../../App'

export default {
  setLoadingSharedWithMe(boolean) {
    return {type: 'SET_LOADING_SHAREDWITHME', loading: boolean}
  },
  setLoadingSharedWithOthers(boolean) {
    return {type: 'SET_LOADING_SHAREDWITHOTHERS', loading: boolean}
  },
  getCatalogsSharedWithMe() {
    return dispatch => {
      apiPrivate.get(Community + '/catalogPermission', {params: {type: 'received'}})
        .then(response => {
          dispatch(this.setLoadingSharedWithMe(false))
          dispatch({type: 'GET_CATALOGS_SHAREDWITHME', sharedWithMe: response.data.description.catalogPermissions})
        })
        .catch(error => {
          dispatch(this.setLoadingSharedWithMe(false))
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },
  getCatalogsSharedWithOthers() {
    return dispatch => {
      apiPrivate.get(Community + '/catalogPermission', {params: {type: 'granted'}})
        .then(response => {
          dispatch(this.setLoadingSharedWithOthers(false))
          dispatch({
            type: 'GET_CATALOGS_SHAREDWITHOTHERS',
            sharedWithOthers: response.data.description.catalogPermissions,
          })
        })
        .catch(error => {
          dispatch(this.setLoadingSharedWithOthers(false))
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },
  revokePermissionToMyCatalog(permissionID) {
    return dispatch => {
      apiPrivate.delete(Community + '/catalogPermission/' + permissionID)
        .then(response => {
          dispatch(this.getCatalogsSharedWithOthers())
          Alert.alert('Permiso Removido', response.data.description)
        })
        .catch(error => {
          dispatch(this.setLoadingSharedWithOthers(false))
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },
  getCatalogObjects(userID, params = {}) {
    return dispatch => {
      apiPrivate.get(Community + '/catalog/' + userID + '/object', {params})
        .then(response => {
          dispatch(this.setLoadingSharedWithMe(false))
          if (response.data.description.myObjects)
            dispatch({type: 'GET_CATALOG_OBJECTS', items: response.data.description.myObjects})
          else
            dispatch({type: 'GET_CATALOG_OBJECTS', items: []})

        })
        .catch(error => {
          dispatch(this.setLoadingSharedWithMe(false))
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },

  loadMoreCatalogObjects(userID, params = {}) {
    return dispatch => {
      apiPrivate.get(Community + '/catalog/' + userID + '/object', {params})
        .then(response => {
          dispatch(this.setLoadingSharedWithMe(false))
          dispatch({type: 'LOAD_MORE_CATALOG_OBJECTS', items: response.data.description.myObjects})
        })
        .catch(error => {
          dispatch(this.setLoadingSharedWithMe(false))
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },

  getItem(userID, id) {
    return dispatch => {
      apiPrivate.get(Community + '/catalog/' + userID + '/object/' + id)
        .then(response => {
          dispatch({type: 'GET_CATALOG_ITEM', item: response.data.description})
          dispatch(this.setLoadingSharedWithMe(false))
        })
        .catch(error => {
          dispatch(this.setLoadingSharedWithMe(false))
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },
  shareCatalog(formData) {
    return dispatch => {
      apiPrivate.post(Community + '/catalogPermission', formData, {params: {type: 'received'}})
        .then(response => {
          dispatch(this.getCatalogsSharedWithOthers())
          Alert.alert('Éxito', response.data.description)
        })
        .catch(error => {
          dispatch(this.setLoadingSharedWithOthers(false))
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },

  setSearchBar(boolean) {
    return {type: 'SET_SEARCH_BAR', value: boolean}
  },

  setParams(params, userID) {
    return dispatch => {
      dispatch({type: 'SET_PARAMS_CATALOG', params})
      dispatch(this.setLoadingSharedWithMe(true))
      dispatch(this.getCatalogObjects(userID, params))
    }
  },

  clearSearch(userID) {
    return dispatch => {
      dispatch({type: 'CLEAR_SEARCH'})
      dispatch(this.setLoadingSharedWithMe(true))
      dispatch(this.getCatalogObjects(userID, {...store.getState().ShareCatalog.params, name: undefined}))
    }
  },

  setSharedUserID(id) {
    return {type: 'SET_SHARED_USER_ID', id}
  },
}
