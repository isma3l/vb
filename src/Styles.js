import {StyleSheet} from "react-native";
import {Colors} from "./Layout/Colors";
import {Platform, Dimensions} from "react-native";

const DimensionWidth25 = Dimensions.get('window').width * 30 / 100;


export const DepositStyles = StyleSheet.create({
    depositDescription: {
        width: '100%',
        paddingHorizontal: 16,
        paddingTop: 20,
        paddingBottom: 0,
        alignSelf: "center"
    },
    timeSlotContainer: {
        padding: 10,
        width: DimensionWidth25,
        borderWidth: 1,
        borderColor: Colors.borderColor,
        margin: 5,
        borderRadius: 5,
        justifyContent: "center",
        alignItems: "center",
        shadowColor: "black",
        shadowRadius: 2,
        shadowOffset: {width: 2, height: 2},
        shadowOpacity: 0.2
    },
    addressSelector: {
        borderRadius: 5,
        backgroundColor: "white",
        shadowRadius: 2,
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.2,
        padding: 16,
        marginHorizontal: 16,
        marginTop: 10,
    },
    timeSlotSelectedContainer: {
        padding: 10,
        width: DimensionWidth25,
        backgroundColor: Colors.primaryColor,
        margin: 5,
        borderRadius: 5,
        justifyContent: "center",
        alignItems: "center",
        shadowColor: "black",
        shadowRadius: 2,
        shadowOffset: {width: 2, height: 2},
        shadowOpacity: 0.2
    }
})

export const ProductDetailsStyle = StyleSheet.create({
    rentButton: {
        width: "90%",
        height: 40,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: Colors.primaryColor,
        elevation: 2,
        borderRadius: 5,
        marginTop: 20,
        alignSelf: "center"
    },
    sendProductEditButton: {
        width: "40%",
        height: 40,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: Colors.primaryColor,
        elevation: 1,
        alignSelf: "center"
    },
    cancelEditButton: {
        width: "40%",
        height: 40,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "white",
        elevation: 1,
        alignSelf: "center"
    },
    editButton: {
        position: "absolute",
        right: 0,
        top: 0,
        bottom: 0,
        justifyContent: "center",
        alignItems: "center"
    },
    productContainer: {
        marginTop: 20,
        flexGrow: 1,
        backgroundColor: "white",
        width: "100%",
        flexDirection: "column",
        alignContent: "space-between"
    }
})

export const NotObjectStyles = StyleSheet.create ({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent:'space-between',
        alignItems: 'center',
        height: "100%"
    },
    buttonStyle:{
        flex: 1,
        marginLeft: 10,
        marginRight: 10,
        height: 40,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: Colors.primaryColor,
        elevation: 2,
    },
    registerButton:{
        flex: 1,
        marginLeft: 10,
        marginRight: 10,
        height: 40,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: Colors.primaryBackgroundColor,
        elevation: 2,
    },
})

export const VerifyPhoneStyles = StyleSheet.create({
    centerContainer: {
        flex: 1,
        paddingHorizontal:16,
        paddingTop:18
    },
    descriptionContainer: {
        backgroundColor: "white",
        width: "90%",
        borderRadius: 1,
        elevation: 1,
        marginTop: 10,
        marginBottom: 10,
        padding: 10,
        alignSelf: "center"
    },
    description: {
        textAlign: "justify",
        lineHeight: 20
    },
    title: {
        fontSize:30,
        color: "black",
        textAlign:"left",
        fontWeight: "bold"
    },
    subtitle: {
        color: Colors.subtitleLogoColor,
        fontSize:15,
        textAlign:"left",
        marginTop: 8,
        marginBottom: 40
    },
    verifyContainer: {
        backgroundColor: "white",
        width: "90%",
        borderRadius: 1,
        elevation: 1,
        marginTop: 10,
        marginBottom: 10,
        padding: 10,
        justifyContent: "center",
        alignItems: "center"
    },
    phoneNumberContainer: {
        flexDirection: "row",
        alignItems: "center",
        //alignContent: "flex-start",
        marginBottom: Platform.OS === "ios" ? 24 : 0,
        justifyContent:"center"
    },
    containerFormInput: {
        height:90,
        alignItems:"center"
    },
    contentFormInput: {
        paddingHorizontal:0,
        marginLeft:0,
        marginRight:0,
        width:"90%"
    },
    containerDescriptionForm: {
        width:"80%",
        marginBottom:8,
        marginTop:8
    },
    descriptionForm: {
        textAlign:"center",
        fontSize:10,
        color: Colors.subtitleLogoColor
    },
    prefix: {
        width: 60,
        flexGrow: 0
    },
    phoneNumber: {
        flex: 2,
        width: "100%"
    },
    code: {
        width: "100%",
        marginTop: Platform.OS === "ios" ? 10 : 0,
        marginBottom: Platform.OS === "ios" ? 10 : 0,
    }
})

export const EditAvatar = StyleSheet.create({
    changePhoto: {
        width: "90%",
        height: 40,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: Colors.primaryColor,
        elevation: 2,
        marginBottom: 10,
        alignSelf: "center"
    },
    deletePhoto: {
        width: "90%",
        height: 40,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "white",
        elevation: 1,
        marginBottom: 10,
        alignSelf: "center"
    },
    confirmChanges: {
        width: "90%",
        height: 40,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: Colors.primaryColor,
        elevation: 2,
        marginTop: 20,
        alignSelf: "center"
    },
    discardChanges: {
        width: "90%",
        height: 40,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "white",
        elevation: 2,
        marginTop: 20,
        alignSelf: "center"
    }
})

export const styles = StyleSheet.create ({
    colorInputTextField: {
        color: "black",
        width: "100%"
    },
    defaultContainer: {
        backgroundColor: "white",
        width: "90%",
        borderRadius: 1,
        elevation: 1,
        paddingBottom: 10,
        marginTop: 20
    },
    personalData: {
        margin: 10,
        fontWeight: "bold",
        fontSize: 14,
        color: "black"
    },
    paymentMethodLabel: {
        fontWeight: "bold",
        fontSize: 14,
        color: "black",
        marginLeft: 10
    },
    containerItem: {
        position: "relative",
        borderWidth: 0.5,
        borderColor: "transparent"
    },
    profileList: {
        marginTop: 0,
        marginBottom: 20,
        paddingTop: 0,
        borderTopWidth: 0,
        borderBottomWidth: 0,
    },
    labelItem: {
        position: "absolute",
        bottom: 10,
        left: 10,
        zIndex: 2,
        elevation: 2,
        shadowRadius: 5,
        padding: 5,
        borderRadius: 5,
        backgroundColor: Colors.primaryColor
    },

    userIcon: {
        position: "absolute",
        top: 10,
        right: 10,
        zIndex: 2,
    },
    labelColor: {
        color: "white",
        fontSize: 12
    },
    categoryLabel: {
        fontSize: 15,
        marginTop: 5,
        color: Colors.subtitleLogoColor
    },
    priceDetailLabel: {
        fontSize: 34,
        fontWeight: "bold",
        color: Colors.primaryColor
    },
    productDescriptionLabel: {
        fontSize: 17,
        color: Colors.subtitleLogoColor
    },
    boldPrice: {
        fontWeight: "bold"
    },
    loginButton:{
        minWidth: "90%",
        height: 40,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: Colors.primaryColor,
        borderRadius: 5,
        marginBottom: 10,
        marginTop: 10,
        alignSelf: "center",
        paddingLeft: 10,
        paddingRight: 10,
    },
    cancelAppointmentButton:{
        width: "100%",
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: Colors.primaryColor,
        alignSelf: "center"
    },
    buttonStyle:{
        minWidth: "60%",
        height: 40,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: Colors.primaryColor,
        marginBottom: 10,
        alignSelf: "center",
        paddingLeft: 10,
        paddingRight: 10,
    },
    continueButton:{
        minWidth: "90%",
        height: 40,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: Colors.primaryColor,
        marginBottom: 10,
        alignSelf: "center",
        paddingLeft: 10,
        paddingRight: 10,
        borderRadius: 5,
    },
    continueDisabledButton:{
        minWidth: "90%",
        height: 40,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: Colors.disableColor,
        marginBottom: 10,
        alignSelf: "center",
        paddingLeft: 10,
        paddingRight: 10,
        borderRadius: 5,
    },
    titleNewDesign: Platform.OS === "ios" ? {fontSize: 34, fontWeight: "bold"} : {fontSize: 14},
    title: {
        fontSize: 40,
        color: Colors.primaryColor,
        textAlign: "center"
    },
    subtitle: {
        fontSize: 18
    },
    logo: {
        width: 200
    },
    container: {
        width: "100%",
        height: "100%",
        backgroundColor: Colors.backgroundColor
    },
    loginContainer: {
        alignItems:'center',
        minHeight: "100%",
        backgroundColor: Colors.backgroundColor,
    },
    centerContainer: {
        flex: 1,
        alignItems:'center',
        justifyContent:'center'
    },
    addressContainer: {
        flex: 1,
        alignItems:'center',
        justifyContent:'center',
        position: "relative"
    },
    loginInputsContainer: {
        backgroundColor: "white",
        width: "100%",
        alignItems: "center",
        borderRadius: 1,
        elevation: 1,
        marginTop: 10,
        marginBottom: 10,
        paddingBottom: 10
    },
    text: {
        color: '#4f603c'
    },
    forgetPasswordText: {
        color: Colors.primaryColor,
        padding: 10,
        textDecorationLine: 'underline'
    },
    input: {
        margin: 15,
        height: 40,
        width: "90%",
        borderColor: "white"
    },

    submitButton: {
        backgroundColor: '#7a42f4',
        padding: 10,
        margin: 15,
        width: "40%"
    },

    submitButtonText:{
        color: 'white'
    },

    code: {
        marginTop: Platform.OS === "ios" ? 10 : 0,
        marginBottom: Platform.OS === "ios" ? 10 : 0
    },

    floorContainer: {
        alignSelf: "flex-start",
        flexDirection: "row",
        width: "100%",
        justifyContent: "space-between",
        alignContent: "space-between",
        marginBottom: Platform.OS === "ios" ? 10 : 0
    },
    disableButton:{
        minWidth: "60%",
        height: 40,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: Colors.disableColor,
        elevation: 2,
        marginBottom: 10,
        alignSelf: "center",
        paddingLeft: 10,
        paddingRight: 10,
    },
});

export const WelcomeStyle = StyleSheet.create({
    title: Platform.OS === "ios" ?
        {
            fontSize: 34,
            color: Colors.titleLogoColor,
            textAlign: "center",
            fontWeight: "bold",
            lineHeight: 32,
            marginVertical: 5
        } : {
            fontSize: 24,
            color: "#222222",
            textAlign: "center",
            lineHeight: 32,
            margin: 5
    },
    subtitle:
        {
        fontSize: Platform.OS === "ios" ? 17 : 14,
        color: "#5B687C",
        textAlign: "center",
        lineHeight: 20,
        margin: 5
    },
    facebookButton:{
        width: "90%",
        padding: 0,
        elevation: 0,
        alignSelf: "center",
        marginBottom: 20,
    },
    welcomeButtons:{
        width: "90%",
        height: 50,
        elevation: 0,
        borderRadius: 5,
        alignSelf: "center",
        justifyContent: "center"
    },
    buttonText: {
        textAlign: "center",
        color: "white",
        fontSize: Platform.OS === "ios" ? 17 : 14,
    }
})

export const LogoStyle = StyleSheet.create({
    container: Platform.OS === "ios" ? {
        flex: 1,
        margin: 0,
    } : {
        justifyContent: "center",
        flex: 1,
        alignItems: "center"
    },
    title: Platform.OS === "ios" ?
        {
            fontSize: 34,
            color: Colors.titleLogoColor,
            fontWeight: "bold",
            lineHeight: 32,
            textAlign: "left",
        } :
        {
            fontSize: 24,
            color: Colors.titleLogoColor,
            textAlign: "center",
            lineHeight: 32,
            marginVertical: 5
    },
    subtitle: Platform.OS === "ios" ?
        {
            fontSize: 17,
            color: Colors.subtitleLogoColor,
            lineHeight: 24
        } : {
        fontSize: 16,
        color: Colors.subtitleLogoColor,
        lineHeight: 24,
        textAlign: "center"
    }
})

export const alertShareStyles = StyleSheet.create({
    modalContainer : {
        flex: 1,
        backgroundColor: 'rgba(49,49,49, 0.7)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalView: {
        backgroundColor: 'white',
        borderRadius: Platform.OS === "ios" ? 5 : 0,
        width: 275,
        borderColor: 'black',
        borderWidth: StyleSheet.hairlineWidth
    },
    checkBox: {
        marginBottom: 10
    },
    checkBoxText: {
        marginLeft: 4,
        alignSelf: 'center',
        fontSize: 15,
        justifyContent: 'center'
    },
    titleText: {
        fontSize: 20,
        fontWeight: '600',
        padding: 15,
        alignSelf: Platform.OS === "ios" ? 'center' : "flex-start"
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        borderColor: 'gray',
        borderTopWidth: StyleSheet.hairlineWidth
    },
    buttonText: {
        fontSize: 16,
        color: Colors.primaryColor
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 15,
        borderColor: 'gray'
    }
});

export const alertSelectAddressAlert = StyleSheet.create({
    modalContainer : {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalView: {
        backgroundColor: 'white',
        borderRadius: Platform.OS === "ios" ? 5 : 0,
        width: "100%",
        borderColor: 'black',
        borderWidth: StyleSheet.hairlineWidth
    },
    titleContainer: {
        borderColor: Colors.borderColor,
        borderBottomWidth: 1
    },
    titleText: {
        fontSize: 17,
        fontWeight: '600',
        padding: 25,
        alignSelf: Platform.OS === "ios" ? 'center' : "flex-start",
    },
    buttonContainer: {
        flexDirection: 'row',
        borderColor: '#D4D8E0',
        borderTopWidth: StyleSheet.hairlineWidth
    },
    buttonText: {
        fontSize: 16,
        color: Colors.primaryColor
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 15,
        flex: 1,
        borderColor: '#D4D8E0'
    }
});

export const alertAddressStyles = StyleSheet.create({
    modalContainer : {
        flex: 1,
        backgroundColor: 'rgba(49,49,49, 0.7)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalView: {
        backgroundColor: 'white',
        borderRadius: Platform.OS === "ios" ? 15 : 0,
        width: 275,
        borderColor: 'black',
        borderWidth: StyleSheet.hairlineWidth
    },
    checkBox: {
        marginBottom: 10
    },
    checkBoxText: {
        marginLeft: 4,
        alignSelf: 'center',
        fontSize: 15,
        justifyContent: 'center'
    },
    titleText: {
        padding: 0
    },
    buttonContainer: {
        justifyContent: 'center',
        borderColor: 'gray',
        borderTopWidth: StyleSheet.hairlineWidth
    },
    buttonText: {
        fontSize: 17,
        color: Colors.primaryColor
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: "center",
        flex: 1,
        padding: 15
    }
});

export const SmsVerification = StyleSheet.create({
    containerFormInput: {
        width:"20%",
        marginLeft:8,
        marginRight:8,
        borderBottomWidth:0
    },
    input: {
        color: "black",
        width: "100%",
        textAlign: "center",
        borderBottomWidth:1.5
    }
})

export const NotificationStyles = StyleSheet.create({
    containerItem: {
      width:"100%",
      justifyContent:"center",
      alignItems:"center",
      marginVertical:4
    },
    contentItem: {
      backgroundColor:"white",
      width:"96%",
      borderRadius:4,
      paddingVertical:8,
      paddingHorizontal:10,
      shadowOpacity:0.8,
      elevation: 2,
      shadowOffset:{ width:0, height:5 },
      shadowRadius: 5
    },
    message: {
      color: Colors.darkGray,
      marginBottom:4
    },
    date: {
      color: Colors.darkGray,
      fontSize:13
    }
})

export const ProfileStyles = StyleSheet.create({
  container: {
    paddingHorizontal:16,
    paddingBottom: 32
  },
  containerAvatar: {
    marginTop:24,
    marginBottom:16,
    alignItems:"center"
  },
  textField: {
    width: "100%",
    height: 60
  },
  modifiedPassword: {
    alignItems: "flex-start",
    justifyContent: "center"
  },
  containerAddress: {
    marginTop:24,
    marginBottom:8
  },
  titleAddress: {
    fontSize:18,
    color: "black",
    marginBottom:8
  },
  addresses: {
    backgroundColor:"white",
    borderRadius:4,
    shadowOpacity:0.8,
    elevation: 2,
    shadowOffset:{ width:0, height:5 },
    shadowRadius: 5
  },
  linkNewAddress : {
    flexDirection:"row",
    width:"100%",
    justifyContent:"flex-end"
  },
  containerLinkNewAddress: {
    width:"50%",
    alignItems:"flex-end"
  },
  subContainerLinkNewAddress: {
    alignItems: "flex-start",
    justifyContent: "center"
  },
  containerItemAddress: {
    width:"100%",
    paddingTop:12,
    paddingBottom:8,
    paddingLeft:16,
    paddingRight:21,
    borderColor: Colors.borderColor
  },
  titleItemAddress: {
    color: Colors.secondary3Color,
    marginBottom:8
  },
  mainAddress: {
    color:"black",
    fontSize:13
  },
  iconItemAddress: {
    position: "absolute",
    right:0,
    top:16
  }
})
