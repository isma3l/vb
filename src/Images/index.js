export const LOGOWITHNAME = require('./Logo_Colette.png');
export const LOGO = require('./Logo_Colette-solo.png');
export const NOIMAGE = require("./Not_available.png");
export const WELCOMEIMAGE = require("./empty_states_mxm.png");
export const CALENDAR = require("./Calendar.png");
export const BIKE = require('./Bike.png')
export const CLOSEDDEPOSIT = require('./closedDeposit.png');
export const RETIRO_PROGRAMADO = require('./Retiro_Programado.png')
export const CLOTHES = require('./clothes.png')
export const BOX = require('./box.png')
export const BICI = require('./Bici.png')
export const NEWDEPOSITDONE = require('./newDepositDone/newDepositDone.png');
export const ITEMS = require('./Ico/Items/Violet.png');

export const ICON_ACCOUNT = {
  ACTIVE: require('./Ico/Account/Violet/Active.png'),
  INACTIVE: require('./Ico/Account/Violet/Inactive.png'),
}
export const ICON_CHAT = {
  ACTIVE: require('./Ico/Chat/Violet/Active.png'),
  INACTIVE: require('./Ico/Chat/Violet/Inactive.png'),
}
export const ICON_HOME = {
  ACTIVE: require('./Ico/Home/Violet/Active.png'),
  INACTIVE: require('./Ico/Home/Violet/Inactive.png'),
}
export const ICON_NOTIFICATIONS = {
  ACTIVE: require('./Ico/Notifications/Violet/Active.png'),
  INACTIVE: require('./Ico/Notifications/Violet/Inactive.png'),
}
