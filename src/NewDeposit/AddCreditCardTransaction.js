import React, { Component } from 'react';
import {Text, View, ActivityIndicator, Keyboard, ScrollView} from 'react-native'
import {LogoStyle, styles} from "../Styles";
import {Colors} from "../Layout/Colors";
import {connect} from "react-redux";
import CreditCardActions from "../CreditCard/CreditCard.actions";
import {PrimaryHighlightButton} from "../MyComponents"
import DoneBar from "done-bar";
import {CreditCardInput} from "react-native-credit-card-input";
import KeyboardView from "../KeyboardView";
import DepositActions from "./Deposit.actions";

class AddCreditCardTransaction extends Component {
    state = {
        form: {
            valid: false
        },
        keyboardType: "default"
    }

    componentDidMount() {
        this.props.getPaymentMethods();
        this._sub = this.props.navigation.addListener(
            'willBlur',
            () => Keyboard.dismiss()
        );
    }

    componentWillUnmount() {
        this._sub.remove();
    }

    renderSpinner = () => (
        <View style={styles.centerContainer}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    addCreditCard = () => {
        const values = this.state.form.values;
        const card_number = values.number.replace(/\s/g,'');

        const data = {
            "security_code": values.cvc,
            "expiration_month": parseInt(values.expiry.substring(0, 2), 10),
            "expiration_year": 2000 + parseInt(values.expiry.substring(3, 5), 10),
            "card_number": card_number,
            "cardholder": {
                "name": values.name
            }
        };
        this.props.addCreditCardAndModifyTransaction(this.props.token, data, this.props.methods, this.props.transactionID ,this.props.redirectRoute);
    }

    renderAddCreditCard = () => {
        return (
            <View style={{minHeight: '100%'}}>
                <KeyboardView style={{flex: 1}} behavior="position">
                    <ScrollView>
                        <View style={{marginHorizontal: 16, paddingTop: 20}}>
                            <Text style={LogoStyle.title}>
                                Indicanos el
                            </Text>
                            <Text style={LogoStyle.title}>
                                método de pago
                            </Text>
                            <Text style={[LogoStyle.subtitle, {marginTop: 5, lineHeight: 17, marginBottom: 30}]}>
                                Los costos de almacenaje dependen del tamaño del ítem almacenado. Para más información, chequea los costos.
                            </Text>
                        </View>
                        <CreditCardInput
                            labels={{number: "NUMERO DE TARJETA", expiry: "EXP.", cvc: "CVC/CCV", name: "NOMBRE COMPLETO"}}
                            placeholders={{ number: "1234 5678 1234 5678", expiry: "MM/YY", cvc: "CVC", name: "Nombre y Apellido" }}
                            onChange={form => this.setState({form})}
                            onFocus={name => {
                                if (name === "name") {
                                    this.setState({keyboardType: "default"})
                                } else {
                                    this.setState({keyboardType: "numeric"})
                                }
                            }}
                            inputContainerStyle={{borderBottomColor: Colors.primaryColor, borderBottomWidth: 1}}
                            requiresName={true}
                        />
                        <View style={{marginTop: 30, marginBottom: 100}}>
                            <PrimaryHighlightButton
                                styles={this.state.form.valid ? styles.buttonStyle : styles.disableButton}
                                onPress={() => this.addCreditCard()}
                                disabled={!this.state.form.valid}
                                label="Continuar"
                            />
                        </View>
                    </ScrollView>
                </KeyboardView>
                <DoneBar
                  keyboardType={this.state.keyboardType}
                  text={"Cerrar"}
                />
            </View>
        )
    }

    render() {
        return this.props.loading ? this.renderSpinner() : this.renderAddCreditCard()
    }
}

const mapStateToProps = state => {
    return {
        token: state.Login.token,
        loading: state.CreditCard.loading,
        methods: state.CreditCard.methods,
        redirectRoute: state.VerifyInformation.redirectRoute,
        transactionID: state.Deposit.deposit.ID
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        setLoading(boolean) {
            dispatch({type: "SET_LOADING", loading: boolean})
        },
        getPaymentMethods() {
            dispatch({type: "SET_CARDS_LOADING", loading: true});
            dispatch(CreditCardActions.getPaymentMethods());
        },
        addCreditCardAndModifyTransaction(token, data, methods, transactionID, redirectRoute) {
            dispatch({type: "SET_CARDS_LOADING", loading: true})
            dispatch(DepositActions.addCreditCardAndModifyTransaction(token, data, methods, transactionID, redirectRoute))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddCreditCardTransaction);
