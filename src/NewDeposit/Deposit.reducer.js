import moment from "moment";
import {Platform} from "react-native";

const initialState = {
    items: {},
    loading: true,
    loadingPickUpDays: true,
    loadingTimeSlots: true,
    dataPerDay: {},
    products: [],
    params: {},
    selectedDay: Platform.OS === "ios" ? {dateString: moment().format("YYYY-MM-DD")} : moment().valueOf(),
    selectedHour: undefined,
    hourString: "",
    timeSlots: [],
    occupiedDays: [],
    deposit: {
        Address: {
            Address: ""
        }
    },
    disableContinue: true,
    currentAddress: "",
    prices:[], // used for modal
    loadingPrices: false,
    currentCard: ""
};

const Deposit = (state = initialState, action) => {
    switch (action.type) {
        case "SET_LOADING_DEPOSIT":
            return {...state, loading: action.loading}
        case "SET_LOADING_PICKUP_DAYS":
            return {...state, loadingPickUpDays: action.loading}
        case "SET_LOADING_TIMESLOTS":
            return {...state, loadingTimeSlots: action.loading}
        case "SET_CURRENT_DEPOSIT":
            return {...state, deposit: {...state.deposit, ...action.deposit}, loading: false, currentAddress: action.deposit.ClientAddress.ID, currentCard: action.deposit.Card.ID}
        case "SET_OCCUPIED_DAYS":
            return {...state, occupiedDays: action.occupiedDays, loadingPickUpDays: false}
        case "SET_TIMESLOTS":
            return {...state, timeSlots: action.timeSlots, loadingTimeSlots: false}
        case "SET_DEPOSIT_DATE":
            return {...state, ...action.date}
        case "GET_DEPOSIT_PRICES":
            return {...state, prices: action.prices};
        case "SET_CURRENT_ADDRESS":
            return {...state, currentAddress: action.id}
        case "SET_CURRENT_CARD":
            return {...state, currentCard: action.id}
        case "BACK_TO_INITIAL_STATE":
            return {...initialState};
        case "SET_PRICES":
            return {...state, prices: action.prices, loadingPrices: false}
        case "SHOW_LOADING_PRICES":
            return {...state, loadingPrices: action.value}
        default:
            return state;
    }
};

export default Deposit;