import React, { Component } from 'react';
import {Text,  View,  TouchableOpacity, ActivityIndicator, FlatList, ScrollView, Platform} from 'react-native';
import {List, ListItem, FormLabel, FormInput, FormValidationMessage, Avatar} from "react-native-elements";
import Icon from 'react-native-vector-icons/MaterialIcons';
import DepositActions from './Deposit.actions';
import {connect} from "react-redux";
import {Colors} from "../Layout/Colors";
import {DepositStyles, styles, VerifyPhoneStyles} from "../Styles";
import ButtonNative, {PrimaryHighlightButton} from "../MyComponents";
import {FadeInView} from "../AnimatedComponents";
import {NavigationActions} from "react-navigation";
import moment from "moment/moment";
import KeyboardView from "../KeyboardView";
import {TextField} from "react-native-material-textfield";
import { PrimaryButton} from "../MyComponents";


moment.locale('es');

class Summary extends Component {

    state = {
        comments: ""
    }

    componentDidMount(){
        this.props.getDepositPrice(this.props.token, this.props.transactionID)
    }

    finishTransaction = (token, transactionID) => {
        const comments = this.state.comments;
        if (comments === "") {
            this.props.finishTransaction(token, transactionID);
        } else {
            const formData = new FormData();
            formData.append("comments", comments);

            this.props.modifyTransaction(token, formData, transactionID, true);
        }
    }

    renderSpinner = (styles) => (
        <View style={styles}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    renderSummary = () => {
        const deposit = this.props.deposit;
        const card = deposit.Card;
        let date = this.props.deposit.Reserve.Date;
        const minutesZone = moment().utcOffset();
        date = moment(date).subtract(minutesZone, 'minutes')
            .add(deposit.Reserve.TimeSlot.Hour, "hours")
            .add(deposit.Reserve.TimeSlot.Minute, "minutes");
        let customDate = date.format('DD/MMM/YYYY').toUpperCase().replace('.','');
        let time = date.format('hh:mm A');
        const subtitleCard = card.ID === 0 ? "No seleccionada" : "Tarjeta terminada en " + card.LastDigits + " (" + card.Type +")";

        let clientAddress = deposit.ClientAddress;

        if (Platform.OS === "android") {
            return (
                <KeyboardView style={{height: "100%"}}>
                    <ScrollView contentContainerStyle={{minHeight: "100%"}}>
                        <View>
                            <List containerStyle={styles.profileList}>
                                <View style={{borderBottomWidth: 1, borderBottomColor: Colors.borderColor}}>
                                    <ListItem
                                        title={"Domicilio"}
                                        component={ButtonNative}
                                        wrapperStyle={{paddingTop: 10, paddingBottom: 10, margin: 0}}
                                        subtitleStyle={{width: "100%", flexWrap: "wrap", paddingRight: 10}}
                                        subtitle={deposit.ClientAddress.Address}
                                        leftIcon={{name: "location-on", color: Colors.primaryColor}}
                                        onPress={() => this.props.navigation.navigate("AddressList", {transactionID: this.props.transactionID, modifyTransaction: this.props.modifyTransaction})}
                                        hideChevron={true}
                                    />
                                </View>
                                <View style={{borderBottomWidth: 1, borderBottomColor: Colors.borderColor}}>
                                    <ListItem
                                        title={"Fecha y hora"}
                                        component={ButtonNative}
                                        subtitle={customDate + " " + time}
                                        onPress={() => this.props.navigation.dispatch(NavigationActions.back())}
                                        wrapperStyle={{paddingTop: 10, paddingBottom: 10, margin: 0}}
                                        leftIcon={{name: "calendar-clock", type: "material-community", color: Colors.primaryColor}}
                                        hideChevron={true}
                                    />
                                </View>
                            </List>
                        </View>
                        <View>
                            <View style={{backgroundColor: "white", borderBottomColor: Colors.borderColor, borderBottomWidth: 1, paddingBottom: 10}}>
                                <FormLabel>MÉTODO DE PAGO</FormLabel>
                            </View>
                            <List containerStyle={styles.profileList}>
                                <View style={{borderBottomWidth: 1, borderBottomColor: Colors.borderColor}}>
                                    <ListItem
                                        title={"Tarjeta de Crédito"}
                                        component={ButtonNative}
                                        subtitle={subtitleCard}
                                        onPress={() => this.props.navigation.navigate("CreditCardList", {transactionID: this.props.transactionID, modifyTransaction: this.props.modifyTransaction})}
                                        wrapperStyle={{paddingTop: 10, paddingBottom: 10, margin: 0}}
                                        leftIcon={{name: "credit-card", color: Colors.primaryColor}}
                                        hideChevron={true}
                                    />
                                </View>
                            </List>
                        </View>
                        <View>
                            <View style={{backgroundColor: "white", borderBottomColor: Colors.borderColor, borderBottomWidth: 1, paddingBottom: 10}}>
                                <FormLabel>FACTURACIÓN</FormLabel>
                            </View>
                        </View>
                        <View style={{backgroundColor: "white"}}>
                            <FormLabel>COMENTARIO</FormLabel>
                            <FormInput
                                placeholder={"Ingrese un comentario"}
                                multiline={true}
                                inputStyle={styles.colorInputTextField}
                                underlineColorAndroid={Colors.primaryColor}
                                onChangeText={text => this.setState({comments: text})}
                            />
                        </View>
                        <View style={{backgroundColor: "white", justifyContent: "center", marginTop: "auto", paddingTop: 10}}>
                            <ButtonNative
                                onPress={() => this.finishTransaction(this.props.token, this.props.deposit.ID)}
                                styles={deposit.Card.ID === 0 ? styles.continueDisabledButton : styles.continueButton}
                                disabled={deposit.Card.ID === 0}
                            >
                                <Text style={{color: "white", fontWeight: "bold"}}>Confirmar depósito</Text>
                            </ButtonNative>
                        </View>
                    </ScrollView>
                </KeyboardView>
            )
        } else {
            return (
                <KeyboardView style={{height: "100%"}}>
                    <ScrollView contentContainerStyle={{minHeight: "100%"}}>
                        <View style={{width:"100%" }}>
                            <Text style={{fontSize:34, fontWeight:"bold", color:"black", marginLeft:16, marginTop:30}}>
                                Ok, Resumiendo:
                            </Text>
                            <View style={{width:"100%", justifyContent:"center" }}>
                                <Text style={{fontSize:13, fontWeight:"bold", color:"black", marginTop:18, textAlign:"center",
                                marginBottom:8}}>
                                    Dirección de envío
                                </Text>
                                <Text style={{fontSize:17, color: Colors.subtitleLogoColor, fontWeight:"normal", textAlign:"center"}}>
                                    {clientAddress.Address ? clientAddress.Address + " " + (clientAddress.Floor ? ("Piso " + clientAddress.Floor) : "") + (clientAddress.Apartment ? (" Dpto. " + clientAddress.Apartment) : "") : "No hay dirección seleccionada"}
                                </Text>
                                <Text
                                    onPress={() => this.props.navigation.navigate("AddressList", {transactionID: this.props.transactionID, modifyTransaction: this.props.modifyTransaction})}
                                    style={{fontSize:17, fontWeight:"normal", color: Colors.primaryColor, textAlign:"center"}}>
                                    Modificar dirección de envío
                                </Text>
                            </View>
                            <View style={{flexDirection:"row", alignItems:"center",
                                justifyContent:"space-around", width:"100%", marginTop:24}}>
                                <View style={{width:"45%"}}>
                                    <TextField label="Fecha"
                                               inputContainerStyle={{width: "100%", height: 60}}
                                               autoCapitalize="none"
                                               lineWidth={0.5}
                                               placeholderTextColor="grey"
                                               value={customDate}
                                               editable={false}
                                    />
                                </View>
                                <View style={{width:"45%"}}>
                                    <TextField label="Horario de recepción"
                                               inputContainerStyle={{width: "100%", height: 60}}
                                               autoCapitalize="none"
                                               lineWidth={0.5}
                                               placeholderTextColor="grey"
                                               value={time}
                                               editable={false}
                                    />
                                </View>
                            </View>

                            <View style={{width:"100%", justifyContent:"center" }}>
                                <Text style={{fontSize:13, fontWeight:"bold", color:"black", marginTop:18, textAlign:"center",
                                    marginBottom:8}}>
                                    Tarjeta de crédito
                                </Text>
                                <Text style={{fontSize:17, color: Colors.subtitleLogoColor, fontWeight:"normal", textAlign:"center"}}>
                                    {subtitleCard}
                                </Text>
                                <Text
                                    onPress={() => this.props.navigation.navigate("CreditCardList", {transactionID: this.props.transactionID, modifyTransaction: this.props.modifyTransaction})}
                                    style={{fontSize:17, fontWeight:"normal", color: Colors.primaryColor, textAlign:"center"}}>
                                    Ingresar tarjeta de crédito
                                </Text>
                            </View>

                            <View style={{marginTop:24, width:"90%", marginLeft: "5%"}}>
                                <TextField label="Comentario"
                                           inputContainerStyle={{width: "100%", height: 60}}
                                           lineWidth={0.5}
                                           placeholderTextColor="grey"
                                           tintColor={Colors.primaryColor}
                                           onChangeText={text => this.setState({comments: text})}
                                />
                            </View>

                            <View style={{width:"100%", justifyContent:"center", alignItems:"center", marginTop:24}}>
                                <PrimaryHighlightButton
                                    disabled={clientAddress.address || deposit.Card.ID === 0}
                                    onPress={ () => this.finishTransaction(this.props.token, this.props.deposit.ID) } label="Depositar"
                                />
                            </View>

                        </View>
                    </ScrollView>
                </KeyboardView>
            )
        }


    }

    render() {
        return (
            <FadeInView >
                {this.props.loading ? this.renderSpinner({height: "100%", alignItems: "center", justifyContent: "center"}) : this.renderSummary()}
            </FadeInView>
        )
    }
}

const fff = () => {

}

const mapStateToProps = state => {
    return {
        deposit: state.Deposit.deposit,
        loading: state.Deposit.loading,
        token: state.Login.token,
        transactionID: state.Deposit.deposit.ID,
        profile: state.Profile
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        getDepositPrice(token, transactionID){
            dispatch(DepositActions.getDepositPrice(token, transactionID));
        },
        getDeposit(){
            dispatch(DepositActions.getDeposit())
        },
        modifyTransaction(token, formData, transactionID, finishTransaction) {
            dispatch(DepositActions.setLoading(true));
            dispatch(DepositActions.modifyDeposit(formData, transactionID, finishTransaction))
        },
        finishTransaction(token, transactionID) {
            dispatch(DepositActions.setLoading(true));
            dispatch(DepositActions.finishDeposit(transactionID))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Summary);
