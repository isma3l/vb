import React, { Component } from 'react';
import {Platform, Text, View, ActivityIndicator, Image, StatusBar, Keyboard, ScrollView} from 'react-native'
import Actions from "../Login/Login.actions";
import {connect} from "react-redux";
import {Colors} from "../Layout/Colors";
import {alertAddressStyles, LogoStyle, styles} from "../Styles";
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import ProfileActions from "../Profile/Profile.actions";
import {PrimaryButton} from "../MyComponents";
import KeyboardView from "../KeyboardView";
import {GOOGLEAPIKEY} from "../Keys";
import DepositActions from "./Deposit.actions";
import {TextField} from "react-native-material-textfield";
import AwesomeAlert from "react-native-awesome-alert";
import {CLOSEDDEPOSIT} from "../Images";

class AddAddress extends Component {

    componentDidMount() {
        this._sub = this.props.navigation.addListener(
            'willBlur',
            () => Keyboard.dismiss()
        );
    }

    componentWillUnmount() {
        this._sub.remove();
    }

    addAddress = () => {
        const address = this.props.address;
        const formData = new FormData();
        formData.append("address", address.address);
        if (address.floor) formData.append("floor", address.floor);
        if (address.apartment) formData.append("apartment", address.apartment);
        formData.append("postal_code", address.postalCode);
        this.props.addAddressAndModifyTransaction(this.props.token, formData, this.props.transactionID, this.openAlert, this.props.redirectRoute);
    }

    renderSpinner = () => (
        <View style={styles.centerContainer}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    renderAlertBody = () => {
        return (
            <View style={{justifyContent: "center", alignItems: "center"}}>
                <StatusBar backgroundColor={"rgba(49,49,49, 0.7)"}/>
                <View style={{justifyContent: "center", alignItems: "center", width: "90%", marginTop: 20}}>
                    <Image source={CLOSEDDEPOSIT} resizeMode={"contain"}/>
                    <Text style={{fontWeight: "bold", textAlign: "center", marginVertical: 20, fontSize: 17}}>Ups, no hay depósito disponible en tu ciudad</Text>
                    <Text style={{textAlign: "center", color: Colors.subtitleLogoColor, fontSize: 13, marginBottom: 20, fontWeight: "bold"}}>¡Lamentamos comentarte que aún no hemos arribado a tu ciudad. ¡Esperamos llegar pronto!</Text>
                </View>
            </View>

        )
    }

    openAlert = () => {
        this.awesomeAlert.alert(
            "",
            this.renderAlertBody(),
            [
                {text: "Continuar", onPress: () => console.log("Cancel touch")}
            ]
        )

    }

    GooglePlacesInput = () => {
        return (
            <GooglePlacesAutocomplete
                placeholder='Calle'
                minLength={2}
                autoFocus={false}
                returnKeyType={'search'}
                listViewDisplayed='false'
                fetchDetails={true}
                renderDescription={(row) => row.description || row.vicinity}
                onPress={(data, details = null) => {
                    let address = details.formatted_address;
                    let postalCode = "";
                    details.address_components.forEach(comp => {
                        comp.types.forEach(type => {
                            if (type === "postal_code") postalCode = comp.long_name.substring(0, 5);
                        })
                    })
                    this.props.setAddress({address, postalCode})
                }}
                getDefaultValue={() => this.props.address.address ? this.props.address.address : ""}
                query={{
                    // available options: https://developers.google.com/places/web-service/autocomplete
                    key: GOOGLEAPIKEY,
                    language: 'es',
                    types: 'address',
                    components: "country:ar"
                }}
                placeholderTextColor="grey"
                underlineColorAndroid={Colors.primaryColor}
                autoCapitalize="none"
                styles={{
                    container: {
                        height: 50,
                        marginHorizontal: 16,
                        backgroundColor: "transparent",
                        margin: 0,
                        padding: 0
                    },
                    textInputContainer: {
                        borderTopWidth: 0,
                        borderBottomWidth: 0,
                        padding: 0,
                        marginLeft: 0,
                        flex: 1,
                        backgroundColor: "transparent"
                    },
                    textInput: {
                        marginLeft: 0,
                        marginRight: 0,
                        marginBottom: 0,
                        backgroundColor: "transparent",
                        height: Platform.OS === "ios" ? 40 : 50,
                        borderBottomWidth: Platform.OS === "ios" ? 1 : 0,
                        borderBottomColor: "grey",
                        fontSize: Platform.OS === "ios" ? 17 : 14,
                        padding: 0,
                    },
                    description: {
                        fontWeight: 'bold',
                    },
                    predefinedPlacesDescription: {
                        color: '#1faadb'
                    },
                    listView: {
                        zIndex: 999,
                        position: "absolute",
                        top: 50,
                        backgroundColor: "white",
                        elevation: 3
                    }
                }}
                nearbyPlacesAPI='GooglePlacesSearch'
                debounce={200}
                //renderLeftButton={()  => <Image source={require('path/custom/left-icon')} />}
            />
        );
    }

    render() {
        return (
            <View
                style={{height: "100%", width: "100%"}}
            >
                <View style={styles.container}>
                    {this.props.loading ? this.renderSpinner() : this.renderAddress()}
                </View>
                <AwesomeAlert
                    styles={alertAddressStyles}
                    ref={ref => (this.awesomeAlert = ref)}
                    transparent={true}
                    animationType={"fade"}
                />
            </View>
        )
    }

    renderAddress = () => {
        return (
            <KeyboardView style={{flex: 1}}>
              <ScrollView>
                <View style={{marginHorizontal: 16, paddingTop: 20}}>
                    <Text style={LogoStyle.title}>
                        ¿Dónde retiraremos
                    </Text>
                    <Text style={LogoStyle.title}>
                        los ítems?
                    </Text>
                    <Text style={[LogoStyle.subtitle, {marginTop: 5}]}>
                        Completa tu dirección para saber dónde buscar los ítems que deposites.
                    </Text>
                </View>
                <View style={{
                    height: 50,
                    zIndex: Platform.OS === "ios" ? 1 : undefined,
                    width: "100%",
                    justifyContent: "center"
                }}>
                    {this.GooglePlacesInput()}
                </View>
                <View style={styles.floorContainer}>
                    <TextField
                        label="Piso"
                        underlineColorAndroid={Colors.primaryColor}
                        inputStyle={styles.colorInputTextField}
                        containerStyle={{flexGrow: 1, flexBasis: 0, alignSelf: "flex-start", marginLeft: 16, marginRight: 3}}
                        lineWidth={0.5}
                        placeholderTextColor="grey"
                        tintColor={Colors.primaryColor}
                        autoCapitalize="none"
                        defaultValue={this.props.address.floor}
                        onChangeText={text => this.props.setAddress({floor: text})}
                    />
                    <TextField
                        label="Departamento"
                        underlineColorAndroid={Colors.primaryColor}
                        containerStyle={{flexGrow: 1, flexBasis: 0, alignSelf: "flex-end", marginLeft: 3, marginRight: 16}}
                        inputStyle={styles.colorInputTextField}
                        lineWidth={0.5}
                        tintColor={Colors.primaryColor}
                        placeholderTextColor="grey"
                        autoCapitalize="none"
                        defaultValue={this.props.address.apartment}
                        onChangeText={text => this.props.setAddress({apartment: text})}
                    />
                </View>
                <View style={{marginBottom: 20, marginHorizontal: 16}}>
                    <PrimaryButton
                        style={{width: '100%'}}
                        onPress={() => this.addAddress()}
                        label={"Finalizar"}
                    />
                </View>
              </ScrollView>
            </KeyboardView>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        loading: state.Login.loading,
        missingInfo: state.Login.missingInformation,
        token: state.Login.token,
        address: state.Profile.address,
        addressAction: state.Profile.addressAction,
        redirectRoute: state.VerifyInformation.redirectRoute,
        profile: state.Profile,
        transactionID: state.Deposit.deposit.ID
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        setLoading(boolean){
            dispatch(Actions.loading(boolean))
        },
        setAddress(address) {
            dispatch(ProfileActions.setAddress(address))
        },
        addAddress(token, formData, redirectRoute) {
            dispatch(ProfileActions.addAddress(token, formData, redirectRoute))
        },
        newTransaction(params) {
            dispatch(DepositActions.setLoading(true))
            dispatch(DepositActions.newDeposit(params))
        },
        modifyTransaction(token, formData, transactionID) {
            dispatch(DepositActions.setLoading(true));
            dispatch(DepositActions.modifyDeposit(formData, transactionID))
        },
        getProfile(token) {
            dispatch(DepositActions.setLoading(true));
            dispatch(ProfileActions.getProfile(token))
        },
        addAddressAndModifyTransaction(token, formData, transactionID, alert, redirectRoute) {
            dispatch(Actions.loading(true))
            dispatch(DepositActions.addAddressAndModifyTransaction(token, formData, transactionID, alert, redirectRoute))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddAddress)

