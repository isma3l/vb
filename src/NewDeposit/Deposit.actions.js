import {Endpoint, Transactions, Timeslots, UserProfile, Category, MercadoPago} from '../Endpoint'
import {Alert} from 'react-native'
import moment from 'moment'
import {navigatorRef} from '../../App'
import {NavigationActions} from 'react-navigation'
import ProfileActions from '../Profile/Profile.actions'
import {MERCADOPAGOPUBLICKEY} from '../Keys'
import CardActions from '../CreditCard/CreditCard.actions'
import {apiPrivate, apiExternal, apiPublic} from '../api'
import {ApiError, GetUserTransactionsCreated, GetTimeSlotOccupiedDates, GetTimeSlotForDate} from './schemas'

export default {
  setLoading(boolean) {
    return {type: 'SET_LOADING_DEPOSIT', loading: boolean}
  },
  setLoadingPickupDates(boolean) {
    return {type: 'SET_LOADING_PICKUP_DAYS', loading: boolean}
  },
  setLoadingTimeslots(boolean) {
    return {type: 'SET_LOADING_TIMESLOTS', loading: boolean}
  },
  newDeposit(params) {
    return dispatch => {

      apiPrivate.get(Transactions + '/transactions/created', {params: {type: 3}})
        .then(response => {

          const transaction = new GetUserTransactionsCreated(response.data.description)

          dispatch({type: 'SET_CURRENT_DEPOSIT', deposit: transaction})

          if (transaction.Reserve.ID === 0) {
            dispatch(this.getTimeslots(params))
            return
          }

          const date = transaction.Reserve.Date
          const minutesZone = moment().utcOffset()
          const selectedDay = moment(date).subtract(minutesZone, 'minutes').valueOf()

          const timeslot = transaction.Reserve.TimeSlot

          const hours = timeslot.Hour < 10 ? '0' + timeslot.Hour.toString() : timeslot.Hour.toString()
          const minutes = timeslot.Minute < 10 ? '0' + timeslot.Minute.toString() : timeslot.Minute.toString()

          dispatch(this.setDate({
            selectedDay: {dateString: moment(selectedDay).format('YYYY-MM-DD')},
            selectedHour: transaction.Reserve.TimeSlotID,
            disableContinue: false,
            hourString: moment(hours + minutes, 'HHmm').format('hh:mm A'),
          }))
          dispatch(this.getTimeslots({date: Math.round(selectedDay / 1000)}))

        })
        .catch(error => {
          dispatch(this.setLoading(false))
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },
  getDeposit() {
    return dispatch => {
      apiPrivate.get(Transactions + '/transactions/created', {params: {type: 3}})
        .then(response => {
          const transaction = new GetUserTransactionsCreated(response.data.description)

          dispatch({type: 'SET_CURRENT_DEPOSIT', deposit: transaction})
        })
        .catch(error => {
          dispatch(this.setLoading(false))
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },
  getOccupiedDates() {
    return dispatch => {
      apiPrivate.get(Timeslots + '/timeSlot/occupiedDates', {params: {daysOnwards: 60}})
        .then(response => {
          const occupiedDays = new GetTimeSlotOccupiedDates({list: response.data.description})
          dispatch({type: 'SET_OCCUPIED_DAYS', occupiedDays: occupiedDays.list})
        })
        .catch(error => {
          dispatch(this.setLoadingPickupDates(false))
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },

  getTimeslots(params) {
    return dispatch => {
      apiPrivate.get(Timeslots + '/timeSlot', {params})
        .then(response => {
          //TODO don't know why this is crashing down the line, need to figure it out later
          // const timeSlots = new GetTimeSlotForDate({list: response.data.description})
          dispatch({type: 'SET_TIMESLOTS', timeSlots: response.data.description})
        })
        .catch(error => {
          dispatch(this.setLoadingTimeslots(false))
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },

  setDate(date) {
    return {type: 'SET_DEPOSIT_DATE', date}
  },

  modifyDeposit(formData, transactionID, finishTransaction = false) {
    return dispatch => {
      apiPrivate.put(Transactions + '/transactions/' + transactionID, formData)
        .then(() => {
          if (finishTransaction) {
            dispatch(this.finishDeposit(transactionID))
          } else {
            dispatch(this.getDeposit())
          }
        })
        .catch(error => {
          dispatch(this.setLoading(false))
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },

  finishDeposit(transactionID) {
    return dispatch => {
      apiPrivate.put(Transactions + '/transactions/' + transactionID + '/confirm')
        .then(() => {
          navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Done'}))
          dispatch({type: 'BACK_TO_INITIAL_STATE'})
        })
        .catch(error => {
          dispatch(this.setLoading(false))
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },
  getDepositPrice() {
    return dispatch => {
      apiPublic.get(Timeslots + '/timeZones')
        .then(response => {
          dispatch({type: 'GET_DEPOSIT_PRICES', prices: response.data.description})
        })
        .catch(error => {
          let message = ''
          if (error.response) {
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },

  addAddressAndModifyTransaction(token, formData, transactionID, alert, redirectRoute) {
    return dispatch => {
      apiPrivate.post(UserProfile + '/user/address', formData)
        .then(response => {
          const address = response.data.description;
          dispatch({type: "CLEAN_ADDRESS"});
          if (address.InDeliveryRange) {
            dispatch(ProfileActions.getUserAddress(token))
            dispatch(this.setLoading(true))
            const formData = new FormData()
            formData.append('address', address.ID)
            dispatch(this.modifyDeposit(formData, transactionID))
            if (redirectRoute) {
              navigatorRef.dispatch(NavigationActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({routeName: redirectRoute})],
              }))
              dispatch({type: 'SET_REDIRECT_ROUTE', route: undefined})
            }
            else
              navigatorRef.dispatch(NavigationActions.back())
          } else {
            dispatch({type: 'SET_LOADING', loading: false})
            alert()
          }
        })
        .catch(error => {
          dispatch({type: 'SET_CARDS_LOADING', loading: false})
          dispatch({type: 'SET_LOADING', loading: false})
          let message = ''
          if (error.response) {
            if (error.response.status === 401 || error.response.status === 403) {
              console.warn('redirected due to 401 or 403')
              navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
              return
            }
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })
    }
  },

  addCreditCardAndModifyTransaction(token, data, methods, transactionID, redirectRoute) {
    return dispatch => {
      apiExternal.post(MercadoPago + '/v1/card_tokens', data, {params: {public_key: MERCADOPAGOPUBLICKEY}})
        .then(response => {
          const formData = new FormData()
          const first_six_digits = response.data.first_six_digits

          let type = ''

          methods.forEach(method => {
            method.settings.forEach(setting => {
              const pattern = new RegExp(setting.bin.pattern)
              const exclusionPattern = new RegExp(setting.bin.exclusion_pattern)
              if (pattern.test(first_six_digits) && !exclusionPattern.test(first_six_digits))
                type = method.id
            })
          })

          formData.append('token', response.data.id)
          formData.append('payment_method', type)

          apiPrivate.post(UserProfile + '/user/cards', formData)
            .then(response => {
              dispatch(CardActions.getCreditCards(token))
              dispatch(this.setLoading(true))

              const formData = new FormData()
              //TODO schema
              formData.append('card_id', response.data.description.ID)
              dispatch(this.modifyDeposit(formData, transactionID))

              if (redirectRoute) {
                navigatorRef.dispatch(NavigationActions.reset({
                  index: 0,
                  actions: [NavigationActions.navigate({routeName: redirectRoute})],
                }))
                dispatch({type: 'SET_REDIRECT_ROUTE', route: undefined})
              } else {
                navigatorRef.dispatch(NavigationActions.back())
              }
            })
            .catch(error => {
              dispatch({type: 'SET_CARDS_LOADING', loading: false})
              let message = ''
              if (error.response) {
                if (error.response.status === 401 || error.response.status === 403) {
                  console.warn('redirected due to 401 or 403')
                  navigatorRef.dispatch(NavigationActions.navigate({routeName: 'Welcome'}))
                  return
                }
                const response = new ApiError(error.response.data)
                message = response.description
              } else if (error.request) {
                console.warn(error.request)
                message = 'Algo salió mal, revisá tu conexión a internet'
              } else {
                console.warn('Error', error.message)
                message = 'Algo salió mal.'
              }
              Alert.alert('Error', message, [{text: 'OK'}])
            })
        })
        .catch(error => {
          dispatch({type: 'SET_LOADING', loading: false})
          console.warn(error.response)
          //TODO should use a schema
          if (error.response.data.cause.code = 306) {
            Alert.alert('Error', 'Se han ingresado carácteres incorrectos en el nombre de la tarjeta')
          } else {
            Alert.alert('Error', 'Hubo un problema al comunicarse con Mercado Pago')
          }
        })
    }
  },

  setCurrentAddress(id) {
    return {type: 'SET_CURRENT_ADDRESS', id}
  },

  getPrices() {
    return dispatch => {
      dispatch({type: 'SHOW_LOADING_PRICES', value: true})
      apiPublic.get(Category + '/prices')
        .then(response => {
          //TODO schema
          const prices = response.data.description.prices || []

          dispatch({
            type: 'SET_PRICES',
            prices,
          })
        })
        .catch(error => {
          dispatch({type: 'SHOW_LOADING_PRICES', value: false})
          let message = ''
          if (error.response) {
            const response = new ApiError(error.response.data)
            message = response.description
          } else if (error.request) {
            console.warn(error.request)
            message = 'Algo salió mal, revisá tu conexión a internet'
          } else {
            console.warn('Error', error.message)
            message = 'Algo salió mal.'
          }
          Alert.alert('Error', message, [{text: 'OK'}])
        })

    }
  },

  setCurrentCard(id) {
    return {type: 'SET_CURRENT_CARD', id}
  },
}
