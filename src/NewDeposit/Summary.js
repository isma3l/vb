import React, { Component } from 'react';
import {Text,  View,  TouchableOpacity, ActivityIndicator, ScrollView, Platform, Keyboard} from 'react-native';
import {List, ListItem, FormLabel, FormInput} from "react-native-elements";
import DepositActions from './Deposit.actions';
import {connect} from "react-redux";
import {Colors} from "../Layout/Colors";
import {alertSelectAddressAlert, DepositStyles, styles} from '../Styles'
import ButtonNative, {PrimaryHighlightButton} from "../MyComponents";
import {FadeInView} from "../AnimatedComponents";
import {NavigationActions} from "react-navigation";
import moment from "moment/moment";
import KeyboardView from "../KeyboardView";
import {TextField} from "react-native-material-textfield";
import {SecondaryButton} from "../MyComponents";
import CardsActions from "../CreditCard/CreditCard.actions";
import LoginActions from "../Login/Login.actions";
import ProfileActions from "../Profile/Profile.actions";
import {AddressModal, CreditCardModal} from "../Transactions/Modals";
import Modal from "react-native-modal";
import ModalPrices from "../Transactions/ModalPrices";


moment.locale('es');

class Summary extends Component {

    state = {
        comments: "",
        isVisibleAddress: false,
        isVisibleCard: false,
        isVisibleStorage: false,
    }

    componentDidMount() {
        this.props.getDepositPrice(this.props.token, this.props.transactionID);
        this.props.getCreditCards(this.props.token);
        this.props.getUserAddress(this.props.token);

        this._sub = this.props.navigation.addListener(
          'willBlur',
          () => Keyboard.dismiss()
        );
    }

    componentWillUnmount() {
      this._sub.remove();
    }

    selectAddress(ID) {
        const formData = new FormData();
        formData.append("address", ID);
        this.props.modifyTransaction(this.props.token, formData, this.props.transactionID);
    }

    selectCreditCard(ID) {
        const formData = new FormData();
        formData.append("card_id", ID);
        this.props.modifyTransaction(this.props.token, formData, this.props.transactionID);
    }

    finishTransaction = (token, transactionID) => {
        const comments = this.state.comments;
        if (comments === "") {
            this.props.finishTransaction(token, transactionID);
        } else {
            const formData = new FormData();
            formData.append("comments", comments);

            this.props.modifyTransaction(token, formData, transactionID, true);
        }
    }

    renderDetailItem = (title, subtitle, buttonAction) => (
        <View style={{
            borderBottomColor: Colors.borderColor,
            borderBottomWidth: 1,
            position: "relative",
            paddingTop: 14,
            padding: 16,
        }}>
            <View style={{flexDirection: 'row'}}>
              <View style={{flex: 1}}>
                  <Text style={{marginBottom: 5, color: "black", fontWeight: "bold", fontSize: 13}}>{title}</Text>
                  <Text style={{fontSize: 15, color: Colors.secondaryTextColor}}>{subtitle}</Text>
              </View>
              <TouchableOpacity style={{width: 65}} onPress={() => buttonAction()}>
                  <Text style={{color: Colors.primaryColor, fontWeight: "bold", fontSize: 13, textAlign: 'right'}}>Modificar</Text>
              </TouchableOpacity>
            </View>
        </View>
    )

    renderSpinner = (styles) => (
        <View style={styles}>
            <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
        </View>
    )

    renderAndroidSummary = (deposit, customDate, time, subtitleCard) => (
      <KeyboardView style={{height: "100%"}}>
        <ScrollView contentContainerStyle={{minHeight: "100%"}}>
          <View>
            <List containerStyle={styles.profileList}>
              <View style={{borderBottomWidth: 1, borderBottomColor: Colors.borderColor}}>
                <ListItem
                  title={"Domicilio"}
                  component={ButtonNative}
                  wrapperStyle={{paddingTop: 10, paddingBottom: 10, margin: 0}}
                  subtitleStyle={{width: "100%", flexWrap: "wrap", paddingRight: 10}}
                  subtitle={deposit.ClientAddress.Address}
                  leftIcon={{name: "location-on", color: Colors.primaryColor}}
                  onPress={() => this.props.navigation.navigate("AddressList", {transactionID: this.props.transactionID, modifyTransaction: this.props.modifyTransaction})}
                  hideChevron={true}
                />
              </View>
              <View style={{borderBottomWidth: 1, borderBottomColor: Colors.borderColor}}>
                <ListItem
                  title={"Fecha y hora"}
                  component={ButtonNative}
                  subtitle={customDate + " " + time}
                  onPress={() => this.props.navigation.dispatch(NavigationActions.back())}
                  wrapperStyle={{paddingTop: 10, paddingBottom: 10, margin: 0}}
                  leftIcon={{name: "calendar-clock", type: "material-community", color: Colors.primaryColor}}
                  hideChevron={true}
                />
              </View>
            </List>
          </View>
          <View>
            <View style={{backgroundColor: "white", borderBottomColor: Colors.borderColor, borderBottomWidth: 1, paddingBottom: 10}}>
              <FormLabel>MÉTODO DE PAGO</FormLabel>
            </View>
            <List containerStyle={styles.profileList}>
              <View style={{borderBottomWidth: 1, borderBottomColor: Colors.borderColor}}>
                <ListItem
                  title={"Tarjeta de Crédito"}
                  component={ButtonNative}
                  subtitle={subtitleCard}
                  onPress={() => this.props.navigation.navigate("CreditCardList", {transactionID: this.props.transactionID, modifyTransaction: this.props.modifyTransaction})}
                  wrapperStyle={{paddingTop: 10, paddingBottom: 10, margin: 0}}
                  leftIcon={{name: "credit-card", color: Colors.primaryColor}}
                  hideChevron={true}
                />
              </View>
            </List>
          </View>
          <View>
            <View style={{backgroundColor: "white", borderBottomColor: Colors.borderColor, borderBottomWidth: 1, paddingBottom: 10}}>
              <FormLabel>FACTURACIÓN</FormLabel>
            </View>
          </View>
          <View style={{backgroundColor: "white"}}>
            <FormLabel>COMENTARIO</FormLabel>
            <FormInput
              placeholder={"Ingrese un comentario"}
              multiline={true}
              inputStyle={styles.colorInputTextField}
              underlineColorAndroid={Colors.primaryColor}
              onChangeText={text => this.setState({comments: text})}
            />
          </View>
          <View style={{backgroundColor: "white", justifyContent: "center", marginTop: "auto", paddingTop: 10}}>
            <ButtonNative
              onPress={() => this.finishTransaction(this.props.token, this.props.deposit.ID)}
              styles={deposit.Card.ID === 0 ? styles.continueDisabledButton : styles.continueButton}
              disabled={deposit.Card.ID === 0}
            >
              <Text style={{color: "white", fontWeight: "bold"}}>Confirmar depósito</Text>
            </ButtonNative>
          </View>
        </ScrollView>
      </KeyboardView>
    )

    renderIOSSummary = (addressString, customDate, subtitleCard, clientAddress, deposit) => (
      <KeyboardView style={{height: '100%'}}>
        <ScrollView contentContainerStyle={{minHeight: '100%'}}>
          <View>
            <Text style={{fontSize: 34, fontWeight: 'bold', color: 'black', marginVertical: 40, marginHorizontal: 16}}>
              Chequea la información:
            </Text>
            <View style={[DepositStyles.addressSelector, {padding: 0}]}>
              {this.renderDetailItem('Dirección de retiro', addressString, () => this.setState({isVisibleAddress: true}))}
              {this.renderDetailItem('Día y horario', customDate, () => this.props.navigation.dispatch(NavigationActions.back()))}
              {this.renderDetailItem('Método de Pago', subtitleCard, () => this.setState({isVisibleCard: true}))}
              <View style={{
                backgroundColor: Colors.primaryColor,
                position: 'relative',
                height: 50,
                padding: 16,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                borderBottomLeftRadius: 5,
                borderBottomRightRadius: 5,
              }}>
                <Text style={{color: 'white', fontWeight: 'bold', fontSize: 13}}>COSTO DE RETIRO</Text>
                <View>
                  <Text style={{color: Colors.secondaryColor, fontWeight: 'bold'}}>¡GRATIS!</Text>
                </View>
              </View>
            </View>
            <View style={{marginHorizontal: 18}}>
              <TextField
                label="Notas"
                inputContainerStyle={{height: 60}}
                lineWidth={0.5}
                placeholderTextColor="grey"
                tintColor={Colors.primaryColor}
                labelTextStyle={{color: Colors.secondaryTextColor}}
                onChangeText={text => this.setState({comments: text})}
              />
            </View>
            <View style={{marginBottom: 10, marginTop: 40, marginHorizontal: 16}}>
              <SecondaryButton
                label="Costos de almacenaje"
                onPress={() => this.setState({isVisibleStorage: true})}
                style={{width: '100%', marginHorizontal: 16}}
              />
            </View>
            <View style={{marginBottom: 30, marginHorizontal: 16}}>
              <PrimaryHighlightButton
                disabled={clientAddress.address || deposit.Card.ID === 0}
                onPress={() => this.finishTransaction(this.props.token, this.props.deposit.ID)} label="Depositar"
                style={{width: '100%', marginHorizontal: 16}}
              />
            </View>
          </View>
        </ScrollView>
      </KeyboardView>
    )

    renderSummary = () => {

        const deposit = this.props.deposit;

        const card = deposit.Card;

        const date = moment.utc(deposit.Reserve.Date).utcOffset(moment().utcOffset(), true)
          .hour(deposit.Reserve.TimeSlot.Hour)
          .minute(deposit.Reserve.TimeSlot.Minute)

        let customDate = date.format('D') + ' de ' + date.format('MMMM, h:mm A')
        let time = date.format('hh:mm A');
        const subtitleCard = card.ID === 0 ? "No seleccionada" : "Tarjeta terminada en " + card.LastDigits + " (" + card.Type +")";

        let clientAddress = deposit.ClientAddress;

        const addressString = clientAddress.Address ? clientAddress.Address + " " + (clientAddress.Floor ? ("Piso " + clientAddress.Floor) : "") + (clientAddress.Apartment ? (" Dpto. " + clientAddress.Apartment) : "") : "No hay dirección seleccionada"

        if (Platform.OS === "android") {
            return this.renderAndroidSummary(deposit, customDate, time, subtitleCard)
        } else {
            return this.renderIOSSummary(addressString, customDate, subtitleCard, clientAddress, deposit)
        }
    }

  hideModal = () => {
    this.setState({ isVisibleStorage: false })
  }

    render() {
        const cardList = this.props.cards;

        return (
            <FadeInView >
                {this.props.loading ? this.renderSpinner({
                        height: "100%",
                        alignItems: "center",
                        justifyContent: "center"
                    }) :
                    <View>
                        <AddressModal
                            {...this.props}
                            exitModal={() => this.setState({ isVisibleAddress: false})}
                            selectAddress={(id) => this.selectAddress(id)}
                            isVisibleAddress={this.state.isVisibleAddress}
                        />
                        <CreditCardModal
                            {...this.props}
                            exitModal={() => this.setState({ isVisibleCard: false})}
                            selectCard={(id) => this.selectCreditCard(id)}
                            isVisibleCard={this.state.isVisibleCard}
                        />
                        <Modal
                          style={alertSelectAddressAlert.modalContainer}
                          isVisible={this.state.isVisibleStorage}
                          onBackdropPress={() => this.setState({ isVisibleStorage: false })}
                        >
                          <View>
                            <ModalPrices hideModal={this.hideModal}/>
                          </View>
                        </Modal>
                        {this.renderSummary()}
                    </View>
                }
            </FadeInView>
        )
    }
}

const mapStateToProps = state => {
    return {
        deposit: state.Deposit.deposit,
        loading: state.Deposit.loading || state.CreditCard.loading,
        token: state.Login.token,
        cards: state.CreditCard.cards,
        transactionID: state.Deposit.deposit.ID,
        profile: state.Profile,
        addresses: state.Profile.addresses,
        currentAddress: state.Deposit.currentAddress,
        currentCard: state.Deposit.currentCard,
        transaction: state.Deposit.deposit
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        getDepositPrice(token, transactionID){
            dispatch(DepositActions.getDepositPrice(token, transactionID));
        },
        getDeposit(){
            dispatch(DepositActions.getDeposit())
        },
        modifyTransaction(token, formData, transactionID, finishTransaction) {
            dispatch(DepositActions.setLoading(true));
            dispatch(DepositActions.modifyDeposit(formData, transactionID, finishTransaction))
        },
        finishTransaction(token, transactionID) {
            dispatch(DepositActions.setLoading(true));
            dispatch(DepositActions.finishDeposit(transactionID))
        },
        getCreditCards(token) {
            dispatch({type: "SET_CARDS_LOADING", loading: true});
            dispatch(CardsActions.getCreditCards(token));
        },
        getUserAddress(token) {
            dispatch(LoginActions.loading(true));
            dispatch(ProfileActions.getUserAddress(token));
        },
        setCurrentAddress(id) {
            dispatch(DepositActions.setCurrentAddress(id))
        },
        setCurrentCard(id) {
            dispatch(DepositActions.setCurrentCard(id))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Summary);
