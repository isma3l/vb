import React, {Component} from 'react'
import {View, ActivityIndicator, Text, Image, Dimensions} from 'react-native'
import {connect} from 'react-redux'
import {Colors} from '../Layout/Colors'
import {FadeInView} from '../AnimatedComponents'
import {NEWDEPOSITDONE} from '../Images'
import moment from 'moment/moment'
import {PrimaryHighlightButton} from '../MyComponents'

moment.locale('es')

const style = {
  spinner: {
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').width / 6.25,
    marginTop: 40,
    marginBottom: 40,
  },
}

class Done extends Component {

  renderSpinner = () => (
    <View style={style.spinner}>
      <ActivityIndicator animating={true} color={Colors.primaryColor} size={'large'}/>
    </View>
  )

  render() {
    return (
      <FadeInView>
        <View>
          <Text style={{fontSize: 34, fontWeight: 'bold', color: 'black', marginLeft: 16, marginTop: 30}}>
            ¡Tu retiro ha sido programado!
          </Text>
          <Text style={{fontSize: 17, color: '#5B687C', marginTop: 10, marginLeft: 16}}>
            Hemos enviado una confirmación a tu dirección de email.
          </Text>
          <Image
            source={NEWDEPOSITDONE}
            style={style.image}
          />
          <PrimaryHighlightButton
            disabled={false}
            onPress={() => this.props.navigation.navigate("Notifications")}
            label="Continuar"
          />
        </View>
      </FadeInView>
    )
  }
}

const mapStateToProps = state => {
  return {
    profile: state.Profile,
  }
}

const mapDispatchToProps = dispatch => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(Done)
