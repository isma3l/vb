import SchemaObject from 'schema-object'
import {CreditCard, TransactionReserve, User, Address, TimeSlot} from '../schemas'

export {ApiError, TimeSlot} from '../schemas'

export const GetUserTransactionsCreated = new SchemaObject({
  ID: String,
  Card: CreditCard,
  Origin: String,
  Destination: String,
  Reserve: TransactionReserve,
  Client: User,
  Comments: String,
  ClientAddress: Address,
})

export const GetTimeSlotForDate = new SchemaObject({
  list: [TimeSlot],
})

export const GetTimeSlotOccupiedDates = new SchemaObject({
  list: [TimeSlot],
})
