import React, { Component } from 'react';
import {Platform} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';
import DepositActions from './Deposit.actions';
import {connect} from "react-redux";
import VerifyInformation from "../VerifyInformation/VerifyInformation";
import moment from "moment";
import SelectTimeslot from "../Transactions/SelectTimeSlot";
import SelectTimeSlotNewDesign from "../Transactions/SelectTimeSlotNewDesign";
import { DeviceEventEmitter } from 'react-native'
import ProfileActions from "../Profile/Profile.actions";
import LoginActions from "../Login/Login.actions"

class NewDeposit extends Component {
    static navigationOptions = {
        headerTitle: 'Nuevo depósito',
        drawerLabel: 'Nuevo depósito',
        drawerIcon: ({ tintColor }) => (
            <Icon name="add-circle" size={24} />
        )
    };

    componentDidMount() {
        this.props.newTransaction({date: Math.round(this.props.selectedDay / 1000)});
        this.props.getOccupiedDates();
        this.props.getUserAddress(this.props.token);
    }

    render() {
        if (Platform.OS === "ios") {
            return (
                <VerifyInformation {...this.props} redirectRoute="NewDeposit">
                    <SelectTimeSlotNewDesign {...this.props} isDeposit={true}/>
                </VerifyInformation>
            )
        } else {
            return (
                <VerifyInformation {...this.props} redirectRoute="NewDeposit">
                    <SelectTimeslot {...this.props} isDeposit={true}/>
                </VerifyInformation>
            )
        }
    }
}


const mapStateToProps = state => {
    return {
        transaction: state.Deposit.deposit,
        loading: state.Deposit.loading || state.Deposit.loadingPickUpDays || state.Login.loading,
        selectedDay: state.Deposit.selectedDay,
        selectedHour: state.Deposit.selectedHour,
        token: state.Login.token,
        loadingTimeSlots: state.Deposit.loadingTimeSlots,
        timeSlots: state.Deposit.timeSlots,
        occupiedDays: state.Deposit.occupiedDays,
        transactionID: state.Deposit.deposit.ID,
        disableContinue: state.Deposit.disableContinue,
        redirectRoute: "Summary",
        minDate: moment(),
        maxDate: moment().add(60, "days"),
        addresses: state.Profile.addresses,
        hourString: state.Deposit.hourString,
        currentAddress: state.Deposit.currentAddress
    }
};

const mapDispatchToProps = dispatch =>{
    return {
        newTransaction(params) {
            dispatch(DepositActions.setLoading(true))
            dispatch(DepositActions.newDeposit(params))
        },
        getOccupiedDates() {
            dispatch(DepositActions.setLoadingPickupDates(true));
            dispatch(DepositActions.getOccupiedDates())
        },
        getTimeslots(params) {
            dispatch(DepositActions.setLoadingTimeslots(true));
            dispatch(DepositActions.getTimeslots(params))
        },
        setDate(date) {
            dispatch(DepositActions.setDate(date))
        },
        modifyTransaction(token, formData, transactionID) {
            dispatch(DepositActions.setLoading(true));
            dispatch(DepositActions.modifyDeposit(formData, transactionID))
        },
        getUserAddress(token) {
            dispatch(LoginActions.loading(true));
            dispatch(ProfileActions.getUserAddress(token));
        },
        setCurrentAddress(id) {
            dispatch(DepositActions.setCurrentAddress(id))
        }

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(NewDeposit);
