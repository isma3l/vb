import {Platform, View, KeyboardAvoidingView} from "react-native";
import React from "react";

export default class KeyboardView extends React.Component {
    render() {
        if (this.props.behavior) {
            return (
                <KeyboardAvoidingView {...this.props} behavior={this.props.behavior}>
                    {this.props.children}
                </KeyboardAvoidingView>
            )
        }
        if (Platform.OS === "android") {
            return (
                <View {...this.props}>
                    {this.props.children}
                </View>
            );
        } else {
            let keyboardVerticalOffset;
            if (this.props.keyboardVerticalOffset) {
                keyboardVerticalOffset = this.props.keyboardVerticalOffset;
            } else {
                keyboardVerticalOffset = this.props.keyboardType === 'numeric' ? 40 : 0;
            }

            return (
                <KeyboardAvoidingView
                    {...this.props}
                    behavior="padding"
                    keyboardVerticalOffset={keyboardVerticalOffset}
                >
                    {this.props.children}
                </KeyboardAvoidingView>
            )
        }

    }
}
