import React from 'react';
import {StatusBar, View} from 'react-native';
import RootNavigator from "./src/Layout/LayoutNavigation";
import {connect, Provider} from 'react-redux';

import store from "./src/store";
import {Colors} from "./src/Layout/Colors";
import Actions from "./src/Login/Login.actions"

export let navigatorRef;

export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Drawer/>
            </Provider>
        );
    }
}


class DrawerComponent extends React.Component {
    componentDidMount() {
        navigatorRef = this.navigator;
    }

    render () {
        return (
            <View style={{width: "100%", height: "100%", backgroundColor: Colors.backgroundColor}}>
                <StatusBar backgroundColor={Colors.backgroundColor} barStyle='dark-content'/>
                <RootNavigator ref={nav => this.navigator = nav}/>
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        Login: state.Login
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setGuest(boolean) {
            dispatch(Actions.setGuest(boolean))
        },
        setName(name) {
            dispatch(Actions.setName(name))
        }
    }
}

const Drawer = connect(mapStateToProps, mapDispatchToProps)(DrawerComponent);
